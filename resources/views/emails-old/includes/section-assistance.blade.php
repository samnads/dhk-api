<div class="add-on-section" style="width: 700px; height: auto; margin: 0px auto 0;">
    <p
        style="font: normal 13px/18px 'Poppins', sans-serif; color: #555; display: block; margin: 30px 0px 0px; padding: 10px 20px; text-align: left; background: #eee; border-radius: 5px; overflow: hidden; -webkit-text-size-adjust: 100%; -moz-text-size-adjust: 100%; -ms-text-size-adjust: 100%;">
        Feel free to contact us at <a href="mailto:office@dubaihousekeeping.com" style="text-decoration: underline;"
            target="_blank">office@dubaihousekeeping.com</a> if you require further assistance.</p>
</div>
