<?php

namespace App\Http\Controllers;

use App\Models\Settings;
use Config;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Response;
use Validator;

class CustomerApiDataController extends Controller
{
    public function data(Request $request)
    {
        /**************************************************************** */
        $data = json_decode($request->getContent(), true);
        $input = @$data['params'];
        $validator = Validator::make(
            (array) $input,
            [
                'platform' => 'required|in:mobile,web',
            ],
            [],
            [
                'platform' => 'Platform',
            ]
        );
        if ($validator->fails()) {
            return Response::json(array('result' => array('status' => 'failed', 'message' => $validator->errors()->first())), 200, [], JSON_PRETTY_PRINT);
        }
        $settings = Settings::first();
        /**************************************************************** */
        $mapped_services = DB::table('service_type_category_and_service_type_mapping as sm')->select(
            'st.service_type_id',
            'sm.service_type_category_id',
            DB::raw('IFNULL(st.customer_app_service_type_name,st.service_type_name) as service_type_name'),
            'stc.service_category_name as service_type_category_name',
            DB::raw('(CASE WHEN (stc.customer_app_icon_file IS NOT NULL AND stc.customer_app_icon_file != "") THEN CONCAT("' . Config::get('values.customer_app_assets_prefix_url') . 'service-category-icons/",stc.customer_app_icon_file,"?v=' . $settings->customer_app_img_version . '") ELSE NULL END) AS service_type_category_icon'),
            DB::raw('(CASE WHEN (st.customer_app_thumbnail_file IS NOT NULL AND st.customer_app_thumbnail_file != "") THEN CONCAT("' . Config::get('values.customer_app_assets_prefix_url') . 'service-thumbnails/",st.customer_app_thumbnail_file,"?v=' . $settings->customer_app_img_version . '") ELSE NULL END) AS service_type_thumbnail'),
            'st.service_type_model_id',
            'stm.model as service_type_model',
            'st.web_url_slug',
            'st.info_html',
            'st.min_no_of_maids as min_no_of_professionals',
            'st.max_no_of_maids as max_no_of_professionals'
        )
            ->leftJoin('service_types as st', 'sm.service_type_id', 'st.service_type_id')
            ->leftJoin('service_type_categories as stc', 'sm.service_type_category_id', 'stc.id')
            ->leftJoin('service_type_models as stm', 'st.service_type_model_id', 'stm.id')
            ->where([['stc.deleted_at', '=', null], ['sm.deleted_at', '=', null], ['st.customer_app_status', '=', 1]])
            ->orderBy('stc.sort_order', 'ASC')
            ->orderBy('sm.sort_order', 'ASC')
            ->get();
        /**************************************************************** */
        // service subscription packages
        $subscription_packages = DB::table('package as p')->select(
            'p.package_id',
            'p.package_name',
            'p.service_type_id',
            DB::raw('IFNULL(st.customer_app_service_type_name,st.service_type_name) as service_type_name'),
            'p.cleaning_material',
            'p.no_of_bookings',
            'p.working_hours',
            'p.strikethrough_amount',
            'p.amount',
            'p.description',
            'p.tc_html',
            'p.promotion_image',
            'p.promotion_image_web',
            DB::raw('2 as professionals_count'),
        )
            ->leftJoin('service_types as st', 'p.service_type_id', 'st.service_type_id')
            ->where([
                ['p.status', '=', 1],
                ['p.subscription_end_date', '>=', date('Y-m-d')],
                ['p.show_in_bottom_banner', '=', 1],
            ])
            ->orderBy('p.working_hours', 'ASC')
            ->get();
        foreach ($subscription_packages as $key => &$subscription_package) {
            if ($input['platform'] == 'mobile') {
                $subscription_package->banner_image_url = Config::get('values.file_server_url') . "upload/promotion_images/" . $subscription_package->promotion_image . "?v=" . $settings->customer_app_img_version;
            } else if ($input['platform'] == 'web') {
                $subscription_package->banner_image_url = Config::get('values.file_server_url') . "upload/promotion_images/" . $subscription_package->promotion_image_web . "?v=" . $settings->customer_app_img_version;
            } else {
                $subscription_package->banner_image_url = null;
            }
        }
        /**************************************************************** */
        /**
         * Top Banners
         */
        $banner_data = DB::table('package as p')->select(
            'p.package_id',
            'p.package_name',
            'p.service_type_id',
            DB::raw('IFNULL(st.customer_app_service_type_name,st.service_type_name) as service_type_name'),
            'p.cleaning_material',
            'p.no_of_bookings',
            'p.working_hours',
            'p.strikethrough_amount',
            'p.amount',
            'p.description',
            'p.tc_html',
            'p.promotion_image',
            'p.promotion_image_web',
            DB::raw('2 as professionals_count')
        )
            ->leftJoin('service_types as st', 'p.service_type_id', 'st.service_type_id')
            ->where([
                ['p.status', '=', 1],
                ['p.subscription_end_date', '>=', date('Y-m-d')],
                ['p.show_in_top_banner', '=', 1],
            ])
            ->orderBy('p.working_hours', 'DESC')
            ->get();

        foreach ($banner_data as $key => &$banner_package) {
            if ($input['platform'] == 'mobile') {
                $banner_package->banner_image_url = Config::get('values.file_server_url') . "upload/promotion_images/" . $banner_package->promotion_image . "?v=" . $settings->customer_app_img_version;
            } else if ($input['platform'] == 'web') {
                $banner_package->banner_image_url = Config::get('values.file_server_url') . "upload/promotion_images/" . $banner_package->promotion_image_web . "?v=" . $settings->customer_app_img_version;
            } else {
                $banner_package->banner_image_url = null;
            }
        }
        $response['banner_data'] = $banner_data;
        /**************************************************************** */
        $response['service_categories'] = [];
        foreach ($mapped_services as $key => $service_type) {
            $key = array_search($service_type->service_type_category_id, array_column($response['service_categories'], 'service_type_category_id'));
            if ($key > -1) {
                // exist
                $objectB = clone $service_type;
                if ($objectB->service_type_model_id == 1) {
                    $objectB->min_working_hours = 2;
                    $objectB->max_working_hours = 8;
                } else {
                    $objectB->min_working_hours = null;
                    $objectB->max_working_hours = null;
                    $objectB->min_no_of_professionals = null;
                    $objectB->max_no_of_professionals = null;
                }
                /*$objectB->subscription_packages = array_filter($subscription_packages->toArray(), function ($subscription_package) use ($service_type) {
                    return $subscription_package->service_type_id == $service_type->service_type_id;
                });
                $objectB->subscription_packages = [];*/
                $response['service_categories'][$key]->sub_categories[] = $objectB;
            } else {
                // not exist
                $objectB = clone $service_type;
                unset($objectB->service_type_id);
                unset($objectB->service_type_name);
                unset($objectB->service_type_thumbnail);
                unset($objectB->service_type_model);
                unset($objectB->service_type_model_id);
                unset($objectB->web_url_slug);
                unset($objectB->info_html);
                // sub_categories add from same object
                $objectC = clone $service_type;
                if ($objectC->service_type_model_id == 1) {
                    $objectC->min_working_hours = 2;
                    $objectC->max_working_hours = 8;
                } else {
                    $objectC->min_working_hours = null;
                    $objectC->max_working_hours = null;
                    $objectC->min_no_of_professionals = null;
                    $objectC->max_no_of_professionals = null;
                }
                /*$objectC->subscription_packages = array_filter($subscription_packages->toArray(), function ($subscription_package) use ($service_type) {
                    return $subscription_package->service_type_id == $service_type->service_type_id;
                });*/
                $objectB->sub_categories[] = $objectC;
                $response['service_categories'][] = $objectB;
            }
        }
        //$response['subscription_packages'] = $subscription_packages;
        /**************************************************************** */
        $special_offers = DB::table('offers_sb as so')
            ->select(
                'so.id_offers_sb as offer_id',
                'so.offer_heading',
                'so.offer_subheading',
                'so.offer_content',
                'cc.coupon_name as coupon_code',
                'cc.coupon_id',
                'st.service_type_id',
                'st.web_url_slug',
                DB::raw('IFNULL(st.customer_app_service_type_name,st.service_type_name) as service_type_name'),
                'st.service_type_model_id',
                'stm.model as service_type_model',
                DB::raw('null as banner_image_url'),
                'st.info_html'
            )
            ->leftJoin('coupon_code as cc', 'so.coupon_id', 'cc.coupon_id')
            ->leftJoin('service_types as st', 'cc.service_id', 'st.service_type_id')
            ->leftJoin('service_type_models as stm', 'st.service_type_model_id', 'stm.id')
            ->where(['so.offer_status' => 0])
            ->orderBy('so.id_offers_sb', 'DESC')
            ->get();
        foreach ($special_offers as $key => $special_offer) {
            if ($special_offer->service_type_model_id == '1') {
                $special_offers[$key]->min_working_hours = 2;
                $special_offers[$key]->max_working_hours = 8;
            } else {
                $special_offers[$key]->min_working_hours = null;
                $special_offers[$key]->max_working_hours = null;
                $special_offers[$key]->min_no_of_professionals = null;
                $special_offers[$key]->max_no_of_professionals = null;
            }
        }
        /**************************************************************** */
        $response['subscription_packages_and_special_offers'] = array_merge($subscription_packages->toArray(), $special_offers->toArray());
        /**************************************************************** */
        $response['payment_types'] = DB::table('payment_types as pt')
            ->select(
                'pt.id',
                'pt.name',
                'pt.description',
                'pt.code',
                'pt.charge',
                'pt.charge_type',
                'pt.default',
                'pt.show_in_web',
                'pt.show_in_app',
                DB::raw('CONCAT("' . Config::get('values.customer_app_assets_prefix_url') . '","payment-method-icons/",IFNULL(pt.customer_app_icon,"default.png"),"?v=' . $settings->customer_app_img_version . '") as icon_url'),
            )
            ->where(['deleted_at' => null]);
        if (@$input['platform'] == 'web') {
            $response['payment_types']->where(['show_in_web' => 1]);
        } else if (@$input['platform'] == 'mobile') {
            $response['payment_types']->where(['show_in_app' => 1]);
        }
        $response['payment_types'] = $response['payment_types']->orderBy('pt.sort_order', 'ASC')->get();
        /**************************************************************** */
        $response['genders'] = DB::table('genders as g')
            ->select(
                'g.gender_id',
                'g.gender'
            )
            ->where(['g.deleted_at' => null])
            ->orderBy('g.sort_order_id', 'ASC')
            ->get();
        /**************************************************************** */
        $response['cancel_reasons'] = DB::table('booking_cancel_reasons as bcr')
            ->select(
                'bcr.id',
                'bcr.reason',
            )
            ->where([['bcr.deleted_at', '=', null]])
            ->orderBy('bcr.order_priority', 'ASC')
            ->get();
        /**************************************************************** */
        $response['address_types'] = array(
            ['code' => 'HO', 'name' => 'Home'],
            ['code' => 'OF', 'name' => 'Office'],
            ['code' => 'OT', 'name' => 'Other'],
        );
        /**************************************************************** */
        $response['google_maps_api_key'] = $settings->customer_app_google_maps_api_key;
        /**************************************************************** */
        $response['urls'] = [
            'terms_and_conditions' => route('terms-conditions'),
            'privacy_policy' => route('privacy-policy'),
            'about_us' => route('about-us'),
            'apps' => [
                'android' => 'https://play.google.com/store/apps/details?id=com.dubaihousekeeping.services',
                'ios' => 'https://apps.apple.com/us/app/dubai-housekeeping/id6468903684'
            ]
        ];
        /**************************************************************** */
        $response['checkout_data'] = [
            // DON'T include sensitive data here
            'checkout_primary_key' => Config::get('values.checkout_primary_key'),
            'checkout_token_url' => Config::get('values.checkout_token_url'),
        ];
        /**************************************************************** */
        $response['mobile_numbers'] = [
            // DON'T include sensitive data here
            'call' => Config::get('values.mobile_number_call'),
            'whatsapp' => Config::get('values.mobile_number_whatsapp'),
        ];
        /**************************************************************** */
        $response['otp_expire_seconds'] = Config::get('values.customer_login_otp_expire_seconds');
        /**************************************************************** */
        /**
         * CCAVENUE details- please don't reveal sensitive gateway data here
         */
        $response['ccavenue'] = [
            'merchant_id' => config('ccavenue.merchant_id'),
            'req_handler' => config('ccavenue.req_handler'),
            'resp_handler' => config('ccavenue.resp_handler')
        ];
        /**************************************************************** */
        $response['input'] = $input;
        $response['status'] = 'success';
        $response['message'] = 'Data fetched successfully.';
        return Response::json(array('result' => $response), 200, [], JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES | JSON_NUMERIC_CHECK);
    }
}
