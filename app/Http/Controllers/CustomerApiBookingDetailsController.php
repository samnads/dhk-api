<?php

namespace App\Http\Controllers;

use App\Models\Booking;
use App\Models\BookingAnswer;
use App\Models\BookingCleaningSupply;
use App\Models\BookingDeleteRemarks;
use App\Models\BookingAddons;
use App\Models\BookingExtraService;
use App\Models\BookingPackages;
use App\Models\BookingWeekday;
use App\Models\CustomerAddress;
use App\Models\ServiceTypeDetailAnswer;
use App\Models\Settings;
use Carbon\Carbon;
use Config;
use App\Models\Customer;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Response;
use stdClass;

class CustomerApiBookingDetailsController extends Controller
{
    public function booking_details(Request $request)
    {
        try {
            $settings = Settings::first();
            $data = json_decode($request->getContent(), true);
            // required input check
            $input = @$data['params'];
            $validator = Validator::make(
                (array) $input,
                [
                    'booking_id' => 'required|exists:bookings,booking_id',
                ],
                [],
                [
                    'booking_id' => 'Booking ID',
                ]
            );
            if ($validator->fails()) {
                return Response::json(array('result' => array('status' => 'failed', 'message' => $validator->errors()->first())), 200, [], customerResponseJsonConstants());
            }
            /************************************************************* */
            $booking = Booking::findOrFail($input['booking_id']);
            if ($booking->booking_common_id) {
                $input['booking_id'] = $booking->booking_common_id;
            }
            $booking = DB::table('bookings as b')
                ->select(
                    'b.booking_id',
                    'b.booking_common_id',
                    DB::raw('CONCAT((CASE WHEN b.reference_id = "" THEN b.booking_id ELSE b.reference_id END),(CASE WHEN b.booking_common_id IS NULL THEN "" ELSE CONCAT("","","") END)) as booking_reference'),
                    DB::raw('IFNULL(st.service_type_name,"Unknown") as service'),
                    'b.customer_address_id',
                    'b.crew_in',
                    'b.booking_note',
                    'b.supervisor_selected',
                    'b._supervisor_amount as _supervisor_amount'
                )
                ->leftJoin('service_types as st', 'b.service_type_id', 'st.service_type_id')
                ->leftJoin('maids as m', 'b.maid_id', 'm.maid_id')
                ->leftJoin('customers as c', 'b.customer_id', 'c.customer_id')
                ->leftJoin('frequencies as f', 'b.booking_type', 'f.code')
                ->leftJoin('payment_types as pt', 'b.payment_type_id', 'pt.id')
                ->leftJoin('customer_addresses as ca', 'b.customer_address_id', 'ca.customer_address_id')
                ->leftJoin('areas as a', 'a.area_id', 'ca.area_id')
                ->where([['b.customer_id', '=', $input['id']], ['b.booking_id', '=', $input['booking_id']]])
                ->orderBy('b.booking_id', 'ASC')
                ->first();
            $response['booking'] = $booking;
            $response['customer'] = Customer::select('customer_name')->where('customer_id', '=', $input['id'])->first();
            $response['address'] = CustomerAddress::
                select(
                    'customer_addresses.*',
                    'a.area_name',
                )
                ->leftJoin('areas as a', 'customer_addresses.area_id', 'a.area_id')
                ->where('customer_address_id', '=', $booking->customer_address_id)
                ->first();
            /*$response['addons'] = DB::table('booking_addons as ba')
                ->select(
                    'ba.service_addons_id',
                    DB::raw('IFNULL(ba.service_addon_name,sao.service_addon_name) as service_addon_name'),
                    'sao.service_addon_description',
                    DB::raw('"' . Config::get('values.currency_code') . '" as currency'),
                    'ba.service_minutes',
                    'ba.strike_amount',
                    'ba.strike_amount',
                    'ba.unit_price',
                    'ba.quantity',
                    'ba.amount',
                    DB::raw('CONCAT("' . Config::get('values.file_server_url') . 'customer-app-assets/service-addon-images/",IFNULL(sao.customer_app_thumbnail,"service-addons-thumbnail.png"),"?v=' . $settings->customer_app_img_version . '") as image_url'),
                )
                ->leftJoin('service_addons as sao', 'ba.service_addons_id', 'sao.service_addons_id')
                ->where([['ba.booking_id', "=", $input['booking_id']]])
                ->orderBy('sao.populariry', 'DESC')
                ->get();*/
            $response['cleaning_materials'] = BookingCleaningSupply::
                select(
                    'booking_cleaning_supplies.id',
                    'booking_cleaning_supplies.cleaning_supply_id',
                    'ccs.name',
                    'booking_cleaning_supplies.total_amount'
                )
                ->leftJoin('config_cleaning_supplies as ccs', 'booking_cleaning_supplies.cleaning_supply_id', 'ccs.id')
                ->where('booking_cleaning_supplies.booking_id', $input['booking_id'])
                ->where('ccs.type', 'PlanBased')
                ->get();
            $response['tools'] = BookingCleaningSupply::
                select(
                    'booking_cleaning_supplies.id',
                    'booking_cleaning_supplies.cleaning_supply_id',
                    'ccs.name',
                    'booking_cleaning_supplies.total_amount'
                )
                ->leftJoin('config_cleaning_supplies as ccs', 'booking_cleaning_supplies.cleaning_supply_id', 'ccs.id')
                ->where('booking_cleaning_supplies.booking_id', $input['booking_id'])
                ->where('ccs.type', 'Custom')
                ->get();
            $response['details'] = BookingAnswer::
                select(
                    'booking_answers.id',
                    'booking_answers.question',
                    'booking_answers.answer'
                )
                ->where('booking_answers.booking_id', $input['booking_id'])
                ->get();
            $response['extra_services'] = BookingExtraService::
                select(
                    'booking_extra_services.id',
                    'booking_extra_services.extra_service_id',
                    'es.service as name',
                    'booking_extra_services.duration',
                    'booking_extra_services.total_amount'
                )
                ->leftJoin('extra_services as es', 'booking_extra_services.extra_service_id', 'es.id')
                ->where('booking_extra_services.booking_id', $input['booking_id'])
                ->get();
            $response['weekdays'] = BookingWeekday::
                select(
                    'booking_weekdays.id',
                    'booking_weekdays.week_day_id',
                    'wd.week_name',
                )
                ->leftJoin('week_days as wd', 'booking_weekdays.week_day_id', 'wd.week_day_id')
                ->where('booking_weekdays.booking_id', $input['booking_id'])
                ->orderBy('wd.order_id', 'ASC')
                ->get();
            $response['packages'] = BookingPackages::
                select(
                    'booking_packages.building_type',
                    'booking_packages.building_room',
                    'booking_packages.unit_price',
                    'booking_packages.quantity',
                    DB::raw('(booking_packages.unit_price * booking_packages.quantity) as total'),
                )
                ->leftJoin('building_type_room_packages as btrp', 'booking_packages.building_type_room_package_id', 'btrp.id')
                ->leftJoin('building_rooms as br', 'btrp.building_room_id', 'br.id')
                ->leftJoin('building_types as bt', 'br.building_type_id', 'bt.id')
                ->where('booking_packages.booking_id', $input['booking_id'])
                ->get();
            $response['status'] = 'success';
            $response['message'] = "Booking details fetched successfully.";
            return Response::json(array('result' => $response), 200, array(), customerResponseJsonConstants());
        } catch (\Exception $e) {
            DB::rollback();
            return Response::json(array('result' => array('status' => 'failed', 'message' => $e->getMessage())), 200, array(), customerResponseJsonConstants());
        }
    }
}
