<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class PriceSettings extends Model
{
    use HasFactory;
    protected $table = 'price_settings';
    public $timestamps = false;
    protected $primaryKey = 'id';
}
