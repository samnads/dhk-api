<?php

namespace App\Http\Controllers;

use App\Models\Booking;
use App\Models\OnlinePayment;
use Illuminate\Http\Request;
use Log;
use Response;

class PaymentWebhookController extends Controller
{
    public function ccavenue_order_status(Request $request)
    {
        Log::channel('debug')->info('++++++++++++++++++ WEB HOOK Request ++++++++++++++++++');
        Log::channel('debug')->info(json_encode(@$request->all() ?: []));
        Log::channel('debug')->info('++++++++++++++++++ WEB HOOK Request ++++++++++++++++++');
        $encResponse = $request->encResp; //This is the response sent by the CCAvenue Server
        try {
            $workingKey = config('ccavenue.workingkey'); //Working Key should be provided here.
            $rcvdString = ccavDecrypt($encResponse, $workingKey); //Crypto Decryption used as per the specified working key.
            parse_str($rcvdString, $params);
            $mail = new AdminApiMailController;
            $ccavenuePaymentController = new ccavenuePaymentController();
            if (@$params['order_status'] == 'Success') {
                if ($params['merchant_param1'] == 'web-booking' || $params['merchant_param1'] == 'mobile-booking') {
                    $ccavenuePaymentController->after_ccavenue_payment_success($params);
                    /**
                     * Send mails
                     */
                    $booking = Booking::where('reference_id', '=', $params['order_id'])->orderBy('booking_id', 'desc')->first();
                    $mail->booking_confirmation_to_admin($booking->booking_id);
                    $mail->booking_confirmation_to_customer($booking->booking_id);
                } else if ($params['merchant_param1'] == 'invoice-payment') {
                    $ccavenuePaymentController->after_ccavenue_payment_success($params);
                    $online_payment = OnlinePayment::where('reference_id', $params['order_id'])->first();
                    $mail->online_payment_confirm_to_admin($online_payment->payment_id);
                    $mail->online_payment_confirm_to_customer($online_payment->payment_id);
                } else {
                    $online_payment = OnlinePayment::where('reference_id', $params['order_id'])->first();
                    $mail->online_payment_confirm_to_admin($online_payment->payment_id);
                    $mail->online_payment_confirm_to_customer($online_payment->payment_id);
                }
            }
            Log::channel('debug')->info('++++++++++++++++++ WEB HOOK ++++++++++++++++++');
            Log::channel('debug')->info(json_encode(@$params ?: []));
            Log::channel('debug')->info('++++++++++++++++++ WEB HOOK ++++++++++++++++++');
        } catch (\Exception $e) {
            Log::channel('debug')->info('++++++++++++++++++ WEB HOOK EXCEPTION ++++++++++++++++++');
            Log::channel('debug')->info($e->getMessage());
            Log::channel('debug')->info('++++++++++++++++++ WEB HOOK EXCEPTION ++++++++++++++++++');
            return Response::json(array('result' => array('status' => 'failed', 'message' => $e->getMessage())), 502, [], JSON_PRETTY_PRINT);
        }
    }
}
