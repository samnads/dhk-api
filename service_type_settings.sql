-- Adminer 4.8.1 MySQL 11.3.2-MariaDB dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

DROP TABLE IF EXISTS `service_type_settings`;
CREATE TABLE `service_type_settings` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `service_type_id` int(11) NOT NULL,
  `cleaning_materials_opted` tinyint(1) NOT NULL DEFAULT 0,
  `cleaning_materials_option_show` tinyint(1) NOT NULL DEFAULT 1,
  `supervisor_option_show` tinyint(1) NOT NULL DEFAULT 0,
  `instructions_field_label` varchar(155) NOT NULL,
  `instructions_field_placeholder` varchar(155) NOT NULL,
  `instructions_field_required` tinyint(1) NOT NULL DEFAULT 0,
  `default_material_id` bigint(20) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `service_type_id` (`service_type_id`),
  CONSTRAINT `service_type_settings_ibfk_1` FOREIGN KEY (`service_type_id`) REFERENCES `service_types` (`service_type_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;

INSERT INTO `service_type_settings` (`id`, `service_type_id`, `cleaning_materials_opted`, `cleaning_materials_option_show`, `supervisor_option_show`, `instructions_field_label`, `instructions_field_placeholder`, `instructions_field_required`, `default_material_id`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1,	5,	0,	0,	0,	'Any instructions or special requirements?',	'Write here...',	0,	NULL,	'2024-06-11 12:28:24',	'2024-06-11 12:28:24',	NULL),
(2,	4,	0,	1,	1,	'Any instructions or special requirements?',	'Write here...',	0,	NULL,	'2024-06-11 12:28:24',	'2024-06-11 12:28:24',	NULL),
(3,	1,	0,	1,	1,	'Any instructions or special requirements?',	'Write here...',	0,	NULL,	'2024-06-11 12:28:24',	'2024-06-11 12:28:24',	NULL),
(4,	52,	0,	0,	0,	'Number of child and age',	'Please describe number of child and age of each child...',	1,	NULL,	'2024-06-11 12:28:24',	'2024-06-11 12:28:24',	NULL),
(5,	47,	1,	0,	0,	'Any instructions or special requirements?',	'Write here...',	0,	8,	'2024-06-11 12:28:24',	'2024-06-11 12:28:24',	NULL),
(6,	53,	0,	0,	0,	'Any instructions or special requirements?',	'Write here...',	0,	NULL,	'2024-06-14 15:38:26',	'2024-06-14 15:38:26',	NULL),
(7,	55,	0,	0,	0,	'Any instructions or special requirements?',	'Write here...',	0,	NULL,	'2024-06-14 15:38:26',	'2024-06-14 15:38:26',	NULL),
(8,	54,	0,	0,	0,	'Any instructions or special requirements?',	'Write here...',	0,	NULL,	'2024-06-14 15:38:26',	'2024-06-14 15:38:26',	NULL),
(9,	56,	0,	0,	0,	'Any instructions or special requirements?',	'Write here...',	0,	NULL,	'2024-06-14 15:38:26',	'2024-06-14 15:38:26',	NULL),
(10,	62,	0,	0,	0,	'Any instructions or special requirements?',	'Write here...',	0,	NULL,	'2024-06-14 15:38:26',	'2024-06-14 15:38:26',	NULL),
(11,	61,	0,	0,	0,	'Any instructions or special requirements?',	'Write here...',	0,	NULL,	'2024-06-14 15:38:26',	'2024-06-14 15:38:26',	NULL),
(12,	60,	0,	0,	0,	'Any instructions or special requirements?',	'Write here...',	0,	NULL,	'2024-06-14 15:38:26',	'2024-06-14 15:38:26',	NULL),
(13,	57,	0,	0,	0,	'Any instructions or special requirements?',	'Write here...',	0,	NULL,	'2024-06-14 15:38:26',	'2024-06-14 15:38:26',	NULL),
(14,	59,	0,	0,	0,	'Any instructions or special requirements?',	'Write here...',	0,	NULL,	'2024-06-14 15:38:26',	'2024-06-14 15:38:26',	NULL),
(15,	58,	0,	0,	0,	'Any instructions or special requirements?',	'Write here...',	0,	NULL,	'2024-06-14 15:38:26',	'2024-06-14 15:38:26',	NULL);

-- 2024-06-15 06:54:31
