<?php

namespace App\Http\Controllers;

use App\Models\Booking;
use App\Models\BookingCancel;
use App\Models\BookingDeleteRemarks;
use App\Models\BookingDeletes;
use App\Models\Customer;
use App\Models\DayServices;
use Carbon\Carbon;
use Config;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Response;
use stdClass;

class CustomerApiBookingCancelController extends Controller
{
    public function booking_cancel(Request $request)
    {
        $debug = toggleDebug(false); // pass boolean to overide default
        /************************************************************* */
        if (!$debug) {
            // live input
            $data = json_decode($request->getContent(), true);
        } else {
            // test input
            $data['params']['id'] = Config::get('values.debug_customer_id');
            $data['params']['booking_id'] = 176742;
            $data['params']['date'] = "05/11/2023";
            $data['params']['cancel_reason'] = "test cancel";
        }
        /************************************************************* */
        // required input check
        $input = @$data['params'];
        $validator = Validator::make(
            (array) $input,
            [
                'booking_id' => 'required|integer',
                'date' => 'required|date_format:d/m/Y',
                'cancel_reason' => 'nullable|string',
                //'date' => 'date_format:d/m/Y',
                //'cancel_reason' => 'string',
            ],
            [],
            [
                'booking_id' => 'Booking ID',
                'date' => 'Schedule Date',
                'cancel_reason' => 'Cancel Reason',
            ]
        );
        if ($validator->fails()) {
            return Response::json(array('result' => array('status' => 'failed', 'message' => $validator->errors()->first()), 'debug' => $debug), 200, array(), customerResponseJsonConstants());
        }
        $input['date'] = Carbon::createFromFormat('d/m/Y', $input['date'])->format('Y-m-d');
        /************************************************************* */
        DB::beginTransaction();
        try {
            $customer = Customer::where('customer_id', '=', $input['id'])->first();
            $booking = DB::table('bookings as b')
                ->select(
                    'b.booking_id',
                    'b.booking_common_id',
                    'b.service_start_date',
                    'b.time_from',
                    DB::raw('IFNULL(st.service_type_name,"Unknown") as service'),
                    DB::raw('(CASE WHEN b.reference_id = "" THEN b.booking_id ELSE b.reference_id END) as booking_reference'),
                    'f.name as frequency',
                    'f.code as frequency_code',
                    'ca.customer_address as address',
                    DB::raw('"' . Config::get('values.currency_code') . '" as currency'),
                    'b.total_amount as total',
                    DB::raw('(CASE WHEN b.payment_type_id IS NULL THEN b.pay_by ELSE pt.name END) as payment_method'),
                    DB::raw('"Cancelled" as status'),
                    DB::raw('DATE_FORMAT(SEC_TO_TIME(TIME_TO_SEC(TIMEDIFF(b.time_to,b.time_from))),"%H:%i") as hours'),
                    DB::raw('(CASE WHEN b.cleaning_material = "Y" THEN "yes" ELSE "no" END) as cleaning_materials'),
                    DB::raw('"' . $input['date'] . '" as date'),
                )
                ->leftJoin('service_types as st', 'b.service_type_id', 'st.service_type_id')
                ->leftJoin('frequencies as f', 'b.booking_type', 'f.code')
                ->leftJoin('customer_addresses as ca', 'b.customer_address_id', 'ca.customer_address_id')
                ->leftJoin('payment_types as pt', 'b.payment_type_id', 'pt.id')
                ->leftJoin('maids as m', 'b.maid_id', 'm.maid_id')
                ->where([['booking_id', '=', $input['booking_id']], ['b.customer_id', '=', $input['id']], ['b.booking_status', '!=', 2]])->first();
            if (!$booking) {
                return Response::json(array('result' => array('status' => 'failed', 'message' => 'Booking not found.'), 'debug' => $debug), 200, array(), customerResponseJsonConstants());
            }
            $day_service = DayServices::where([['booking_id', '=', $input['booking_id']], ['service_date', '=', $input['date']], ['service_status', '!=', 3]])->first(); // 3 - get started or ended
            if ($day_service) {
                return Response::json(array('result' => array('status' => 'failed', 'message' => 'Schedule entry found.'), 'debug' => $debug), 200, array(), customerResponseJsonConstants());
            }
            /************************************************************* */
            // stop cancel process based on time left
            $booking_time = Carbon::parse($booking->service_start_date . ' ' . $booking->time_from)->format('Y-m-d H:i:s'); // booked date time
            $booking_time_minus_60_minutes = Carbon::parse($booking_time)->subMinutes(60);
            $current_time = Carbon::now();
            if ($current_time >= $booking_time_minus_60_minutes) {
                throw new \ErrorException('Cancellation is not available at this moment, please contact us.');
            }
            /************************************************************* */
            if ($booking->booking_common_id) {
                $bookings = Booking::where('booking_common_id', $booking->booking_common_id)->get();
                $bookings_ids = array_column($bookings->toArray(), 'booking_id');
            } else {
                $bookings_ids = [$input['booking_id']];
            }
            foreach ($bookings_ids as $index => $bookings_id) {
                $booking_row = Booking::find($bookings_id);
                if ($booking_row->booking_status == 0) {
                    // still it's not a confirmed booking
                    if ($booking_row->payment_type_id == 1) {
                        // cash mode, don't care
                        $booking_row->web_book_show = 0;
                        $booking_row->booking_status = 2;
                        //
                        $booking_delete_remark = BookingDeleteRemarks::where([['booking_id', '=', $bookings_id], ['service_date', '=', null]])->first() ?: new BookingDeleteRemarks();
                        $booking_delete_remark->booking_id = $bookings_id;
                        $booking_delete_remark->service_date = null;
                        $booking_delete_remark->remarks = @$input['cancel_reason'];
                        $booking_delete_remark->deleted_date_time = Carbon::now();
                        $booking_delete_remark->save();
                    } else {
                        // show in lead list
                        $booking_row->web_book_show = 0;
                        $booking_row->booking_status = 2;
                        //
                        $booking_delete_remark = BookingDeleteRemarks::where([['booking_id', '=', $bookings_id], ['service_date', '=', $booking_row->service_start_date]])->first() ?: new BookingDeleteRemarks();
                        $booking_delete_remark->booking_id = $bookings_id;
                        $booking_delete_remark->service_date = $booking_row->service_start_date;
                        $booking_delete_remark->remarks = @$input['cancel_reason'];
                        $booking_delete_remark->deleted_date_time = Carbon::now();
                        $booking_delete_remark->save();
                    }
                } else if ($booking->frequency_code == "OD") {
                    $booking_row->booking_status = 2; // only for one day booking
                    $booking_row->web_book_show = 0;
                    $booking_cancel = BookingCancel::where([['booking_id', '=', $bookings_id], ['service_date', '=', $input['date']]])->first() ?: new BookingCancel();
                    $booking_cancel->booking_id = $bookings_id;
                    $booking_cancel->service_date = $input['date'];
                    $booking_cancel->added_datetime = Carbon::now();
                    $booking_cancel->save();
                    //
                    $booking_delete_remark = BookingDeleteRemarks::where([['booking_id', '=', $bookings_id], ['service_date', '=', $input['date']]])->first() ?: new BookingDeleteRemarks();
                    $booking_delete_remark->booking_id = $bookings_id;
                    $booking_delete_remark->service_date = $input['date'];
                    $booking_delete_remark->remarks = @$input['cancel_reason'];
                    $booking_delete_remark->deleted_date_time = Carbon::now();
                    $booking_delete_remark->save();
                } else if ($booking->frequency_code == "WE") {
                    // service date validation
                    $no_of_service = (Carbon::parse($input['date'])->diffInDays($booking_row->service_start_date) / 7) + 1;
                    if (floor($no_of_service) != $no_of_service) {
                        DB::rollback();
                        return Response::json(array('result' => array('status' => 'failed', 'message' => 'Invalid schedule date.'), 'debug' => $debug), 200, array(), customerResponseJsonConstants());
                    }
                    $booking_cancel = BookingCancel::where([['booking_id', '=', $bookings_id], ['service_date', '=', $input['date']]])->first() ?: new BookingCancel();
                    $booking_cancel->booking_id = $bookings_id;
                    $booking_cancel->service_date = $input['date'];
                    $booking_cancel->added_datetime = Carbon::now();
                    $booking_cancel->save();
                    //
                    $booking_delete_remark = BookingDeleteRemarks::where([['booking_id', '=', $bookings_id], ['service_date', '=', $input['date']]])->first() ?: new BookingDeleteRemarks();
                    $booking_delete_remark->booking_id = $bookings_id;
                    $booking_delete_remark->service_date = $input['date'];
                    $booking_delete_remark->remarks = @$input['cancel_reason'];
                    $booking_delete_remark->deleted_date_time = Carbon::now();
                    $booking_delete_remark->save();
                    //
                    $booking_delete = BookingDeletes::where([['booking_id', '=', $bookings_id], ['service_date', '=', $input['date']]])->first() ?: new BookingDeletes();
                    $booking_delete->booking_id = $bookings_id;
                    $booking_delete->service_date = $input['date'];
                    $booking_delete->added_datetime = Carbon::now();
                    $booking_delete->save();
                } else if ($booking->frequency_code == "BW") {
                    // service date validation
                    $no_of_service = (Carbon::parse($input['date'])->diffInDays($booking_row->service_start_date) / 14) + 1;
                    if (floor($no_of_service) != $no_of_service) {
                        DB::rollback();
                        return Response::json(array('result' => array('status' => 'failed', 'message' => 'Invalid schedule date.'), 'debug' => $debug), 200, array(), customerResponseJsonConstants());
                    }
                    $booking_cancel = BookingCancel::where([['booking_id', '=', $bookings_id], ['service_date', '=', $input['date']]])->first() ?: new BookingCancel();
                    $booking_cancel->booking_id = $bookings_id;
                    $booking_cancel->service_date = $input['date'];
                    $booking_cancel->added_datetime = Carbon::now();
                    $booking_cancel->save();
                    //
                    $booking_delete_remark = BookingDeleteRemarks::where([['booking_id', '=', $bookings_id], ['service_date', '=', $input['date']]])->first() ?: new BookingDeleteRemarks();
                    $booking_delete_remark->booking_id = $bookings_id;
                    $booking_delete_remark->service_date = $input['date'];
                    $booking_delete_remark->remarks = @$input['cancel_reason'];
                    $booking_delete_remark->deleted_date_time = Carbon::now();
                    $booking_delete_remark->save();
                    //
                    $booking_delete = BookingDeletes::where([['booking_id', '=', $bookings_id], ['service_date', '=', $input['date']]])->first() ?: new BookingDeletes();
                    $booking_delete->booking_id = $input['booking_id'];
                    $booking_delete->service_date = $input['date'];
                    $booking_delete->added_datetime = Carbon::now();
                    $booking_delete->save();
                }
                $booking_row->save();
            }
            DB::commit();
            /********************************************************************************************* */
            // send booking cancellation mails
            $mail = new AdminApiMailController;
            try {
                $mail->schedule_cancel_to_admin($input['booking_id'], $input['date']);
                $mail->schedule_cancel_to_customer($input['booking_id'], $input['date']);
            } catch (\Exception $e) {
                //throw new \ErrorException($e->getMessage());
            }
            /********************************************************************************************* */
            $notify = new stdClass();
            $notify->customer_id = $input['id'];
            $notify->booking_id = $booking->booking_id;
            $notify->service_date = $input['date'];
            $notify->content = "Schedule on " . Carbon::parse($input['date'])->format('d/m/Y') . " for booking with Ref. Id. {{booking_ref_id}} is cancelled.";
            addCustomerNotification($notify);
            // send push notification
            pushNotification($customer, ['title' => "Cancelled !", 'body' => "Booking with Ref. Id " . $booking->booking_reference . " is cancelled."], ['screen' => 'BOOKING_HISTORY']);
            /********************************************************************************************* */
            $response['status'] = 'success';
            $booking->total = $booking->total * sizeof($bookings_ids);
            $response['service_details'] = $booking;
            unset($response['service_details']->frequency_code);
            $response['service_details']->service_details = $response['service_details']->hours . " hours, " . sizeof($bookings_ids) . " Professional " . ($response['service_details']->cleaning_materials == "yes" ? "with " : "without ") . "cleaning material";
            $response['message'] = 'Schedule cancelled successfully.';
        } catch (\Exception $e) {
            DB::rollback();
            return Response::json(array('result' => array('status' => 'failed', 'message' => $e->getMessage()), 'debug' => $debug), 200, array(), customerResponseJsonConstants());
        }
        /************************************************************* */
        return Response::json(array('result' => $response, 'debug' => $debug), 200, array(), customerResponseJsonConstants());
    }
    public function booking_all_cancel(Request $request)
    {
        $debug = toggleDebug(); // pass boolean to overide default
        /************************************************************* */
        if (!$debug) {
            // live input
            $data = json_decode($request->getContent(), true);
        } else {
            // test input
            $data['params']['id'] = 1; // match with middleware for testing
            $data['params']['booking_id'] = 359568;
            $data['params']['cancel_reason'] = "test cancel all 2";
        }
        /************************************************************* */
        // required input check
        $input = @$data['params'];
        $validator = Validator::make(
            (array) $input,
            [
                'booking_id' => 'required|integer',
                'cancel_reason' => 'nullable|string|min:3',
            ],
            [],
            [
                'booking_id' => 'Booking ID',
                'cancel_reason' => 'Cancel Reason',
            ]
        );
        if ($validator->fails()) {
            return Response::json(array('result' => array('status' => 'failed', 'message' => $validator->errors()->first()), 'debug' => $debug), 200, array(), customerResponseJsonConstants());
        }
        /************************************************************* */
        $booking = Booking::where([['customer_id', '=', $input['id']], ['booking_id', '=', $input['booking_id']], ['booking_status', '!=', 2]])->first();
        if (!$booking) {
            return Response::json(array('result' => array('status' => 'failed', 'message' => 'Booking not found.'), 'debug' => $debug), 200, array(), customerResponseJsonConstants());
        } elseif ($booking->booking_type == "OD") {
            return Response::json(array('result' => array('status' => 'failed', 'message' => 'Invalid booking type.'), 'debug' => $debug), 200, array(), customerResponseJsonConstants());
        }
        $booking_delete_remark = BookingDeleteRemarks::where([['booking_id', '=', $input['booking_id']], ['service_date', '=', null]])->first();
        if ($booking_delete_remark) {
            return Response::json(array('result' => array('status' => 'failed', 'message' => 'Bookings already cancelled.'), 'debug' => $debug), 200, array(), customerResponseJsonConstants());
        }
        DB::beginTransaction();
        try {
            $customer = Customer::where('customer_id', '=', $input['id'])->first();
            $booking = Booking::findOrFail($input['booking_id']);
            if ($booking->booking_common_id) {
                $bookings = Booking::where('booking_common_id', $booking->booking_common_id)->get();
                $bookings_ids = array_column($bookings->toArray(), 'booking_id');
            } else {
                $bookings_ids = [$input['booking_id']];
            }
            $today = date("Y-m-d");
            $service_week_name = Carbon::createFromFormat('Y-m-d', $booking['service_start_date'])->format('l');
            if ($booking->booking_type == "WE") {
                $service_end_date = Carbon::parse($today)->previous($service_week_name)->format('Y-m-d');
                if ($service_end_date < $booking['service_start_date']) {
                    $service_end_date = $booking['service_start_date'];
                }
            } else if ($booking->booking_type == "BW") {
                $service_end_date = Carbon::parse($today)->previous($service_week_name)->format('Y-m-d');
                $no_of_service = (Carbon::parse($service_end_date)->diffInDays($booking->service_start_date) / 14) + 1;
                if (floor($no_of_service) != $no_of_service) {
                    $service_end_date = Carbon::parse($service_end_date)->addWeeks(-1)->format('Y-m-d');
                }
                if ($service_end_date < $booking['service_start_date']) {
                    $service_end_date = $booking['service_start_date'];
                }
            } else {
                DB::rollback();
                return Response::json(array('result' => array('status' => 'failed', 'message' => "Invalid booking type."), 'debug' => $debug), 200, array(), customerResponseJsonConstants());
            }
            foreach ($bookings_ids as $index => $bookings_id) {
                $booking_row = Booking::findOrFail($bookings_id);
                $booking_row->service_end = 1;
                $booking_row->service_end_date = $service_end_date;
                $booking_row->service_actual_end_date = $service_end_date;
                $booking_row->save();
                /*$booking_cancel = BookingCancel::where([['booking_id', '=', $bookings_id], ['service_date', '=', $input['date']]])->first() ?: new BookingCancel();
                $booking_cancel->booking_id = $bookings_id;
                $booking_cancel->service_date = $input['date'];
                $booking_cancel->added_datetime = Carbon::now();
                $booking_cancel->save();*/
                $booking_delete_remark = BookingDeleteRemarks::where([['booking_id', '=', $bookings_id], ['service_date', '=', null]])->first() ?: new BookingDeleteRemarks();
                $booking_delete_remark->booking_id = $bookings_id;
                $booking_delete_remark->service_date = null; // not a single delete, it's for permanent
                $booking_delete_remark->remarks = @$input['cancel_reason'];
                $booking_delete_remark->deleted_date_time = Carbon::now();
                $booking_delete_remark->save();
            }
            /*************************************************** */
            $notify = new stdClass();
            $notify->customer_id = $input['id'];
            $notify->booking_id = $booking->booking_id;
            $notify->content = "All future schedules for booking with Ref. Id. {{booking_ref_id}} is cancelled.";
            addCustomerNotification($notify);
            // send push notification
            pushNotification($customer, ['title' => "Cancelled !", 'body' => "All future bookings with Ref. Id " . $booking->booking_reference . " is cancelled."], ['screen' => 'BOOKING_HISTORY']);
            /*************************************************** */
            DB::commit();
            $response['status'] = 'success';
            $response['message'] = 'Bookings cancelled successfully.';
        } catch (\Exception $e) {
            DB::rollback();
            return Response::json(array('result' => array('status' => 'failed', 'message' => $e->getMessage()), 'debug' => $debug), 200, array(), customerResponseJsonConstants());
        }
        return Response::json(array('result' => $response, 'debug' => $debug), 200, array(), customerResponseJsonConstants());
    }
}
