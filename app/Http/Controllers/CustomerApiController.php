<?php

namespace App\Http\Controllers;

use App\Models\Booking;
use App\Models\Customer;
use App\Models\CustomersTemp;
use App\Models\Settings;
use Carbon\Carbon;
use Config;
use File;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use Intervention\Image\Facades\Image;
use Mail;
use Response;
use stdClass;

class CustomerApiController extends Controller
{
    public function customer_logout(Request $request)
    {
        $debug = toggleDebug(); // pass boolean to overide default
        /************************************************************* */
        if (!$debug) {
            // live input
            $data = json_decode($request->getContent(), true);
        } else {
            // test input
            $data['params']['id'] = Config::get('values.debug_customer_id');
        }
        /************************************************************* */
        $input = @$data['params'];
        /************************************************************* */
        $response['status'] = 'success';
        $response['message'] = 'Signed out successfully !';
        $customer = Customer::where([['customer_id', '=', $input['id']]])->first();
        $customer->oauth_token = null;
        $customer->save();
        return Response::json(array('result' => $response, 'debug' => $debug), 200, array(), customerResponseJsonConstants());
    }
    public function name_entry(Request $request)
    {
        $settings = Settings::first();
        /************************************************************* */
        // required input check
        $data = json_decode($request->getContent(), true);
        $input = @$data['params'];
        $input['name'] = trim($input['name']);
        $input['country_code'] = str_replace('+', '', @$input['country_code']);
        $input['mobilenumber'] = str_replace(' ', '', @$input['mobilenumber']);
        $validator = Validator::make(
            (array) $input,
            [
                'name' => 'required|string|min:3',
                'email' => 'nullable|email:rfc,dns|unique:customers,email_address,' . $input['id'] . ',customer_id',
                'mobilenumber' => 'nullable|numeric|unique:customers,mobile_number_1',
                'country_code' => 'required_with:mobilenumber|numeric|min:1',
            ],
            [],
            [
                'name' => 'Name',
                'email' => 'Email address',
                'mobilenumber' => 'Mobile Number',
                'country_code' => 'Country Code',
            ]
        );
        if ($validator->fails()) {
            return Response::json(array('result' => array('status' => 'failed', 'message' => $validator->errors()->first())), 200, [], customerResponseJsonConstants());
        }
        /************************************************************* */
        $customer = Customer::find($input['id']);
        DB::beginTransaction();
        try {
            /******************************************************* */
            if (@$input['mobilenumber']) {
                // validate with db
                $mobile_exist = Customer::where([
                    ['mobile_number_1', '=', @$input['country_code'] . @$input['mobilenumber']],
                    ['customer_id', '!=', $input['id']]
                ])->first();
                if ($mobile_exist) {
                    throw new \ErrorException('Mobile number already exist.');
                }
            }
            /******************************************************* */
            $sent_email = true;
            if ($customer->email_address) {
                $sent_email = false;
            }
            $customer->customer_name = $input['name']; // full name
            $customer->customer_nick_name = $input['name']; // nick name
            $customer->customer_last_modified_datetime = Carbon::now();
            if (@$input['email']) {
                $customer->email_address = $input['email'];
            }
            if (@$input['mobilenumber']) {
                $customer->mobile_number_1 = @$input['country_code'] . @$input['mobilenumber'];
            }
            $customer->mobile_status = 1;
            $customer->save();
            $response['status'] = 'success';
            $response['message'] = 'Details updated successfully.';
            $response['UserDetails']['id'] = $customer->customer_id;
            $response['UserDetails']['UserName'] = $customer->customer_name;
            $response['UserDetails']['email'] = $customer->email_address;
            $response['UserDetails']['dob'] = $customer->dob;
            $response['UserDetails']['gender_id'] = $customer->gender_id;
            $response['UserDetails']['image'] = Config::get('values.customer_avatar_prefix_url') . (($customer->customer_photo_file != '') ? (File::exists(Config::get('values.customer_avatar_storage_path') . $customer->customer_photo_file) ? ($customer->customer_photo_file . '?v=' . $settings->customer_app_img_version) : (Config::get('values.customer_avatar_default_file') . '?v=' . $settings->customer_app_img_version)) : (Config::get('values.customer_avatar_default_file') . '?v=' . $settings->customer_app_img_version));
            $response['UserDetails']['country_code'] = null;
            $response['UserDetails']['mobile'] = $customer->mobile_number_1;
            $response['UserDetails']['token'] = $customer->oauth_token;
            /********************************************************************************************* */
            // first time welcome notify
            $notify = new stdClass();
            $notify->customer_id = $input['id'];
            $notify->content = "Hi {{customer_name}}, Welcome to \"{{company_name}}\".";
            addCustomerNotification($notify);
            /********************************************************************************************* */
            DB::commit();
            /********************************************************************************************* */
            // send regstraion mail
            $mail = new AdminApiMailController;
            $sent_email ? $mail->registration_success_to_customer($input['id']) : '';
            /********************************************************************************************* */
            return Response::json(array('result' => $response), 200, [], JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES);
        } catch (\Exception $e) {
            DB::rollback();
            return Response::json(array('result' => array('status' => 'failed', 'message' => $e->getMessage())), 200, [], customerResponseJsonConstants());
        }
    }
    public function frequencies(Request $request)
    {
        $debug = toggleDebug(); // pass boolean to overide default
        try {
            $response['status'] = 'success';
            $response['payment_types'] = DB::table('payment_types as pt')
                ->select(
                    'pt.id',
                    'pt.name',
                    'pt.code',
                    'pt.charge',
                    'pt.charge_type'
                )
                ->where(['deleted_at' => null])
                ->get();
            $response['frequency_list'] = DB::table('frequencies as fl')
                ->select(
                    'fl.id',
                    'fl.name',
                    'fl.description',
                    'fl.code',
                    'fl.offer_percentage as percentage',
                    'fl.hourly_rate',
                    'fl.cleaning_material_hourly_rate as cleaning_material_rate',
                    'fl.max_working_hour',
                )
                ->where(['deleted_at' => null])
                ->get();
            foreach ($response['frequency_list'] as $key => $frequency) {
                $response['frequency_list'][$key]->details[] = "Demo detail content 1 here";
                $response['frequency_list'][$key]->details[] = "Demo detail content 2 here";
            }
            return Response::json(array('result' => $response, 'debug' => $debug), 200, array(), customerResponseJsonConstants());
        } catch (\Exception $e) {
            return Response::json(array('result' => array('status' => 'failed', 'message' => $e->getMessage()), 'debug' => $debug), 200, array(), customerResponseJsonConstants());
        }
    }
    public function update_customer_data(Request $request)
    {
        $settings = Settings::first();
        $debug = toggleDebug(false); // pass boolean to overide default
        /************************************************************* */
        if (!$debug) {
            // live input
            $data = json_decode($request->getContent(), true);
        } else {
            // test input
            $data['params']['id'] = Config::get('values.debug_customer_id'); // match with middleware for testing
            $data['params']['name'] = "erer";
            $data['params']['image'] = "/9j/4AAQSkZJRgABAQEASABIAAD/2wBDAAMCAgICAgMCAgIDAwMDBAYEBAQEBAgGBgUGCQgKCgkICQkKDA8MCgsOCwkJDRENDg8QEBEQCgwSExIQEw8QEBD/2wBDAQMDAwQDBAgEBAgQCwkLEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBAQEBD/wAARCACAAIADASIAAhEBAxEB/8QAHQAAAQUBAQEBAAAAAAAAAAAACAQFBgcJAwECAP/EAD8QAAEDAgUCAwUFBgUEAwAAAAECAwQFEQAGBxIhEzEIIkEUUWFxgSMkMpGhFTM0QrHBFlJicuEJF4LRY/Dx/8QAGgEAAgMBAQAAAAAAAAAAAAAABAUBAgMABv/EADARAAEDBAECAwcDBQAAAAAAAAEAAhEDBCExEhNBBVFhIjJxgZGhsRTh8BUjQsHR/9oADAMBAAIRAxEAPwDQiqo+6/XCuAn7in5Y5VRH3Qj44VQE/cU/LBR2hwmuePTHyofc0/PCic2N2Plbd4qRiJXEJQQP2YcLaOn7q38sJlt2ppTbCmmrQxESpawkJFyT6DHEqIXksfarw708JTGb7DjASeLLx70HSic/krThLFZzHYiQ/Yuswj6ApH41/C4A9b4z/rviW13zbXzmGrah5uckKN07akqOy17ghlohCB8AMYuqtGESy3c7Jwtw8yTI6XHG3F28oHY98RmmOtKdc2uJUCfQ3xks54ntd8v0Jnq52qkqFPTZTsqQp9TZHcJUq+08dxid6T+LLPlMfTOTmlyqgHc9HlKCrj1BNgfrjE3QGxhbCycdHK00mJBeJ9DhOtu6TimtMfE/kXUsswFvinVgpAMZZ4WR32n1H64udp1LzQWCCFC9wb42a8PEhCvpupGHBKaSn7AD3E4aKm3Z9s+5wf1w+UkDpkD0UcNlWbIdHHZwf1xfss1JFo3Uv/xGIfU0eds2/nH9cTVKd1K/8RiI1VHKT/rGJOlylFTTeKrCmAPuSfljjUR90VhTAH3JPyxYqUgmtqKr482WYSMKpRSO+E0hexkKGIXFd5Cw3B7G1rk2wL3jO8TsXRjI4yzQ54/xRXW1Nx2kK80ZkghTyvcewSPr6YI6vT/ZMvuzXHdiGklxZ9Akcm/0GMMtadSqnqlqpmLOlTlLeZclluMlR4Q1uOxAHuCB+pxhWeW4GyibakHmToJvoVPzTnrMD8XLVCl1Wc7d990eZRKjfcpR9/x7+gxPqZoDqOt5ciqUN1DMfzuoU0TYDkjBK+EDI8fL+T4lSfiJE6r/AHx9ZTz5uUp+QTYYLinU6LIZLamUkKFlXSOcIKl87mQzS9Uzwxgph1Q5WS+dqg+llvLsaA61HS5vUXEWKXO3l9wHuxG6DUn6fVRBqiOm8P3MpsbUuD0Cx6fP340P8VmhtIiZLl54p9Mjn2NPUWAgAg+/5YBmrU2juyYtakK2spWtpZQPVQsB8iQME0K5qNIcEJc2wokFrpBUvodQkw3malEeWy82oOIWk2UhQPCgR25wePhz1sezpTkUqoy2hUWEhLqVdlkC24fA/ocZ1wprlKgMpKrEcJKjwSDa1/iP6YtLTrOk3L0uLmemdRtTC09ZpJsbHhQ49LYs2o6i6RpY1KLbhkHa1Qy88p5tZWkBQVyL35xxrSbLPwUD+uI9pnmP9qUpqYUhSJSEvNrSeFJUAR9bYklYKV7ik+44bsdybK8+9vF0KRxhupY/2Yi1Wb4v8RiVQPNTR/sxG6snyE/HF+yon6eLxV4UQR9zT8scZvMZeO8HiIgfDFjtSkU8kKxwkfwwwoqI8wwhnObIoF7X9fdiFx2hg8c3iCGmOnLmUaMla6xmKK8ykpPEZg+Raz/qJVtSPgfdjKChwocysxoE+c3EZcePVfcBUlJPK1H3gJSR9cG1/wBTGiSm80ZfzTFmJcZlxDCEcrG5pSVbt9u+0lQ5ta4t64Gjw8aaM6l54k0mYrbFp7KVHd2KlD/g4WXVUMLnHsndhR6nFjRtFZoZr5lqPOi5Or0VEWW2lAZdYCui62R5F7VALQCLdxb44LCLmSlwEokTZbLDC+d7iwlJ+pwJmY9FKTLq8JFNhttVqSuOwiQ1bqEN2CBcjygAC5HNh3wUuqumUn/tUxSaWQqfHiIHULIcKl28yrHt+uEbmtceVLS9O0loDK2yU850n5Iz/kGsZdiZkpMhcmKtst+1tk3IsPXvfGQ1UhTqPUapQajdKYM4iyjxdLl/7WwdWRclar6e5gqFbyyzDrmWiywVQp7KDKWqw63mIFrHcbXsRYWwGGt1QanZ1zPPhU5EEOSeoGEp2pQCs3FvT1/PBtGA+B3CXXTf7c+R77X1WIzshlMZtsL3NWHzAuD/AFGPKHnR2mdCC4grTfpvpCrqQf8A12OO1DrsaoZYYnEbZURwMvpI52nsr5dvy+eIpVYlO/xKzUXrJSpxsubP5rcKPyIIP1wQ2HAgoNwLHAhadeFjU+BmfLTdGjykolU1pCHWOyxxYLA9Qff8bYIV6UH0nkHy889jjKTRXVFOTtQ6dVIshTLSXEsygnncwpXNwPVP/rGnFHrcKoRWZ0OSl2LLaQ8y8CShaFAFKgfkf6YLtakt4nsld9Q4u5jRVpUo7qan/ZiP1VPkVh8oagumpsbjbhnqibpWMHdktTvL/hl4UQv4RPywnlG8deO8P+ET8sWO1KSVHuMRrOlTaouXplWdN0Q463lDvfaL2/TEkqHpiv8AV2uU6g5MmzKmre0pvppZAup1Z7ISPUnt+vpijzxaSrtbLgFk/wCIHxDSNSX5cb2Hb15XVkPvG7iim4Qgf5UIBICR8SeScO3gyqtPYzpmGxAW5EirQn4jeD/bFQapQqNL1Eq6KE6lMFElR6YXuDS1G5Ru/mtwL+vOJD4ep0fK+rEimvyLOyIJAKTwTf8AtfCK5aXUHL09i4Mrs8v2haHaWqo1T1HTNrE1pr2JsutpcUALeqvpgkqtmCEqKh5h5mQw6AEKbIUkp+mAh0zzLWW5r37dyn+0uivpiVDWEuJ5uFbFG5vx2uOcFIM1wX6AlqJlGsIUlIKWm4NgB3JNuw+JwtolzWEBeivLN8tquGPkpPNapkSizaiW2mwlhalenABxj1q66mdmfNFQjtC0h99aPhffbGo2p2YTEyHJLLhQiRHJ5NiE25BxlrmqtUddVqUaRJQCouA3/nuSePeb3xrbP5VceSXXLAKBJOZTXlcxn8rxpDSNshd23ATYFJI4P5HDVmIMJqsNS0r9lcbSSEdxcEG3vtxxhIw9JZyvCapzxUtxYXzx5bkH62P64UswnKxTUQ6gW2i2d0d5R/CvuMMWCDKVVM4TvQq9RcrVFmRLS3NLJG1TardVo/1Hw7j340V0B1zyrqFRW41LaRTZMJIYNJU7ushKRZTYNvJaw+BH1xlmlNJy1UYsaop9qe6JCFbrpQSb2PvPOLd0dzVXcuV2kVGmbkoblpClgXWoFQ4CT+IlNxb1vjWem6Qh3M6zeJW1OSJiZtEbWlRBCbEHuMcakPxj54hWgGe6BnegrqVCfk9FClMuIfQUlK0nlJv7uR9MTeokK3kHDJp5NBSJ7S10FOEr9wv5YURP4VPywmkq+wVhTFP3ZPyxqdrklnkW5wAfjZ1P1EzdmataW6ao9nj5RiIfqUlBR1ZEh9lSiwlSlpDYSybmwUolR4FsHbmmsU+gUebXKtLTGg09hcmS8rs22hJUpX5A4yW8Qud6VmXWytaj6SVVUmgV2At2fIkQVtoMhKNqko3gbl/hIUB5b4FuX8WwjLWmXPlDdT6NPckIRUHENKaUpyQVKv8AaE2tcdyP7YYYmYZ+Ts+QMyhReER8FW3kPNX89vmkn5HEq6bzjp9tdDDKL8kfiJN+ffiJZlix402NETIcMNS9r8sN+RtThuRf4AEn5HALTywe6aOHAcpghasaFu6f6hZegVqBU2Xm3m0lDrKxe/uPuI9RghAql5fpamRUWm2Up5UohIt8TjHbw6RqplPUOq0unVl4NxygFUWSS04Co2WCk2Nxbn44PGgVGRVYjbc6TIeV/wDItSr/AJ4TV2C3cWNyn9Kq67phzsenZfOv+fzVaVIouX3F+zlJS5I7C3qE+/54zLz91H86Px07koSCLA97cn9bY0U1NhA0xwNotZBucAFqRSXKfmJdSUChCipKle4H1xPh9SHkFV8QpzSBb2SnJ1ahClexOArWylTIBQVXP09Ox/8AzHFtNSeccjVJ1tDRVuU4hVxxyLfH++GvL0aWhsxoyrNvICeo2bnfc3UD7xwflj2TEq0KCpE0dRBRvNl+fduBJt6euG7YBwkz5xK+WUUKZOUqsyVITYrbUkdx6/L/AIxIsnVuRGr0JqbUlLi9QKisoVe6gRZRPoPUet8Io9Jy6/TWVTYqj1U7BdRAv8xhspzBoFYSotq6boG1LiSU8G4/I+vwxd0EQsmy0rQbLPjiVptQYFGyxp1T48GMPtn5b6lKKj+JexAHr8ScFhpPrnTtUo/sU6JHp1Y9nElDLMgOsyWSP3jSu9veDyPjjH2uKrMqheWSqS4vasIbVcISO5J92Ll8MGudFy9LpkCrNOLrlAkXpziX9vXjK/E2L8FSebD1Fh6YKsiXgtefgl3iFIUyHMHxWvcg/YqwqjfwyflhHIP2Jwrjn7un5YMO0CmDOgQvLtSS4wy+DFc+zfRubUdptuHqL9xjLXxWHJ1Oq1OzZQ4c1qnvRnGanHQ2tlU54JStCnAAEJQVDaNo7JJPpjTHVOr1OmZbqTtHiOSpTMJ58Mo/msk23H0HftyewxnxqzlKjRdNKi/WMx1DOOY5cF96i06KT7LCS2bOSnAPwBIunzG5PYd8L7syQ0JjZgD2ig3frbuYJjcsxG5inrr6TXCEqsDxbsBa2IhnEQgqOmHGciuOjqPtKJKgq54+g4+uLBzAxSqRNhJyspbqWGGWpDtwS7MWNzhQP8gJsL+6/rhph0SfUK9HiTG2zLR1A686pISm6h51qPr5rAdySBgdmDITMtLhxKlfhWpj79Wq0pLS9kWM0SVevnODo0/lNSIXXI4B2A+8+uKGyDk+k6c0GYxBlok1CrFtorHayblVvgCcXLkKnVSTT2IqHER4zPZSjyo+pwnuj1qnIJ3b0zRohjlJc1UxuqxFtBAVfsMCprJpWp9p7a1cOXA44+WC/aqmVI9Gcqn+IokqOw77OpbCw4C5cjaNt7m4Iv2uCO+KhrdRr+oCKoWskyqTQoz5gtVh5p2UmVIXwylpphCleY8nda3F/XE0LGu53Jg1/NbW3XYWBrwSD3gx9dfdAXAcm5efk0htfQdYc84cH7u3Fx8bHHedVFszl1KMHpLrLP3hSGztIJ7n0H/OLzzb4WtV5TX+K3KGEsFpTqltqS46FBSrNqSgqBUlCdx5HqLA2BpGsuSqR1aZJjBh4WQ6UptuHv8AiPX8sPOi+mA542vP1YDnMacgrrRFPVN9yo9VAjuAgxyQLWA85HYHH6qRpU0+yPTDJaG0JITtDRPPBHx/qcIMtw0R0OsTJBZTIIG/fz77H54lseJFjRpLNKmFalBKSs9zfvt/+3xnI7KvE7K55cqpp0IQi0CybhSgTdQHfn6Yh+YZFPiIcbiw3xIUARIU7uAFri1h3sLd/jifaeiVl/MLdQk0dNVTJ3xkwHQoqueCpO29zb+uJZq7og41p6jVqj0h9ukKUY0+GyNhhO7lALJ58pPBHFjb07TSMPWVwZZC2zfP2RwpbQpTCdrik8dxb++Ej5+ywpK3UQlLZSFOBBKQexNuMNTtIVFNQZIo2WZs9NRjxENJK5D8kgJKCLKuokAHng+nHGM+/FTnjLUenV2oZZq/Rk1uhxKex7K2UJfQl5Sn7KNiryK2qUOCTb0weGeqBNr9NKavUUOUyMtE51gM7FOLaO9KSq5GwEA2tc2AvjMjV6hZ71O1nrCp7q3X2FKYlOIb2tQWEAHpISocbUnan/Mo3vgG45OcGgbTOyYDJJ0h/oVIlV9Ds6G2hhkBMZguqG1byjtF7n+Xk4sBUWqZUy3GjU+kw6tVa5VGmYvWNylDCL9Q+W6WwSFFVxwQe1jiw9NstaavVCe5Ep82S3TXGokam3IdbmuD7NCln7MuKUo7hc8k9gMTLUPJmSI+W6tUjVqi1UUtrpcye86fu5cI6wZdV5VN3G0rAI7jjBFO1FJnVq6APfcTr+flEG6dUqdG1JLpbkAQ3kBsnBMnQOJgjBTVSaPUlSGH6fQnZkWfFjexOwSXk+0KueltH4VLSN1ySlIKbkeijJrmqDeVsyxtZaD+wl5bZTNjVeMtIUwFFLiEjpkpWbBAJ7D+YG+HXSR/J2UcjTKxJz1Op9BkJTGjMPxipEdhIDbkiOW0Fyyl7VEn+XcTa+Je/qPlDUzKsyju06royrDU5T4FSkOKSazFDaepKsobz2WAu1ze457EMo0KYNaq0ZgiMfCIzPoPgquq3FwxtC1eZbIMjkSIHIuJgQYgEgAe9GYVc6WaM5zzbn6vTstZ4RVcpu7J8yShIdQ+65ZO1DhCW95VdAsLWKrJ45uyJl3VPVBtORtOsvU6jIprkqGaq848w3TChfT67A6h9rdPnG8ISm+4k2VgbdHqZnjNmpooOXqNOg02LIRGpFMizlx4+9XKHSEgJcb2J3lR7AC1zbBx551E0Q0Llw6pniszm52WWFuMwW2XHFvOvlO4suBPIVcKCFL2pClEp4xnZ0G1WdRvvE/NOL2/faVXW9SAxrcECRMD2QJxvcD08i30vT/NNHyn/wBs61ntVVp8yW9TBV1q9lnMS1NuuMuOuo6l1hwIQE2b4CRzcginqzpZRs00OvPVGjRY2Z8o1J6kVN2I2romQ2jclYQedjgCiU8kG5TcA2lnhlp2YM7a50nWKtUmu1nLOZqhKk1RHQeksNSwVBiRIt5VFCloHayb7gLXwadU0xoGbWMyoqeRITEqoK6ipKoy2lplR0KbZdSV3Su6VK2qABBJuOb4YVrYU3BjzII8ki/qP623dVpsDS0iTyOBmT69zmYICw5nTlVeChx+C1EiIBCV7fO25flO71xIKbHiyVKcTNSoxmQGQAU3IHB4+PrjrqblE5V1Nr2SKs/sNPqC2wlSdlwTuBA7C4UDjrANKjymY6WC8gJIQ7faon4j3Y8w88TBTbhGJlSXIzFaj1pqfTEtxZCkKdaFi6ncrhW2/ZXe3uODk0rpCplAbosjpyIK0GLPhvpC0m4uQUWsO5H1wFWVnJaiJbTToUwUqjFkkbl7wCD8CO/HfB+aQQlrjNVBCUsollLrraSSoqAA85PN7AD6dsXpe0UBdjiIRYPmyPrhc3bojDe+fsx88Lm/3I+WGx2kaYs3S49OokyoS3GksRmlOOB38KgB+H69h88Z0mBm2NnSZXszPJ9urda6rNODpDL0axSA6pKTzYki/ZKOOQoghfEVrzWXM0o07yRlqZUWY0gtTp3kTH3t7eslN/M4W96U2SPxm1+DigMs0Ku6iZ1zFmVusQ4NCSz+yJDrrm9qRLHlUraSCClKukkbrHz8ckHPkx3sxJ+0f6/b1TW3tq9MivIDdDMO5GCIBBBMfITOwvWKNl7I1WhUGDVvZqnSX3swFht0feXlE3CGCbWWVlKEkghKSoHgHEUy2iJqtq2mh53SAnL5Zkw40RlaI4kOlDiiopN1qSrdye6QTbviV5kyrBo2cctZcqedFS4KHnZDcGI22DJDTCw6uS+lW7othCABYDz7eb3w4ozdlyBmunZxfp8GmSn6wzluNT0pUxJmO7bGWluyRtSH1WKz5gQQRcY5pfTrCm8iJ8sbgfL/AIjn8bqyNzRa6QADDoMFsuOYHKMSYguMellZmqbml0Oo1KbBlmguNrY9jplLQ8sG1i6pN+opO4kqsNg3NjuLigs154yHR6xEoGVU12oxKfCiyGI066Y0Vkm13klO9IUlKW0trG3con3HFhZkg1Gk+IfJEReZJTYzhTpsJSZyBGNMO5IW42nde7gRtSVAXuNvIuId4ocwU9miR8kZcEagy5ldbp015+L99cZYbHQeXYFxQXvWpCFAbtqVA3wV1KdWoOP+JnezsfZUfa3FG1f1eLeTYwDLW5afKTy7mdZGlIfD9p1UaTLzbqhmV6ttPS4Lc3LtRp6EvJgSEBaSlaQdqUjfcJXdHTKr2JGL7gaXq1N02VlXNU6HmB2uMdPNUuNKC33agFpT1FEWCVJCE7RtBTsSLd8TzSjImVqJp9TdPnZaak27AaiSFPthpL7imUrWi3ClLKFbrgWHrY8YGvxFaL5tyqxTs0ZYzlHjP5cbCHmmQ4lieyxI6qHXgDsukuebqBRVtuCSojFq9BlNr3UgREHHnOu2p+yp4Z4jcOfb292Wk1C5o5Ay5vH35ySSG+Y3M7ixMy52keGrKeXclaQZRqWaYNPluxWUie84qFJsRd0cl1pRJ4G1KNpsRcAWZW9U9T5EpmgQ40Nx5ENiVVHKOg1B9pp0Eo6TfBBG0oJUO5BAIPFB5C1Qz9nTIWY2qblOoy8zUuG90Z7MNCqVMBeCnFtLSC4pZZCtu7y3Che9hi5VSNPNJtQGqLT6Y5libnD9nNQEIjOpE11ptaVpC9pQEpGwq5B4ueCMQ2pXrgVXs3nynX0+oW7/AA/w20P6OlVlzMAuzEkl24DpnHsuz3xnMjxZUGZRdd11NxhbseoRGZt3m1IO/ctKmyk+re0INuPJx3xDIb7r0f2liGhx1I3KUnzHbfzW/IH88E3/ANQKnZeque6ZmGl1CTvbROiLZeasyhfVQ+lSDwNikPoV68kn1wM2Vm5UKZOcpslLTiFMoQFcBTa023EH/UFAj4jCW9E1HFaUppHg7v8AX8D8Kw8l1OKjY3UnTscUC2pItZYIPBHIHAv/AM4P/SFaJlHhrVt6vTCXFA33W7E29cAFTcrfs+oxXqkp0x23ksyfZxZKEqUE3SLcm5F/fg/tE6Smk0uO0Um+0hJWfMpA4SVeu63fGVsTJQ1+BAX/2Q==";
            $data['params']['dob'] = "20/05/2001";
            $data['params']['gender_id'] = 1;
            $data['params']['email'] = "k@gmail.com";
            $data['params']['country_code'] = '+971';
            $data['params']['mobilenumber'] = 979797979;
        }
        /************************************************************* */
        $fields['email'] = 'required|email:rfc,dns';
        $names['email'] = 'Email';
        $fields['country_code'] = 'nullable|string';
        $names['country_code'] = 'Country Code';
        /************************************************************* */
        // required input check
        $input = @$data['params'];
        // single api multi field update, so create dynamic validation fields
        if (isset($input['name'])) {
            $input['name'] = trim($input['name']);
            $fields['name'] = 'required|string|min:3';
            $names['name'] = 'Name';
        }
        if (isset($input['image'])) {
            $fields['image'] = 'required|string';
            $names['image'] = 'Profile Image';
        }
        $fields['dob'] = 'nullable|date_format:d/m/Y';
        $names['dob'] = 'Date of Birth';
        $fields['gender_id'] = 'nullable|integer';
        $names['gender_id'] = 'Gender';
        if (isset($input['mobilenumber'])) {
            $input['mobilenumber'] = substr(@$input['mobilenumber'], -9); // get last 9 digits only
            $fields['mobilenumber'] = 'required|numeric|digits:9';
            $names['mobilenumber'] = 'Mobile Number';
        }
        if (!$fields) {
            return Response::json(array('result' => array('status' => 'failed', 'message' => 'No data found to update.'), 'debug' => $debug), 200, array(), customerResponseJsonConstants());
        }
        $validator = Validator::make(
            (array) $input,
            $fields,
            [],
            $names
        );
        //dd($names);
        if ($validator->fails()) {
            return Response::json(array('result' => array('status' => 'failed', 'message' => $validator->errors()->first()), 'debug' => $debug), 200, array(), customerResponseJsonConstants());
        }
        /************************************************************* */
        // format available data
        if (isset($input['dob'])) {
            $input['dob'] = Carbon::createFromFormat('d/m/Y', $input['dob'])->format('Y-m-d');
        }
        /************************************************************* */
        $response['check_otp'] = false;
        $customer = Customer::where(['customer_id' => $input['id']])->first();
        DB::beginTransaction();
        try {
            $response['status'] = 'success';
            if (@$input['name']) {
                $customer->customer_name = $input['name'];
                $response['message'] = 'Name updated successfully.';
            }
            if (@$input['image']) {
                // image upload function here
                $image = $input['image'];
                $image = str_replace('data:image/png;base64,', '', $image);
                $image = str_replace(' ', '+', $image);
                $imageName = "customer-avatar-" . $input['id'] . "-" . time() . ".png";
                //dd(base64_decode($image));
                $path = Config::get('values.customer_avatar_storage_path') . $imageName;
                //dd(File::put($path, base64_decode($image)));
                File::put($path, base64_decode($image));
                // resize
                $image = Image::make($path)->resize(250, 250);
                $image->save($path);
                /******************* */
                $customer->customer_photo_file = File::exists($path) ? $imageName : null;
                $response['message'] = 'Profile image updated successfully.';
            }
            if (@$input['mobilenumber']) {
                $response['message'] = 'Profile updated successfully.';
                if ($input['mobilenumber'] != substr($customer->mobile_number_1, -9)) {
                    // user changed mobile number in UI
                    $new_mobile_exist = Customer::where('mobile_number_1', 'like', '%' . $input['mobilenumber'])->first();
                    if ($new_mobile_exist) {
                        // duplicate mobile number
                        throw new \ErrorException('Mobile number already exist.');
                    } else {
                        // insert temp data
                        $customer_temp = CustomersTemp::where(['customer_id' => $input['id']])->first() ?: new CustomersTemp();
                        $customer_temp->customer_id = $input['id'];
                        $customer_temp->mobile_number_1_country_code = @$input['country_code'];
                        $customer_temp->mobile_number_1 = $input['mobilenumber'];
                        if ($debug || in_array($input['mobilenumber'], debug_mobiles()) || strpos($_SERVER['REQUEST_URI'], 'demo') !== false) {
                            $otp = '1234'; // debug /test otp
                        } else {
                            $otp = mt_rand(1000, 9999);
                            send_customer_login_otp($input['mobilenumber'], $otp);
                        }
                        $customer_temp->mobile_number_1_otp = $otp;
                        $customer_temp->mobile_number_1_otp_expired_at = Carbon::now()->addSeconds(Config::get('values.customer_login_otp_expire_seconds'));
                        $customer_temp->save();
                        // need verification for new mobile number
                        $response['check_otp'] = true;
                        $response['message'] = 'Enter the OTP to update mobile number.';
                    }
                }
            }
            $customer->dob = @$input['dob'];
            $customer->gender_id = @$input['gender_id'];
            if (@$input['email']) {
                /******************************************************* */
                // validate with db
                $email_exist = Customer::where([['customer_id', '!=', $input['id']], ['email_address', '=', $input['email']]])->first();
                if ($email_exist) {
                    throw new \ErrorException('Email address already exist.');
                }
                /******************************************************* */
                $customer->email_address = $input['email'];
            }
            //$customer->oauth_token = Str::random(40);
            $customer->save();
            DB::commit();
            $response['UserDetails']['id'] = $customer->customer_id;
            $response['UserDetails']['dob'] = $customer->dob;
            $response['UserDetails']['gender_id'] = $customer->gender_id;
            $response['UserDetails']['email'] = $customer->email_address;
            $response['UserDetails']['UserName'] = $customer->customer_name;
            $response['UserDetails']['token'] = $customer->oauth_token;
            $response['UserDetails']['image'] = Config::get('values.customer_avatar_prefix_url') . (($customer->customer_photo_file != '') ? ($customer->customer_photo_file . '?v=' . $settings->customer_app_img_version) : (Config::get('values.customer_avatar_default_file') . '?v=' . $settings->customer_app_img_version));
            $response['UserDetails']['country_code'] = '+971';
            //$response['UserDetails']['mobile'] = $customer->mobile_number_1;
            $response['UserDetails']['current_mobile'] = $customer->mobile_number_1;
            $response['UserDetails']['mobile'] = $input['mobilenumber'];
            return Response::json(array('result' => $response, 'debug' => $debug), 200, array(), JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES);
        } catch (\Exception $e) {
            DB::rollback();
            return Response::json(array('result' => array('status' => 'failed', 'message' => $e->getMessage()), 'debug' => $debug), 200, array(), customerResponseJsonConstants());
        }
    }
    public function update_avatar(Request $request)
    {
        try {
            DB::beginTransaction();
            $settings = Settings::first();
            $data = json_decode($request->getContent(), true);
            /************************************************************* */
            // required input check
            $input = @$data['params'];
            // single api multi field update, so create dynamic validation fields
            $fields['image'] = 'required|string';
            $names['image'] = 'Profile Image';
            $validator = Validator::make(
                (array) $input,
                $fields,
                [],
                $names
            );
            //dd($names);
            if ($validator->fails()) {
                return Response::json(array('result' => array('status' => 'failed', 'message' => $validator->errors()->first())), 200, array(), customerResponseJsonConstants());
            }
            /************************************************************* */
            $customer = Customer::where(['customer_id' => $input['id']])->first();
            $response['status'] = 'success';
            // image upload function here
            $image = $input['image'];
            $image = str_replace('data:image/png;base64,', '', $image);
            $image = str_replace(' ', '+', $image);
            $imageName = "customer-avatar-" . $input['id'] . "-" . time() . ".png";
            //dd(base64_decode($image));
            $path = Config::get('values.customer_avatar_storage_path') . $imageName;
            //dd(File::put($path, base64_decode($image)));
            File::put($path, base64_decode($image));
            // resize
            $image = Image::make($path)->resize(250, 250);
            $image->save($path);
            /******************* */
            $customer->customer_photo_file = File::exists($path) ? $imageName : null;
            $response['message'] = 'Profile image updated successfully.';
            //$customer->oauth_token = Str::random(40);
            $customer->save();
            DB::commit();
            $response['UserDetails']['id'] = $customer->customer_id;
            $response['UserDetails']['dob'] = $customer->dob;
            $response['UserDetails']['gender_id'] = $customer->gender_id;
            $response['UserDetails']['email'] = $customer->email_address;
            $response['UserDetails']['UserName'] = $customer->customer_name;
            $response['UserDetails']['token'] = $customer->oauth_token;
            $response['UserDetails']['image'] = Config::get('values.customer_avatar_prefix_url') . (($customer->customer_photo_file != '') ? ($customer->customer_photo_file . '?v=' . $settings->customer_app_img_version) : (Config::get('values.customer_avatar_default_file') . '?v=' . $settings->customer_app_img_version));
            $response['UserDetails']['country_code'] = '+971';
            $response['UserDetails']['mobile'] = $customer->mobile_number_1;
            return Response::json(array('result' => $response), 200, array(), JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES);
        } catch (\Exception $e) {
            DB::rollback();
            return Response::json(array('result' => array('status' => 'failed', 'message' => $e->getMessage())), 200, array(), customerResponseJsonConstants());
        }
    }
    public function customer_soa(Request $request)
    {
        $debug = toggleDebug(); // pass boolean to overide default
        /************************************************************* */
        if (!$debug) {
            // live input
            $data = json_decode($request->getContent(), true);
        } else {
            // test input
            $data['params']['id'] = 1; // match with middleware for testing
        }
        /************************************************************* */
        $response['status'] = 'success';
        $response['soa_list'] = array('pending' => [], 'paid' => [], 'cancelled' => []);
        return Response::json(array('result' => $response, 'debug' => $debug), 200, array(), customerResponseJsonConstants());
    }
    public function contact_send(Request $request)
    {
        $data = json_decode($request->getContent(), true);
        /*$data['params']['id'] = Config::get('values.debug_customer_id');
        $data['params']['email'] = "samnad.s@azinova.info";
        $data['params']['body'] = "nb fg dgf hfh fh hfhfh";*/
        // required input check
        $input = @$data['params'];
        $validator = Validator::make(
            (array) $input,
            [
                'email' => 'required|email:rfc,dns',
                'body' => 'required|string|min:10',
            ],
            [],
            [
                'email' => 'Email Address',
                'body' => 'Description',
            ]
        );
        if ($validator->fails()) {
            return Response::json(array('result' => array('status' => 'failed', 'message' => $validator->errors()->first())), 200, array(), customerResponseJsonConstants());
        }
        /************************************************************* */
        try {
            $customer = Customer::where('customer_id', $input['id'])->first();
            Mail::send('emails.to_admin.customer_app_contact', ['body' => $input['body'], 'customer' => $customer], function ($m) use ($input, $customer) {
                $m->from($input['email'], $customer->customer_name);
                $m->to(Config::get('mail.mail_to_admin_address'));
                $m->subject(Config::get('values.company_name') . ' App Enquiry');
            });
        } catch (\Exception $e) {
            return Response::json(array('result' => array('status' => 'failed', 'message' => $e->getMessage())), 200, array(), customerResponseJsonConstants());
        }
        /************************************************************* */
        $response['status'] = 'success';
        $response['message'] = 'Mail send successfully.';
        return Response::json(array('result' => $response), 200, array(), customerResponseJsonConstants());
    }
    public function deleteAccount(Request $request)
    {
        $debug = toggleDebug(); // pass boolean to overide default
        /************************************************************* */
        if (!$debug) {
            // live input
            $data = json_decode($request->getContent(), true);
        } else {
            // test input
            $data['params']['id'] = Config::get('values.debug_customer_id');
        }
        /************************************************************* */
        // required input check
        $input = @$data['params'];
        $validator = Validator::make(
            (array) $input,
            [
            ],
            [],
            [
            ]
        );
        if ($validator->fails()) {
            return Response::json(array('result' => array('status' => 'failed', 'message' => $validator->errors()->first()), 'debug' => $debug), 200, array(), customerResponseJsonConstants());
        }
        /************************************************************* */
        $upcoming_booking_ids = DB::table('bookings as b')
            ->select(
                DB::raw('IFNULL(st.service_type_name,"Unknown") as service'),
                'b.booking_id',
                'b.booking_common_id',
                'b.booking_status',
                DB::raw('CONCAT((CASE WHEN b.reference_id = "" THEN b.booking_id ELSE b.reference_id END),(CASE WHEN b.booking_common_id IS NULL THEN "" ELSE CONCAT("","","") END)) as booking_reference'),
                DB::raw('null as schedule_reference'),
                'm.maid_id',
                'm.maid_name',
                DB::raw('(CASE WHEN b.cleaning_material = "Y" THEN "yes" ELSE "no" END) as cleaning_materials'),
                DB::raw('DATE_FORMAT(SEC_TO_TIME(TIME_TO_SEC(TIMEDIFF(time_to,b.time_from))),"%H:%i") as hours'),
                'f.name as frequency',
                'b.booking_type',
                'b.customer_address_id as address_id',
                'ca.customer_address as address',
                DB::raw('"' . Config::get('values.currency_code') . '" as currency'),
                DB::raw('(CASE WHEN b.payment_type_id IS NULL THEN b.pay_by ELSE pt.name END) as payment_method'),
                'b.total_amount as total',
                'pt.charge as payment_type_charge',
                DB::raw('DATE_FORMAT(b.time_from,"%H:%i") as start_time'),
                DB::raw('DATE_FORMAT(b.time_to,"%H:%i") as end_time'),
                'c.customer_name',
                'b.service_week_day',
                DB::raw('DAYNAME(b.service_start_date) as service_week_day_name'),
                'b.service_start_date',
                'b.service_end',
                'b.service_end_date',
                'b.service_actual_end_date',
            )
            ->leftJoin('service_types as st', 'b.service_type_id', 'st.service_type_id')
            ->leftJoin('maids as m', 'b.maid_id', 'm.maid_id')
            ->leftJoin('customers as c', 'b.customer_id', 'c.customer_id')
            ->leftJoin('frequencies as f', 'b.booking_type', 'f.code')
            ->leftJoin('payment_types as pt', 'b.payment_type_id', 'pt.id')
            ->leftJoin('customer_addresses as ca', 'b.customer_address_id', 'ca.customer_address_id')
            ->where([['b.customer_id', '=', $input['id']], ['b.booking_status', '!=', 2]])
            ->orderBy('b.booking_status', 'ASC')
            ->orderBy('b.booked_datetime', 'DESC')
            ->orderBy('b.booking_id', 'DESC');
        $today = date('Y-m-d');
        $today_time = date('H:i:s');
        $upcoming_booking_ids->where(function ($query) use ($today, $today_time) {
            $query->where([['b.service_end', '=', 1], ['b.service_actual_end_date', '>=', $today]]);
            $query->orWhere([['b.service_end', '=', 0]]);
        });
        $upcoming_booking_ids->where(function ($query) {
            $query->whereColumn('b.booking_common_id', 'b.booking_id'); // two columns same
            $query->orWhereNull('b.booking_common_id');
        });
        $upcoming_booking_ids = $upcoming_booking_ids->first();
        $ongoing_bookings = DB::table('day_services as ds')
            ->select(
                'm.maid_id',
                'm.maid_name',
                DB::raw('IFNULL(st.service_type_name,"Unknown") as service'),
                'b.booking_id',
                'b.booking_common_id',
                'b.booking_status',
                DB::raw('(CASE WHEN b.reference_id = "" THEN b.booking_id ELSE b.reference_id END) as booking_reference'),
                'ds.day_service_id as schedule_reference',
                DB::raw('"service_details" as service_details'),
                'ds.service_date as date',
                DB::raw('(CASE WHEN b.cleaning_material = "Y" THEN "yes" ELSE "no" END) as cleaning_materials'),
                DB::raw('DATE_FORMAT(SEC_TO_TIME(TIME_TO_SEC(TIMEDIFF(b.time_to,b.time_from))),"%H:%i") as hours'),
                'f.name as frequency',
                'b.customer_id as customer_id',
                'b.customer_address_id as address_id',
                'ca.customer_address as address',
                DB::raw('"' . Config::get('values.currency_code') . '" as currency'),
                DB::raw('(CASE WHEN b.payment_type_id IS NULL THEN b.pay_by ELSE pt.name END) as payment_method'),
                'b.total_amount as total',
                'pt.charge as payment_type_charge',
                DB::raw('DATE_FORMAT(ds.start_time,"%H:%i") as start_time'),
            )
            ->leftJoin('bookings as b', 'ds.booking_id', 'b.booking_id')
            ->leftJoin('service_types as st', 'b.service_type_id', 'st.service_type_id')
            ->leftJoin('maids as m', 'b.maid_id', 'm.maid_id')
            ->leftJoin('frequencies as f', 'b.booking_type', 'f.code')
            ->leftJoin('customer_addresses as ca', 'b.customer_address_id', 'ca.customer_address_id')
            ->leftJoin('payment_types as pt', 'b.payment_type_id', 'pt.id')
            ->where([['b.customer_id', '=', $input['id']], ['ds.service_status', '=', 1]])
            ->orderBy('ds.service_date', 'DESC')
            ->orderBy('ds.start_time', 'DESC');
        $ongoing_bookings->where(function ($query) {
            $query->whereColumn('b.booking_common_id', 'b.booking_id'); // two columns same
            $query->orWhereNull('b.booking_common_id');
        });
        $ongoing_bookings = $ongoing_bookings->first();
        if ($upcoming_booking_ids || $ongoing_bookings) {
            return Response::json(array('result' => array('status' => 'failed', 'message' => 'Active or pending schedules found, please contact us.'), 'debug' => $debug), 200, array(), customerResponseJsonConstants());
        }
        DB::beginTransaction();
        try {
            $customer = Customer::where([['customer_id', '=', $input['id']]])->first();
            $customer->customer_name = null;
            $customer->customer_photo_file = null;
            $customer->customer_last_modified_datetime = Carbon::now();
            $customer->oauth_token = null;
            $customer->mobile_verification_code = null;
            $customer->login_otp_expired_at = null;
            $customer->save();
            $response['status'] = 'success';
            $response['message'] = 'Account deleted successfully.';
            /********************************************************************************************* */
            DB::commit();
            return Response::json(array('result' => $response, 'debug' => $debug), 200, array(), customerResponseJsonConstants());
        } catch (\Exception $e) {
            DB::rollback();
            return Response::json(array('result' => array('status' => 'failed', 'message' => $e->getMessage()), 'debug' => $debug), 200, array(), customerResponseJsonConstants());
        }
    }
    public function customer_data(Request $request)
    {
        $debug = toggleDebug(); // pass boolean to overide default
        /************************************************************* */
        if (!$debug) {
            // live input
            $data = json_decode($request->getContent(), true);
        } else {
            // test input
            $data['params']['id'] = Config::get('values.debug_customer_id');
        }
        /************************************************************* */
        // required input check
        $input = @$data['params'];
        $validator = Validator::make(
            (array) $input,
            [
            ],
            [],
            [
            ]
        );
        if ($validator->fails()) {
            return Response::json(array('result' => array('status' => 'failed', 'message' => $validator->errors()->first()), 'debug' => $debug), 200, array(), customerResponseJsonConstants());
        }
        /************************************************************* */
        $bookings_count = Booking::select(
            'booking_type',
            DB::raw('COUNT(booking_type) as count')
        )
            ->where([['customer_id', '=', $input['id']], ['booking_status', '!=', 2]])
            ->where(function ($query) {
                $query->whereColumn('booking_common_id', 'booking_id'); // two columns same
                $query->orWhereNull('booking_common_id');
            })
            ->groupBy('booking_type')
            ->get();
        $response['count']['bookings']['OD'] = 0;
        $response['count']['bookings']['WE'] = 0;
        $response['count']['bookings']['BW'] = 0;
        $response['count']['bookings']['total'] = 0;
        foreach ($bookings_count as $key => $booking_count) {
            $response['count']['bookings'][$booking_count->booking_type] = $booking_count->count;
            $response['count']['bookings']['total'] += $booking_count->count;
        }
        /************************************************************* */
        $response['status'] = 'success';
        $response['message'] = 'Customer data fetched suceessfully!';
        return Response::json(array('result' => $response, 'debug' => $debug), 200, array(), JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES);
    }
    public function validate_token(Request $request)
    {
        $debug = toggleDebug(); // pass boolean to overide default
        /************************************************************* */
        if (!$debug) {
            // live input
            $data = json_decode($request->getContent(), true);
        } else {
            // test input
            $data['params']['id'] = Config::get('values.debug_customer_id');
        }
        /************************************************************* */
        $input = @$data['params'];
        /************************************************************* */
        $response['status'] = 'success';
        $response['message'] = 'You\'ve a healthy token!';
        $response['customer'] = Customer::find($input['id']);
        return Response::json(array('result' => $response, 'debug' => $debug), 200, array(), customerResponseJsonConstants());
    }
}