<?php

namespace App\Http\Controllers;

use App\Models\Booking;
use Carbon\Carbon;
use Config;
use App\Models\Customer;
use App\Models\CustomerAddress;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Response;
use stdClass;

class CustomerApiRetryPaymentController extends Controller
{
    public function retry_payment(Request $request)
    {
        /************************************************************* */
        $data = json_decode($request->getContent(), true);
        //$data['params']['id'] = Config::get('values.debug_customer_id');
        //$data['params']['booking_id'] = 176730;
        //$data['params']['PaymentMethod'] = 2;
        /************************************************************* */
        // required input check
        $input = @$data['params'];
        $validator = Validator::make(
            (array) $input,
            [
                'booking_id' => 'required|integer',
                'PaymentMethod' => 'required|integer',
                'platform' => 'nullable|string',
            ],
            [],
            [
                'booking_id' => 'Booking ID',
                'PaymentMethod' => 'Payment Method',
                'platform' => 'Platform',
            ]
        );
        if ($validator->fails()) {
            return Response::json(array('result' => array('status' => 'failed', 'message' => $validator->errors()->first())), 200, array(), customerResponseJsonConstants());
        }
        /************************************************************* */
        try {
            DB::beginTransaction();
            // validate booking
            $bookings = DB::table('bookings as b')
                ->select(
                    'b.*',
                    'bsp.id as booking_subscription_package_id',
                    'b._total_payable',
                )
                /*************************************************************/
                ->leftJoin('booking_subscription_packages as bsp', function ($join) {
                    $join->on('b.booking_id', '=', 'bsp.booking_id');
                    $join->whereColumn('b.booking_id', 'bsp.booking_id');
                })
                /*************************************************************/
                ->where([['b.customer_id', '=', $input['id']]])
                ->where(function ($query) use ($input) {
                    $query->where('b.booking_common_id', $input['booking_id']); // multiple maids
                    $query->orWhere('b.booking_id', $input['booking_id']);
                })
                ->get();
            if (sizeof($bookings) == 0) {
                throw new \ErrorException('Booking not found.');
            }
            // format for machine :/
            $booking = $bookings[0];
            $input['time_from'] = Carbon::createFromFormat('H:i:s', $booking->time_from)->format('H:i:s');
            $input['hours'] = Carbon::createFromFormat('H:i:s', $booking->time_from)->diffInMinutes(Carbon::createFromFormat('H:i:s', $booking->time_to)) / 60; // based on db
            $input['time_to'] = Carbon::createFromFormat('H:i:s', $booking->time_from)->addMinutes($input['hours'] * 60)->format('H:i:s');
            //dd($input['hours']);
            $customer = Customer::where('customer_id', '=', $bookings[0]->customer_id)->first();
            $customer_address = CustomerAddress::where(['customer_address_id' => $bookings[0]->customer_address_id])->first();
            /****************************************************************************************** */
            $total_service_charge = 0;
            $total_vat_charge = 0;
            $total_payment_type_charge = 0;
            $booking_ids = array_column($bookings->toArray(), 'booking_id');
            foreach ($bookings as $key => $row) {
                $total_service_charge += $row->service_charge;
                $total_vat_charge += $row->vat_charge;
                $total_payment_type_charge += $row->payment_type_charge;
            }
            if($bookings[0]->subscription_package_id > 0)
            {
                $taxed_amount = $bookings[0]->_taxed_amount;
            } else {
                $taxed_amount = $total_service_charge + $total_vat_charge;
            }

            /************************ */
            $payment_type = DB::table('payment_types as pt')
                ->select(
                    'pt.*',
                )
                ->where(['pt.id' => $input['PaymentMethod']])
                ->first();
            if ($payment_type->charge_type == "F") {
                // fixed charge
                $data['total_payment_type_charge'] = $payment_type->charge;
            } else {
                // percentage of total
                $data['total_payment_type_charge'] = ($taxed_amount / 100) * $payment_type->charge;
            }
            /************************ */
            $service_fee_total = 0;
            $cleaning_material_fee_total = 0;
            $total_discount = 0;
            $tax_amount = 0;
            $payment_type_charge = 0;
            $amount_payable = 0;
            foreach ($booking_ids as $key => $booking_id) {
                $row = Booking::find($booking_id);
                $row->payment_type_charge = $data['total_payment_type_charge'] / $row->no_of_maids;
                $row->total_amount = $row->service_charge + $row->vat_charge + $row->payment_type_charge;
                $row->booking_status = 0;
                $row->payment_type_id = $payment_type->id;
                $row->pay_by = $payment_type->name;
                /******************************************* */
                $row->_payment_type_charge = $data['total_payment_type_charge'];
                $row->_total_payable = $row->_taxed_amount + $data['total_payment_type_charge'];
                /******************************************* */
                if ($payment_type->id == "1") {
                    // show on web booking section
                    $row->web_book_show = 1;
                } else {
                    // hide if other than cash mode, means payment not done
                    $row->web_book_show = 0;
                }
                /******************************************* */
                $row->save();
                $service_fee_total += $row->service_charge;
                $cleaning_material_fee_total += $row->cleaning_material_fee;
                $total_discount += $row->discount;
                $tax_amount += $row->vat_charge;
                $payment_type_charge += $row->payment_type_charge;
                $amount_payable += $row->total_amount;
            }
            /********************************************************************************************* */
            if ($payment_type->id == "1") {
                // cash mode
                $notify = new stdClass();
                $notify->customer_id = $input['id'];
                $notify->booking_id = $input['booking_id'];
                $notify->service_date = $booking->booking_type == "OD" ? $booking->service_start_date : null;
                $notify->content = "Booking is allocated with Ref. Id. {{booking_ref_id}}, we'll contact you soon for confirmation.";
                addCustomerNotification($notify);
            } else if ($payment_type->id == "2") {
                // card
                /******************************************************************************
                 *
                 *
                 * Request contains checkout card token data
                 *
                 *
                 **************************************************************************** */
                if (@$input['checkout_token_data']) {
                    // have checkout token data in request
                    $response['checkout_token_data'] = $input['checkout_token_data'];
                    $checkout_data = checkoutPayment($input['checkout_token_data'], $input['booking_id']);
                    $response['checkout_data'] = $checkout_data;
                    if (strtolower(@$checkout_data['status']) == "pending") {
                        // have redirect link to enter OTP
                    } else if (@$checkout_data['approved'] == true) {
                        // payment success
                        afterCheckoutPaymentSuccess($input['booking_id'], $checkout_data, @$input['platform']);
                    } else {
                        throw new \ErrorException('Card payment not successful.');
                    }
                }
                /****************************************************************************** */
            } else if ($payment_type->id == 4) {
                // google pay mode
                /******************************************************************************
                 * Request contains checkout google pay token data
                 **************************************************************************** */
                if (@$input['checkout_token_data']) {
                    // have checkout token data in request
                    $response['checkout_token_data'] = $input['checkout_token_data'];
                    $checkout_token_data = generateGooglePayToken($input['checkout_token_data']);
                    $checkout_data = checkoutGooglePay($checkout_token_data, $bookings[0]->booking_id);
                    $response['checkout_data'] = $checkout_data;
                    if (strtolower(@$checkout_data['status']) == "pending") {
                        // have redirect link to enter OTP
                    } else if (@$checkout_data['approved'] == true) {
                        // payment success
                        afterCheckoutPaymentSuccess($bookings[0]->booking_id, $checkout_data, @$input['platform']);
                    } else {
                        throw new \ErrorException('Payment not successful.');
                    }
                }
                /****************************************************************************** */
            }
            /********************************************************************************************* */
            $input['professionals_count'] = sizeof($booking_ids);
            $response['status'] = 'success';
            /********************************************** */
            if ($bookings[0]->booking_subscription_package_id != null) {
                // subscription package found
                $amount_payable = $bookings[0]->_total_payable;
            }
            /********************************************** */
            $response['booking_details'] = array(
                "booking_id" => $input['booking_id'],
                "booking_reference_id" => $booking->reference_id,
                "professionals_count" => $input['professionals_count'],
                "total_hours" => $input['hours'],
                "payment_method" => $payment_type->name,
                "payment_type_id" => $payment_type->id,
                "status" => $payment_type->id == 1 ? "Booking Received" : "Payment Pending",
                "total_amount" => $amount_payable,
            );
            $response['payment_details'] = array(
                'service_fee' => $service_fee_total,
                "cleaning_materials" => $cleaning_material_fee_total,
                "discount" => $total_discount,
                "tax_amount" => $tax_amount,
                "payment_type_charge" => $payment_type_charge,
                "total" => $amount_payable,
            );
            /********************************************** */
            $response['payment_details']['payment_url'] = null;
            $response['payment_details']['payment_success_url'] = null;
            $response['payment_details']['payment_failed_url'] = null;
            $response['payment_details']['payment_url_method'] = "GET";
            $response['payment_details']['payment_url_params'] = new stdClass();
            /********************************************** */
            // customised payment gateway params
            if ($payment_type->id == 1) {
                // cash mode
                //  send mail
                send_booking_confirmation_sms_to_customer($bookings[0]->booking_id);
                $customer = Customer::where('customer_id', '=', $bookings[0]->customer_id)->first();
                pushNotification($customer, ['title' => "Booking Received !", 'body' => "Booking received with Ref. Id " . $bookings[0]->reference_id . "."], ['screen' => 'BOOKING_HISTORY']);
            } else if ($payment_type->id == 2) {
                // card mode
                $response['payment_details']['payment_url'] = url('payment/gateway/checkout/entry/' . $bookings[0]->booking_id);
                $response['payment_details']['payment_success_url'] = url('payment/gateway/checkout/success/' . $bookings[0]->booking_id);
                $response['payment_details']['payment_failed_url'] = url('payment/gateway/checkout/failed/' . $bookings[0]->booking_id);
                $response['payment_details']['payment_url_params'] = [
                    'booking_id' => $bookings[0]->booking_id,
                    'customer_id' => $input['id'],
                    'booking_reference' => $bookings[0]->reference_id,
                    'amount' => $amount_payable,
                ];
            }
            $booking = Booking::findOrFail($bookings[0]->booking_id);
            $response['billing'] = [
                'order_id' => $booking->reference_id,
                'amount' => $booking->_total_payable,
                'name' => ccavenueRemoveSpecialChars($customer->customer_name),
                'address' => ccavenueRemoveSpecialChars(@$customer_address->customer_address),
                'city' => 'United Arab Emirates',
                'state' => null,
                'zip' => null,
                'country' => 'United Arab Emirates',
                'tel' => $customer->mobile_number_1,
                'email' => $customer->email_address,
            ];
            /********************************************** */
            DB::commit();
            $response['message'] = 'Payment mode changed to ' . $payment_type->name . ' successfully.';
            return Response::json(array('result' => $response), 200, array(), customerResponseJsonConstants());
        } catch (\Exception $e) {
            DB::rollback();
            return Response::json(array('result' => array('status' => 'failed', 'message' => $e->getMessage())), 200, array(), customerResponseJsonConstants());
        }
    }
}
