<div class="content-section2" style="width: 700px; height: auto; margin: 30px auto 30px;">
    <table width="100%" cellpadding="0" cellspacing="0">
        <tr>
            <td style="border: 1px solid #ddd;">
                <p
                    style="font: bold 14px/18px 'Poppins', sans-serif; color: #555; display: block; margin: 0px; padding: 5px 20px; text-align: left; -webkit-text-size-adjust: 100%; -moz-text-size-adjust: 100%; -ms-text-size-adjust: 100%;">
                    <label
                        style="font-size: 16px; line-height: 25px; display: block; margin: 0px; padding: 2px 0px 0px;">Booking Address</label>
                </p>
                <hr style="border: 0px;border-top: 1px solid #ddd;">
                <p
                    style="font: normal 14px/18px 'Poppins', sans-serif; color: #555; display: block; margin: 0px; padding: 0px 20px; text-align: left; -webkit-text-size-adjust: 100%; -moz-text-size-adjust: 100%; -ms-text-size-adjust: 100%;">
                    <label
                        style="font-size: 16px; line-height: 25px; display: block; margin: 0px; padding: 2px 0px 0px;">Name
                        - {{ $customer->customer_name }}</label>
                </p>
                <p
                    style="font: normal 14px/18px 'Poppins', sans-serif; color: #555; display: block; margin: 0px; padding: 0px 20px; text-align: left; -webkit-text-size-adjust: 100%; -moz-text-size-adjust: 100%; -ms-text-size-adjust: 100%;">
                    <label
                        style="font-size: 16px; line-height: 25px; display: block; margin: 0px; padding: 2px 0px 0px;">Address
                        - {{ $customer_address->customer_address }}</label>
                </p>
                <p
                    style="font: normal 14px/18px 'Poppins', sans-serif; color: #555; display: block; margin: 0px; padding: 0px 20px; text-align: left; -webkit-text-size-adjust: 100%; -moz-text-size-adjust: 100%; -ms-text-size-adjust: 100%;">
                    <label
                        style="font-size: 16px; line-height: 25px; display: block; margin: 0px; padding: 2px 0px 0px;">Area
                        - {{ $customer_address->area_name }}</label>
                </p>
                @if($customer->email_address != '')
                <p
                    style="font: normal 14px/18px 'Poppins', sans-serif; color: #555; display: block; margin: 0px; padding: 0px 20px; text-align: left; -webkit-text-size-adjust: 100%; -moz-text-size-adjust: 100%; -ms-text-size-adjust: 100%;">
                    <label
                        style="font-size: 16px; line-height: 25px; display: block; margin: 0px; padding: 2px 0px 0px;">Email
                        - {{ $customer->email_address}}</label>
                </p>
                @endif
                <p
                    style="font: normal 14px/18px 'Poppins', sans-serif; color: #555; display: block; margin: 0px; padding: 0px 20px 10px 20px; text-align: left; -webkit-text-size-adjust: 100%; -moz-text-size-adjust: 100%; -ms-text-size-adjust: 100%;">
                    <label
                        style="font-size: 16px; line-height: 25px; display: block; margin: 0px; padding: 2px 0px 0px;">Mobile
                        - {{ $customer->mobile_number_1 }}</label>
                </p>
            </td>
        </tr>
    </table>
</div>