<html>
<head>
	<!-- mithumb w=768 h=1024 t_w=150 t_h=200 -->
    <!--[if gte mso 9]>
    <xml>
        <o:OfficeDocumentSettings>
            <o:AllowPNG/>
            <o:PixelsPerInch>96</o:PixelsPerInch>
        </o:OfficeDocumentSettings>
    </xml>
  <![endif]-->
    <!--[if mso]>
        <style>
            * {
                font-family: sans-serif !important;
            }
        </style>
    <![endif]-->
    <style type="text/css">
/*! normalize.css v8.0.1 | MIT License | github.com/necolas/normalize.css */

        /* Document
    ========================================================================== */

        /**
    * 1. Correct the line height in all browsers.
    * 2. Prevent adjustments of font size after orientation changes in iOS.
    */


html {
line-height: 1.15;
/* 1 */
-webkit-text-size-adjust: 100%;/* 2 */
}
/* Sections
    ========================================================================== */

        /**
    * Remove the margin in all browsers.
    */

body {
margin: 0px;
}
/**
    * Render the `main` element consistently in IE.
    */

main {
display: block;
}
/**
    * Correct the font size and margin on `h1` elements within `section` and
    * `article` contexts in Chrome, Firefox, and Safari.
    */

h1 {
font-size: 2em;
margin: 0.67em 0px;
}
/* Grouping content
    ========================================================================== */

        /**
    * 1. Add the correct box sizing in Firefox.
    * 2. Show the overflow in Edge and IE.
    */

hr {
box-sizing: content-box;
/* 1 */
height: 0px;
/* 1 */
overflow: visible;/* 2 */
}
/**
    * 1. Correct the inheritance and scaling of font size in all browsers.
    * 2. Correct the odd `em` font sizing in all browsers.
    */

pre {
font-family: monospace, monospace;
/* 1 */
font-size: 1em;/* 2 */
}
/* Text-level semantics
    ========================================================================== */

        /**
    * Remove the gray background on active links in IE 10.
    */

a {
background-color: transparent;
}
/**
    * 1. Remove the bottom border in Chrome 57-
    * 2. Add the correct text decoration in Chrome, Edge, IE, Opera, and Safari.
    */

abbr[title] {
border-bottom: none;
/* 1 */
text-decoration: underline;
/* 2 */
text-decoration: underline dotted;/* 2 */
}
/**
    * Add the correct font weight in Chrome, Edge, and Safari.
    */

b,  strong {
font-weight: bolder;
}
/**
    * 1. Correct the inheritance and scaling of font size in all browsers.
    * 2. Correct the odd `em` font sizing in all browsers.
    */

code,  kbd,  samp {
font-family: monospace, monospace;
/* 1 */
font-size: 1em;/* 2 */
}
/**
    * Add the correct font size in all browsers.
    */

small {
font-size: 80%;
}
/**
    * Prevent `sub` and `sup` elements from affecting the line height in
    * all browsers.
    */

sub,  sup {
font-size: 75%;
line-height: 0px;
position: relative;
}
/* Embedded content
    ========================================================================== */

        /**
    * Remove the border on images inside links in IE 10.
    */

img {
border-style: none;
}
/* Forms
    ========================================================================== */

        /**
    * 1. Change the font styles in all browsers.
    * 2. Remove the margin in Firefox and Safari.
    */

button,  input,  optgroup,  select,  textarea {
font-family: inherit;
/* 1 */
font-size: 100%;
/* 1 */
line-height: 1.15;
/* 1 */
margin: 0px;/* 2 */
}
/**
    * Show the overflow in IE.
    * 1. Show the overflow in Edge.
    */

button,  input {
/* 1 */
overflow: visible;
}
/**
    * Remove the inheritance of text transform in Edge, Firefox, and IE.
    * 1. Remove the inheritance of text transform in Firefox.
    */

button,  select {
/* 1 */
text-transform: none;
}

        /**
    * Correct the inability to style clickable types in iOS and Safari.
    */

        button,  [type="button"],  [type="reset"],  [type="submit"] {
 -webkit-appearance: button;
}

        /**
    * Remove the inner border and padding in Firefox.
    */

        button::-moz-focus-inner,  [type="button"]::-moz-focus-inner,  [type="reset"]::-moz-focus-inner,  [type="submit"]::-moz-focus-inner {
 border-style: none;
 padding: 0px;
}

        /**
    * Restore the focus styles unset by the previous rule.
    */

        button:-moz-focusring,  [type="button"]:-moz-focusring,  [type="reset"]:-moz-focusring,  [type="submit"]:-moz-focusring {
 outline: 1px dotted ButtonText;
}
/**
    * Correct the padding in Firefox.
    */

fieldset {
padding: 0.35em 0.75em 0.625em;
}
/**
    * 1. Correct the text wrapping in Edge and IE.
    * 2. Correct the color inheritance from `fieldset` elements in IE.
    * 3. Remove the padding so developers are not caught out when they zero out
    *    `fieldset` elements in all browsers.
    */

legend {
box-sizing: border-box;
/* 1 */
color: inherit;
/* 2 */
display: table;
/* 1 */
max-width: 100%;
/* 1 */
padding: 0px;
/* 3 */
white-space: normal;/* 1 */
}
/**
    * Add the correct vertical alignment in Chrome, Firefox, and Opera.
    */

progress {
vertical-align: baseline;
}
/**
    * Remove the default vertical scrollbar in IE 10+.
    */

textarea {
overflow: auto;
}

        /**
    * 1. Add the correct box sizing in IE 10.
    * 2. Remove the padding in IE 10.
    */

        [type="checkbox"],  [type="radio"] {
 box-sizing: border-box;
            /* 1 */
            padding: 0px;
/* 2 */
}

        /**
    * Correct the cursor style of increment and decrement buttons in Chrome.
    */

        [type="number"]::-webkit-inner-spin-button,  [type="number"]::-webkit-outer-spin-button {
 height: auto;
}

        /**
    * 1. Correct the odd appearance in Chrome and Safari.
    * 2. Correct the outline style in Safari.
    */

        [type="search"] {
 -webkit-appearance: textfield;
            /* 1 */
            outline-offset: -2px;
/* 2 */
}

        /**
    * Remove the inner padding in Chrome and Safari on macOS.
    */

        [type="search"]::-webkit-search-decoration {
 -webkit-appearance: none;
}

        /**
    * 1. Correct the inability to style clickable types in iOS and Safari.
    * 2. Change font properties to `inherit` in Safari.
    */

        ::-webkit-file-upload-button {
 -webkit-appearance: button;
            /* 1 */
            font: inherit;
/* 2 */
}
/* Interactive
    ========================================================================== */

        /*
    * Add the correct display in Edge, IE 10+, and Firefox.
    */

details {
display: block;
}
/*
    * Add the correct display in all browsers.
    */

summary {
display: list-item;
}
/* Misc
    ========================================================================== */

        /**
    * Add the correct display in IE 10+.
    */

template {
display: none;
}

        /**
    * Add the correct display in IE 10.
    */

        [hidden] {
 display: none;
}
/* What it does: Remove spaces around the email design added by some email clients. */
        /* Beware: It can remove the padding / margin and add a background color to the compose a reply window. */
html,  body {
margin: 0 auto !important;
padding: 0 !important;
height: 100% !important;
width: 100% !important;
}
/* What it does: Stops email clients resizing small text. */
* {
-ms-text-size-adjust: 100%;
-webkit-text-size-adjust: 100%;
}
/* What it does: Centers email on Android 4.4 */
div[style*="margin: 16px 0"] {
margin: 0 !important;
}
/* What it does: forces Samsung Android mail clients to use the entire viewport */
#MessageViewBody,  #MessageWebViewDiv {
width: 100% !important;
}
/* What it does: Stops Outlook from adding extra spacing to tables. */
table,  td {
mso-table-lspace: 0pt !important;
mso-table-rspace: 0pt !important;
}
/* What it does: Replaces default bold style. */
th {
font-weight: normal;
}
/* What it does: Fixes webkit padding issue. */
table {
border-spacing: 0 !important;
border-collapse: collapse !important;
table-layout: fixed !important;
margin: 0 auto !important;
}
/* What it does: Prevents Windows 10 Mail from underlining links despite inline CSS. Styles for underlined links should be inline. */
a {
text-decoration: none;
}
/* What it does: Uses a better rendering method when resizing images in IE. */
img {
-ms-interpolation-mode: bicubic;
}
/* What it does: A work-around for email clients meddling in triggered links. */
a[x-apple-data-detectors],         /* iOS */
        .unstyle-auto-detected-links a,  .aBn {
border-bottom: 0 !important;
cursor: default !important;
color: inherit !important;
text-decoration: none !important;
font-size: inherit !important;
font-family: inherit !important;
font-weight: inherit !important;
line-height: inherit !important;
}
/* What it does: Prevents Gmail from changing the text color in conversation threads. */
.im {
color: inherit !important;
}
/* What it does: Prevents Gmail from displaying a download button on large, non-linked images. */
.a6S {
display: none !important;
opacity: 0.01 !important;
}
/* If the above doesn't work, add a .g-img class to any image in question. */
img.g-img+div {
display: none !important;
}

        /* What it does: Removes right gutter in Gmail iOS app: https://github.com/TedGoas/Cerberus/issues/89  */
        /* Create one of these media queries for each additional viewport size you'd like to fix */

        /* iPhone 4, 4S, 5, 5S, 5C, and 5SE */
        @media only screen and (min-device-width: 320px) and (max-device-width: 374px) {
u~div .email-container {
min-width: 320px !important;
}
}

        /* iPhone 6, 6S, 7, 8, and X */
        @media only screen and (min-device-width: 375px) and (max-device-width: 413px) {
u~div .email-container {
min-width: 375px !important;
}
}

        /* iPhone 6+, 7+, and 8+ */
        @media only screen and (min-device-width: 414px) {
u~div .email-container {
min-width: 414px !important;
}
}
/* PYKSYS */

u+#body a {
color: inherit;
text-decoration: none;
font-size: inherit;
font-family: inherit;
font-weight: inherit;
line-height: inherit;
}
body {
margin: 0px;
padding: 0px;
-webkit-text-size-adjust: 100%;
-ms-text-size-adjust: 100%;
}
table,  th,  td {
border-collapse: collapse;
mso-table-lspace: 0pt;
mso-table-rspace: 0pt;
}
img {
border: 0px;
height: auto;
line-height: 100%;
outline: none;
text-decoration: none;
-ms-interpolation-mode: bicubic;
}
 @media print {
* {
-webkit-print-color-adjust: exact
}
}
.hide {
width: 0px !important;
height: 0px !important;
color: transparent !important;
line-height: 0 !important;
overflow: hidden !important;
font-size: 0px !important;
display: none !important;
visibility: hidden !important;
opacity: 0 !important;
mso-hide: all !important;
}
 @media screen and (max-width: 639px) {
.email-container {
width: 100% !important;
margin: auto !important
}
th.stack-column,  th.stack-column-center {
display: block !important;
width: 100% !important;
max-width: 100% !important;
direction: ltr !important;
padding-left: 0px !important;
padding-right: 0px !important
}
th.stack-column-center,  th.stack-column-center th,  th.stack-column-center td {
text-align: center !important
}
td.stack-column,  td.stack-column-center {
display: block !important;
width: 100% !important;
max-width: 100% !important;
direction: ltr !important;
padding-left: 0px !important;
padding-right: 0px !important
}
td.stack-column-center,  td.stack-column-center th,  td.stack-column-center td {
text-align: center !important
}
.padding-0-on-mobile {
padding: 0px !important;
}
.hide-on-mobile {
width: 0px !important;
height: 0px !important;
color: transparent !important;
line-height: 0 !important;
overflow: hidden !important;
font-size: 0px !important;
display: none !important;
visibility: hidden !important;
opacity: 0 !important;
mso-hide: all !important;
}
.mb-10 {
margin-bottom: 10px !important;
}
.mt-20 {
margin-top: 20px !important;
}
.mobile-width {
width: 80% !important;
}
}
:root {
color-scheme: light only;
}
</style>
    <!--[if mso]>
    <style>
      sup {
        font-size: 100% !important;
      }
    </style>
  <![endif]-->
</head>

<body>
<div class="main-wrapper" style="width: 800px; margin:0 auto; background:#ddf8ed;">
	<div style="width: 800px;"><img src="{{ Config::get('values.file_server_url').'images/email/email-header-banner.jpg?v='.$settings->customer_app_img_version }}"  width="800" height="303" alt="" /></div>
	
	<div style="width: 798px; background:#ddf8ed;">
	
		<div style="width: 600px; height:auto; margin: 0 auto; padding: 0px 30px;">
			<div style="width: 600px; height:auto; padding: 0px 0px 30px 0px; margin-bottom: 30px;">
				<p style="font-family: Tahoma, Geneva, sans-serif; font-size:14px; color: #555; line-height:20px; padding:20px 0px 0px 0px; font-weight: bold; margin:0px; -webkit-text-size-adjust: 100%; -moz-text-size-adjust: 100%; -ms-text-size-adjust: 100%;">Hello {{@$customer['customer_name']}},</p>
				@if(@$bookings['pay_by'] == 'Card')
				<p style="font-family: Tahoma, Geneva, sans-serif; font-size:14px; color: #555; line-height:20px; padding: 12px 0px 15px 0px; margin:0px; -webkit-text-size-adjust: 100%; -moz-text-size-adjust: 100%; -ms-text-size-adjust: 100%;">Thank you for your Payment!</p>
				@endif
				<p style="font-family: Tahoma, Geneva, sans-serif; font-size:14px; color: #555; line-height:20px; padding: 12px 0px 15px 0px; margin:0px; -webkit-text-size-adjust: 100%; -moz-text-size-adjust: 100%; -ms-text-size-adjust: 100%;">Welcome to Dubai Housekeeping.<br>Thank You for booking with us. You can find your booking details below. We strive to remain your first choice.</p>

				<p style="font-family: Tahoma, Geneva, sans-serif; font-size:14px; color: #555; line-height:20px; padding: 12px 0px 15px 0px; font-weight: bold; margin:0px; -webkit-text-size-adjust: 100%; -moz-text-size-adjust: 100%; -ms-text-size-adjust: 100%;">Payment transaction id - {{@$online['transaction_id']}}</p>

				<p style="font-family: Tahoma, Geneva, sans-serif; font-size:14px; line-height:20px; color: #FFF; padding: 12px 0px 0px 0px; margin:0px; -webkit-text-size-adjust: 100%; -moz-text-size-adjust: 100%; -ms-text-size-adjust: 100%;"> <span style="font-size:14px; background: #016ab6; padding: 12px 15px; border-radius: 50px; margin:0px; font-weight: bold;">Booking Reference ID - {{@@$bookings['reference_id']}}</span> </p>
			</div>
			<div style="width: 560px; height:auto; background: #fafafa; border:1px solid #e5e2e2; padding: 20px 20px; ">
				<p style="font-family: Tahoma, Geneva, sans-serif; font-size:14px; color: #555; line-height:20px; padding:0px 0px 0px 0px; font-weight: bold; margin:0px; -webkit-text-size-adjust: 100%; -moz-text-size-adjust: 100%; -ms-text-size-adjust: 100%;">Address Summary</p>
				<p style="font-family: Tahoma, Geneva, sans-serif; font-size:14px; color: #555; line-height:20px; padding:5px 0px 0px 0px; margin:0px; -webkit-text-size-adjust: 100%; -moz-text-size-adjust: 100%; -ms-text-size-adjust: 100%;">Address : @if(@$customerAddress !=''){{@@$customerAddress['customer_address']}}@else NA @endif<br />
					Area -  {{@$areaName}}</p>
			</div>
			<div style="width: 600px; height:auto; background: #fafafa; border:1px solid #e5e2e2; border-top:0px; padding:0px 0px;">
				<table width="600" border="0" cellspacing="0" cellpadding="0" bordercolor="#FFF">
					<tr style="background: #FFF;">
						<td width="298"><p style="font-family: Tahoma, Geneva, sans-serif; font-size:14px; color: #555; line-height:20px; padding: 12px 20px; margin:0px; -webkit-text-size-adjust: 100%; -moz-text-size-adjust: 100%; -ms-text-size-adjust: 100%;">Service Name</p></td>
						<td width="34">:</td>
						<td width="268"><p style="font-family: Tahoma, Geneva, sans-serif; font-size:14px; color: #555; line-height:20px; padding: 12px 10px; margin:0px; -webkit-text-size-adjust: 100%; -moz-text-size-adjust: 100%; -ms-text-size-adjust: 100%;">{{@@$services['service_type_name']}}</p></td>
					</tr>
					@if(@$services['service_type_name'] == 'Maid Service' || @$services['service_type_name'] == 'Floor Scrubbing')
					<tr style="background: #FFF;">
						<td><p style="font-family: Tahoma, Geneva, sans-serif; font-size:14px; color: #555; line-height:20px; padding: 12px 20px; margin:0px; -webkit-text-size-adjust: 100%; -moz-text-size-adjust: 100%; -ms-text-size-adjust: 100%;">No of Maids</p></td>
						<td>:</td>
						<td><p style="font-family: Tahoma, Geneva, sans-serif; font-size:14px; color: #555; line-height:20px; padding: 12px 10px; margin:0px; -webkit-text-size-adjust: 100%; -moz-text-size-adjust: 100%; -ms-text-size-adjust: 100%;">{{@@$bookings['no_of_maids']}}</p></td>
					</tr>
					@endif  
					@if( @$services['service_type_name'] == 'Floor Scrubbing')
					<tr style="background: #FFF;">
						<td><p style="border-top:1px solid #e5e2e2; font-family: Tahoma, Geneva, sans-serif; font-size:14px; color: #555; line-height:20px; padding: 12px 20px; margin:0px; -webkit-text-size-adjust: 100%; -moz-text-size-adjust: 100%; -ms-text-size-adjust: 100%;">Total Hours</p></td>
						<td style="border-top:1px solid #e5e2e2;">:</td>
						<td><p style="border-top:1px solid #e5e2e2; font-family: Tahoma, Geneva, sans-serif; font-size:14px; color: #555; line-height:20px; padding: 12px 10px; margin:0px; -webkit-text-size-adjust: 100%; -moz-text-size-adjust: 100%; -ms-text-size-adjust: 100%;">{{@@$bookings['no_of_hrs']}}</p></td>
					</tr>
					@endif
					<tr style="  ">
						<td><p style="font-family: Tahoma, Geneva, sans-serif; font-size:14px; color: #555; line-height:20px; padding: 12px 20px; margin:0px; -webkit-text-size-adjust: 100%; -moz-text-size-adjust: 100%; -ms-text-size-adjust: 100%;">Service Date & Time</p></td>
						<td style="">:</td>
						<?php
                                            $date = @@$bookings['service_start_date'];
                                            
                                            $old_date_timestamp = strtotime($date);

											$dateFormat = date('l jS, F',$old_date_timestamp); 
											if(@@$bookings['booking_type'] == 'OD') {
												$val = 'one-time';
											} else if(@$bookings['booking_type'] == 'WE') {
												$val = 'weekly';
											} else {
												$val = 'bi-weekly';
											}
											$start = date("g:i a", strtotime(@@$bookings['time_from'])); 
											$end = date("g:i a", strtotime(@@$bookings['time_to'])); 
                                            ?>
						<td><p style="font-family: Tahoma, Geneva, sans-serif; font-size:14px; color: #555; line-height:20px; padding: 12px 10px; margin:0px; -webkit-text-size-adjust: 100%; -moz-text-size-adjust: 100%; -ms-text-size-adjust: 100%;">{{@$dateFormat}} ({{@$val}}) <br/>
								{{@$start}} - {{@$end}}</p></td>
					</tr>
					<tr style="background: #FFF;">
						<td><p style="border-top:1px solid #e5e2e2; font-family: Tahoma, Geneva, sans-serif; font-size:14px; color: #555; line-height:20px; padding: 12px 20px; margin:0px; -webkit-text-size-adjust: 100%; -moz-text-size-adjust: 100%; -ms-text-size-adjust: 100%;">No. of Maids</p></td>
						<td style="border-top:1px solid #e5e2e2;">:</td>
						<td><p style="border-top:1px solid #e5e2e2; font-family: Tahoma, Geneva, sans-serif; font-size:14px; color: #555; line-height:20px; padding: 12px 10px; margin:0px; -webkit-text-size-adjust: 100%; -moz-text-size-adjust: 100%; -ms-text-size-adjust: 100%;">{{@@$bookings['no_of_maids']}}</p></td>
					</tr>
					@if(@@$data['service_data']['coupon'])
					<tr style="background: #FFF;">
						<td><p style="border-top:1px solid #e5e2e2; font-family: Tahoma, Geneva, sans-serif; font-size:14px; color: #555; line-height:20px; padding: 12px 20px; margin:0px; -webkit-text-size-adjust: 100%; -moz-text-size-adjust: 100%; -ms-text-size-adjust: 100%;">Discount Coupon</p></td>
						<td style="border-top:1px solid #e5e2e2;">:</td>
						<td><p style="border-top:1px solid #e5e2e2; font-family: Tahoma, Geneva, sans-serif; font-size:14px; color: #555; line-height:20px; padding: 12px 10px; margin:0px; -webkit-text-size-adjust: 100%; -moz-text-size-adjust: 100%; -ms-text-size-adjust: 100%;">{{@@@$data['service_data']['coupon']['coupon_name']}}</p></td>
					</tr>
					@endif
					<tr style="background: #FFF;">
						<td><p style="border-top:1px solid #e5e2e2; font-family: Tahoma, Geneva, sans-serif; font-size:14px; color: #555; line-height:20px; padding: 12px 20px; margin:0px; -webkit-text-size-adjust: 100%; -moz-text-size-adjust: 100%; -ms-text-size-adjust: 100%;">Crew In</p></td>
						<td style="border-top:1px solid #e5e2e2;">:</td>
						<td><p style="border-top:1px solid #e5e2e2; font-family: Tahoma, Geneva, sans-serif; font-size:14px; color: #555; line-height:20px; padding: 12px 10px; margin:0px; -webkit-text-size-adjust: 100%; -moz-text-size-adjust: 100%; -ms-text-size-adjust: 100%;">{{@@$bookings['crew_in']}}</p></td>
					</tr>
					<tr>
						<td><p style="border-top:1px solid #e5e2e2; font-family: Tahoma, Geneva, sans-serif; font-size:14px; color: #555; line-height:20px; padding: 12px 20px; margin:0px; -webkit-text-size-adjust: 100%; -moz-text-size-adjust: 100%; -ms-text-size-adjust: 100%;">Instructions</p></td>
						<td style="border-top:1px solid #e5e2e2;">:</td>
						<td><p style="border-top:1px solid #e5e2e2; font-family: Tahoma, Geneva, sans-serif; font-size:14px; color: #555; line-height:20px; padding: 12px 10px; margin:0px; -webkit-text-size-adjust: 100%; -moz-text-size-adjust: 100%; -ms-text-size-adjust: 100%;">{{@@$bookings['booking_note'] ?: '-'}}</p></td>
					</tr>
					{{-- Customer Cart Details START --}}
					<!------------------------ ------------------------------------------------->
{{--
    @if (@$data['service_data']['service_mappings'])
    <tr style="background: #FFF;">
        <td colspan="3">
            <table border="1"
                style="width: 100%;border-color: #e5e2e2;font-family: Tahoma, Geneva, sans-serif;color: #555;text-align: center;">
                <tr style="background: #efefef;">
                    <th colspan="4" style="font-weight: bold;">Service Details</th>
                </tr>
                <tr style="background: #fafafa;">
                    <th>Name</th>
                    <th>Rate</th>
                    <th>Quantity</th>
                    <th>Amount</th>
                </tr>
                @foreach (@$data['service_data']['service_mappings'] as $key => $service_mapping)
                    <tr>
                        <td>{{ $service_mapping['name'] }}</td>
                        <td>{{ $service_mapping['service_cost'] }}</td>
                        <td>{{ $service_mapping['quantity'] }}</td>
                        <td>{{ $service_mapping['service_cost'] * $service_mapping['quantity'] }}</td>
                    </tr>
                @endforeach
            </table>
        </td>
    </tr>
@endif
--}}
<!------------------------ ------------------------------------------------->
@if (@$data['service_data']['cleaning_supplies'])
<tr style="background: #FFF;">
    <td colspan="3">
        <table border="1"
            style="width: 100%;border-color: #e5e2e2;font-family: Tahoma, Geneva, sans-serif;color: #555;text-align: center;">
            <tr style="background: #efefef;">
                <th colspan="4" style="font-weight: bold;">Cleaning Supplies</th>
            </tr>
            <tr style="background: #fafafa;">
                <th>Name</th>
                <th>Rate</th>
                <th>Quantity</th>
                <th>Amount</th>
            </tr>
            @foreach (@$data['service_data']['cleaning_supplies'] as $key => $cleaning_supply)
                <tr>
                    <td>{{ $cleaning_supply['cleaning_supply_name'] }}</td>
                    <td>{{ $cleaning_supply['cleaning_supply_unit_rate'] }}</td>
                    <td>{{ $cleaning_supply['cleaning_supply_quantity'] }}</td>
                    <td>{{ $cleaning_supply['cleaning_supply_unit_rate'] * $cleaning_supply['cleaning_supply_quantity'] }}
                    </td>
                </tr>
            @endforeach
        </table>
    </td>
</tr>
@endif
<!------------------------ ------------------------------------------------->
@if (@$data['service_data']['extra_services'])
<tr style="background: #FFF;">
    <td><p style="border-top:1px solid #e5e2e2; font-family: Tahoma, Geneva, sans-serif; font-size:14px; color: #555; line-height:20px; padding: 12px 20px; margin:0px; -webkit-text-size-adjust: 100%; -moz-text-size-adjust: 100%; -ms-text-size-adjust: 100%;">Extra Services</p></td>
    <td style="border-top:1px solid #e5e2e2;">:</td>
    <td>
        <p style="border-top:1px solid #e5e2e2; font-family: Tahoma, Geneva, sans-serif; font-size:14px; color: #555; line-height:20px; padding: 12px 10px; margin:0px; -webkit-text-size-adjust: 100%; -moz-text-size-adjust: 100%; -ms-text-size-adjust: 100%;">
        @php
            $extra_services = array_column(@$data['service_data']['extra_services'],'extra_service_name');
            echo join(' and ', array_filter(array_merge(array(join(', ', array_slice($extra_services, 0, -1))), array_slice($extra_services, -1)), 'strlen'));
        @endphp
        </p>
    </td>
</tr>
@endif
<!------------------------ ------------------------------------------------->
{{--@if (@$data['service_data']['extra_services'])
<tr style="background: #FFF;">
    <td colspan="3">
        <table border="1"
            style="width: 100%;border-color: #e5e2e2;font-family: Tahoma, Geneva, sans-serif;color: #555;text-align: center;">
            <tr style="background: #efefef;">
                <th colspan="5" style="font-weight: bold;">Extra Services</th>
            </tr>
            <tr style="background: #fafafa;">
                <th>Service</th>
                <th>Duration</th>
                <th>Rate</th>
                <th>Quantity</th>
                <th>Amount</th>
            </tr>
            @foreach (@$data['service_data']['extra_services'] as $key => $extra_service)
                <tr>
                    <td>{{ $extra_service['extra_service_name'] }}</td>
                    <td>{{ $extra_service['extra_service_duration'] }} Mins.</td>
                    <td>{{ $extra_service['extra_service_unit_rate'] }}</td>
                    <td>{{ $extra_service['extra_service_quantity'] }}</td>
                    <td>{{ $extra_service['extra_service_total_amount'] }}</td>
                </tr>
            @endforeach
        </table>
    </td>
</tr>
@endif--}}
<!------------------------ ------------------------------------------------->

					{{-- Customer Cart Details END --}}
					@if(@$services['service_type_name'] == 'Maid Service')
					<?php
			if(@$bookings['cleaning_material']=="Y"): ?>
					<tr style="background: #FFF;">
						<td><p style="border-top:1px solid #e5e2e2; font-family: Tahoma, Geneva, sans-serif; font-size:14px; color: #555; line-height:20px; padding: 12px 20px; margin:0px; -webkit-text-size-adjust: 100%; -moz-text-size-adjust: 100%; -ms-text-size-adjust: 100%;">Cleaning Material</p></td>
						<td style="border-top:1px solid #e5e2e2;">:</td>
						<td><p style="border-top:1px solid #e5e2e2; font-family: Tahoma, Geneva, sans-serif; font-size:14px; color: #555; line-height:20px; padding: 12px 10px; margin:0px; -webkit-text-size-adjust: 100%; -moz-text-size-adjust: 100%; -ms-text-size-adjust: 100%;">Yes</p></td>
					</tr>
					<?php endif; ?>
					<?php
			if(@$bookings['interior_window_clean'] == 1)
			{
				$intrior = "Yes";
			} else {
				$intrior = "No";
			}
			if(@$bookings['fridge_cleaning']  == 1)
			{
				$fridge = "Yes";
			} else {
				$fridge = "No";
			}
			if(@$bookings['ironing_services']  == 1)
			{
				$ironing = "Yes";
			} else {
				$ironing = "No";
			}
			if(@$bookings['oven_cleaning']  == 1)
			{
				$oven = "Yes";
			} else {
				$oven = "No";
			}
			?>
					<?php
			if(@$bookings['interior_window_clean'] == 1 || @$bookings['fridge_cleaning'] == 1 || @$bookings['ironing_services'] == 1 || @$bookings['oven_cleaning'] == 1)
			{
			?>
					<tr>
						<td colspan="3"><p style="border-top:1px solid #e5e2e2; font-family: Tahoma, Geneva, sans-serif; font-size:14px; color: #555; line-height:20px; padding: 12px 20px 0px; margin:0px; -webkit-text-size-adjust: 100%; -moz-text-size-adjust: 100%; -ms-text-size-adjust: 100%;">Extra Services</p>
							<p style="font-family: Tahoma, Geneva, sans-serif; font-size:14px; color: #555; line-height:20px; padding: 5px 20px 15px; margin:0px; -webkit-text-size-adjust: 100%; -moz-text-size-adjust: 100%; -ms-text-size-adjust: 100%;">
								<?php
				if(@$bookings['interior_window_clean'] == 1)
				{
				?>
								Interior Windows - <?php echo $intrior; ?><br>
								<?php } ?>
								<?php
				if(@$bookings['fridge_cleaning'] == 1)
				{
				?>
								Fridge Cleaning - <?php echo $fridge; ?>&nbsp;<br>
								<?php
				}
				?>
								<?php
				if(@$bookings['ironing_services']  == 1)
				{
				?>
								Ironing - <?php echo $ironing; ?>&nbsp;<br>
								<?php } ?>
								<?php
				if(@$bookings['oven_cleaning']  == 1)
				{
				?>
								Oven Cleaning - <?php echo $oven; ?><br />
								<?php
				}
				?>
							</p></td>
					</tr>
					<?php
			}
			?>
					<tr style="background: #FFF;">
						<td><p style="border-top:1px solid #e5e2e2; font-family: Tahoma, Geneva, sans-serif; font-size:14px; color: #555; line-height:20px; padding: 12px 20px; margin:0px; -webkit-text-size-adjust: 100%; -moz-text-size-adjust: 100%; -ms-text-size-adjust: 100%;">Total Hours</p></td>
						<td style="border-top:1px solid #e5e2e2;">:</td>
						<td><p style="border-top:1px solid #e5e2e2; font-family: Tahoma, Geneva, sans-serif; font-size:14px; color: #555; line-height:20px; padding: 12px 10px; margin:0px; -webkit-text-size-adjust: 100%; -moz-text-size-adjust: 100%; -ms-text-size-adjust: 100%;">{{@@$bookings['no_of_hrs']}}</p></td>
					</tr>
					@endif
					<tr>
						<td><p style="border-top:1px solid #e5e2e2; font-family: Tahoma, Geneva, sans-serif; font-size:14px; color: #555; line-height:20px; padding: 12px 20px; margin:0px; -webkit-text-size-adjust: 100%; -moz-text-size-adjust: 100%; -ms-text-size-adjust: 100%;">Payment Method</p></td>
						<td style="border-top:1px solid #e5e2e2;">:</td>
						<?php $payby=ucfirst(@$bookings['pay_by']);?>
						<td><p style="border-top:1px solid #e5e2e2; font-family: Tahoma, Geneva, sans-serif; font-size:14px; color: #555; line-height:20px; padding: 12px 10px; margin:0px; -webkit-text-size-adjust: 100%; -moz-text-size-adjust: 100%; -ms-text-size-adjust: 100%;">{{@$payby}}</p></td>
					</tr>
					<tr>
						<td><p style="border-top:1px solid #e5e2e2; font-family: Tahoma, Geneva, sans-serif; font-size:14px; color: #555; line-height:20px; padding: 12px 20px; margin:0px; -webkit-text-size-adjust: 100%; -moz-text-size-adjust: 100%; -ms-text-size-adjust: 100%;">Service Fee</p></td>
						<td style="border-top:1px solid #e5e2e2;">:</td>
						<td><p style="border-top:1px solid #e5e2e2; font-family: Tahoma, Geneva, sans-serif; font-size:14px; color: #555; line-height:20px; padding: 12px 10px; margin:0px; -webkit-text-size-adjust: 100%; -moz-text-size-adjust: 100%; -ms-text-size-adjust: 100%;">AED {{number_format(@@$bookings['service_charge'],2,'.','')}}</p></td>
					</tr>
					@if(@$bookings['custom_supplies'] == 1)
					<tr>
						<td><p style="border-top:1px solid #e5e2e2; font-family: Tahoma, Geneva, sans-serif; font-size:14px; color: #555; line-height:20px; padding: 12px 20px; margin:0px; -webkit-text-size-adjust: 100%; -moz-text-size-adjust: 100%; -ms-text-size-adjust: 100%;">Cleaning Supplies</p></td>
						<td style="border-top:1px solid #e5e2e2;">:</td>
						<td><p style="border-top:1px solid #e5e2e2; font-family: Tahoma, Geneva, sans-serif; font-size:14px; color: #555; line-height:20px; padding: 12px 10px; margin:0px; -webkit-text-size-adjust: 100%; -moz-text-size-adjust: 100%; -ms-text-size-adjust: 100%;">AED {{number_format(@@$bookings['custom_supplies_amount'],2,'.','')}}</p></td>
					</tr>
					@endif
					@if(@$bookings['plan_based_supplies'] == 1)
					<tr>
						<td><p style="border-top:1px solid #e5e2e2; font-family: Tahoma, Geneva, sans-serif; font-size:14px; color: #555; line-height:20px; padding: 12px 20px; margin:0px; -webkit-text-size-adjust: 100%; -moz-text-size-adjust: 100%; -ms-text-size-adjust: 100%;">Materials</p></td>
						<td style="border-top:1px solid #e5e2e2;">:</td>
						<td><p style="border-top:1px solid #e5e2e2; font-family: Tahoma, Geneva, sans-serif; font-size:14px; color: #555; line-height:20px; padding: 12px 10px; margin:0px; -webkit-text-size-adjust: 100%; -moz-text-size-adjust: 100%; -ms-text-size-adjust: 100%;">AED {{number_format(@@$bookings['plan_based_supplies_amount'],2,'.','')}}</p></td>
					</tr>
					@endif
					@if(@$bookings['supervisor_selected'] == "Y")
					<tr>
						<td><p style="border-top:1px solid #e5e2e2; font-family: Tahoma, Geneva, sans-serif; font-size:14px; color: #555; line-height:20px; padding: 12px 20px; margin:0px; -webkit-text-size-adjust: 100%; -moz-text-size-adjust: 100%; -ms-text-size-adjust: 100%;">Supervisor Fee</p></td>
						<td style="border-top:1px solid #e5e2e2;">:</td>
						<td><p style="border-top:1px solid #e5e2e2; font-family: Tahoma, Geneva, sans-serif; font-size:14px; color: #555; line-height:20px; padding: 12px 10px; margin:0px; -webkit-text-size-adjust: 100%; -moz-text-size-adjust: 100%; -ms-text-size-adjust: 100%;">AED {{number_format(@@$bookings['supervisor_charge_total'] * @@$bookings['no_of_maids'],2,'.','')}}</p></td>
					</tr>
					@endif
					@if(@$bookings['coupon_id'] != '' && @$bookings['coupon_id'] != NULL && @$bookings['coupon_id'] != 0)
					<tr>
						<td><p style="border-top:1px solid #e5e2e2; font-family: Tahoma, Geneva, sans-serif; font-size:14px; color: #555; line-height:20px; padding: 12px 20px; margin:0px; -webkit-text-size-adjust: 100%; -moz-text-size-adjust: 100%; -ms-text-size-adjust: 100%;">Discount Price</p></td>
						<td style="border-top:1px solid #e5e2e2;">:</td>
						<td><p style="border-top:1px solid #e5e2e2; font-family: Tahoma, Geneva, sans-serif; font-size:14px; color: #555; line-height:20px; padding: 12px 10px; margin:0px; -webkit-text-size-adjust: 100%; -moz-text-size-adjust: 100%; -ms-text-size-adjust: 100%;">AED {{number_format(@@$bookings['discount'],2,'.','')}}</p></td>
					</tr>
					@endif
					<tr>
						<td><p style="border-top:1px solid #e5e2e2; font-family: Tahoma, Geneva, sans-serif; font-size:14px; color: #555; line-height:20px; padding: 12px 20px; margin:0px; -webkit-text-size-adjust: 100%; -moz-text-size-adjust: 100%; -ms-text-size-adjust: 100%;">VAT</p></td>
						<td style="border-top:1px solid #e5e2e2;">:</td>
						<td><p style="border-top:1px solid #e5e2e2; font-family: Tahoma, Geneva, sans-serif; font-size:14px; color: #555; line-height:20px; padding: 12px 10px; margin:0px; -webkit-text-size-adjust: 100%; -moz-text-size-adjust: 100%; -ms-text-size-adjust: 100%;">AED {{number_format(@@$bookings['vat_charge'],2,'.','')}}</p></td>
					</tr>
					@if(@$bookings['pay_by'] == 'Cash' && @@$bookings['pay_by_cash_charge'] > 0)
					<tr>
						<td><p style="border-top:1px solid #e5e2e2; font-family: Tahoma, Geneva, sans-serif; font-size:14px; color: #555; line-height:20px; padding: 12px 20px; margin:0px; -webkit-text-size-adjust: 100%; -moz-text-size-adjust: 100%; -ms-text-size-adjust: 100%;">Convenience Fee</p></td>
						<td style="border-top:1px solid #e5e2e2;">:</td>
						<td><p style="border-top:1px solid #e5e2e2; font-family: Tahoma, Geneva, sans-serif; font-size:14px; color: #555; line-height:20px; padding: 12px 10px; margin:0px; -webkit-text-size-adjust: 100%; -moz-text-size-adjust: 100%; -ms-text-size-adjust: 100%;">AED {{@@$bookings['pay_by_cash_charge']}}</p></td>
					</tr>
					@elseif(@$bookings['pay_by'] == 'Card')
					<tr>
						<td><p style="border-top:1px solid #e5e2e2; font-family: Tahoma, Geneva, sans-serif; font-size:14px; color: #555; line-height:20px; padding: 12px 20px; margin:0px; -webkit-text-size-adjust: 100%; -moz-text-size-adjust: 100%; -ms-text-size-adjust: 100%;">Convenience Fee</p></td>
						<td style="border-top:1px solid #e5e2e2;">:</td>
						<td><p style="border-top:1px solid #e5e2e2; font-family: Tahoma, Geneva, sans-serif; font-size:14px; color: #555; line-height:20px; padding: 12px 10px; margin:0px; -webkit-text-size-adjust: 100%; -moz-text-size-adjust: 100%; -ms-text-size-adjust: 100%;">AED {{number_format($online['transaction_charge'],2,'.','')}}</p></td>
					</tr>
					<?php 
			  $total_amount = $online['transaction_charge'] + @$bookings['total_amount'];
			?>
					<tr style="background: #FFF;">
						<td><p style="border-top:1px solid #e5e2e2; font-family: Tahoma, Geneva, sans-serif; font-size:14px; color: #555; line-height:20px; padding: 12px 20px; margin:0px; -webkit-text-size-adjust: 100%; -moz-text-size-adjust: 100%; -ms-text-size-adjust: 100%;">Net Payable</p></td>
						<td style="border-top:1px solid #e5e2e2;">:</td>
						<td><p style="border-top:1px solid #e5e2e2; font-family: Tahoma, Geneva, sans-serif; font-size:14px; color: #555; line-height:20px; font-weight: bold; padding: 12px 10px; margin:0px; -webkit-text-size-adjust: 100%; -moz-text-size-adjust: 100%; -ms-text-size-adjust: 100%;">AED {{number_format(@$total_amount,2,'.','')}}</p></td>
					</tr>
					@else
					<tr style="background: #FFF;">
						<td><p style="border-top:1px solid #e5e2e2; font-family: Tahoma, Geneva, sans-serif; font-size:14px; color: #555; line-height:20px; padding: 12px 20px; margin:0px; -webkit-text-size-adjust: 100%; -moz-text-size-adjust: 100%; -ms-text-size-adjust: 100%;">Net Payable</p></td>
						<td style="border-top:1px solid #e5e2e2;">:</td>
						<td><p style="border-top:1px solid #e5e2e2; font-family: Tahoma, Geneva, sans-serif; font-size:14px; color: #555; line-height:20px; font-weight: bold; padding: 12px 10px; margin:0px; -webkit-text-size-adjust: 100%; -moz-text-size-adjust: 100%; -ms-text-size-adjust: 100%;">AED {{number_format(@$bookings['total_amount']+(@@$bookings['pay_by_cash_charge'] ?: 0),2,'.','')}}</p></td>
					</tr>
					@endif
				</table>
				<p style="font-family: Tahoma, Geneva, sans-serif; font-size:14px; color: #555; line-height:20px; padding: 12px 0px 15px 20px; margin:0px; -webkit-text-size-adjust: 100%; -moz-text-size-adjust: 100%; -ms-text-size-adjust: 100%;">
					<a href="https://dubaihousekeeping.com/terms-conditions/">Click here </a>to read standard terms & conditions.
				</p>
			</div>
			
			<div style="width: 600px; height:auto; padding: 20px 0px 0px 0px; margin-bottom: 30px; text-align: center;">
				<div style="width: 100%; text-align: center;"><img src="{{asset('images/dhk-logo.png')}}?v={{Config::get('version.mail_img')}}" width="300" height="33" alt="" style="margin:0 auto;" /></div>
				<p style="font-family: Tahoma, Geneva, sans-serif; font-size:14px; color: #555; line-height:20px; padding: 5px 0px 15px 0px; margin:0px; -webkit-text-size-adjust: 100%; -moz-text-size-adjust: 100%; -ms-text-size-adjust: 100%;">
					@include('emails.includes.address_html_footer')
				</p>
			</div>
			
		</div>
		
		<div style="width: 740px; height:auto; padding: 0px 30px; background: #1cac51; text-align: center;">
			<p style="font-family: Tahoma, Geneva, sans-serif; font-size:12px; color: #FFF; line-height:20px; padding: 12px 0px 10px 0px; margin:0px; -webkit-text-size-adjust: 100%; -moz-text-size-adjust: 100%; -ms-text-size-adjust: 100%;">© <?php echo date('Y'); ?> Dubai Housekeeping All Rights Reserved.</p>
		</div>
		
	</div>
</div>
</body>
</html>
<?php die(); ?>