<div class="content-section2" style="width: 700px; height: auto; margin: 30px auto 0;background-color:#fff;">
    <table width="100%" cellpadding="0" cellspacing="0">
        <tr>
            <td style="border: 1px solid #ddd; border-right: 0px">
                <p
                    style="font: normal 14px/18px 'Poppins', sans-serif; color: #555; display: block; margin: 0px; padding: 15px 20px; text-align: left; -webkit-text-size-adjust: 100%; -moz-text-size-adjust: 100%; -ms-text-size-adjust: 100%;">
                    Frequency<br />
                    <label
                        style="font-size: 18px; line-height: 25px; display: block; margin: 0px; padding: 2px 0px 0px;">{{ $booking->frequency }}</label>
                </p>
            </td>
            <td style="border: 1px solid #ddd; border-right: 0px">
                <p
                    style="font: normal 14px/18px 'Poppins', sans-serif; color: #555; display: block; margin: 0px; padding: 15px 20px; text-align: left; -webkit-text-size-adjust: 100%; -moz-text-size-adjust: 100%; -ms-text-size-adjust: 100%;">
                    No. of Professionals<br />
                    <label
                        style="font-size: 18px; line-height: 25px; display: block; margin: 0px; padding: 2px 0px 0px;">{{ sizeof($bookings) }}</label>
                </p>
            </td>
            <td style="border: 1px solid #ddd;">
                <p
                    style="font: normal 14px/18px 'Poppins', sans-serif; color: #555; display: block; margin: 0px; padding: 15px 20px; text-align: left; -webkit-text-size-adjust: 100%; -moz-text-size-adjust: 100%; -ms-text-size-adjust: 100%;">
                    Cleaning Material<br />
                    <label
                        style="font-size: 18px; line-height: 25px; display: block; margin: 0px; padding: 2px 0px 0px;">{{ $booking->cleaning_material_opted }}</label>
                </p>
            </td>
        </tr>
        @if ($subscription_package)
            <tr>
                <td style="border: 1px solid #ddd;" colspan="3">
                    <p
                        style="font: normal 14px/18px 'Poppins', sans-serif; color: #555; display: block; margin: 0px; padding: 15px 20px; text-align: left; -webkit-text-size-adjust: 100%; -moz-text-size-adjust: 100%; -ms-text-size-adjust: 100%;">
                        Subscription Package<br />
                        <label
                            style="font-size: 18px; line-height: 25px; display: block; margin: 0px; padding: 2px 0px 0px;">{{ $subscription_package->package_name }}</label>
                    </p>
                </td>
            </tr>
        @endif
        @if ($weekdays)
            @php
                $last_day = array_pop($weekdays);
                $weekdays_text = $weekdays ? implode(', ', $weekdays) . ' and ' . $last_day : $last_day;
            @endphp
            <tr>
                <td style="border: 1px solid #ddd;">
                    <p
                        style="font: normal 14px/18px 'Poppins', sans-serif;font-weight:bold; color: #555; display: block; margin: 0px; padding: 15px 20px; text-align: left; -webkit-text-size-adjust: 100%; -moz-text-size-adjust: 100%; -ms-text-size-adjust: 100%;">
                        Week Days
                    </p>
                </td>
                <td style="border: 1px solid #ddd;" colspan="2">
                    <p
                        style="font: normal 14px/18px 'Poppins', sans-serif; color: #555; display: block; margin: 0px; padding: 15px 20px; -webkit-text-size-adjust: 100%; -moz-text-size-adjust: 100%; -ms-text-size-adjust: 100%;">
                        {{ $weekdays_text }}
                    </p>
                </td>
            </tr>
        @endif
        @if ($booking->crew_in)
            <tr>
                <td style="border: 1px solid #ddd;">
                    <p
                        style="font: normal 14px/18px 'Poppins', sans-serif;font-weight:bold; color: #555; display: block; margin: 0px; padding: 15px 20px; text-align: left; -webkit-text-size-adjust: 100%; -moz-text-size-adjust: 100%; -ms-text-size-adjust: 100%;">
                        Crew In
                    </p>
                </td>
                <td style="border: 1px solid #ddd;" colspan="2">
                    <p
                        style="font: normal 14px/18px 'Poppins', sans-serif; color: #555; display: block; margin: 0px; padding: 15px 20px; -webkit-text-size-adjust: 100%; -moz-text-size-adjust: 100%; -ms-text-size-adjust: 100%;">
                        {{ $booking->crew_in }}
                    </p>
                </td>
            </tr>
        @endif
        @if (trim($booking->booking_note) != '')
            <tr>
                <td style="border: 1px solid #ddd;" colspan="3">
                    <p
                        style="font: normal 15px/18px 'Poppins', sans-serif;font-weight:bold; color: #555; display: block; margin: 0px; padding: 15px 20px; text-align: left; -webkit-text-size-adjust: 100%; -moz-text-size-adjust: 100%; -ms-text-size-adjust: 100%;">
                        Instructions<br />
                        <label
                            style="font-size: 14px; line-height: 25px;font-weight:normal; display: block; margin: 0px; padding: 2px 0px 0px;color: #9e1717;">{{ $booking->booking_note }}</label>
                    </p>
                </td>
            </tr>
        @endif
        @if ($booking->supervisor_selected == 'Y')
            <tr>
                <td style="border: 1px solid #ddd;">
                    <p
                        style="font: normal 14px/18px 'Poppins', sans-serif;font-weight:bold; color: #555; display: block; margin: 0px; padding: 15px 20px; text-align: left; -webkit-text-size-adjust: 100%; -moz-text-size-adjust: 100%; -ms-text-size-adjust: 100%;">
                        Supervisor
                    </p>
                </td>
                <td style="border: 1px solid #ddd;">
                    <p
                        style="font: normal 14px/18px 'Poppins', sans-serif; color: #555; display: block; margin: 0px; padding: 15px 20px; text-align: center; -webkit-text-size-adjust: 100%; -moz-text-size-adjust: 100%; -ms-text-size-adjust: 100%;">
                        YES
                    </p>
                </td>
                <td style="border: 1px solid #ddd;">
                    <p
                        style="font: normal 14px/18px 'Poppins', sans-serif; color: #555; display: block; margin: 0px; padding: 15px 20px; text-align: right; -webkit-text-size-adjust: 100%; -moz-text-size-adjust: 100%; -ms-text-size-adjust: 100%;">
                        {{ $booking->_supervisor_amount }}
                    </p>
                </td>
            </tr>
        @endif
        @if (sizeof($packages))
            <tr>
                <td style="border: 1px solid #ddd;padding:20px;" colspan="3">
                    <p
                        style="font: normal 14px/18px 'Poppins', sans-serif; font-weight:bold; color: #555; display: block; margin: 0px; padding: 5px 0px 15px 0px; text-align: left; -webkit-text-size-adjust: 100%; -moz-text-size-adjust: 100%; -ms-text-size-adjust: 100%;">
                        Packages
                    </p>
                    <table style="width: 100%;text-align:left;">
                        <tr style="font: normal 14px/18px 'Poppins', sans-serif;">
                            <th style="border: 1px solid #ecebeb;"><label
                                    style="font-size: 14px/18px; line-height: 25px; display: block; margin: 0px; padding: 2px 10px 0px;color: #555;">Name</label>
                            </th>
                            <th style="border: 1px solid #ecebeb;text-align:right;"><label
                                    style="font-size: 14px/18px; line-height: 25px; display: block; margin: 0px; padding: 2px 10px 0px;color: #555;">Rate</label>
                            </th>
                            <th style="border: 1px solid #ecebeb;text-align:right;"><label
                                    style="font-size: 14px/18px; line-height: 25px; display: block; margin: 0px; padding: 2px 10px 0px;color: #555;">Quantity</label>
                            </th>
                            <th style="border: 1px solid #ecebeb;text-align:right;"><label
                                    style="font-size: 14px/18px; line-height: 25px; display: block; margin: 0px; padding: 2px 10px 0px;color: #555;">Total</label>
                            </th>
                        </tr>
                        @foreach ($packages as $package)
                            <tr style="font: normal 14px/18px 'Poppins', sans-serif;">
                                <td style="border: 1px solid #ecebeb;"><label
                                        style="font-size: 14px/18px; line-height: 25px; display: block; margin: 0px; padding: 2px 10px 0px;color: #555;">{{ $package->name }}</label>
                                </td>
                                <td style="border: 1px solid #ecebeb;text-align:right;"><label
                                        style="font-size: 14px/18px; line-height: 25px; display: block; margin: 0px; padding: 2px 10px 0px;color: #555;">{{ $package->unit_price }}</label>
                                </td>
                                <td style="border: 1px solid #ecebeb;text-align:right;"><label
                                        style="font-size: 14px/18px; line-height: 25px; display: block; margin: 0px; padding: 2px 10px 0px;color: #555;">{{ $package->quantity }}</label>
                                </td>
                                <td style="border: 1px solid #ecebeb;text-align:right;"><label
                                        style="font-size: 14px/18px; line-height: 25px; display: block; margin: 0px; padding: 2px 10px 0px;color: #555;">{{ $package->total }}</label>
                                </td>
                            </tr>
                        @endforeach
                    </table>
                </td>
            </tr>
        @endif
        @if (sizeof($cleaning_supplies))
            <tr>
                <td style="border: 1px solid #ddd;padding:20px;" colspan="3">
                    <p
                        style="font: normal 14px/18px 'Poppins', sans-serif; font-weight:bold; color: #555; display: block; margin: 0px; padding: 5px 0px 15px 0px; text-align: left; -webkit-text-size-adjust: 100%; -moz-text-size-adjust: 100%; -ms-text-size-adjust: 100%;">
                        Supplies / Tools
                    </p>
                    <table style="width: 100%;text-align:left;">
                        <tr style="font: normal 14px/18px 'Poppins', sans-serif;">
                            <th style="border: 1px solid #ecebeb;"><label
                                    style="font-size: 14px/18px; line-height: 25px; display: block; margin: 0px; padding: 2px 10px 0px;color: #555;">Name</label>
                            </th>
                            <th style="border: 1px solid #ecebeb;text-align:right;"><label
                                    style="font-size: 14px/18px; line-height: 25px; display: block; margin: 0px; padding: 2px 10px 0px;color: #555;">Rate</label>
                            </th>
                            <th style="border: 1px solid #ecebeb;text-align:right;"><label
                                    style="font-size: 14px/18px; line-height: 25px; display: block; margin: 0px; padding: 2px 10px 0px;color: #555;">Quantity</label>
                            </th>
                            <th style="border: 1px solid #ecebeb;text-align:right;"><label
                                    style="font-size: 14px/18px; line-height: 25px; display: block; margin: 0px; padding: 2px 10px 0px;color: #555;">Total</label>
                            </th>
                        </tr>
                        @foreach ($cleaning_supplies as $cleaning_supply)
                            <tr style="font: normal 14px/18px 'Poppins', sans-serif;">
                                <td style="border: 1px solid #ecebeb;"><label
                                        style="font-size: 14px/18px; line-height: 25px; display: block; margin: 0px; padding: 2px 10px 0px;color: #555;">{{ $cleaning_supply->name }}</label>
                                </td>
                                <td style="border: 1px solid #ecebeb;text-align:right;"><label
                                        style="font-size: 14px/18px; line-height: 25px; display: block; margin: 0px; padding: 2px 10px 0px;color: #555;">{{ $cleaning_supply->unit_rate }}</label>
                                </td>
                                <td style="border: 1px solid #ecebeb;text-align:right;"><label
                                        style="font-size: 14px/18px; line-height: 25px; display: block; margin: 0px; padding: 2px 10px 0px;color: #555;">{{ $cleaning_supply->quantity }}</label>
                                </td>
                                <td style="border: 1px solid #ecebeb;text-align:right;"><label
                                        style="font-size: 14px/18px; line-height: 25px; display: block; margin: 0px; padding: 2px 10px 0px;color: #555;">{{ $cleaning_supply->total_amount }}</label>
                                </td>
                            </tr>
                        @endforeach
                    </table>
                </td>
            </tr>
        @endif
        @if (sizeof($booking_answers))
            <tr>
                <td style="border: 1px solid #ddd;padding:20px;" colspan="3">
                    <p
                        style="font: normal 14px/18px 'Poppins', sans-serif;font-weight:bold;color: #555; display: block; margin: 0px; padding: 5px 0px 15px 0px; text-align: left; -webkit-text-size-adjust: 100%; -moz-text-size-adjust: 100%; -ms-text-size-adjust: 100%;">
                        Details
                    </p>
                    <table style="width: 100%;">
                        @foreach ($booking_answers as $question_answers)
                            <tr style="font: normal 14px/18px 'Poppins', sans-serif;">
                                <td style="border: 1px solid #ecebeb;"><label
                                        style="font-size: 14px/18px; line-height: 25px; display: block; margin: 0px; padding: 2px 10px 0px;color: #555;">{{ $question_answers->question }}</label>
                                </td>
                                <td style="border: 1px solid #ecebeb;"><label
                                        style="font-size: 14px/18px; line-height: 25px; display: block; margin: 0px; padding: 2px 10px 0px;color: #555;">{{ $question_answers->answer }}</label>
                                </td>
                            </tr>
                        @endforeach
                    </table>
                </td>
            </tr>
        @endif
        @if (sizeof($extra_services))
            <tr>
                <td style="border: 1px solid #ddd;padding:20px;" colspan="3">
                    <p
                        style="font: normal 14px/18px 'Poppins', sans-serif; font-weight:bold; color: #555; display: block; margin: 0px; padding: 5px 0px 15px 0px; text-align: left; -webkit-text-size-adjust: 100%; -moz-text-size-adjust: 100%; -ms-text-size-adjust: 100%;">
                        Extra Services
                    </p>
                    <table style="width: 100%;text-align:left;">
                        <!--<tr style="font: normal 14px/18px 'Poppins', sans-serif;">
                            <td style="border: 1px solid #ecebeb;"><label
                                    style="font-size: 14px/18px; line-height: 25px; display: block; margin: 0px; padding: 2px 10px 0px;color: #555;">Name</label>
                            </td>
                            <td style="border: 1px solid #ecebeb;text-align:right;"><label
                                    style="font-size: 14px/18px; line-height: 25px; display: block; margin: 0px; padding: 2px 10px 0px;color: #555;">Rate</label>
                            </td>
                            <td style="border: 1px solid #ecebeb;text-align:right;"><label
                                    style="font-size: 14px/18px; line-height: 25px; display: block; margin: 0px; padding: 2px 10px 0px;color: #555;">Quantity</label>
                            </td>
                            <td style="border: 1px solid #ecebeb;text-align:right;"><label
                                    style="font-size: 14px/18px; line-height: 25px; display: block; margin: 0px; padding: 2px 10px 0px;color: #555;">Total</label>
                            </td>
                        </tr>-->
                        @foreach ($extra_services as $extra_service)
                            <tr style="font: normal 14px/18px 'Poppins', sans-serif;">
                                <td style="border: 1px solid #ecebeb;"><label
                                        style="font-size: 14px/18px; line-height: 25px; display: block; margin: 0px; padding: 2px 10px 0px;color: #555;">{{ $extra_service->service }}</label>
                                </td>
                                <!--<td style="border: 1px solid #ecebeb;text-align:right;"><label
                                        style="font-size: 14px/18px; line-height: 25px; display: block; margin: 0px; padding: 2px 10px 0px;color: #555;">{{ $extra_service->unit_rate }}</label>
                                </td>
                                <td style="border: 1px solid #ecebeb;text-align:right;"><label
                                        style="font-size: 14px/18px; line-height: 25px; display: block; margin: 0px; padding: 2px 10px 0px;color: #555;">{{ $extra_service->quantity }}</label>
                                </td>
                                <td style="border: 1px solid #ecebeb;text-align:right;"><label
                                        style="font-size: 14px/18px; line-height: 25px; display: block; margin: 0px; padding: 2px 10px 0px;color: #555;">{{ $extra_service->total_amount }}</label>
                                </td>-->
                            </tr>
                        @endforeach
                    </table>
                </td>
            </tr>
        @endif
    </table>
</div>
