<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class DayServices extends Model
{
    use HasFactory;
    protected $table = 'day_services';
    public $timestamps = false;
    protected $primaryKey = 'day_service_id';
}
