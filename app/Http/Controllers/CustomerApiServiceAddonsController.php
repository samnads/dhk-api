<?php

namespace App\Http\Controllers;

use App\Models\Settings;
use Config;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Response;

class CustomerApiServiceAddonsController extends Controller
{
    public function service_addons(Request $request)
    {
        $settings = Settings::first();
        $debug = toggleDebug(); // pass boolean to overide default
        /************************************************************* */
        if (!$debug) {
            // live input
            $data = $request->getContent();
        } else {
            // test input
            $data = '{"params":{"service_type_id": 1}}';
        }
        $data = json_decode($data, true);
        /************************************************************* */
        // required input check
        $input = @$data['params'];
        $validator = Validator::make((array) $input,
            [
                'service_type_id' => 'required|integer',
            ],
            [],
            [
                'service_type_id' => 'Service Type ID',
            ]
        );
        if ($validator->fails()) {
            return Response::json(array('result' => array('status' => 'failed', 'message' => $validator->errors()->first()), 'debug' => $debug), 200, array(), customerResponseJsonConstants());
        }
        /************************************************************* */
        try {
            $service_type = DB::table('service_types as st')
                ->select(
                    'st.service_type_name as service_type'
                )
                ->where([['st.service_type_id', "=", $input['service_type_id']]])
                ->first();
            if (!$service_type) {
                throw new \Exception('Service not found.');
            }
            $response['status'] = 'success';
            $response['service_type_id'] = $input['service_type_id'];
            $response['service_type'] = $service_type->service_type;
            $response['service_addons'] = DB::table('service_addons as sao')
                ->select(
                    'sao.service_addons_id',
                    'sao.service_addon_name',
                    'sao.service_addon_description',
                    DB::raw('"' . Config::get('values.currency_code') . '" as currency'),
                    'sao.service_minutes as additional_minutes',
                    'sao.strike_amount',
                    'sao.amount',
                    'sao.populariry',
                    'sao.cart_limit',
                    DB::raw('CONCAT("' . Config::get('values.customer_app_assets_prefix_url') . 'service-addon-images/",IFNULL(sao.customer_app_thumbnail,"service-addons-thumbnail.png"),"?v='.$settings->customer_app_img_version.'") as image_url'),
                )
                ->where([['sao.service_type_id', "=", $input['service_type_id']]])
                ->where([['sao.deleted_at', "=", null]])
                ->orderBy('sao.populariry', 'DESC')
                ->get();
            /******************************************************** */
            $service_addon_points_all = DB::table('service_addons_points as sap')
                ->select(
                    'sao.service_type_id',
                    'sap.service_addons_id',
                    'sap.service_addons_id',
                    'sap.point_html',
                    'sap.point_type'
                )
                ->leftJoin('service_addons as sao', 'sap.service_addons_id', 'sao.service_addons_id')
                ->where([['sap.deleted_at', "=", null]])
                ->orderBy('sap.sort_order_id', 'ASC')
                ->get()->toArray();
            /******************************************************** */
            // filter points by service id
            $service_addon_points = array_filter($service_addon_points_all, function ($point) use ($input) {
                return $point->service_type_id == $input['service_type_id'];
            });
            /******************************************************** */
            // filter points by type
            $what_included_points = array_filter($service_addon_points, function ($point) use ($input) {
                return $point->point_type === "WHAT_INCLUDED";
            });
            /******************************************************** */
            // filter points by type
            $what_excluded_points = array_filter($service_addon_points, function ($point) use ($input) {
                return $point->point_type === "WHAT_EXCLUDED";
            });
            /******************************************************** */
            foreach ($response['service_addons'] as $key => $addon) {
                $response['service_addons'][$key]->what_included = array_column(array_filter($what_included_points, function ($point) use ($addon) {
                    // filter points by addon
                    return $point->service_addons_id == $addon->service_addons_id;
                }), 'point_html');
                $response['service_addons'][$key]->what_excluded = array_column(array_filter($what_excluded_points, function ($point) use ($addon) {
                    // filter points by addon
                    return $point->service_addons_id == $addon->service_addons_id;
                }), 'point_html');
            }
            /******************************************************** */
            $response['message'] = sizeof($response['service_addons']) ? 'Service Addons fetched successfully.' : "No Service Addons svailable.";
            return Response::json(array('result' => $response, 'debug' => $debug), 200, array(), customerResponseJsonConstants());
        } catch (\Exception $e) {
            return Response::json(array('result' => array('status' => 'failed', 'message' => $e->getMessage()), 'debug' => $debug), 200, array(), customerResponseJsonConstants());
        }
    }
}
