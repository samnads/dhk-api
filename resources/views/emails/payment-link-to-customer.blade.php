@extends('emails.layouts.main')
@section('content')
    @include('emails.includes.section-message')
    @include('emails.includes.payment-link-summary')
    @include('emails.includes.payment-button')
    @include('emails.includes.section-terms-conditions')
    @include('emails.includes.section-footer-address')
@endsection
