@extends('emails.layouts.main')
@section('content')
    @include('emails.includes.section-message')
    @include('emails.includes.section-assistance')
    @include('emails.includes.section-footer-banner')
    @include('emails.includes.section-app-links')
    @include('emails.includes.section-footer-links')
@endsection
