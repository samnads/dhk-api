<?php
use App\Models\Customer;
use App\Models\CustomerAddress;

function telrCreateOrder($bookings, $customer, $customer_address, $total_payable, $platform)
{
    /***************************************************** */
    $customer_name = $customer->customer_name;
    $names = explode(" ", $customer_name);
    if (sizeof($names) == 1) {
        $customer_name = $customer->customer_name . ' ' . $customer->customer_name;
        $names = explode(" ", $customer_name);
    }
    /***************************************************** */
    $telr_urls = [
        //"authorised" => Config::get('values.web_app_url') . ('api/telr/process/' . $bookings[0]->reference_id . '/' . $bookings[0]->booking_id.'?platform='. $platform),
        "authorised" => url('api/telr/process/' . $bookings[0]->reference_id . '/' . $bookings[0]->booking_id . '?platform=' . $platform),
        "declined" => Config::get('values.web_app_url') . 'booking/failed/' . $bookings[0]->reference_id . '?reference_id=' . $bookings[0]->reference_id . '&payment_method=Card&status=Declined',
        "cancelled" => Config::get('values.web_app_url') . 'booking/failed/' . $bookings[0]->reference_id . '?reference_id=' . $bookings[0]->reference_id . '&payment_method=Card&status=Cancelled'
    ];
    /***************************************************** */
    $params = [
        'method' => 'create',
        'store' => Config::get('values.telr_store_id'),
        'authkey' => Config::get('values.telr_auth_key'),
        "ivp_applepay" => "0",
        "order" => [
            "cartid" => $bookings[0]->booking_id,
            "test" => Config::get('values.telr_test_environment'),
            "amount" => $total_payable,
            "currency" => "AED",
            "description" => $bookings[0]->reference_id,
            "trantype" => "sale"
        ],
        "customer" => [
            "ref" => $customer->customer_id,
            "email" => $customer->email_address,
            "name" => [
                "title" => null,
                "forenames" => $customer_name,
                "surname" => $customer->customer_name
            ],
            "address" => [
                "line1" => $customer_address->customer_address,
                "city" => 'United Arab Emirates',
                "country" => "AE"
            ],
            "phone" => $customer->mobile_number_1
        ],
        "bill_custref" => $customer->customer_id,
        "bill_email" => $customer->email_address,
        "bill_country" => "AE",
        "bill_addr1" => $customer_address->customer_address,
        "bill_fname" => $customer->customer_name,
        "bill_sname" => $customer->customer_name,
        "extra" => [
            "extra1" => $platform,
            "extra2" => null,
            "extra3" => null
        ],
        "return" => $telr_urls
    ];
    $client = new \GuzzleHttp\Client(['verify' => false]);
    $response_telr = $client->request('POST', 'https://secure.telr.com/gateway/order.json', [
        'body' => json_encode($params),
        'headers' => [
            'Content-Type' => 'application/json',
            'accept' => 'application/json',
        ],
    ]);
    $response = json_decode((string) $response_telr->getBody(), true);
    //$response['url'] = $telr_urls;
    return $response;
}
function telrCreateOrderForInvoice($customer_id, $order_id, $amount, $platform)
{
    $customer = Customer::where('customer_id', '=', $customer_id)->first();
    $customer_address = CustomerAddress::where('customer_id', $customer_id)->where('default_address', 1)->where('address_status', 0)->first();
    /***************************************************** */
    $customer_name = $customer->customer_name;
    $names = explode(" ", $customer_name);
    if (sizeof($names) == 1) {
        $customer_name = $customer->customer_name . ' ' . $customer->customer_name;
        $names = explode(" ", $customer_name);
    }
    /***************************************************** */
    $params = [
        'method' => 'create',
        'store' => Config::get('values.telr_store_id'),
        'authkey' => Config::get('values.telr_auth_key'),
        "ivp_applepay" => "0",
        "order" => [
            "cartid" => $order_id,
            "test" => Config::get('values.telr_test_environment'),
            "amount" => $amount,
            "currency" => "AED",
            "description" => $order_id,
            "trantype" => "sale"
        ],
        "customer" => [
            "ref" => $customer->customer_id,
            "email" => $customer->email_address,
            "name" => [
                "title" => null,
                "forenames" => $customer_name,
                "surname" => $customer->customer_name
            ],
            "address" => [
                "line1" => $customer_address->customer_address,
                "city" => 'United Arab Emirates',
                "country" => "AE"
            ],
            "phone" => $customer->mobile_number_1
        ],
        "bill_custref" => $customer->customer_id,
        "bill_email" => $customer->email_address,
        "bill_country" => "AE",
        "bill_addr1" => $customer_address->customer_address,
        "bill_fname" => $customer->customer_name,
        "bill_sname" => $customer->customer_name,
        "extra" => [
            "extra1" => $platform,
            "extra2" => null,
            "extra3" => null
        ],
        "return" => [
            "authorised" => Config::get('values.web_app_url') . ('api/telr/invoice-process/' . $order_id),
            "declined" => Config::get('values.web_app_url') . ('invoice-payment/failed/' . $order_id),
            "cancelled" => Config::get('values.web_app_url') . ('invoice-payment/failed/' . $order_id),
        ]
    ];
    $client = new \GuzzleHttp\Client(['verify' => false]);
    $response_telr = $client->request('POST', 'https://secure.telr.com/gateway/order.json', [
        'body' => json_encode($params),
        'headers' => [
            'Content-Type' => 'application/json',
            'accept' => 'application/json',
        ],
    ]);
    return json_decode((string) $response_telr->getBody(), true);
}
function telrCreateOrderForOnlinePayment($customer_id, $order_id, $amount, $platform)
{
    $customer = Customer::where('customer_id', '=', $customer_id)->first();
    $customer_address = CustomerAddress::where('customer_id', $customer_id)->where('default_address', 1)->where('address_status', 0)->first();
    /***************************************************** */
    $customer_name = $customer->customer_name;
    $names = explode(" ", $customer_name);
    if (sizeof($names) == 1) {
        $customer_name = $customer->customer_name . ' ' . $customer->customer_name;
        $names = explode(" ", $customer_name);
    }
    /***************************************************** */
    $params = [
        'method' => 'create',
        'store' => Config::get('values.telr_store_id'),
        'authkey' => Config::get('values.telr_auth_key'),
        "ivp_applepay" => "0",
        "order" => [
            "cartid" => $order_id,
            "test" => Config::get('values.telr_test_environment'),
            "amount" => $amount,
            "currency" => "AED",
            "description" => $order_id,
            "trantype" => "sale"
        ],
        "customer" => [
            "ref" => $customer->customer_id,
            "email" => $customer->email_address,
            "name" => [
                "title" => null,
                "forenames" => $customer_name,
                "surname" => $customer->customer_name
            ],
            "address" => [
                "line1" => $customer_address->customer_address,
                "city" => 'United Arab Emirates',
                "country" => "AE"
            ],
            "phone" => $customer->mobile_number_1
        ],
        "bill_custref" => $customer->customer_id,
        "bill_email" => $customer->email_address,
        "bill_country" => "AE",
        "bill_addr1" => $customer_address->customer_address,
        "bill_fname" => $customer->customer_name,
        "bill_sname" => $customer->customer_name,
        "extra" => [
            "extra1" => $platform,
            "extra2" => null,
            "extra3" => null
        ],
        "return" => [
            "authorised" => Config::get('values.web_app_url') . ('api/telr/online-payment-process/' . $order_id),
            "declined" => Config::get('values.web_app_url') . ('online-payment/failed/' . $order_id),
            "cancelled" => Config::get('values.web_app_url') . ('online-payment/failed/' . $order_id),
        ]
    ];
    $client = new \GuzzleHttp\Client(['verify' => false]);
    $response_telr = $client->request('POST', 'https://secure.telr.com/gateway/order.json', [
        'body' => json_encode($params),
        'headers' => [
            'Content-Type' => 'application/json',
            'accept' => 'application/json',
        ],
    ]);
    return json_decode((string) $response_telr->getBody(), true);
}
function telrCreateOrderApplePayForInvoice($customer_id, $order_id, $amount, $platform)
{
    $customer = Customer::where('customer_id', '=', $customer_id)->first();
    $customer_address = CustomerAddress::where('customer_id', $customer_id)->where('default_address', 1)->where('address_status', 0)->first();
    /***************************************************** */
    $customer_name = $customer->customer_name;
    $names = explode(" ", $customer_name);
    if (sizeof($names) == 1) {
        $customer_name = $customer->customer_name . ' ' . $customer->customer_name;
        $names = explode(" ", $customer_name);
    }
    /***************************************************** */
    $params = [
        'method' => 'create',
        'store' => Config::get('values.telr_store_id'),
        'authkey' => Config::get('values.telr_auth_key'),
        "ivp_framed" => "0",
        "ivp_applepay" => "1",
        'ivp_method' => 'create',
        'ivp_store' => Config::get('values.telr_store_id'),
        'ivp_authkey' => Config::get('values.telr_auth_key'),
        'ivp_amount' => $amount,
        "ivp_test" => Config::get('values.telr_test_environment'),
        "ivp_desc" => $order_id,
        "ivp_cart" => $order_id,
        "ivp_lang" => "en",
        "ivp_tranclass" => "ecom",
        "ivp_currency" => "AED",
        "order" => [
            "cartid" => $order_id,
            "test" => Config::get('values.telr_test_environment'),
            "amount" => $amount,
            "currency" => "AED",
            "description" => $order_id,
            "trantype" => "sale"
        ],
        "customer" => [
            "ref" => $customer->customer_id,
            "email" => $customer->email_address,
            "name" => [
                "title" => null,
                "forenames" => $customer_name,
                "surname" => $customer->customer_name
            ],
            "address" => [
                "line1" => $customer_address->customer_address,
                "city" => 'United Arab Emirates',
                "country" => "AE"
            ],
            "phone" => $customer->mobile_number_1
        ],
        "bill_custref" => $customer->customer_id,
        "bill_email" => $customer->email_address,
        "bill_country" => "AE",
        "bill_addr1" => $customer_address->customer_address,
        "bill_fname" => $customer->customer_name,
        "bill_sname" => $customer->customer_name,
        "extra" => [
            "extra1" => $platform,
            "extra2" => null,
            "extra3" => null
        ],
        "return" => [
            "authorised" => Config::get('values.web_app_url') . ('api/telr/invoice-process/' . $order_id),
            "declined" => Config::get('values.web_app_url') . ('invoice-payment/failed/' . $order_id),
            "cancelled" => Config::get('values.web_app_url') . ('invoice-payment/failed/' . $order_id),
        ]
    ];
    $client = new \GuzzleHttp\Client(['verify' => false]);
    $response_telr = $client->request('POST', 'https://secure.telr.com/gateway/order.json', [
        'body' => json_encode($params),
        'headers' => [
            'Content-Type' => 'application/json',
            'accept' => 'application/json',
        ],
    ]);
    return json_decode((string) $response_telr->getBody(), true);
}
function telrCreateOrderApplePayForOnlinePayment($customer_id, $order_id, $amount, $platform)
{
    $customer = Customer::where('customer_id', '=', $customer_id)->first();
    $customer_address = CustomerAddress::where('customer_id', $customer_id)->where('default_address', 1)->where('address_status', 0)->first();
    /***************************************************** */
    $customer_name = $customer->customer_name;
    $names = explode(" ", $customer_name);
    if (sizeof($names) == 1) {
        $customer_name = $customer->customer_name . ' ' . $customer->customer_name;
        $names = explode(" ", $customer_name);
    }
    /***************************************************** */
    $params = [
        'method' => 'create',
        'store' => Config::get('values.telr_store_id'),
        'authkey' => Config::get('values.telr_auth_key'),
        "ivp_framed" => "0",
        "ivp_applepay" => "1",
        'ivp_method' => 'create',
        'ivp_store' => Config::get('values.telr_store_id'),
        'ivp_authkey' => Config::get('values.telr_auth_key'),
        'ivp_amount' => $amount,
        "ivp_test" => Config::get('values.telr_test_environment'),
        "ivp_desc" => $order_id,
        "ivp_cart" => $order_id,
        "ivp_lang" => "en",
        "ivp_tranclass" => "ecom",
        "ivp_currency" => "AED",
        "order" => [
            "cartid" => $order_id,
            "test" => Config::get('values.telr_test_environment'),
            "amount" => $amount,
            "currency" => "AED",
            "description" => $order_id,
            "trantype" => "sale"
        ],
        "customer" => [
            "ref" => $customer->customer_id,
            "email" => $customer->email_address,
            "name" => [
                "title" => null,
                "forenames" => $customer_name,
                "surname" => $customer->customer_name
            ],
            "address" => [
                "line1" => $customer_address->customer_address,
                "city" => 'United Arab Emirates',
                "country" => "AE"
            ],
            "phone" => $customer->mobile_number_1
        ],
        "bill_custref" => $customer->customer_id,
        "bill_email" => $customer->email_address,
        "bill_country" => "AE",
        "bill_addr1" => $customer_address->customer_address,
        "bill_fname" => $customer->customer_name,
        "bill_sname" => $customer->customer_name,
        "extra" => [
            "extra1" => $platform,
            "extra2" => null,
            "extra3" => null
        ],
        "return" => [
            "authorised" => Config::get('values.web_app_url') . ('api/telr/online-payment-process/' . $order_id),
            "declined" => Config::get('values.web_app_url') . ('online-payment/failed/' . $order_id),
            "cancelled" => Config::get('values.web_app_url') . ('online-payment/failed/' . $order_id),
        ]
    ];
    $client = new \GuzzleHttp\Client(['verify' => false]);
    $response_telr = $client->request('POST', 'https://secure.telr.com/gateway/order.json', [
        'body' => json_encode($params),
        'headers' => [
            'Content-Type' => 'application/json',
            'accept' => 'application/json',
        ],
    ]);
    return json_decode((string) $response_telr->getBody(), true);
}
function telrCreateOrderApplePay($bookings, $customer, $customer_address, $total_payable, $platform)
{
    /***************************************************** */
    $customer_name = $customer->customer_name;
    $names = explode(" ", $customer_name);
    if (sizeof($names) == 1) {
        $customer_name = $customer->customer_name . ' ' . $customer->customer_name;
        $names = explode(" ", $customer_name);
    }
    /***************************************************** */
    $telr_urls = [
        //"authorised" => Config::get('values.web_app_url') . ('api/telr/process/' . $bookings[0]->reference_id . '/' . $bookings[0]->booking_id.'?platform='. $platform),
        "authorised" => url('api/telr/process/' . $bookings[0]->reference_id . '/' . $bookings[0]->booking_id . '?platform=' . $platform),
        "declined" => Config::get('values.web_app_url') . 'booking/failed/' . $bookings[0]->reference_id . '?reference_id=' . $bookings[0]->reference_id . '&payment_method=Card&status=Declined',
        "cancelled" => Config::get('values.web_app_url') . 'booking/failed/' . $bookings[0]->reference_id . '?reference_id=' . $bookings[0]->reference_id . '&payment_method=Card&status=Cancelled'
    ];
    /***************************************************** */
    $params = [
        'method' => 'create',
        'store' => Config::get('values.telr_store_id'),
        'authkey' => Config::get('values.telr_auth_key'),
        "ivp_framed" => "0",
        "ivp_applepay" => "1",
        'ivp_method' => 'create',
        'ivp_store' => Config::get('values.telr_store_id'),
        'ivp_authkey' => Config::get('values.telr_auth_key'),
        'ivp_amount' => $total_payable,
        "ivp_test" => Config::get('values.telr_test_environment'),
        "ivp_desc" => $bookings[0]->reference_id,
        "ivp_cart" => $bookings[0]->reference_id,
        "ivp_lang" => "en",
        "ivp_tranclass" => "ecom",
        "ivp_currency" => "AED",
        "order" => [
            "cartid" => $bookings[0]->booking_id,
            "test" => Config::get('values.telr_test_environment'),
            "amount" => $total_payable,
            "currency" => "AED",
            "description" => $bookings[0]->reference_id,
            "trantype" => "sale"
        ],
        "customer" => [
            "ref" => $customer->customer_id,
            "email" => $customer->email_address,
            "name" => [
                "title" => null,
                "forenames" => $customer_name,
                "surname" => $customer->customer_name
            ],
            "address" => [
                "line1" => $customer_address->customer_address,
                "city" => 'United Arab Emirates',
                "country" => "AE"
            ],
            "phone" => $customer->mobile_number_1
        ],
        "bill_custref" => $customer->customer_id,
        "bill_email" => $customer->email_address,
        "bill_country" => "AE",
        "bill_addr1" => $customer_address->customer_address,
        "bill_fname" => $customer->customer_name,
        "bill_sname" => $customer->customer_name,
        "extra" => [
            "extra1" => $platform,
            "extra2" => null,
            "extra3" => null
        ],
        "return" => $telr_urls
    ];
    $client = new \GuzzleHttp\Client(['verify' => false]);
    $response_telr = $client->request('POST', 'https://secure.telr.com/gateway/order.json', [
        'body' => json_encode($params),
        'headers' => [
            'Content-Type' => 'application/json',
            'accept' => 'application/json',
        ],
    ]);
    return json_decode((string) $response_telr->getBody(), true);
}
function telrCheckOrder($request)
{
    $data = json_decode($request->getContent(), true);
    $input = $data['params'];
    $params = [
        'method' => 'check',
        'store' => Config::get('values.telr_store_id'),
        'authkey' => Config::get('values.telr_auth_key'),
        "order" => [
            "ref" => $input['telr_order_ref'],
        ],
    ];
    $client = new \GuzzleHttp\Client(['verify' => false]);
    $response1 = $client->request('POST', 'https://secure.telr.com/gateway/order.json', [
        'body' => json_encode($params),
        'http_errors' => false,
        'headers' => [
            'Content-Type' => 'application/json',
            'accept' => 'application/json',
        ],
    ]);
    return json_decode((string) $response1->getBody(), true);
}
function telrCheckOrderUrl($request)
{
    $params = [
        'method' => 'check',
        'store' => Config::get('values.telr_store_id'),
        'authkey' => Config::get('values.telr_auth_key'),
        "order" => [
            "ref" => $request->OrderRef,
        ],
    ];
    $client = new \GuzzleHttp\Client(['verify' => false]);
    $response1 = $client->request('POST', 'https://secure.telr.com/gateway/order.json', [
        'body' => json_encode($params),
        'http_errors' => false,
        'headers' => [
            'Content-Type' => 'application/json',
            'accept' => 'application/json',
        ],
    ]);
    return json_decode((string) $response1->getBody(), true);
}