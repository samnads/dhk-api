<div class="content-section1" style="width: 700px; height: auto; margin: 30px auto 0;">
    <table width="100%" border="0" cellpadding="0" cellspacing="0">
        <tr>
            <td colspan="3">
                <p
                    style="font: bold 20px/25px 'Poppins', sans-serif;  color: #555; display: block; margin: 0px; padding: 0px 0px 15px; text-align: left; -webkit-text-size-adjust: 100%; -moz-text-size-adjust: 100%; -ms-text-size-adjust: 100%;">
                    {{ @$booking->service_type_name }}</p>
            </td>
        </tr>
        <tr>
            <td>
                <p
                    style="font: normal 16px/20px 'Poppins', sans-serif; color: #555; display: block; margin: 0px; padding: 0px 0px 10px; text-align: left; -webkit-text-size-adjust: 100%; -moz-text-size-adjust: 100%; -ms-text-size-adjust: 100%;">
                    {{ \Carbon\Carbon::createFromFormat('Y-m-d', @$booking->service_start_date)->format('d M Y') }}
                </p>
                <p
                    style="font: normal 16px/20px 'Poppins', sans-serif; color: #555; display: block; margin: 0px; padding: 0px 0px 10px; text-align: left; -webkit-text-size-adjust: 100%; -moz-text-size-adjust: 100%; -ms-text-size-adjust: 100%;">
                    {{ \Carbon\Carbon::createFromFormat('H:i:s', @$booking->time_from)->format('h:i A') }} - {{ \Carbon\Carbon::createFromFormat('H:i:s', @$booking->time_to)->format('h:i A') }}
                </p>
                <table width="100%" cellpadding="0" cellspacing="0">
                    <tr>
                        @foreach ($bookings as $booking)
                            <!--<td width="50px">
                                <div class="maid-photo"
                                    style="width: 40px; height: 40px; border-radius: 50px; overflow: hidden">
                                    <a href="javascript:void(0);"
                                        style="display: block; cursor: default !important;"><img
                                            src="http://demo.azinova.me/website/emaid-booking2023/images/maid.jpg"
                                            width="100%" height="" alt=""
                                            style="border-radius: 5px; cursor: default !important;" />
                                    </a>
                                </div>
                            </td>-->
                        @endforeach
                        <td>&nbsp;</td>
                    </tr>
                </table>
            </td>
            <td>&nbsp;</td>
            <td align="right">
                <p
                    style="font: normal 16px/20px 'Poppins', sans-serif; color: #555; display: block; margin: 0px; padding: 0px 0px 10px; text-align: right; -webkit-text-size-adjust: 100%; -moz-text-size-adjust: 100%; -ms-text-size-adjust: 100%;">
                    Booking Ref.</p>
                <p
                    style="font: normal 20px/25px 'Poppins', sans-serif; color: #f76161; display: block; margin: 0px; padding: 0px 0px 15px; text-align: right; -webkit-text-size-adjust: 100%; -moz-text-size-adjust: 100%; -ms-text-size-adjust: 100%;">
                    {{ @$booking->reference_id }}
                </p>
            </td>
        </tr>
        @if($booking->payment_type_id != 1)
        <tr>
            <td colspan="3" align="right">
                <p
                    style="font: normal 16px/20px 'Poppins', sans-serif; color: #555; display: block; margin: 0px; padding: 0px 0px 10px; text-align: right; -webkit-text-size-adjust: 100%; -moz-text-size-adjust: 100%; -ms-text-size-adjust: 100%;">
                    Transaction Ref.</p>
                <p
                    style="font: normal 20px/25px 'Poppins', sans-serif; color: #5180ec; display: block; margin: 0px; padding: 0px 0px 15px; text-align: right; -webkit-text-size-adjust: 100%; -moz-text-size-adjust: 100%; -ms-text-size-adjust: 100%;">
                    {{$bookings[0]->op_transaction_id}}
                </p>
            </td>
        </tr>
        @endif
    </table>
</div>


<div class="content-section2" style="width: 700px; height: auto; margin: 30px auto 0;">
    <table width="100%" cellpadding="0" cellspacing="0">
        <tr>
            <td style="border: 1px solid #ddd; border-right: 0px">
                <p
                    style="font: normal 14px/18px 'Poppins', sans-serif; color: #555; display: block; margin: 0px; padding: 15px 20px; text-align: left; -webkit-text-size-adjust: 100%; -moz-text-size-adjust: 100%; -ms-text-size-adjust: 100%;">
                    Service Frequency<br />
                    <label
                        style="font-size: 18px; line-height: 25px; display: block; margin: 0px; padding: 2px 0px 0px;">{{ @$frequency->name }}</label>
                </p>
            </td>
            <td style="border: 1px solid #ddd; border-right: 0px">
                <p
                    style="font: normal 14px/18px 'Poppins', sans-serif; color: #555; display: block; margin: 0px; padding: 15px 20px; text-align: left; -webkit-text-size-adjust: 100%; -moz-text-size-adjust: 100%; -ms-text-size-adjust: 100%;">
                    No. of Professionals<br />
                    <label
                        style="font-size: 18px; line-height: 25px; display: block; margin: 0px; padding: 2px 0px 0px;">{{ @sizeof($bookings) }}</label>
                </p>
            </td>
            <td style="border: 1px solid #ddd;">
                <p
                    style="font: normal 14px/18px 'Poppins', sans-serif; color: #555; display: block; margin: 0px; padding: 15px 20px; text-align: left; -webkit-text-size-adjust: 100%; -moz-text-size-adjust: 100%; -ms-text-size-adjust: 100%;">
                    Cleaning Material<br />
                    <label
                        style="font-size: 18px; line-height: 25px; display: block; margin: 0px; padding: 2px 0px 0px;">{{ @$booking->cleaning_material_opted }}</label>
                </p>
            </td>
        </tr>
        @if(trim($booking->booking_note) != '')
        <tr>
            <td style="border: 1px solid #ddd;" colspan="3">
                <p
                    style="font: normal 14px/18px 'Poppins', sans-serif; color: #555; display: block; margin: 0px; padding: 15px 20px; text-align: left; -webkit-text-size-adjust: 100%; -moz-text-size-adjust: 100%; -ms-text-size-adjust: 100%;">
                    Instructions<br />
                    <label
                        style="font-size: 18px; line-height: 25px; display: block; margin: 0px; padding: 2px 0px 0px;color: #c40000;">{{$booking->booking_note}}</label>
                </p>
            </td>
        </tr>
        @endif
    </table>
</div>
