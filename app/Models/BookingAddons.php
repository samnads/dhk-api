<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BookingAddons extends Model
{
    use HasFactory;
    protected $table = 'booking_addons';
    public $timestamps = false;
    protected $primaryKey = 'id';
}
