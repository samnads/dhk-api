@extends('emails.layouts.main')
@section('content')
    @include('emails.includes.section-message')
    @include('emails.includes.booking-cancel-summary')
    @include('emails.includes.details-booking-cancel')
    @include('emails.includes.section-terms-conditions')
    @include('emails.includes.section-footer-address')
@endsection
