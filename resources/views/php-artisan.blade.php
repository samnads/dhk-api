<p><a href="{{url('clear-cache')}}" target="_blank">php artisan clear:cache</a></p>
<p><a href="{{url('clear-config')}}" target="_blank">php artisan config:clear</a></p>
<p><a href="{{url('cache-config')}}" target="_blank">php artisan config:cache</a></p>
<p><a href="{{url('cache-route')}}" target="_blank">php artisan route:cache</a></p>
<p><a href="{{url('clear-route')}}" target="_blank">php artisan route:clear</a></p>