<div style="width: 700px; margin-top: 50px;">
    <table width="100%" cellpadding="0" cellspacing="0">
        <tr>
            <td>
                <div> <a href="https://www.facebook.com/DubaiHousekeeping" target="_blank" style="width: 32px; display: inline-block; margin-right: 3px;"><img
                            src="{{ Config::get('values.file_server_url') . 'images/email/social-facebook.jpg?v=' . @$settings->customer_app_img_version }}"
                            width="100%" height="" alt="" style="border-radius: 5px;" /></a>
                    <a href="https://twitter.com/DubaiHousekeep" target="_blank"
                        style="width: 32px; display: inline-block; margin-right: 3px;"><img
                            src="{{ Config::get('values.file_server_url') . 'images/email/social-twitter.jpg?v=' . @$settings->customer_app_img_version }}"
                            width="100%" height="" alt="" style="border-radius: 5px;" /></a>
                    <a href="https://www.linkedin.com/company/dubaihousekeeping" target="_blank"
                        style="width: 32px; display: inline-block; margin-right: 3px;"><img
                            src="{{ Config::get('values.file_server_url') . 'images/email/social-linkedin.jpg?v=' . @$settings->customer_app_img_version }}"
                            width="100%" height="" alt="" style="border-radius: 5px;" /></a>
                    <a href="https://www.instagram.com/dubaihousekeeping" target="_blank"
                        style="width: 32px; display: inline-block; margin-right: 3px;"><img
                            src="{{ Config::get('values.file_server_url') . 'images/email/social-instagram.jpg?v=' . @$settings->customer_app_img_version }}"
                            width="100%" height="" alt="" style="border-radius: 5px;" /></a>
                </div>

                <div style="margin: 10px 0px 0px 0px;">
                    <p
                        style="font: normal 13px/18px 'Poppins', sans-serif; color: #555; display: block; margin: 0px; padding: 0px 0px 15px; text-align: left; -webkit-text-size-adjust: 100%; -moz-text-size-adjust: 100%; -ms-text-size-adjust: 100%;">
                        <a href="https://dubaihousekeeping.com/terms-conditions/"
                            style="color: #555; padding: 0px 10px 0px 0px;">Terms &
                            Conditions</a> |
                        <a href="https://dubaihousekeeping.com/privacy-policy/"
                            style="color: #555; padding: 0px 10px 0px 10px;">Privacy
                            Policy</a> |
                        <a href="https://dubaihousekeeping.com/blog/"
                            style="color: #555; padding: 0px 0px 0px 10px;">Blog</a>
                    </p>
                </div>
            </td>
            <td>
                <p
                    style="font: normal 13px/30px 'Poppins', sans-serif; color: #555; display: block; margin: 0px; padding: 0px 0px 15px; text-align: right; -webkit-text-size-adjust: 100%; -moz-text-size-adjust: 100%; -ms-text-size-adjust: 100%;">
                    Best Regards,<br />
                    {{Config::get('values.company_name')}}</p>
            </td>
        </tr>
    </table>
</div>
