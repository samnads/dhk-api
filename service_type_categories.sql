-- Adminer 4.8.1 MySQL 11.3.2-MariaDB dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

SET NAMES utf8mb4;

DROP TABLE IF EXISTS `service_type_categories`;
CREATE TABLE `service_type_categories` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `service_category_name` varchar(255) NOT NULL,
  `sort_order` int(11) NOT NULL DEFAULT 0,
  `customer_app_icon_file` varchar(255) DEFAULT NULL,
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_at` datetime NOT NULL DEFAULT current_timestamp(),
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

INSERT INTO `service_type_categories` (`id`, `service_category_name`, `sort_order`, `customer_app_icon_file`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1,	'General',	10,	'general-cleaning.png',	'2023-11-13 05:38:25',	'2023-11-13 05:38:25',	NULL),
(2,	'Custom',	20,	'general-cleaning.png',	'2024-06-11 16:54:42',	'2024-06-11 16:54:42',	NULL);

-- 2024-06-11 11:53:37
