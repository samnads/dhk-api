-- Adminer 4.8.1 MySQL 11.3.2-MariaDB dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

SET NAMES utf8mb4;

DROP TABLE IF EXISTS `service_types`;
CREATE TABLE `service_types` (
  `service_type_id` int(11) NOT NULL AUTO_INCREMENT,
  `service_type_name` varchar(110) NOT NULL,
  `customer_app_service_type_name` varchar(155) DEFAULT NULL,
  `web_url_slug` varchar(255) CHARACTER SET utf8mb4 COLLATE utf8mb4_general_ci DEFAULT NULL,
  `url_slug` varchar(150) DEFAULT NULL,
  `order_id` int(11) NOT NULL,
  `web_order_id` int(10) unsigned DEFAULT NULL,
  `service_rate` float(10,2) NOT NULL,
  `material_incl` varchar(1) NOT NULL DEFAULT 'N',
  `warning_notes` varchar(1) NOT NULL DEFAULT 'N',
  `web_status` int(11) NOT NULL DEFAULT 0,
  `service_category` varchar(10) DEFAULT 'C' COMMENT 'C-cleaning,M-maintenance',
  `detail` longtext NOT NULL,
  `service_type_status` tinyint(1) NOT NULL COMMENT '0 - Not Active, 1 - Active',
  `photourl` longtext DEFAULT NULL,
  `odoo_service_id` int(11) NOT NULL,
  `readmore` varchar(1) DEFAULT 'N',
  `readmore_link` varchar(1500) DEFAULT NULL,
  `category_available` tinyint(4) DEFAULT NULL,
  `subcategory_available` tinyint(4) DEFAULT NULL,
  `is_furnished` tinyint(4) NOT NULL DEFAULT 0,
  `floor_cleaning` tinyint(4) DEFAULT NULL,
  `service_type` char(1) NOT NULL DEFAULT 'H',
  `price_condition` char(5) NOT NULL DEFAULT 'CP',
  `enable_scrubbing` enum('Y','N') NOT NULL DEFAULT 'N',
  `cleaning_icon` varchar(1500) DEFAULT NULL,
  `special_price` float NOT NULL,
  `maid_status` int(11) NOT NULL DEFAULT 0,
  `imgAltTag` varchar(255) DEFAULT NULL,
  `customer_app_thumbnail_file` varchar(255) DEFAULT NULL,
  `service_type_model_id` int(10) unsigned NOT NULL DEFAULT 1,
  `info_html` text DEFAULT NULL COMMENT 'for web / app (html content)',
  `customer_app_status` tinyint(1) unsigned DEFAULT NULL,
  `customer_app_order_id` int(5) unsigned DEFAULT NULL,
  `min_no_of_maids` tinyint(3) unsigned NOT NULL DEFAULT 1,
  `max_no_of_maids` tinyint(3) unsigned NOT NULL DEFAULT 1,
  `min_no_of_hours` tinyint(3) unsigned NOT NULL DEFAULT 1,
  `max_no_of_hours` tinyint(3) unsigned NOT NULL DEFAULT 1,
  `default_no_of_hours` tinyint(3) unsigned NOT NULL DEFAULT 1,
  PRIMARY KEY (`service_type_id`),
  UNIQUE KEY `url_slug` (`url_slug`),
  KEY `service_category` (`service_category`),
  KEY `service_type_status` (`service_type_status`),
  KEY `service_type_name` (`service_type_name`),
  KEY `service_type_model_id` (`service_type_model_id`),
  CONSTRAINT `service_types_ibfk_1` FOREIGN KEY (`service_type_model_id`) REFERENCES `service_type_models` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;

INSERT INTO `service_types` (`service_type_id`, `service_type_name`, `customer_app_service_type_name`, `web_url_slug`, `url_slug`, `order_id`, `web_order_id`, `service_rate`, `material_incl`, `warning_notes`, `web_status`, `service_category`, `detail`, `service_type_status`, `photourl`, `odoo_service_id`, `readmore`, `readmore_link`, `category_available`, `subcategory_available`, `is_furnished`, `floor_cleaning`, `service_type`, `price_condition`, `enable_scrubbing`, `cleaning_icon`, `special_price`, `maid_status`, `imgAltTag`, `customer_app_thumbnail_file`, `service_type_model_id`, `info_html`, `customer_app_status`, `customer_app_order_id`, `min_no_of_maids`, `max_no_of_maids`, `min_no_of_hours`, `max_no_of_hours`, `default_no_of_hours`) VALUES
(1,	'Housekeeping Service',	'Housekeeping Service',	'house-cleaning-dubai',	'house-cleaning-dubai',	1,	10,	0.00,	'N',	'N',	1,	'C',	'Enjoy a sparkling clean home without lifting a finger. Our expert team ensures every nook and cranny is pristine.',	1,	'house-cleaning.png',	0,	'N',	NULL,	NULL,	NULL,	0,	NULL,	'H',	'CP',	'N',	'house-cleaning-new.jpg',	40,	0,	'Book House cleaning service',	'house-cleaning.jpg',	1,	'info_html',	1,	NULL,	1,	10,	2,	8,	3),
(3,	'Window Cleaning',	NULL,	NULL,	NULL,	3,	NULL,	0.00,	'N',	'N',	0,	'C',	'A stained window can be a huge embarrassment if you are trying to look clean and perfect. With our professional cleaners, we can make it possible for you to get yours looking crystal clear.',	1,	'windowcleaning.png',	0,	'N',	NULL,	NULL,	NULL,	0,	NULL,	'H',	'CP',	'N',	NULL,	0,	1,	NULL,	NULL,	1,	NULL,	NULL,	NULL,	1,	1,	1,	1,	1),
(4,	'Office Cleaning',	'Office Cleaning',	'office-cleaning-service',	'office-cleaning-service',	4,	110,	0.00,	'N',	'N',	1,	'C',	'A tidy workspace boosts productivity. Let us transform your office into an organized, dust-free environment.',	1,	'party.png',	0,	'N',	NULL,	NULL,	NULL,	0,	NULL,	'H',	'CP',	'N',	'office-cleaning-new.jpg',	0,	1,	'book office cleaning',	'office-cleaning.jpg',	1,	NULL,	1,	NULL,	1,	10,	2,	8,	2),
(5,	'Ironing Service',	'Ironing Service',	'ironing-service',	'ironing-service',	5,	20,	0.00,	'N',	'N',	1,	'C',	'Wrinkled clothes? Say goodbye to them! Our ironing service keeps your attire looking sharp and professional.',	1,	'floorcleaning.png',	0,	'N',	NULL,	NULL,	NULL,	0,	NULL,	'H',	'CP',	'N',	'	\r\nironing-new.jpg',	0,	1,	'Book ironing service',	'ironing.jpg',	3,	NULL,	1,	NULL,	1,	1,	2,	4,	2),
(25,	'Deep / Move In Cleaning',	NULL,	NULL,	NULL,	0,	NULL,	0.00,	'Y',	'N',	0,	'C',	'Having a deep clean session can be the perfect solution for your recurring cleaning necessities. In this increasingly frenetic lifestyle, deep cleaning ensures to keep your house fresh, clean, and allergen-free.',	1,	'floorcleaning.png',	0,	'N',	NULL,	NULL,	NULL,	0,	NULL,	'C',	'CP',	'N',	'deepcleaning.png',	0,	1,	NULL,	NULL,	1,	NULL,	NULL,	NULL,	1,	1,	1,	1,	1),
(36,	'Disinfection Only',	NULL,	NULL,	NULL,	0,	NULL,	0.00,	'N',	'N',	0,	'C',	'We have disinfected more than 1,000+ properties across Dubai in 2020 alone.  Our clients range from apartment and villa owners to corporate offices, warehouses, medical facilities, bloggers and commercial shops.Our disinfection crew underwent meticulous training on the disinfection process and our professional sales, marketing and quality management department carefully planned and executed the disinfection services program.',	1,	NULL,	0,	'N',	NULL,	NULL,	NULL,	0,	NULL,	'C',	'CP',	'N',	'disinfection.png',	0,	1,	NULL,	NULL,	1,	NULL,	NULL,	NULL,	1,	1,	1,	1,	1),
(37,	'Holiday Home',	NULL,	NULL,	'holiday-home-cleaning-service',	11,	100,	0.00,	'N',	'N',	0,	'C',	'We have disinfected more than 1,000+ properties across Dubai in 2020 alone.  Our clients range from apartment and villa owners to corporate offices, warehouses, medical facilities, bloggers and commercial shops.Our disinfection crew underwent meticulous training on the disinfection process and our professional sales, marketing and quality management department carefully planned and executed the disinfection services program.',	1,	'house-cleaning.png',	0,	'N',	NULL,	NULL,	NULL,	0,	NULL,	'C',	'CP',	'N',	'HouseCleaning.jpg',	0,	1,	NULL,	NULL,	1,	NULL,	NULL,	NULL,	1,	1,	1,	1,	1),
(39,	'Curtains Cleaning',	NULL,	NULL,	NULL,	0,	NULL,	0.00,	'N',	'N',	0,	'C',	'We have disinfected more than 1,000+ properties across Dubai in 2020 alone.  Our clients range from apartment and villa owners to corporate offices, warehouses, medical facilities, bloggers and commercial shops.Our disinfection crew underwent meticulous training on the disinfection process and our professional sales, marketing and quality management department carefully planned and executed the disinfection services program.',	1,	NULL,	0,	'N',	NULL,	NULL,	NULL,	0,	NULL,	'C',	'CP',	'N',	'disinfection.png',	0,	1,	NULL,	NULL,	1,	NULL,	NULL,	NULL,	1,	1,	1,	1,	1),
(47,	'Organic Cleaning',	'Organic Cleaning',	'organic-cleaning-service',	'organic-cleaning-service',	0,	30,	0.00,	'N',	'N',	1,	'C',	'Clean with care. Our eco-friendly products leave your space spotless and the planet smiling.',	1,	NULL,	0,	'N',	NULL,	NULL,	NULL,	0,	NULL,	'H',	'CP',	'N',	'organic-cleaning-new.jpg\r\n',	0,	0,	'Book Organic cleaning service',	'organic-cleaning.jpg',	3,	NULL,	1,	NULL,	1,	10,	2,	8,	2),
(48,	'Mold Cleaning',	NULL,	NULL,	'mold-cleaning-service',	11,	180,	0.00,	'N',	'N',	0,	'C',	'',	1,	'house-cleaning.png',	0,	'N',	NULL,	NULL,	NULL,	0,	NULL,	'H',	'CP',	'N',	'HouseCleaning.jpg',	40,	0,	NULL,	NULL,	1,	NULL,	NULL,	NULL,	1,	1,	1,	1,	1),
(49,	'Laundry Services',	NULL,	NULL,	NULL,	0,	NULL,	0.00,	'N',	'N',	0,	'C',	'',	1,	NULL,	0,	'N',	NULL,	NULL,	NULL,	0,	NULL,	'H',	'CP',	'N',	NULL,	0,	0,	NULL,	NULL,	1,	NULL,	NULL,	NULL,	1,	1,	1,	1,	1),
(50,	'AC Cleaning',	NULL,	NULL,	NULL,	0,	NULL,	0.00,	'N',	'N',	0,	'C',	'',	1,	NULL,	0,	'N',	NULL,	NULL,	NULL,	0,	NULL,	'H',	'CP',	'N',	NULL,	0,	0,	NULL,	NULL,	1,	NULL,	NULL,	NULL,	1,	1,	1,	1,	1),
(51,	'Estimation',	NULL,	NULL,	NULL,	0,	NULL,	0.00,	'N',	'N',	0,	'C',	'',	1,	NULL,	0,	'N',	NULL,	NULL,	NULL,	0,	NULL,	'H',	'CP',	'N',	NULL,	0,	0,	NULL,	NULL,	1,	NULL,	NULL,	NULL,	1,	1,	1,	1,	1),
(52,	'Baby Sitting Service',	'Baby Sitting Service',	'baby-sitting-service',	'baby-sitting-service',	1,	80,	0.00,	'N',	'N',	1,	'C',	'Need a trusted hand with your little one? Our experienced babysitters provide safe and attentive care.',	1,	'house-cleaning.png\r\n',	0,	'N',	NULL,	NULL,	NULL,	0,	NULL,	'H',	'CP',	'N',	'babysitting-new.jpg',	40,	0,	'book babysitting service',	'babysitting.jpg',	3,	NULL,	1,	NULL,	1,	10,	2,	8,	2),
(53,	'Deep Cleaning',	'Deep Cleaning',	'deep-cleaning-service',	'deep-cleaning-service',	1,	40,	0.00,	'N',	'N',	1,	'C',	'When cleanliness demands perfection. Our deep cleaning service revives and refreshes every inch.',	1,	'house-cleaning.png\r\n',	0,	'N',	NULL,	NULL,	NULL,	0,	NULL,	'H',	'CP',	'N',	'deep-cleaning-new.jpg',	40,	0,	'Book Deep cleaning service',	'deep-cleaning.jpg',	2,	NULL,	1,	NULL,	1,	1,	1,	1,	1),
(54,	'Move Out Cleaning',	'Move Out Cleaning',	'move-out-cleaning-service',	'move-out-cleaning-service',	1,	60,	0.00,	'N',	'N',	1,	'C',	'Leaving? Make sure you leave behind a clean and inviting space for the next occupants.',	1,	'house-cleaning.png',	0,	'N',	NULL,	NULL,	NULL,	0,	NULL,	'H',	'CP',	'N',	'move-out-new.jpg',	40,	0,	'Book move out cleaning',	'move-out-cleaning.jpg',	2,	NULL,	1,	NULL,	1,	1,	1,	1,	1),
(55,	'Move In Cleaning',	'Move In Cleaning',	'move-in-cleaning-service',	'move-in-cleaning-service',	1,	50,	0.00,	'N',	'N',	1,	'C',	'A fresh start begins with a clean slate. Move into a spotless new home with our expert cleaning.',	1,	'house-cleaning.png',	0,	'N',	NULL,	NULL,	NULL,	0,	NULL,	'H',	'CP',	'N',	'move-in-new.jpg',	40,	0,	'Book move in cleaning',	'move-in-cleaning.jpg',	2,	NULL,	1,	NULL,	1,	1,	1,	1,	1),
(56,	'Steam Cleaning',	'Steam Cleaning',	'steam-cleaning-service',	'steam-cleaning-service',	1,	70,	0.00,	'N',	'N',	1,	'C',	'The power of steam: an effective, eco-friendly way to sanitize and refresh your space.',	1,	'house-cleaning.png',	0,	'N',	NULL,	NULL,	NULL,	0,	NULL,	'H',	'CP',	'N',	'steam-cleaning-new.jpg',	40,	0,	'Book steam cleaning',	'steam-cleaning.jpg',	2,	NULL,	1,	NULL,	1,	1,	1,	1,	1),
(57,	'Sofa Cleaning',	'Sofa Cleaning',	'sofa-cleaning-service',	'sofa-cleaning-service',	1,	120,	0.00,	'N',	'N',	1,	'C',	'Your sofa is your haven. Keep it clean and cozy with our specialized sofa cleaning service.',	1,	'house-cleaning.png',	0,	'N',	NULL,	NULL,	NULL,	0,	NULL,	'H',	'CP',	'N',	'sofa-cleaning-new.jpg',	40,	0,	'book sofa cleaning',	'sofa-cleaning.jpg',	2,	NULL,	1,	NULL,	1,	1,	1,	1,	1),
(58,	'Mattress Cleaning',	'Mattress Cleaning',	'mattress-cleaning-service',	'mattress-cleaning-service',	1,	140,	0.00,	'N',	'N',	1,	'C',	'Sleep easy with a clean mattress. Our service removes allergens and ensures a restful night\'s sleep.',	1,	'house-cleaning.png',	0,	'N',	NULL,	NULL,	NULL,	0,	NULL,	'H',	'CP',	'N',	'mattress-cleaning-new.jpg',	40,	0,	'book mattress cleaning',	'mattress-cleaning.jpg',	2,	NULL,	1,	NULL,	1,	1,	1,	1,	1),
(59,	'Carpet Cleaning',	'Carpet Cleaning',	'carpet-cleaning-service',	'carpet-cleaning-service',	11,	130,	0.00,	'N',	'N',	1,	'C',	'Carpets collect dust and dirt. Our deep cleaning restores their beauty and extends their life.',	1,	'house-cleaning.png',	0,	'N',	NULL,	NULL,	NULL,	0,	NULL,	'H',	'CP',	'N',	'carpet-cleaning-new.jpg',	40,	0,	'book carpet cleaning',	'carpet-cleaning.jpg',	2,	NULL,	1,	NULL,	1,	1,	1,	1,	1),
(60,	'Floor Scrubbing',	'Floor Scrubbing',	'floor-scrubbing-service',	'floor-scrubbing-service',	11,	150,	0.00,	'N',	'N',	1,	'C',	'Say hello to gleaming floors. Our floor scrubbing service leaves them sparkling and slip-free.',	1,	'house-cleaning.png',	0,	'N',	NULL,	NULL,	NULL,	0,	NULL,	'H',	'CP',	'N',	'floor-scrubbing-new.jpg',	40,	0,	'book floor scrubbing',	'floor-scrubbing.jpg',	2,	NULL,	1,	NULL,	1,	1,	1,	1,	1),
(61,	'Sanitization Service',	'Sanitization Service',	'sanitization-service',	'sanitization-service',	11,	140,	0.00,	'N',	'N',	1,	'C',	'Keep germs at bay. Our thorough sanitization service ensures a healthy and hygienic environment.',	1,	'house-cleaning.png',	0,	'N',	NULL,	NULL,	NULL,	0,	NULL,	'H',	'CP',	'N',	'sanitization -services-new.jpg',	40,	0,	'book sanitization service',	'sanitization -services.jpg',	2,	NULL,	1,	NULL,	1,	1,	1,	1,	1),
(62,	'Declutter & Organize',	'Declutter & Organize',	'declutter-and-organize-service',	'declutter-and-organize-service',	11,	79,	0.00,	'N',	'N',	1,	'C',	'Unburden your space. Our experts declutter and organize, transforming chaos into order.',	1,	'house-cleaning.png',	0,	'N',	NULL,	NULL,	NULL,	0,	NULL,	'H',	'CP',	'N',	'declutter-new.jpg',	40,	0,	'book Declutter and organize',	'declutter.jpg',	2,	NULL,	1,	NULL,	1,	1,	1,	1,	1),
(65,	'Pest Control',	NULL,	NULL,	NULL,	0,	NULL,	0.00,	'N',	'N',	0,	'C',	'',	1,	NULL,	0,	'N',	NULL,	NULL,	NULL,	0,	NULL,	'H',	'CP',	'N',	NULL,	0,	0,	NULL,	NULL,	1,	NULL,	NULL,	NULL,	1,	1,	1,	1,	1),
(66,	'Train Your Maid',	NULL,	NULL,	NULL,	0,	NULL,	0.00,	'N',	'N',	0,	'C',	'',	1,	NULL,	0,	'N',	NULL,	NULL,	NULL,	0,	NULL,	'H',	'CP',	'N',	NULL,	0,	0,	NULL,	NULL,	1,	NULL,	NULL,	NULL,	1,	1,	1,	1,	1),
(67,	'Complimentary Service',	NULL,	NULL,	NULL,	0,	NULL,	0.00,	'N',	'N',	0,	'C',	'',	1,	NULL,	0,	'N',	NULL,	NULL,	NULL,	0,	NULL,	'H',	'CP',	'N',	NULL,	0,	0,	NULL,	NULL,	1,	NULL,	NULL,	NULL,	1,	1,	1,	1,	1),
(68,	'Handymen Services',	NULL,	NULL,	NULL,	0,	NULL,	0.00,	'N',	'N',	0,	'C',	'',	1,	NULL,	0,	'N',	NULL,	NULL,	NULL,	0,	NULL,	'H',	'CP',	'N',	NULL,	0,	0,	NULL,	NULL,	1,	NULL,	NULL,	NULL,	1,	1,	1,	1,	1),
(69,	'Deep / Move Out cleaning',	NULL,	NULL,	NULL,	0,	NULL,	0.00,	'N',	'N',	0,	'C',	'',	1,	NULL,	0,	'N',	NULL,	NULL,	NULL,	0,	NULL,	'H',	'CP',	'N',	NULL,	0,	0,	NULL,	NULL,	1,	NULL,	NULL,	NULL,	1,	1,	1,	1,	1);

-- 2024-06-15 06:54:43
