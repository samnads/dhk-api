<?php

namespace App\Http\Controllers;

use App\Models\OnlinePayment;
use App\Models\CustomerAddress;
use App\Models\Area;
use App\Models\Booking;
use App\Models\Customer;
use App\Models\BookingDeleteRemarks;
use App\Models\PaymentType;
use Carbon\Carbon;
use Config;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Facades\Log;
use Response;
use stdClass;
use App\Models\CustomerPayments;
use App\Models\Frequencies;
use App\Models\UserActivity;
use Redirect;



class OutstandingPaymentController extends Controller
{
    public function customerOutstandingAmount(Request $request)
    {
        try {
            /************************************************************* */
            $data = json_decode($request->getContent(), true);
            $input = @$data['params'];
            /************************************************************* */
            $validator = Validator::make(
                (array) $input,
                [],
                [],
                []
            );
            // If validation fails, return a JSON response with the error message
            if ($validator->fails()) {
                return response()->json([
                    'result' => [
                        'status' => 'failed',
                        'message' => $validator->errors()->first()
                    ],
                ], 200, array(), customerResponseJsonConstants());
            }
            /************************************************************* */
            $customerAddress = CustomerAddress::
                where([
                    ['customer_addresses.customer_id', '=', $input['id']],
                    ['customer_addresses.address_status', '=', 0]
                ])
                ->first();
            if (!$customerAddress) {
                throw new \ErrorException('Please add address and try again !');
            }
            /************************************************************* */
            // Retrieve customer invoice data
            $invoice = DB::table('invoice as i')
                ->select(
                    DB::raw('i.customer_id'),
                    DB::raw('SUM(i.invoice_net_amount) as invoice_net_amount'),
                    DB::raw('MAX(i.invoice_date) as last_invoice_date')
                )
                ->whereNotIn('i.invoice_status', [0, 2])
                ->where('i.showStatus', 1)
                ->where('i.customer_id', $input['id']) // Use validated input
                ->groupBy('i.customer_id')
                ->first();
            /************************************************************* */
            // Retrieve customer payment data
            $customer_payment = DB::table('customer_payments as cp')
                ->select(
                    DB::raw('cp.customer_id'),
                    DB::raw('SUM(cp.paid_amount) as paid_amount')
                )
                ->whereNull('cp.deleted_at')
                ->where('cp.show_status', 1)
                ->where('cp.customer_id', $input['id']) // Use validated input
                ->groupBy('cp.customer_id')
                ->first();
            /************************************************************* */
            // Calculate the outstanding amount
            $outstanding_amount = round(
                (@$invoice->invoice_net_amount ?: 0) - (@$customer_payment->paid_amount ?: 0),
                2
            );
            // Prepare the response data
            $response_data = [
                'status' => 'success',
                'message' => 'Outstanding amount retrieved successfully.',
                'payment_details' => [
                    'customer_id' => $input['id'],
                    'last_invoice_date' => @$invoice->last_invoice_date,
                    'outstanding_amount' => $outstanding_amount > 0 ? $outstanding_amount : 0,
                    'payment_url' => url('payment/ccavenue/outstanding-entry/'),
                    'payment_success_url' => url('payment/ccavenue/success/'),
                    'payment_failed_url' => url('payment/ccavenue/failed/')

                ],
                //'customer' => Customer::find($input['id']),
            ];
            /************************************************************* */
            // Return a JSON response with the data 
            return response()->json([
                'result' => 'success',
                'data' => $response_data,
            ], 200, array(), customerResponseJsonConstants());
        } catch (\Exception $e) {
            return Response::json(array('result' => array('status' => 'failed', 'message' => $e->getMessage())), 200, [], JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES | JSON_NUMERIC_CHECK);
        }
    }
    // ****************************************************************
    public function outstandingPaymentInitiate(Request $request)
    {
        try {
            $input = $request->only(['customer_id', 'amount', 'message']);
            $validator = Validator::make(
                $input,
                [
                    'customer_id' => 'required|exists:customers,customer_id',
                    'amount' => 'required|numeric|min:1',
                    'message' => 'nullable|string',
                ],
                [],
                [
                    'customer_id' => 'Customer ID',
                    'amount' => 'Amount',
                    'message' => 'Message',
                ]
            );
            if ($validator->fails()) {
                return response()->json([
                    'result' => [
                        'status' => 'failed',
                        'message' => $validator->errors()->first()
                    ],
                ], 200);
            }
            $customerAddress = CustomerAddress::
                select('customer_addresses.*')
                ->where([['customer_addresses.customer_id', '=', $input['customer_id']], ['customer_addresses.address_status', '=', 0]])
                ->first();
            if (!$customerAddress) {
                throw new \ErrorException('Please add address to continue !');
            }
            $customer_id = $input['customer_id'];
            $message = @$input['message'];
            $inv_id = null;
            $data = [];
            // Calculate Payment Type Charge
            $payment_method_id = 2; // For CCAvenue
            $payment_type = PaymentType::find($payment_method_id);
            $transaction_charge = $payment_type->charge_type == "F" ? $payment_type->charge : ($payment_type->charge / 100) * $input['amount'];
            // Create a new OnlinePayment entry
            $onlinePayment = new OnlinePayment();
            $onlinePayment->description = $message;
            $onlinePayment->transaction_charge = $transaction_charge;
            $onlinePayment->amount = $input['amount'];
            $onlinePayment->customer_id = $customer_id;
            $onlinePayment->paid_from = 'O';
            $onlinePayment->payment_datetime = now();
            $onlinePayment->user_agent = $request->header('User-Agent');
            $onlinePayment->post_data = json_encode($request->all());
            $onlinePayment->payment_status = 'initiated';
            $onlinePayment->inv_id = $inv_id;
            $onlinePayment->save();
            $onlinePayment->reference_id = 'DHK-OP-' . now()->year . '-' . $onlinePayment->payment_id;
            $onlinePayment->save();
            $customer = DB::table('customers as c')
                ->select('c.*', )
                ->where([['c.customer_id', '=', $customer_id]])
                ->first();
            $address = $customerAddress ? $customerAddress->customer_address : '';
            $areaName = $customerAddress ? Area::find($customerAddress->area_id)->area_name : 'NA';
            $data = [
                'message' => $message,
                'amount' => $input['amount'],
                'transaction_charge' => $transaction_charge,
                'gross_amount' => $input['amount'] + $transaction_charge,
                'address' => $address,
                'area' => $areaName,
                'pay_details' => $onlinePayment,
                'reference_id' => $onlinePayment->reference_id,
                'inv_id' => $inv_id,
                'payment_id' => $onlinePayment->payment_id,
                'customer_id' => $customer_id,
                'customer' => $customer,
                'customer_address' => $customerAddress
            ];
            return view('webview.ccavenue_entry', $data);
        } catch (\Exception $e) {
            return Response::json(array('result' => array('status' => 'failed', 'message' => $e->getMessage())), 200, [], JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES | JSON_NUMERIC_CHECK);
        }
    }
}
