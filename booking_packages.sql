-- Adminer 4.8.1 MySQL 11.3.2-MariaDB dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

SET NAMES utf8mb4;

DROP TABLE IF EXISTS `booking_packages`;
CREATE TABLE `booking_packages` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `booking_id` bigint(20) unsigned NOT NULL,
  `building_type_room_package_id` bigint(20) NOT NULL,
  `name` varchar(255) NOT NULL,
  `building_type` varchar(255) NOT NULL,
  `building_room` varchar(255) NOT NULL,
  `service_time` time NOT NULL,
  `no_of_maids` int(11) unsigned NOT NULL DEFAULT 1,
  `strike_unit_price` decimal(15,2) unsigned NOT NULL,
  `unit_price` decimal(15,2) unsigned NOT NULL,
  `quantity` int(11) unsigned NOT NULL,
  `total` decimal(15,2) unsigned NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `booking_id_package_id` (`booking_id`,`building_type_room_package_id`),
  KEY `building_type_room_package_id` (`building_type_room_package_id`),
  CONSTRAINT `booking_packages_ibfk_1` FOREIGN KEY (`booking_id`) REFERENCES `bookings` (`booking_id`),
  CONSTRAINT `booking_packages_ibfk_2` FOREIGN KEY (`building_type_room_package_id`) REFERENCES `building_type_room_packages` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

INSERT INTO `booking_packages` (`id`, `booking_id`, `building_type_room_package_id`, `name`, `building_type`, `building_room`, `service_time`, `no_of_maids`, `strike_unit_price`, `unit_price`, `quantity`, `total`, `created_at`, `updated_at`, `deleted_at`) VALUES
(3,	36955,	2,	'1 Bedroom',	'Apartment',	'1 Bedroom',	'02:00:00',	1,	459.00,	399.00,	1,	399.00,	'2024-06-14 13:49:40',	'2024-06-14 13:49:40',	NULL),
(4,	36955,	3,	'2 Bedroom',	'Apartment',	'2 Bedroom',	'02:00:00',	1,	699.00,	599.00,	1,	599.00,	'2024-06-14 13:49:40',	'2024-06-14 13:49:40',	NULL),
(5,	36956,	3,	'2 Bedroom',	'Apartment',	'2 Bedroom',	'02:00:00',	1,	699.00,	599.00,	1,	599.00,	'2024-06-14 14:03:12',	'2024-06-14 14:03:12',	NULL),
(6,	36956,	4,	'3 Bedroom',	'Apartment',	'3 Bedroom',	'02:00:00',	1,	850.00,	799.00,	1,	799.00,	'2024-06-14 14:03:12',	'2024-06-14 14:03:12',	NULL),
(7,	36956,	5,	'4 Bedroom',	'Apartment',	'4 Bedroom',	'02:00:00',	1,	1369.00,	1199.00,	1,	1199.00,	'2024-06-14 14:03:12',	'2024-06-14 14:03:12',	NULL),
(8,	36958,	9,	'5 Bedroom',	'Villa',	'5 Bedroom',	'02:00:00',	1,	3299.00,	2199.00,	1,	2199.00,	'2024-06-14 15:38:53',	'2024-06-14 15:38:53',	NULL),
(9,	36959,	80,	'3 Bedroom',	'Apartment',	'3 Bedroom',	'02:00:00',	1,	699.00,	669.00,	1,	669.00,	'2024-06-14 17:40:36',	'2024-06-14 17:40:36',	NULL),
(10,	36960,	99,	'2 Seats',	'Sofa',	'2 Seats',	'02:00:00',	1,	520.00,	450.00,	1,	450.00,	'2024-06-14 17:50:54',	'2024-06-14 17:50:54',	NULL),
(11,	36967,	110,	'Queen 160x190cm - 1 Nos.',	'Queen',	'Queen 160x190cm - 1 Nos.',	'01:00:00',	1,	210.00,	189.00,	1,	189.00,	'2024-06-15 10:35:09',	'2024-06-15 10:35:09',	NULL),
(12,	36967,	111,	'Queen 160x190cm - 2 Nos.',	'Queen',	'Queen 160x190cm - 2 Nos.',	'01:00:00',	1,	399.00,	378.00,	1,	378.00,	'2024-06-15 10:35:09',	'2024-06-15 10:35:09',	NULL),
(13,	36967,	115,	'King 180x200cm - 3 Nos.',	'King',	'King 180x200cm - 3 Nos.',	'01:00:00',	1,	699.00,	627.00,	1,	627.00,	'2024-06-15 10:35:09',	'2024-06-15 10:35:09',	NULL);

-- 2024-06-15 06:55:08
