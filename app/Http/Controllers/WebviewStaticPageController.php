<?php

namespace App\Http\Controllers;

use App\Models\Settings;
use Illuminate\Http\Request;

class WebviewStaticPageController extends Controller
{
    public function terms_and_conditions(Request $request)
    {
        return view('webview.terms_and_conditions', []);
    }
    public function privacy_policy(Request $request)
    {
        return view('webview.privacy_policy', []);
    }
    public function about_us(Request $request)
    {
        $data['settings'] = Settings::first();
        return view('webview.about_us', $data);
    }
}
