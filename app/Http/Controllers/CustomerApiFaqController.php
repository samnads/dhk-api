<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Response;

class CustomerApiFaqController extends Controller
{
    public function faq(Request $request)
    {
        /************************************************************* */
        $data = json_decode($request->getContent(), true);
        $input = @$data['params'];
        /************************************************************* */
        $response['status'] = 'success';
        $response['faq'] = DB::table('customer_faq as cf')
            ->select(
                'cf.question',
                'cf.answer',
                'cf.order_priority',
            )
            ->where([['cf.deleted_at', '=', null]])
            ->orderBy('cf.order_priority', 'ASC')
            ->get();
        $response['message'] = sizeof($response['faq']) ? "FAQ fetched successfully." : "No FAQ found.";
        return Response::json(array('result' => $response), 200, [], customerResponseJsonConstants());
    }
    public function faq_help(Request $request)
    {
        $debug = toggleDebug(); // pass boolean to overide default
        /************************************************************* */
        if (!$debug) {
            // live input
            $data = json_decode($request->getContent(), true);
        } else {
            // test input
        }
        /************************************************************* */
        $input = @$data['params'];
        /************************************************************* */
        $response['status'] = 'success';
        $response['faq'] = DB::table('customer_faq_help as cfh')
            ->select(
                'cfh.question',
                'cfh.answer',
                'cfh.order_priority',
            )
            ->where([['cfh.deleted_at', '=', null]])
            ->orderBy('cfh.order_priority', 'ASC')
            ->get();
        $response['message'] = sizeof($response['faq']) ? "Help FAQ fetched successfully." : "No Help FAQ found.";
        return Response::json(array('result' => $response, 'debug' => $debug), 200, array(), customerResponseJsonConstants());
    }
}
