<div style="width: 600px; height:auto; padding: 0px 0px 10px 0px;">
    @if($greeting)
    <p style="font-family: Tahoma, Geneva, sans-serif; font-size:14px; color: #555; line-height:20px; padding:20px 0px 0px 0px; font-weight: bold; margin:0px; -webkit-text-size-adjust: 100%; -moz-text-size-adjust: 100%; -ms-text-size-adjust: 100%;">{!!$greeting!!},</p>
    @endif
    <p style="font-family: Tahoma, Geneva, sans-serif; font-size:14px; color: #555; line-height:20px; padding: 12px 0px 15px 0px; margin:0px; -webkit-text-size-adjust: 100%; -moz-text-size-adjust: 100%; -ms-text-size-adjust: 100%;">{!!$text_body!!}</p>
</div>