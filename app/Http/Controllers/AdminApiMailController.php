<?php

namespace App\Http\Controllers;

use App\Models\Booking;
use App\Models\BookingAnswer;
use App\Models\BookingCleaningSupply;
use App\Models\BookingExtraService;
use App\Models\BookingPackages;
use App\Models\BookingSubscriptionPackages;
use App\Models\BookingWeekday;
use App\Models\Customer;
use App\Models\CustomerAddress;
use App\Models\DayServices;
use App\Models\OnlinePayment;
use Carbon\Carbon;
use Config;
use File;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use Intervention\Image\Facades\Image;
use Mail;
use Response;
use App\Models\Settings;
use App\Models\BookingDeleteRemarks;
use stdClass;

class AdminApiMailController extends Controller
{
    public function booking_confirmation_to_admin($booking_id)
    {
        try {
            $settings = Settings::first();
            /******************************************************************************************* */
            $bookings = DB::table('bookings as b')
                ->select(
                    'b.*',
                    DB::raw('(CASE WHEN (b.reference_id IS NULL OR b.reference_id = "") THEN b.booking_id ELSE b.reference_id END) as reference_id'),
                    DB::raw('IFNULL(st.customer_app_service_type_name,st.service_type_name) as service_type_name'),
                    'st.service_type_model_id',
                    'stm.model as service_type_model',
                    'm.maid_id',
                    'm.maid_name',
                    DB::raw('(CASE WHEN b.cleaning_material = "Y" THEN "Yes" ELSE "No" END) as cleaning_material_opted'),
                    'f.name as frequency',
                    'ca.customer_address as address',
                    DB::raw('"' . Config::get('values.currency_code') . '" as currency'),
                    DB::raw('(CASE WHEN b.payment_type_id IS NULL THEN b.pay_by ELSE pt.name END) as payment_method'),
                    'pt.charge as payment_type_charge',
                    DB::raw('DAYNAME(b.service_start_date) as service_week_day_name'),
                    'op.transaction_id as op_transaction_id',
                    'op.payment_status as op_payment_status'
                )
                ->leftJoin('service_types as st', 'b.service_type_id', 'st.service_type_id')
                ->leftJoin('service_type_models as stm', 'st.service_type_model_id', 'stm.id')
                ->leftJoin('maids as m', 'b.maid_id', 'm.maid_id')
                ->leftJoin('customers as c', 'b.customer_id', 'c.customer_id')
                ->leftJoin('frequencies as f', 'b.booking_type', 'f.code')
                ->leftJoin('payment_types as pt', 'b.payment_type_id', 'pt.id')
                ->leftJoin('customer_addresses as ca', 'b.customer_address_id', 'ca.customer_address_id')
                ->leftJoin('online_payments as op', function ($join) {
                    //$join->on('b.booking_id', '=', 'op.booking_id');
                    $join->whereColumn('op.reference_id', 'b.reference_id');
                    $join->where('op.payment_status', 'success');
                })
                ->where(function ($query) use ($booking_id) {
                    $query->where('b.booking_common_id', $booking_id);
                    $query->orWhere('b.booking_id', $booking_id);
                })
                ->orderBy('b.booking_id', 'ASC')
                ->get();
            $booking = $bookings[0];
            /******************************************************************************************* */
            $customer = Customer::where('customer_id', '=', $booking->customer_id)->first();
            $customer_address = CustomerAddress::
                select(
                    'customer_addresses.*',
                    'a.area_name',
                )
                ->leftJoin('areas as a', 'customer_addresses.area_id', 'a.area_id')->where('customer_address_id', '=', $booking->customer_address_id)->first();
            /******************************************************************************************* */
            $booking_answers = BookingAnswer::where('booking_id', $booking_id)->get();
            /******************************************************************************************* */
            $cleaning_supplies = BookingCleaningSupply::
                select(
                    'booking_cleaning_supplies.cleaning_supply_id',
                    'ccs.name',
                    'booking_cleaning_supplies.unit_rate',
                    'booking_cleaning_supplies.quantity',
                    'booking_cleaning_supplies.total_amount'
                )
                ->leftJoin('config_cleaning_supplies as ccs', 'booking_cleaning_supplies.cleaning_supply_id', 'ccs.id')
                ->where('booking_cleaning_supplies.booking_id', $booking_id)
                ->get();
            /******************************************************************************************* */
            $extra_services = BookingExtraService::
                select(
                    'booking_extra_services.extra_service_id',
                    'es.service',
                    'booking_extra_services.duration',
                    'booking_extra_services.unit_rate',
                    'booking_extra_services.quantity',
                    'booking_extra_services.total_amount'
                )
                ->leftJoin('extra_services as es', 'booking_extra_services.extra_service_id', 'es.id')
                ->where('booking_extra_services.booking_id', $booking_id)
                ->get();
            /******************************************************************************************* */
            $packages = BookingPackages::
                select(
                    'booking_packages.building_type',
                    'booking_packages.building_room',
                    'booking_packages.name',
                    'booking_packages.unit_price',
                    'booking_packages.quantity',
                    'booking_packages.total'
                )
                ->where('booking_packages.booking_id', $booking_id)
                ->get();
            /******************************************************************************************* */
            $subscription_package = BookingSubscriptionPackages::where('booking_id', $booking_id)->first();
            /******************************************************************************************* */
            $booking_weekdays = BookingWeekday::
                select(
                    'wd.week_name'
                )
                ->leftJoin('week_days as wd', 'booking_weekdays.week_day_id', 'wd.week_day_id')
                ->where('booking_weekdays.booking_id', $booking_id)
                ->orderBy('wd.week_day_id', 'asc')
                ->get();
            $weekdays = array_column($booking_weekdays->toArray(), 'week_name');
            /******************************************************************************************* */
            Mail::send(
                'emails.booking-confirmation-to-admin',
                [
                    'settings' => $settings,
                    'greeting' => 'Hello Admin',
                    'text_body' => 'You have got a new booking.',
                    'booking' => $booking,
                    'bookings' => $bookings,
                    'customer' => $customer,
                    'customer_address' => $customer_address,
                    'booking_answers' => $booking_answers,
                    'cleaning_supplies' => $cleaning_supplies,
                    'extra_services' => $extra_services,
                    'packages' => $packages,
                    'subscription_package' => $subscription_package,
                    'weekdays' => $weekdays
                ],
                function ($m) use ($booking) {
                    $m->from(Config::get('mail.mail_from_address'), Config::get('mail.mail_from_name'));
                    $m->to(Config::get('mail.booking_confirmation_mails_to'));
                    $m->cc(Config::get('mail.booking_confirmation_mails_cc'));
                    $m->bcc(Config::get('mail.booking_confirmation_mails_bcc'));
                    $m->subject('Cleaning Booking Confirmation - ' . $booking->reference_id);
                }
            );
            /******************************************************************************************* */
        } catch (\Exception $e) {
            $booking = Booking::find($booking_id);
            $booking->mail_booking_confirmation_to_admin_status = 0;
            $booking->save();
            //throw new \ErrorException($e->getMessage());
        }
    }
    public function booking_confirmation_to_customer($booking_id)
    {
        try {
            $settings = Settings::first();
            /******************************************************************************************* */
            $bookings = DB::table('bookings as b')
                ->select(
                    'b.*',
                    DB::raw('(CASE WHEN (b.reference_id IS NULL OR b.reference_id = "") THEN b.booking_id ELSE b.reference_id END) as reference_id'),
                    DB::raw('IFNULL(st.customer_app_service_type_name,st.service_type_name) as service_type_name'),
                    'st.service_type_model_id',
                    'stm.model as service_type_model',
                    'm.maid_id',
                    'm.maid_name',
                    DB::raw('(CASE WHEN b.cleaning_material = "Y" THEN "Yes" ELSE "No" END) as cleaning_material_opted'),
                    'f.name as frequency',
                    'ca.customer_address as address',
                    DB::raw('"' . Config::get('values.currency_code') . '" as currency'),
                    DB::raw('(CASE WHEN b.payment_type_id IS NULL THEN b.pay_by ELSE pt.name END) as payment_method'),
                    'pt.charge as payment_type_charge',
                    DB::raw('DAYNAME(b.service_start_date) as service_week_day_name'),
                    'op.transaction_id as op_transaction_id',
                    'op.payment_status as op_payment_status'
                )
                ->leftJoin('service_types as st', 'b.service_type_id', 'st.service_type_id')
                ->leftJoin('service_type_models as stm', 'st.service_type_model_id', 'stm.id')
                ->leftJoin('maids as m', 'b.maid_id', 'm.maid_id')
                ->leftJoin('customers as c', 'b.customer_id', 'c.customer_id')
                ->leftJoin('frequencies as f', 'b.booking_type', 'f.code')
                ->leftJoin('payment_types as pt', 'b.payment_type_id', 'pt.id')
                ->leftJoin('customer_addresses as ca', 'b.customer_address_id', 'ca.customer_address_id')
                ->leftJoin('online_payments as op', function ($join) {
                    //$join->on('b.booking_id', '=', 'op.booking_id');
                    $join->whereColumn('op.reference_id', 'b.reference_id');
                    $join->where('op.payment_status', 'success');
                })
                ->where(function ($query) use ($booking_id) {
                    $query->where('b.booking_common_id', $booking_id);
                    $query->orWhere('b.booking_id', $booking_id);
                })
                ->orderBy('b.booking_id', 'ASC')
                ->get();
            $booking = $bookings[0];
            /******************************************************************************************* */
            $customer = Customer::where('customer_id', '=', $booking->customer_id)->first();
            $customer_address = CustomerAddress::
                select(
                    'customer_addresses.*',
                    'a.area_name',
                )
                ->leftJoin('areas as a', 'customer_addresses.area_id', 'a.area_id')->where('customer_address_id', '=', $booking->customer_address_id)->first();
            /******************************************************************************************* */
            $booking_answers = BookingAnswer::where('booking_id', $booking_id)->get();
            /******************************************************************************************* */
            $cleaning_supplies = BookingCleaningSupply::
                select(
                    'booking_cleaning_supplies.cleaning_supply_id',
                    'ccs.name',
                    'booking_cleaning_supplies.unit_rate',
                    'booking_cleaning_supplies.quantity',
                    'booking_cleaning_supplies.total_amount'
                )
                ->leftJoin('config_cleaning_supplies as ccs', 'booking_cleaning_supplies.cleaning_supply_id', 'ccs.id')
                ->where('booking_cleaning_supplies.booking_id', $booking_id)
                ->get();
            /******************************************************************************************* */
            $extra_services = BookingExtraService::
                select(
                    'booking_extra_services.extra_service_id',
                    'es.service',
                    'booking_extra_services.duration',
                    'booking_extra_services.unit_rate',
                    'booking_extra_services.quantity',
                    'booking_extra_services.total_amount'
                )
                ->leftJoin('extra_services as es', 'booking_extra_services.extra_service_id', 'es.id')
                ->where('booking_extra_services.booking_id', $booking_id)
                ->get();
            /******************************************************************************************* */
            $packages = BookingPackages::
                select(
                    'booking_packages.building_type',
                    'booking_packages.building_room',
                    'booking_packages.name',
                    'booking_packages.unit_price',
                    'booking_packages.quantity',
                    'booking_packages.total'
                )
                ->where('booking_packages.booking_id', $booking_id)
                ->get();
            /******************************************************************************************* */
            $subscription_package = BookingSubscriptionPackages::where('booking_id', $booking_id)->first();
            /******************************************************************************************* */
            $booking_weekdays = BookingWeekday::
                select(
                    'wd.week_name'
                )
                ->leftJoin('week_days as wd', 'booking_weekdays.week_day_id', 'wd.week_day_id')
                ->where('booking_weekdays.booking_id', $booking_id)
                ->orderBy('wd.week_day_id', 'asc')
                ->get();
            $weekdays = array_column($booking_weekdays->toArray(), 'week_name');
            /******************************************************************************************* */
            if ($customer->email_address != '') {
                Mail::send(
                    'emails.booking-confirmation-to-customer',
                    [
                        'settings' => $settings,
                        'greeting' => 'Hello ' . $customer->customer_name,
                        'text_body' => 'Thank You for booking with us. You can find your booking details below. We strive to remain your first choice.',
                        'booking' => $booking,
                        'bookings' => $bookings,
                        'customer' => $customer,
                        'customer_address' => $customer_address,
                        'booking_answers' => $booking_answers,
                        'cleaning_supplies' => $cleaning_supplies,
                        'extra_services' => $extra_services,
                        'packages' => $packages,
                        'subscription_package' => $subscription_package,
                        'weekdays' => $weekdays
                    ],
                    function ($m) use ($booking, $customer) {
                        $m->from(Config::get('mail.mail_from_address'), Config::get('mail.mail_from_name'));
                        $m->to($customer->email_address, $customer->customer_name);
                        $m->bcc('samnad.s@azinova.info', 'Samnad S - Developer');
                        $m->subject('Cleaning Booking Confirmation - ' . $booking->reference_id);
                    }
                );
            } else {
                throw new \ErrorException('Cusomer email address not set.');
            }
            /******************************************************************************************* */
        } catch (\Exception $e) {
            $booking = Booking::find($booking_id);
            $booking->mail_booking_confirmation_to_customer_status = 0;
            $booking->save();
            //throw new \ErrorException($e->getMessage());
        }
    }
    public function schedule_cancel_to_admin($booking_id, $service_date)
    {
        /**
         *
         * send schedule cancellation mail to admin for a specific service date
         *
         */
        try {
            $settings = Settings::first();
            /******************************************************************************************* */
            $bookings = DB::table('bookings as b')
                ->select(
                    'b.*',
                    DB::raw('(CASE WHEN (b.reference_id IS NULL OR b.reference_id = "") THEN b.booking_id ELSE b.reference_id END) as reference_id'),
                    DB::raw('IFNULL(st.customer_app_service_type_name,st.service_type_name) as service_type_name'),
                    'st.service_type_model_id',
                    'stm.model as service_type_model',
                    'm.maid_id',
                    'm.maid_name',
                    DB::raw('(CASE WHEN b.cleaning_material = "Y" THEN "Yes" ELSE "No" END) as cleaning_material_opted'),
                    'f.name as frequency',
                    'ca.customer_address as address',
                    DB::raw('"' . Config::get('values.currency_code') . '" as currency'),
                    DB::raw('(CASE WHEN b.payment_type_id IS NULL THEN b.pay_by ELSE pt.name END) as payment_method'),
                    'pt.charge as payment_type_charge',
                    DB::raw('DAYNAME(b.service_start_date) as service_week_day_name'),
                    'op.transaction_id as op_transaction_id',
                    'op.payment_status as op_payment_status'
                )
                ->leftJoin('service_types as st', 'b.service_type_id', 'st.service_type_id')
                ->leftJoin('service_type_models as stm', 'st.service_type_model_id', 'stm.id')
                ->leftJoin('maids as m', 'b.maid_id', 'm.maid_id')
                ->leftJoin('customers as c', 'b.customer_id', 'c.customer_id')
                ->leftJoin('frequencies as f', 'b.booking_type', 'f.code')
                ->leftJoin('payment_types as pt', 'b.payment_type_id', 'pt.id')
                ->leftJoin('customer_addresses as ca', 'b.customer_address_id', 'ca.customer_address_id')
                ->leftJoin('online_payments as op', function ($join) {
                    //$join->on('b.booking_id', '=', 'op.booking_id');
                    $join->whereColumn('op.reference_id', 'b.reference_id');
                    $join->where('op.payment_status', 'success');
                })
                ->where(function ($query) use ($booking_id) {
                    $query->where('b.booking_common_id', $booking_id);
                    $query->orWhere('b.booking_id', $booking_id);
                })
                ->orderBy('b.booking_id', 'ASC')
                ->get();
            $booking = $bookings[0];
            /******************************************************************************************* */
            $customer = Customer::where('customer_id', '=', $booking->customer_id)->first();
            /******************************************************************************************* */
            $customer_address = CustomerAddress::
                select(
                    'customer_addresses.*',
                    'a.area_name',
                )
                ->leftJoin('areas as a', 'customer_addresses.area_id', 'a.area_id')->where('customer_address_id', '=', $booking->customer_address_id)->first();
            /******************************************************************************************* */
            $booking_delete_remark = BookingDeleteRemarks::where([['booking_id', '=', $booking_id], ['service_date', '=', $service_date]])->first();
            Mail::send(
                'emails.booking-cancel-to-admin',
                [
                    'settings' => $settings,
                    'greeting' => 'Hello Admin',
                    'text_body' => 'New cancellation received.',
                    'booking' => $booking,
                    'bookings' => $bookings,
                    'booking_delete_remark' => $booking_delete_remark,
                    'customer' => $customer,
                    'customer_address' => $customer_address,
                    'service_date' => $service_date,
                    'test' => isset($_GET['test'])
                ],
                function ($m) use ($booking) {
                    $m->from(Config::get('mail.mail_from_address'), Config::get('mail.mail_from_name'));
                    $m->to(Config::get('mail.mail_to_admin_address'), Config::get('mail.mail_to_admin_address_name'));
                    //$m->bcc('samnad.s@azinova.info', 'Samnad S - Developer');
                    $m->subject('Booking Cancelled ' . $booking->reference_id);
                }
            );
            /******************************************************************************************* */
        } catch (\Exception $e) {
            //throw new \ErrorException($e->getMessage());
        }
    }
    public function schedule_cancel_to_customer($booking_id, $service_date)
    {
        /**
         *
         * send schedule cancellation mail to customer for a specific service date
         *
         */
        try {
            $settings = Settings::first();
            /******************************************************************************************* */
            $bookings = DB::table('bookings as b')
                ->select(
                    'b.*',
                    DB::raw('(CASE WHEN (b.reference_id IS NULL OR b.reference_id = "") THEN b.booking_id ELSE b.reference_id END) as reference_id'),
                    DB::raw('IFNULL(st.customer_app_service_type_name,st.service_type_name) as service_type_name'),
                    'st.service_type_model_id',
                    'stm.model as service_type_model',
                    'm.maid_id',
                    'm.maid_name',
                    DB::raw('(CASE WHEN b.cleaning_material = "Y" THEN "Yes" ELSE "No" END) as cleaning_material_opted'),
                    'f.name as frequency',
                    'ca.customer_address as address',
                    DB::raw('"' . Config::get('values.currency_code') . '" as currency'),
                    DB::raw('(CASE WHEN b.payment_type_id IS NULL THEN b.pay_by ELSE pt.name END) as payment_method'),
                    'pt.charge as payment_type_charge',
                    DB::raw('DAYNAME(b.service_start_date) as service_week_day_name'),
                    'op.transaction_id as op_transaction_id',
                    'op.payment_status as op_payment_status'
                )
                ->leftJoin('service_types as st', 'b.service_type_id', 'st.service_type_id')
                ->leftJoin('service_type_models as stm', 'st.service_type_model_id', 'stm.id')
                ->leftJoin('maids as m', 'b.maid_id', 'm.maid_id')
                ->leftJoin('customers as c', 'b.customer_id', 'c.customer_id')
                ->leftJoin('frequencies as f', 'b.booking_type', 'f.code')
                ->leftJoin('payment_types as pt', 'b.payment_type_id', 'pt.id')
                ->leftJoin('customer_addresses as ca', 'b.customer_address_id', 'ca.customer_address_id')
                ->leftJoin('online_payments as op', function ($join) {
                    //$join->on('b.booking_id', '=', 'op.booking_id');
                    $join->whereColumn('op.reference_id', 'b.reference_id');
                    $join->where('op.payment_status', 'success');
                })
                ->where(function ($query) use ($booking_id) {
                    $query->where('b.booking_common_id', $booking_id);
                    $query->orWhere('b.booking_id', $booking_id);
                })
                ->orderBy('b.booking_id', 'ASC')
                ->get();
            $booking = $bookings[0];
            /******************************************************************************************* */
            $customer = Customer::where('customer_id', '=', $booking->customer_id)->first();
            /******************************************************************************************* */
            $customer_address = CustomerAddress::
                select(
                    'customer_addresses.*',
                    'a.area_name',
                )
                ->leftJoin('areas as a', 'customer_addresses.area_id', 'a.area_id')->where('customer_address_id', '=', $booking->customer_address_id)->first();
            /******************************************************************************************* */
            $booking_delete_remark = BookingDeleteRemarks::where([['booking_id', '=', $booking_id], ['service_date', '=', $service_date]])->first();
            if ($customer->email_address != '') {
                Mail::send(
                    'emails.booking-cancel-to-customer',
                    [
                        'settings' => $settings,
                        'greeting' => 'Dear ' . $customer->customer_name,
                        'text_body' => 'Your booking with Reference No. <b>' . $booking->reference_id . '</b> on service date <b>' . Carbon::createFromFormat('Y-m-d', @$service_date)->format('d M Y') . '</b> has been cancelled successfully.',
                        'booking' => $booking,
                        'bookings' => $bookings,
                        'booking_delete_remark' => $booking_delete_remark,
                        'customer' => $customer,
                        'customer_address' => $customer_address,
                        'service_date' => $service_date,
                        'test' => isset($_GET['test'])
                    ],
                    function ($m) use ($booking, $customer) {
                        $m->from(Config::get('mail.mail_from_address'), Config::get('mail.mail_from_name'));
                        $m->to($customer->email_address, $customer->customer_name);
                        //$m->bcc('samnad.s@azinova.info', 'Samnad S - Developer');
                        $m->subject('Booking Cancelled ' . $booking->reference_id);
                    }
                );
            }
            /******************************************************************************************* */
        } catch (\Exception $e) {
            //throw new \ErrorException($e->getMessage());
        }
    }
    public function registration_success_to_customer($customer_id)
    {
        /**
         *
         * send registration confirmation mail to customer
         *
         */
        try {
            $settings = Settings::first();
            $customer = Customer::where('customer_id', '=', $customer_id)->first();
            if ($customer->email_address != '') {
                Mail::send(
                    'emails.registration-success-to-customer',
                    [
                        'settings' => $settings,
                        'greeting' => 'Dear ' . $customer->customer_name,
                        'text_body' => 'Thank you for registration, you\'ve successfully registered on ' . Config::get('values.company_name') . '.',
                        'test' => isset($_GET['test'])
                    ],
                    function ($m) use ($customer) {
                        $m->from(Config::get('mail.mail_from_address'), Config::get('mail.mail_from_name'));
                        $m->to($customer->email_address, $customer->customer_name);
                        //$m->bcc('samnad.s@azinova.info', 'Samnad S - Developer');
                        $m->subject('Registration Confirmation');
                    }
                );
            }
        } catch (\Exception $e) {
            //throw new \ErrorException($e->getMessage());
        }
    }
    public function service_rating_feedback_to_admin($day_service_id)
    {
        /**
         *
         * send service rating feedback  mail to customer
         *
         */
        try {
            $settings = Settings::first();
            // $day_service = DayServices::find($day_service_id);
            $day_service = DayServices::
                select(
                    'day_services.*',
                    'm.maid_name',
                )
                ->leftJoin('maids as m', 'day_services.maid_id', 'm.maid_id')->where('day_services.day_service_id', '=', $day_service_id)->first();
            $booking = Booking::find($day_service->booking_id);
            $customer = Customer::where('customer_id', '=', $booking->customer_id)->first();
            Mail::send(
                'emails.service-rating-feedback-to-admin',
                [
                    'settings' => $settings,
                    'greeting' => 'Hello Admin',
                    'text_body' => 'You have received a new service feedback from <b>' . $customer->customer_name . '</b> for the service dated <b>' . Carbon::createFromFormat('Y-m-d', $day_service->service_date)->format('d M Y') . '</b> from <strong>' . Carbon::createFromFormat('H:i:s', $booking->time_from)->format('h:i A') . '</strong> to <strong>' . Carbon::createFromFormat('H:i:s', $booking->time_to)->format('h:i A') . '</strong>.',
                    'day_service' => $day_service,
                    'test' => isset($_GET['test'])
                ],
                function ($m) use ($day_service) {
                    $m->from(Config::get('mail.mail_from_address'), Config::get('mail.mail_from_name'));
                    $m->to(Config::get('mail.service_rating_feedback_mails_to'));
                    $m->cc(Config::get('mail.service_rating_feedback_mails_cc'));
                    $m->bcc(Config::get('mail.service_rating_feedback_mails_bcc'));
                    $m->subject('New Service Feedback ' . $day_service->day_service_id);
                }
            );
        } catch (\Exception $e) {
            //throw new \ErrorException($e->getMessage());
        }
    }
    public function login_otp_to_customer($customer_id, $otp = '1234')
    {
        /**
         *
         * send login otp mail to customer
         *
         */
        try {
            $settings = Settings::first();
            $customer = Customer::where('customer_id', '=', $customer_id)->first();
            if ($customer->email_address != '') {
                Mail::send(
                    'emails.login-otp-to-customer',
                    [
                        'settings' => $settings,
                        'greeting' => null,
                        'text_body' => 'Your Login OTP for ' . Config::get('values.company_name') . ' is <b>' . $otp . '</b>.',
                        'test' => isset($_GET['test'])
                    ],
                    function ($m) use ($customer) {
                        $m->from(Config::get('mail.mail_from_address'), Config::get('mail.mail_from_name'));
                        $m->to($customer->email_address, $customer->customer_name);
                        //$m->bcc('samnad.s@azinova.info', 'Samnad S - Developer');
                        $m->subject('Login OTP - ' . Config::get('values.company_name'));
                    }
                );
            }
        } catch (\Exception $e) {
            //throw new \ErrorException($e->getMessage());
        }
    }
    public function payment_link_to_customer($customer_id = null, $amount = null, $description = null, Request $request)
    {
        /**
         *
         * payment link mail to customer
         *
         */
        try {
            $settings = Settings::first();
            /************************************** */
            $customer_id = $request->customer_id ?: $customer_id;
            $amount = $request->amount ?: $amount;
            $description = $request->description ?: $description;
            /************************************** */
            $customer = Customer::where('customer_id', '=', $customer_id)->first();
            if ($customer->email_address != '') {
                Mail::send(
                    'emails.payment-link-to-customer',
                    [
                        'settings' => $settings,
                        'greeting' => 'Dear ' . $customer->customer_name,
                        'customer_id' => $customer_id,
                        'amount' => $amount,
                        'description' => $description,
                        'text_body' => 'It seems like you have some payments due. If you wish to make the payment, click the below button.',
                        'test' => isset($_GET['test'])
                    ],
                    function ($m) use ($customer, $amount, $description) {
                        $m->from(Config::get('mail.mail_from_address'), Config::get('mail.mail_from_name'));
                        $m->to($customer->email_address, $customer->customer_name);
                        //$m->bcc('samnad.s@azinova.info', 'Samnad S - Developer');
                        $m->subject('Due Payment of Amount ' . $amount . ' AED');
                    }
                );
            }
        } catch (\Exception $e) {
            throw new \ErrorException($e->getMessage());
        }
    }
    public function schedule_confirmation_to_customer($booking_id, $service_date)
    {
        try {
            $settings = Settings::first();
            /******************************************************************************************* */
            $bookings = DB::table('bookings as b')
                ->select(
                    'b.*',
                    DB::raw('(CASE WHEN (b.reference_id IS NULL OR b.reference_id = "") THEN b.booking_id ELSE b.reference_id END) as reference_id'),
                    DB::raw('IFNULL(st.customer_app_service_type_name,st.service_type_name) as service_type_name'),
                    'st.service_type_model_id',
                    'stm.model as service_type_model',
                    'm.maid_id',
                    'm.maid_name',
                    DB::raw('(CASE WHEN b.cleaning_material = "Y" THEN "Yes" ELSE "No" END) as cleaning_material_opted'),
                    'f.name as frequency',
                    'ca.customer_address as address',
                    DB::raw('"' . Config::get('values.currency_code') . '" as currency'),
                    DB::raw('(CASE WHEN b.payment_type_id IS NULL THEN b.pay_by ELSE pt.name END) as payment_method'),
                    'pt.charge as payment_type_charge',
                    DB::raw('DAYNAME(b.service_start_date) as service_week_day_name'),
                    'op.transaction_id as op_transaction_id',
                    'op.payment_status as op_payment_status'
                )
                ->leftJoin('service_types as st', 'b.service_type_id', 'st.service_type_id')
                ->leftJoin('service_type_models as stm', 'st.service_type_model_id', 'stm.id')
                ->leftJoin('maids as m', 'b.maid_id', 'm.maid_id')
                ->leftJoin('customers as c', 'b.customer_id', 'c.customer_id')
                ->leftJoin('frequencies as f', 'b.booking_type', 'f.code')
                ->leftJoin('payment_types as pt', 'b.payment_type_id', 'pt.id')
                ->leftJoin('customer_addresses as ca', 'b.customer_address_id', 'ca.customer_address_id')
                ->leftJoin('online_payments as op', function ($join) {
                    $join->on('b.booking_id', '=', 'op.booking_id');
                    $join->whereColumn('op.reference_id', 'b.reference_id');
                    $join->where('op.payment_status', 'success');
                })
                ->where(function ($query) use ($booking_id) {
                    $query->where('b.booking_common_id', $booking_id);
                    $query->orWhere('b.booking_id', $booking_id);
                })
                ->orderBy('b.booking_id', 'ASC')
                ->get();
            $booking = $bookings[0];
            /******************************************************************************************* */
            $frequency = DB::table('frequencies as f')
                ->select(
                    'f.*',
                )
                ->where(['f.code' => $booking->booking_type])
                ->first();
            /******************************************************************************************* */
            $customer = Customer::where('customer_id', '=', $booking->customer_id)->first();
            $customer_address = CustomerAddress::
                select(
                    'customer_addresses.*',
                    'a.area_name',
                )
                ->leftJoin('areas as a', 'customer_addresses.area_id', 'a.area_id')->where('customer_address_id', '=', $booking->customer_address_id)->first();
            /******************************************************************************************* */
            if (trim($customer->email_address) != "") {
                Mail::send(
                    'emails.schedule-confirmation-to-customer',
                    [
                        'settings' => $settings,
                        'greeting' => 'Dear ' . $customer->customer_name,
                        'text_body' => 'Your schedule on <strong>' . Carbon::createFromFormat('Y-m-d', $service_date)->format('d M Y') . '</strong> with Ref. No. <b>' . $booking->reference_id . '</b> has been confirmed. There might be chance of delay in dropping the crew due to the traffic, thanks for understanding in advance.',
                        'booking' => $booking,
                        'bookings' => $bookings,
                        'frequency' => $frequency,
                        'customer' => $customer,
                        'customer_address' => $customer_address,
                        'service_date' => $service_date,
                        'test' => isset($_GET['test'])
                    ],
                    function ($m) use ($booking, $customer) {
                        $m->from(Config::get('mail.mail_from_address'), Config::get('mail.mail_from_name'));
                        $m->to($customer->email_address, $customer->customer_name);
                        //$m->bcc('samnad.s@azinova.info', 'Samnad S - Azinova Developer');
                        $m->subject('Booking Confirmation ' . $booking->reference_id);
                    }
                );
            } else {
                return Response::json(['status' => false, 'message' => 'No email address found for customer.'], 200, [], JSON_PRETTY_PRINT);
            }
            /******************************************************************************************* */
            return Response::json(['status' => true, 'message' => 'Mail send succesfully.'], 200, [], JSON_PRETTY_PRINT);
        } catch (\Exception $e) {
            return Response::json(['status' => false, 'message' => $e->getMessage()], 200, [], JSON_PRETTY_PRINT);
        }
    }
    public function mail_to_customer(Request $request)
    {
        try {
            /******************************************************************************************* */
            $customer_id = @$request->customer_id;
            $subject = @$request->subject;
            $text_body = @$request->text_body;
            $validator = Validator::make(
                [
                    'customer_id' => $customer_id,
                    'subject' => $subject,
                    'text_body' => $text_body,
                ],
                [
                    'customer_id' => 'required|integer',
                    'subject' => 'required|string',
                    'text_body' => 'required|string',
                ],
                [],
                [
                    'customer_id' => 'Customer ID',
                    'subject' => 'Subject',
                    'text_body' => 'Mail Body Content',
                ]
            );
            if ($validator->fails()) {
                return Response::json(['status' => false, 'message' => $validator->errors()->first()], 200, [], JSON_PRETTY_PRINT);
            }
            /******************************************************************************************* */
            $settings = Settings::first();
            $customer = Customer::where('customer_id', '=', $customer_id)->first();
            /******************************************************************************************* */
            if (trim($customer->email_address) != "") {
                Mail::send(
                    'emails.mail-to-customer',
                    [
                        'settings' => $settings,
                        'greeting' => 'Dear ' . $customer->customer_name,
                        'text_body' => $text_body,
                        'test' => isset($_GET['test'])
                    ],
                    function ($m) use ($customer, $subject) {
                        $m->from(Config::get('mail.mail_from_address'), Config::get('mail.mail_from_name'));
                        $m->to($customer->email_address, $customer->customer_name);
                        //$m->bcc('samnad.s@azinova.info', 'Samnad S - Azinova Developer');
                        $m->subject($subject);
                    }
                );
            } else {
                return Response::json(['status' => false, 'message' => 'No email address found for customer.'], 200, [], JSON_PRETTY_PRINT);
            }
            /******************************************************************************************* */
            return Response::json(['status' => true, 'message' => 'Mail send succesfully.'], 200, [], JSON_PRETTY_PRINT);
        } catch (\Exception $e) {
            return Response::json(['status' => false, 'message' => $e->getMessage()], 200, [], JSON_PRETTY_PRINT);
        }
    }
    public function online_payment_confirm_to_customer($payment_id)
    {
        try {
            /******************************************************************************************* */
            $validator = Validator::make(
                [
                    'payment_id' => $payment_id,
                ],
                [
                    'payment_id' => 'required|exists:online_payments,payment_id',
                ],
                [],
                [
                    'payment_id' => 'Payment ID',
                ]
            );
            if ($validator->fails()) {
                return Response::json(['status' => false, 'message' => $validator->errors()->first()], 200, [], JSON_PRETTY_PRINT);
            }
            /******************************************************************************************* */
            $settings = Settings::first();
            $online_payment = OnlinePayment::find($payment_id);
            $customer = Customer::where('customer_id', '=', $online_payment['customer_id'])->first();
            /******************************************************************************************* */
            if (trim($customer->email_address) != "") {
                Mail::send(
                    'emails.online_payment_confirm_to_customer',
                    [
                        'settings' => $settings,
                        'greeting' => 'Dear ' . $customer->customer_name,
                        'text_body' => 'Your payment of <b>' . ($online_payment['amount'] + $online_payment['transaction_charge']) . '  AED</b> is received.<br>Thank you!',
                        'online_payment' => $online_payment,
                        'test' => isset($_GET['test'])
                    ],
                    function ($m) use ($customer, $online_payment) {
                        $m->from(Config::get('mail.mail_from_address'), Config::get('mail.mail_from_name'));
                        $m->to($customer->email_address, $customer->customer_name);
                        $m->bcc('samnad.s@azinova.info', 'Samnad S - Azinova Developer');
                        $m->subject('Payment Completed #' . $online_payment['transaction_id']);
                    }
                );
            } else {
                return Response::json(['status' => false, 'message' => 'No email address found for customer.'], 200, [], JSON_PRETTY_PRINT);
            }
            /******************************************************************************************* */
        } catch (\Exception $e) {
            return Response::json(['status' => false, 'message' => $e->getMessage()], 200, [], JSON_PRETTY_PRINT);
        }
    }
    public function online_payment_confirm_to_admin($payment_id)
    {
        try {
            /******************************************************************************************* */
            $validator = Validator::make(
                [
                    'payment_id' => $payment_id,
                ],
                [
                    'payment_id' => 'required|exists:online_payments,payment_id',
                ],
                [],
                [
                    'payment_id' => 'Payment ID',
                ]
            );
            if ($validator->fails()) {
                return Response::json(['status' => false, 'message' => $validator->errors()->first()], 200, [], JSON_PRETTY_PRINT);
            }
            /******************************************************************************************* */
            $settings = Settings::first();
            $online_payment = OnlinePayment::find($payment_id);
            $customer = Customer::where('customer_id', '=', $online_payment['customer_id'])->first();
            $customer_address = CustomerAddress::
                select(
                    'customer_addresses.*',
                    'a.area_name',
                )
                ->leftJoin('areas as a', 'customer_addresses.area_id', 'a.area_id')
                ->where('customer_id', '=', $customer->customer_id)
                ->where('default_address', 1)
                ->first();
            /******************************************************************************************* */
            Mail::send(
                'emails.online_payment_confirm_to_admin',
                [
                    'settings' => $settings,
                    'greeting' => 'Hello Admin',
                    'text_body' => 'New payment of <b>' . ($online_payment['amount'] + $online_payment['transaction_charge']) . '  AED</b> received from customer <strong>' . $customer->customer_name . '</strong>.',
                    'customer' => $customer,
                    'customer_address' => $customer_address,
                    'online_payment' => $online_payment,
                    'test' => isset($_GET['test'])
                ],
                function ($m) use ($online_payment) {
                    $m->from(Config::get('mail.mail_from_address'), Config::get('mail.mail_from_name'));
                    $m->to(Config::get('mail.online_pay_confirmation_mails_to'));
                    $m->cc(Config::get('mail.online_pay_confirmation_mails_cc'));
                    $m->bcc(Config::get('mail.online_pay_confirmation_mails_bcc'));
                    $m->subject('Payment Received #' . $online_payment['transaction_id']);
                }
            );
            /******************************************************************************************* */
        } catch (\Exception $e) {
            return Response::json(['status' => false, 'message' => $e->getMessage()], 200, [], JSON_PRETTY_PRINT);
        }
    }
    public function template()
    {
        $settings = Settings::first();
        Mail::send(
            'emails.template',
            [
                'settings' => $settings,
                'test' => true
            ],
            function ($m) {
            }
        );
    }
}
