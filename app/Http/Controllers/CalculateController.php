<?php

namespace App\Http\Controllers;

use Carbon\Carbon;
use Config;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Log;
use Response;

class CalculateController extends Controller
{
    public function calculate(Request $request)
    {
        try {
            $debug = toggleDebug(false); // pass boolean to overide default
            if (!$debug) {
                // live input
                $data = json_decode($request->getContent(), true);
            } else {
                // test input
                $data['params']['id'] = Config::get('values.debug_customer_id');
                $data['params']['service_type_id'] = 1;
                $data['params']['date'] = "25/11/2023"; // dd/mm/yyyy format
                $data['params']['frequency'] = "WE";
                $data['params']['time'] = "13:00";
                $data['params']['hours'] = 2;
                $data['params']['professionals_count'] = 2;
                $professional_prefer = array(
                    // currently allowing same hours for all profs.
                    //array('professional_id' => 424),
                    //array('professional_id' => 457),
                );
                $data['params']['professional_prefer'] = $professional_prefer;
                $data['params']['cleaning_materials'] = true;
                $data['params']['address_id'] = Config::get('values.debug_customer_address_id');
                $data['params']['instructions'] = "my instruction";
                $data['params']['payment_method'] = 2;
                $data['params']['no_of_weeks'] = 0;
                //$data['params']['coupon_code'] = 'DEEP';
                //$data['params']['packages'] = [array('package_id' => 1, 'quantity' => 2), array('package_id' => 3, 'quantity' => 1)];
                //$data['params']['packages'] = [];
                //$data['params']['frequency_discount'] = null;
                //$data['params']['rush_slot_id'] = null;
                //$data['params']['addons'] = [array('service_addons_id' => 1, 'quantity' => 2), array('service_addons_id' => 2, 'quantity' => 5)];
                //$data['params']['addons'] = [];
                //$data['params']['subscription_package_id'] = 1;
                $data['params']['supervisor'] = true;
            }
            /************************************************************* */
            // required input check
            $input = @$data['params'];
            /************************************************************* */
            // format for machine :/
            if (@$input['date']) {
                $input['date'] = Carbon::createFromFormat('d/m/Y', $input['date'])->format('Y-m-d');
            }
            /************************************************************* */
            $calculate = calculate($input);
            $response['calculation_hash'] = null;
            $response['calculation_data'] = $calculate;
            $response['calculation_data']['input'] = $input;
            $response['calculation_hash'] = Hash::make(serialize($response['calculation_data']));
            $response['calculation_verified'] = Hash::check(serialize($response['calculation_data']), $response['calculation_hash']);
            $response['summary'] = $calculate['summary'];
            $response['coupons_applied'] = $calculate['coupons_applied'];
            $response['status'] = 'success';
            $response['message'] = 'Amount calculated successfully';
            return Response::json(array('result' => $response, 'debug' => $debug), 200, array(), customerResponseJsonConstants());
        } catch (\Exception $e) {
            DB::rollback();
            return Response::json(array('result' => array('status' => 'failed', 'message' => $e->getMessage() . ' ' . $e->getLine()), 'debug' => $debug), 200, array(), customerResponseJsonConstants());
        }
    }
}
