<div style="width: 560px; height:auto; background: #fff; border:1px solid #e5e2e2; padding: 10px 20px 10px 20px;">
    <table width="100%" border="0" cellpadding="0" cellspacing="0">
        <tr>
            <td>
                <p
                    style="font: bold 20px/25px 'Poppins', sans-serif;  color: #555; display: block; margin: 0px; padding: 0px 0px 15px; text-align: left; -webkit-text-size-adjust: 100%; -moz-text-size-adjust: 100%; -ms-text-size-adjust: 100%;">
                    {{ $booking->service_type_name }}</p>
            </td>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>
                <p
                    style="font: normal 16px/20px 'Poppins', sans-serif; color: #555; display: block; margin: 0px; padding: 0px 0px 10px; text-align: left; -webkit-text-size-adjust: 100%; -moz-text-size-adjust: 100%; -ms-text-size-adjust: 100%;">
                    {{ \Carbon\Carbon::createFromFormat('Y-m-d', @$booking->service_start_date)->format('d M Y') }}</p>
                <p
                    style="font: normal 16px/20px 'Poppins', sans-serif; color: #555; display: block; margin: 0px; padding: 0px 0px 10px; text-align: left; -webkit-text-size-adjust: 100%; -moz-text-size-adjust: 100%; -ms-text-size-adjust: 100%;">
                    {{ \Carbon\Carbon::createFromFormat('H:i:s', @$booking->time_from)->format('h:i A') }} -
                    {{ \Carbon\Carbon::createFromFormat('H:i:s', @$booking->time_to)->format('h:i A') }}</p>
                <!--<table width="100%" cellpadding="0" cellspacing="0">
                    <tr>
                        @foreach ($bookings as $key => $booking)
<td width="50px">
                                <div class="maid-photo"
                                    style="width: 40px; height: 40px; border-radius: 50px; overflow: hidden">
                                    <a href="javascript:void(0);"
                                        style="display: block; cursor: default !important;"><img
                                            src="http://demo.azinova.me/website/spectrum-booking/images/maid.jpg"
                                            width="100%" height="" alt=""
                                            style="border-radius: 5px; cursor: default !important;" />
                                    </a>
                                </div>
                            </td>
@endforeach
                        <td>&nbsp;</td>
                    </tr>
                </table>-->
            </td>
            <td align="right">
                <p
                    style="font: normal 16px/20px 'Poppins', sans-serif; color: #555; display: block; margin: 0px; padding: 0px 0px 10px; text-align: right; -webkit-text-size-adjust: 100%; -moz-text-size-adjust: 100%; -ms-text-size-adjust: 100%;">
                    Booking Ref.</p>
                <p
                    style="font: normal 16px/25px 'Poppins', sans-serif;font-weight:bold; color: #f76161; display: block; margin: 0px; padding: 0px 0px 15px; text-align: right; -webkit-text-size-adjust: 100%; -moz-text-size-adjust: 100%; -ms-text-size-adjust: 100%;">
                    {{ $booking->reference_id }}</p>
            </td>
        </tr>
        @if (trim($booking->op_payment_status) == 'success')
            <tr>
                <td align="right" colspan="2">
                    <p
                        style="font: normal 16px/20px 'Poppins', sans-serif; color: #555; display: block; margin: 0px; padding: 0px 0px 10px; text-align: right; -webkit-text-size-adjust: 100%; -moz-text-size-adjust: 100%; -ms-text-size-adjust: 100%;">
                        Transaction Ref.</p>
                    <p
                        style="font: normal 16px/25px 'Poppins', sans-serif;font-weight:bold; color: #5180ec; display: block; margin: 0px; padding: 0px 0px 15px; text-align: right; -webkit-text-size-adjust: 100%; -moz-text-size-adjust: 100%; -ms-text-size-adjust: 100%;">
                        {{ $booking->op_transaction_id }}</p>
                </td>
            </tr>
        @endif
    </table>
</div>
