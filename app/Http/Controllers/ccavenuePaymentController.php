<?php

namespace App\Http\Controllers;

use App\Models\Booking;
use App\Models\Customer;
use App\Models\CustomerAddress;
use App\Models\CustomerPayments;
use App\Models\Frequencies;
use App\Models\OnlinePayment;
use App\Models\UserActivity;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Log;
use Redirect;
use Response;
use Config;
use stdClass;

class ccavenuePaymentController extends Controller
{
    public function response_handler(Request $request)
    {
        try {
            $workingKey = config('ccavenue.workingkey'); //Working Key should be provided here.
            $encResponse = $request->encResp; //This is the response sent by the CCAvenue Server
            $rcvdString = ccavDecrypt($encResponse, $workingKey); //Crypto Decryption used as per the specified working key.
            parse_str($rcvdString, $params);
            // log::info('post cc avenue success response '.json_encode($params));
            if (@$params['order_status'] == 'Success') {
                if ($params['merchant_param1'] == 'web-booking') {
                    $this->after_ccavenue_payment_success($params);
                    $redirect_url = config('values.web_app_url') . 'booking/success/' . $params['order_id'];
                } else if ($params['merchant_param1'] == 'mobile-booking') {
                    $this->after_ccavenue_payment_success($params);
                    $redirect_url = url('payment/ccavenue/success/' . $params['order_id'] . '?status=Waiting');
                } else if ($params['merchant_param1'] == 'invoice-payment') {
                    $this->after_ccavenue_payment_success($params);
                    $redirect_url = config('values.web_app_url') . 'online-payment/success/' . $params['order_id'];
                } else if ($params['merchant_param1'] == 'outstanding-payment') {
                    $this->after_ccavenue_payment_success($params);
                    $redirect_url = url('payment/ccavenue/success/' . $params['order_id']);
                } else {
                    return $params['order_status'];
                }
            } else {
                if ($params['merchant_param1'] == 'web-booking') {
                    $redirect_url = config('values.web_app_url') . 'booking/failed/' . $params['order_id'];
                } else if ($params['merchant_param1'] == 'mobile-booking') {
                    $redirect_url = url('payment/ccavenue/failed/' . (@$params['order_id'] ?: $request->orderNo) . '?status=Payment Failed');
                } else if ($params['merchant_param1'] == 'invoice-payment') {
                    $redirect_url = config('values.web_app_url') . 'online-payment/failed/' . $params['order_id'];
                } else if ($params['merchant_param1'] == 'outstanding-payment') {
                    $redirect_url = url('payment/ccavenue/failed/' . (@$params['order_id'] ?: $request->orderNo) . '?status=Failed');
                } else {
                    return $params['order_status'];
                }
            }
            return Redirect::to($redirect_url);
        } catch (\Exception $e) {
            return Response::json(array('result' => array('status' => 'failed', 'message' => $e->getMessage())), 200, [], JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES | JSON_NUMERIC_CHECK);
        }
    }

    public function after_ccavenue_payment_success($params)
    {
        /**
         * $params - response from ccavenue
         */
        try {
            if (@$params['order_status'] == 'Success') {
                $mail = new AdminApiMailController;
                DB::beginTransaction();
                if ($params['merchant_param1'] == 'web-booking' || $params['merchant_param1'] == 'mobile-booking') {
                    $booking = Booking::where('reference_id', '=', $params['order_id'])->orderBy('booking_id', 'asc')->first(); // order by asc - important
                    $bookings = DB::table('bookings as b')
                        ->select(
                            'b.*',
                        )
                        ->where(function ($query) use ($booking) {
                            $query->where('b.booking_common_id', $booking->booking_id);
                            $query->orWhere('b.booking_id', $booking->booking_id);
                        })
                        ->orderBy('b.booking_id', 'asc')
                        ->get();
                    $bookings_ids = array_column($bookings->toArray(), 'booking_id');
                    // Update bookings
                    Booking::whereIn('booking_id', $bookings_ids)->update(['booking_status' => 0, 'web_book_show' => 1]);
                    /**
                     * Update payment data
                     */
                    // Save in online payments
                    $online_payment = new OnlinePayment;
                    $online_payment->transaction_id = $params['tracking_id'];
                    $online_payment->booking_id = $booking->booking_id;
                    $online_payment->reference_id = $params['order_id'];
                    $online_payment->amount = $booking->_taxed_amount;
                    $online_payment->transaction_charge = $booking->_payment_type_charge;
                    $online_payment->customer_id = $booking->customer_id;
                    $online_payment->payment_status = 'success';
                    $online_payment->payment_type = strtolower($booking->pay_by);
                    $online_payment->ip_address = @$_SERVER['REMOTE_ADDR'];
                    $online_payment->user_agent = @$_SERVER['HTTP_USER_AGENT'];
                    //$online_payment->post_data = null;
                    $online_payment->return_data = json_encode($params);
                    $online_payment->paid_from = bookingPlatform('web');
                    $online_payment->payment_datetime = now();
                    $online_payment->save();
                    // save also in customer payments
                    $customerPay = new CustomerPayments();
                    $customerPay->online_payment_id = $online_payment->payment_id;
                    $customerPay->verified_status = 1;
                    $customerPay->customer_id = $online_payment->customer_id;
                    $customerPay->paid_amount = $online_payment->amount;
                    $customerPay->balance_amount = $online_payment->amount;
                    $customerPay->paid_at = 'O';
                    $customerPay->ps_no = $online_payment->reference_id;
                    $customerPay->receipt_no = $params['tracking_id'];
                    $customerPay->paid_at_id = 1;
                    $customerPay->payment_method = 1;
                    $customerPay->day_service_id = 0;
                    $customerPay->paid_datetime = now();
                    $customerPay->save();
                    // Activity log
                    $frequency = Frequencies::where('code', $booking->booking_type)->first();
                    $customer = Customer::find($booking->customer_id);
                    $start = date("g:i a", strtotime($booking->time_from));
                    $end = date("g:i a", strtotime($booking->time_to));
                    $userActivity = new UserActivity();
                    $userActivity->added_user = 1;
                    $userActivity->booking_type = $frequency->name;
                    $userActivity->shift = date("H:i", strtotime($booking->time_from)) . '-' . date("H:i", strtotime($booking->time_to));
                    $userActivity->action_type = 'Booking_add';
                    $actionContent = 'A new booking for customer: ' . $customer->customer_name . ' at ' . $start . ' - ' . $end . ' is received.';
                    $userActivity->action_content = $actionContent;
                    $userActivity->addeddate = now();
                    $userActivity->date_time_added = now();
                    $userActivity->save();
                    DB::commit();
                    /**
                     * Send mails
                     */
                    $mail->booking_confirmation_to_admin($booking->booking_id);
                    $mail->booking_confirmation_to_customer($booking->booking_id);
                } else if ($params['merchant_param1'] == 'invoice-payment') {
                    // Save in online payments
                    $online_payment = OnlinePayment::where('reference_id', $params['order_id'])->first();
                    $online_payment->transaction_id = $params['tracking_id'];
                    $online_payment->payment_status = 'success';
                    $online_payment->ip_address = @$_SERVER['REMOTE_ADDR'];
                    $online_payment->user_agent = @$_SERVER['HTTP_USER_AGENT'];
                    $online_payment->return_data = json_encode($params);
                    $online_payment->paid_from = bookingPlatform('web');
                    $online_payment->payment_datetime = now();
                    $online_payment->save();
                    // save also in customer payments
                    $customerPay = new CustomerPayments();
                    $customerPay->online_payment_id = $online_payment->payment_id;
                    $customerPay->verified_status = 1;
                    $customerPay->customer_id = @$online_payment->customer_id;
                    $customerPay->paid_amount = $online_payment->amount;
                    $customerPay->balance_amount = $online_payment->amount;
                    $customerPay->paid_at = 'O';
                    $customerPay->ps_no = $online_payment->reference_id;
                    $customerPay->receipt_no = $params['tracking_id'];
                    $customerPay->paid_at_id = 1;
                    $customerPay->payment_method = 1;
                    $customerPay->day_service_id = 0;
                    $customerPay->paid_datetime = now();
                    $customerPay->save();
                    DB::commit();
                    /**
                     * Send mails
                     */
                    $mail->online_payment_confirm_to_admin($online_payment->payment_id);
                    $mail->online_payment_confirm_to_customer($online_payment->payment_id);
                } else if (trim($params['merchant_param1']) == 'outstanding-payment') {
                    // Save in online payments
                    $online_payment = OnlinePayment::where('reference_id', $params['order_id'])->first();
                    $online_payment->user_agent = $_SERVER['HTTP_USER_AGENT'];
                    $online_payment->transaction_id = $params['tracking_id'];
                    $online_payment->payment_status = 'success';
                    $online_payment->ip_address = @$_SERVER['REMOTE_ADDR'];
                    $online_payment->user_agent = @$_SERVER['HTTP_USER_AGENT'];
                    $online_payment->return_data = json_encode($params);
                    $online_payment->paid_from = bookingPlatform('mobile');
                    $online_payment->payment_datetime = now();
                    $online_payment->save();
                    // save also in customer payments
                    $customerPay = new CustomerPayments();
                    $customerPay->online_payment_id = $online_payment->payment_id;
                    $customerPay->verified_status = 1;
                    $customerPay->customer_id = @$online_payment->customer_id;
                    $customerPay->paid_amount = $online_payment->amount;
                    $customerPay->balance_amount = $online_payment->amount;
                    $customerPay->paid_at = 'O';
                    $customerPay->ps_no = $online_payment->reference_id;
                    $customerPay->receipt_no = $params['tracking_id'];
                    $customerPay->paid_at_id = 1;
                    $customerPay->payment_method = 1;
                    $customerPay->day_service_id = 0;
                    $customerPay->paid_datetime = now();
                    $customerPay->save();
                    DB::commit();
                    /**
                     * Send mails
                     */
                    $mail->online_payment_confirm_to_admin($online_payment->payment_id);
                    $mail->online_payment_confirm_to_customer($online_payment->payment_id);
                }
                /**
                 * Add Customer Notification
                 * 
                 */
                if ($online_payment->customer_id) {
                    $notify = new stdClass();
                    $notify->customer_id = $online_payment->customer_id;
                    $notify->online_payment_id = $online_payment->payment_id;
                    $notify->online_paid_amount = $params['amount'];
                    $notify->content = 'Payment of '.$params['amount'].' '.$params['currency'].' for Order Ref. No. '.$params['order_id'].' is successful with Transaction Ref. No. '.$params['tracking_id'].'.';
                    // add to notifications
                    addCustomerNotification($notify);
                }
            }
        } catch (\Exception $e) {
            DB::rollBack();
        }
    }
    public function ccavenue_hidden_form(Request $request, $booking_id)
    {
        $data['booking'] = DB::table('bookings as b')
            ->select('b.*', )
            ->where([['b.booking_id', '=', $booking_id]])
            ->first();
        $data['customer'] = DB::table('customers as c')
            ->select('c.*', )
            ->where([['c.customer_id', '=', $data['booking']->customer_id]])
            ->first();
        $data['customer_address'] = CustomerAddress::
            select('customer_addresses.*')
            ->where([['customer_addresses.customer_address_id', '=', $data['booking']->customer_address_id]])
            ->first();
        return view('payment.gateway-entry.ccavenue_entry', $data);
    }
    public function loading_webview_success()
    {
        return view('webview.loading-success', []);
    }
    public function loading_webview_failed()
    {
        return view('webview.loading-failed', []);
    }
}
