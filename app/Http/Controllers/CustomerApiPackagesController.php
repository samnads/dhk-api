<?php

namespace App\Http\Controllers;

use App\Models\BuildingTypeRoomPackage;
use App\Models\Settings;
use Config;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Response;

class CustomerApiPackagesController extends Controller
{
    public function packages(Request $request)
    {
        $settings = Settings::first();
        /************************************************************* */
        $data = $request->getContent();
        $data = json_decode($data, true);
        /************************************************************* */
        // required input check
        $input = @$data['params'];
        $validator = Validator::make(
            (array) $input,
            [
                'service_type_id' => 'required|integer',
            ],
            [],
            [
                'service_type_id' => 'Service Type ID',
            ]
        );
        if ($validator->fails()) {
            return Response::json(array('result' => array('status' => 'failed', 'message' => $validator->errors()->first())), 200, [], JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES | JSON_NUMERIC_CHECK);
        }
        /************************************************************* */
        $service_type = DB::table('service_types as st')
            ->select(
                'st.service_type_name as service_type',
                'st.service_type_id',
                'st.service_type_model_id',
                'st.customer_app_status'
            )
            ->where([['st.service_type_id', "=", $input['service_type_id']]])
            ->first();
        if ($service_type->customer_app_status != 1) {
            return Response::json(array('result' => array('status' => 'failed', 'message' => 'Service not active.')), 200, [], JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES | JSON_NUMERIC_CHECK);
        } else if ($service_type->service_type_model_id != 2) {
            return Response::json(array('result' => array('status' => 'failed', 'message' => 'This servie is not a package model.')), 200, [], JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES | JSON_NUMERIC_CHECK);
        }
        /************************************************************* */
        $response['status'] = 'success';
        $response['service_type'] = $service_type->service_type;
        $response['service_type_id'] = $service_type->service_type_id;
        $response['data']['Apartment'] = [];
        $response['data']['Villa'] = [];
        $packages = BuildingTypeRoomPackage::
            select(
                'building_type_room_packages.id as package_id',
                DB::raw('CONCAT("' . Config::get('values.customer_app_assets_prefix_url') . 'package-type-banner-images/' . '",IFNULL(bt.customer_app_banner,"default.png")) as building_type_banner_url'),
                'st.service_type_id',
                'building_type_room_packages.is_offer',
                DB::raw('IF(building_type_room_packages.title IS NULL or building_type_room_packages.title = "", br.name, building_type_room_packages.title) as package_name'),
                'building_type_room_packages.description as package_description',
                'building_type_room_packages.customer_app_thumbnail',
                DB::raw('"' . Config::get('values.currency_code') . '" as currency'),
                'building_type_room_packages.actual_total_amount as actual_amount',
                'building_type_room_packages.total_amount as amount',
                'bt.name as building_type',
                'bt.id as building_type_id',
                'bt.customer_app_banner',
                'st.service_type_name as service_type',
                'building_type_room_packages.info_html',
                'building_type_room_packages.service_time',
                'building_type_room_packages.no_of_maids',
                'building_type_room_packages.service_time as service_time_in_minutes',
                DB::raw('TIME_TO_SEC(building_type_room_packages.service_time)/60 as service_time_in_minutes'),
                'building_type_room_packages.cart_limit',
            )
            ->leftJoin('building_rooms as br', 'building_type_room_packages.building_room_id', 'br.id')
            ->leftJoin('building_types as bt', 'br.building_type_id', 'bt.id')
            ->leftJoin('service_types as st', 'bt.service_type_id', 'st.service_type_id')
            ->where([['bt.service_type_id', "=", $input['service_type_id']]])
            ->where([['bt.deleted_at', "=", null], ['br.deleted_at', "=", null]])
            ->orderBy('bt.sort_order')
            ->orderBy('building_type_room_packages.sort_order')
            ->get();
        foreach ($packages as $key => $package) {
            $package_data = clone $package;
            unset($package_data->service_type);
            unset($package_data->is_offer);
            $package_data->building_type_banner_url = Config::get('values.customer_app_assets_prefix_url') . 'package-type-banner-images/' . $package_data->customer_app_banner . '?v=' . $settings->customer_app_img_version;
            $packages[$key]->thumbnail_url = Config::get('values.customer_app_assets_prefix_url') . 'service-package-thumbnails/' . $package_data->customer_app_thumbnail . '?v=' . $settings->customer_app_img_version;
            if ($package->is_offer != 1) {
                // normal package
                //unset($package_data->imageurl);
                $package_data->imageurl = Config::get('values.customer_app_assets_prefix_url') . 'service-package-thumbnails/' . $package_data->customer_app_thumbnail . '?v=' . $settings->customer_app_img_version;
                $normal[] = $package_data;
            } else {
                // offer package
                $package_data->imageurl = Config::get('values.customer_app_assets_prefix_url') . 'service-package-thumbnails/' . $package_data->customer_app_thumbnail . '?v=' . $settings->customer_app_img_version;
                $offer[] = $package_data;
            }
            unset($package_data->building_type_room_id);
            unset($package_data->customer_app_thumbnail);
        }
        $response['data'] = @$normal ?: [];
        $response['Offers'] = @$offer ?: [];
        foreach ($packages as $key => &$package) {
            $package_data = clone $package;
            unset($package_data->service_type);
            unset($package_data->building_type_room_id);
            unset($package_data->is_offer);
            if ($package->is_offer != 1) {
                // normal package
                $response['packages'][$package->building_type]['data'][] = $package_data;
                $response['packages'][$package->building_type]['banner_url'] = $package_data->building_type_banner_url . '?v=' . $settings->customer_app_img_version;
            } else {
                // offer package
                $response['packages']['Offers']['data'][] = $package_data;
                $response['packages']['Offers']['banner_url'] = $package_data->building_type_banner_url . '?v=' . $settings->customer_app_img_version;
            }
        }
        $response['message'] = sizeof($packages) ? 'Packages fetched successfully.' : "No packages available.";
        return Response::json(array('result' => $response), 200, [], JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES | JSON_NUMERIC_CHECK);
    }
}
