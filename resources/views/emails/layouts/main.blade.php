<html>
@include('emails.includes.head_tag_section')

<body>
    <div class="main-wrapper" style="width: 800px; margin:0 auto; background:#ddf8ed;">
        @include('emails.includes.header_banner')
        <div style="width: 798px; background:#ddf8ed;">
            <div style="width: 600px; height:auto; margin: 0 auto; padding: 0px 30px;">
                @yield('content')
            </div>
        </div>
        @include('emails.includes.footer_copyright')
    </div>
</body>

</html>
@if (@$test === true || isset($_GET['test']))
    {{ die() }}
@endif
