CREATE TABLE `genders` (
  `gender_id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `gender` varchar(15) NOT NULL,
  `sort_order_id` int(11) DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`gender_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

INSERT INTO `genders` (`gender_id`, `gender`, `sort_order_id`, `deleted_at`) VALUES
(1,	'Male',	1,	NULL),
(2,	'Female',	2,	NULL);
----------------------------------------------------------------------------------------------------------------------------------------
ALTER TABLE `customers`
ADD `login_otp_expired_at` datetime NULL;
ALTER TABLE `customers`
CHANGE `customer_username` `customer_username` varchar(155) COLLATE 'utf8_general_ci' NULL;
ALTER TABLE `customers`
CHANGE `customer_password` `customer_password` varchar(255) COLLATE 'utf8_general_ci' NULL;
ALTER TABLE `customers`
CHANGE `odoo_customer_newid` `odoo_customer_newid` bigint(20) unsigned NULL;
ALTER TABLE `customers`
CHANGE `customer_name` `customer_name` varchar(155) COLLATE 'utf8_general_ci' NULL;
ALTER TABLE `customers`
CHANGE `customer_nick_name` `customer_nick_name` varchar(155) COLLATE 'utf8_general_ci' NULL;
ALTER TABLE `customers`
CHANGE `website_url` `website_url` varchar(160) COLLATE 'utf8_general_ci' NULL;
ALTER TABLE `customers`
CHANGE `signed` `signed` varchar(5) COLLATE 'utf8_general_ci' NULL;
ALTER TABLE `customers`
CHANGE `customer_notes` `customer_notes` text COLLATE 'utf8_general_ci' NULL;
ALTER TABLE `customers`
CHANGE `customer_source_id` `customer_source_id` int(11) NULL;
ALTER TABLE `customers`
ADD `device_type` varchar(155) NULL;
ALTER TABLE `customers`
ADD `dob` date NULL;
ALTER TABLE `customers`
ADD `gender_id` int(10) unsigned NULL,
ADD FOREIGN KEY (`gender_id`) REFERENCES `genders` (`gender_id`);
----------------------------------------------------------------------------------------------------------------------------------------
CREATE TABLE `customers_temp` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `customer_id` bigint(20) unsigned NOT NULL,
  `mobile_number_1_country_code` varchar(10) DEFAULT NULL,
  `mobile_number_1` varchar(150) DEFAULT NULL,
  `mobile_number_1_otp` varchar(50) DEFAULT NULL,
  `mobile_number_1_otp_expired_at` datetime DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `customer_id` (`customer_id`),
  CONSTRAINT `customers_temp_ibfk_1` FOREIGN KEY (`customer_id`) REFERENCES `customers` (`customer_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='save temperory data for verification';
----------------------------------------------------------------------------------------------------------------------------------------
ALTER TABLE `customer_notifications`
CHANGE `title` `title` text COLLATE 'utf8mb4_general_ci' NULL AFTER `service_date`;
----------------------------------------------------------------------------------------------------------------------------------------
ALTER TABLE `settings`
ADD `customer_app_img_version` varchar(10) NOT NULL DEFAULT '1.0' AFTER `quick_refresh_count`;
----------------------------------------------------------------------------------------------------------------------------------------
ALTER TABLE `customer_notifications`
ADD `expired_at` datetime NULL AFTER `cleared_at`;
----------------------------------------------------------------------------------------------------------------------------------------
CREATE TABLE `frequencies` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(155) NOT NULL,
  `code` varchar(10) NOT NULL,
  `coupon_id` bigint(20) DEFAULT NULL,
  `hourly_rate` decimal(15,2) NOT NULL DEFAULT 0.00,
  `offer_percentage` decimal(15,2) NOT NULL DEFAULT 0.00,
  `max_working_hour` decimal(4,2) NOT NULL DEFAULT 0.00,
  `cleaning_material_hourly_rate` decimal(15,2) NOT NULL DEFAULT 0.00,
  `description` varchar(255) DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`),
  KEY `coupon_id` (`coupon_id`),
  CONSTRAINT `frequencies_ibfk_1` FOREIGN KEY (`coupon_id`) REFERENCES `coupon_code` (`coupon_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

INSERT INTO `frequencies` (`id`, `name`, `code`, `coupon_id`, `hourly_rate`, `offer_percentage`, `max_working_hour`, `cleaning_material_hourly_rate`, `description`, `deleted_at`) VALUES
(1,	'One Time',	'OD',	NULL,	0,	0,	0,	0,	'One time service, will not repeat again.',	NULL),
(2,	'Weekly',	'WE',	NULL,	0,	0,	0,	0,	'Service for the same day every week.',	NULL),
(3,	'Every 2 Week',	'BW',	NULL,	0,	0,	0,	0,	'Service for every two weeks.',	NULL);
----------------------------------------------------------------------------------------------------------------------------------------
CREATE TABLE `payment_types` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `code` varchar(30) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `charge` decimal(15,2) NOT NULL DEFAULT 0.00,
  `charge_type` enum('F','P') NOT NULL,
  `customer_app_icon` varchar(255) DEFAULT NULL,
  `sort_order` int(11) DEFAULT NULL,
  `default` tinyint(4) DEFAULT NULL,
  `show_in_web` tinyint(1) DEFAULT NULL,
  `show_in_app` tinyint(1) DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

INSERT INTO `payment_types` (`id`, `name`, `code`, `description`, `charge`, `charge_type`, `customer_app_icon`, `sort_order`, `default`, `show_in_web`, `show_in_app`, `deleted_at`) VALUES
(1,	'Cash',	'cash',	NULL,	0.00,	'F',	'cash.png',	1,	1,	1,	NULL,	NULL),
(2,	'Card',	'card',	NULL,	0.00,	'F',	'card.png',	2,	NULL,	1,	NULL,	NULL);
----------------------------------------------------------------------------------------------------------------------------------------
ALTER TABLE `bookings`
ADD `payment_type_id` int(11) NULL,
ADD FOREIGN KEY (`payment_type_id`) REFERENCES `payment_types` (`id`);
----------------------------------------------------------------------------------------------------------------------------------------
CREATE TABLE `service_type_categories` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `service_category_name` varchar(255) NOT NULL,
  `sort_order` int(11) NOT NULL DEFAULT 0,
  `customer_app_icon_file` varchar(255) DEFAULT NULL,
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_at` datetime NOT NULL DEFAULT current_timestamp(),
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
----------------------------------------------------------------------------------------------------------------------------------------
CREATE TABLE `service_type_category_and_service_type_mapping` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `service_type_category_id` bigint(20) unsigned NOT NULL,
  `service_type_id` int(11) NOT NULL,
  `created_at` date NOT NULL,
  `updated_at` date NOT NULL,
  `deleted_at` date DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `service_type_category_id_service_type_id` (`service_type_category_id`,`service_type_id`),
  KEY `service_type_id` (`service_type_id`),
  CONSTRAINT `service_type_category_and_service_type_mapping_ibfk_1` FOREIGN KEY (`service_type_category_id`) REFERENCES `service_type_categories` (`id`),
  CONSTRAINT `service_type_category_and_service_type_mapping_ibfk_2` FOREIGN KEY (`service_type_id`) REFERENCES `service_types` (`service_type_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='one service have multiple category';
----------------------------------------------------------------------------------------------------------------------------------------
CREATE TABLE `service_type_models` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `model` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
----------------------------------------------------------------------------------------------------------------------------------------
CREATE TABLE `service_type_models` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `model` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

INSERT INTO `service_type_models` (`id`, `model`) VALUES
(1,	'normal'),
(2,	'package'),
(3,	'custom');
----------------------------------------------------------------------------------------------------------------------------------------
ALTER TABLE `service_types`
ADD `customer_app_service_type_name` varchar(155) COLLATE 'latin1_swedish_ci' NULL AFTER `service_type_name`;
ALTER TABLE `service_types`
ADD `customer_app_thumbnail_file` varchar(255) NULL;
ALTER TABLE `service_types`
ADD `service_type_model_id` int(10) unsigned NOT NULL DEFAULT '1',
ADD FOREIGN KEY (`service_type_model_id`) REFERENCES `service_type_models` (`id`);
ALTER TABLE `service_types`
ADD `web_url_slug` varchar(255) COLLATE 'utf8mb4_general_ci' NULL AFTER `customer_app_service_type_name`;
ALTER TABLE `service_types`
ADD `info_html` text NULL COMMENT 'for web / app (html content)';
ALTER TABLE `service_types`
ADD `customer_app_status` tinyint(1) unsigned NULL;
ALTER TABLE `service_types`
ADD `customer_app_order_id` int(5) unsigned NULL;
----------------------------------------------------------------------------------------------------------------------------------------
ALTER TABLE `package`
ADD `tc_html` text COLLATE 'utf8_general_ci' NULL COMMENT 'terms and conditions' AFTER `description`;
----------------------------------------------------------------------------------------------------------------------------------------
ALTER TABLE `offers_sb`
ADD `banner_image` varchar(250) COLLATE 'utf8mb3_general_ci' NULL AFTER `offer_image`,
CHANGE `offer_timestamp` `offer_timestamp` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE CURRENT_TIMESTAMP AFTER `offer_status`;
ALTER TABLE `offers_sb`
ADD `coupon_id` bigint(20) NULL AFTER `id_offers_sb`,
CHANGE `offer_timestamp` `offer_timestamp` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE CURRENT_TIMESTAMP AFTER `offer_status`,
ADD FOREIGN KEY (`coupon_id`) REFERENCES `coupon_code` (`coupon_id`);
----------------------------------------------------------------------------------------------------------------------------------------
CREATE TABLE `booking_cancel_reasons` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `order_priority` int(11) NOT NULL,
  `reason` text NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

INSERT INTO `booking_cancel_reasons` (`id`, `order_priority`, `reason`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1,	1,	'Personal schedule conflicts',	'2023-06-09 11:31:00',	'2023-06-09 11:31:00',	NULL),
(2,	2,	'Illness or family emergency',	'2023-06-09 11:31:00',	'2023-06-09 11:31:00',	NULL),
(3,	3,	'Dissatisfied with previous cleaning',	'2023-06-09 11:31:00',	'2023-06-09 11:31:00',	NULL),
(4,	4,	'Financial constraints',	'2023-06-09 11:31:00',	'2023-06-09 11:31:00',	NULL),
(5,	5,	'Moving or relocation',	'2023-06-09 11:31:00',	'2023-06-09 11:31:00',	NULL),
(6,	6,	'Change in cleaning needs',	'2023-06-09 11:31:00',	'2023-06-09 11:31:00',	NULL),
(7,	7,	'Unsatisfactory communication with cleaning service',	'2023-06-09 11:31:00',	'2023-06-09 11:31:00',	NULL);
----------------------------------------------------------------------------------------------------------------------------------------
ALTER TABLE `service_type_category_and_service_type_mapping`
CHANGE `created_at` `created_at` datetime NULL AFTER `service_type_id`,
CHANGE `updated_at` `updated_at` datetime NULL AFTER `created_at`,
CHANGE `deleted_at` `deleted_at` datetime NULL AFTER `updated_at`;
----------------------------------------------------------------------------------------------------------------------------------------
CREATE TABLE `service_addons` (
  `service_addons_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `service_type_id` int(11) NOT NULL,
  `service_addon_name` varchar(255) NOT NULL,
  `service_addon_description` varchar(255) DEFAULT NULL,
  `service_minutes` decimal(4,2) NOT NULL DEFAULT 0.00,
  `strike_amount` decimal(15,2) NOT NULL,
  `amount` decimal(15,2) NOT NULL,
  `cart_limit` int(11) NOT NULL DEFAULT 1 COMMENT 'maximum count allowed to add on cart',
  `customer_app_thumbnail` varchar(255) DEFAULT NULL,
  `populariry` bigint(20) NOT NULL DEFAULT 0 COMMENT 'higher the popularity means top selling addon',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`service_addons_id`),
  KEY `service_type_id` (`service_type_id`),
  CONSTRAINT `service_addons_ibfk_1` FOREIGN KEY (`service_type_id`) REFERENCES `service_types` (`service_type_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
----------------------------------------------------------------------------------------------------------------------------------------
CREATE TABLE `service_addons_points` (
  `service_addons_included_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `service_addons_id` bigint(20) unsigned NOT NULL,
  `point_type` enum('WHAT_INCLUDED','WHAT_EXCLUDED') NOT NULL,
  `sort_order_id` int(10) unsigned DEFAULT NULL,
  `point_html` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`service_addons_included_id`),
  KEY `service_addons_id` (`service_addons_id`),
  CONSTRAINT `service_addons_points_ibfk_1` FOREIGN KEY (`service_addons_id`) REFERENCES `service_addons` (`service_addons_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
----------------------------------------------------------------------------------------------------------------------------------------
CREATE TABLE `service_type_charges` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `service_type_id` int(11) NOT NULL,
  `no_of_hours_from` decimal(4,2) DEFAULT NULL,
  `no_of_hours_to` decimal(4,2) DEFAULT NULL,
  `rate_per_hour` decimal(5,2) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `service_type_id` (`service_type_id`),
  CONSTRAINT `service_type_charges_ibfk_1` FOREIGN KEY (`service_type_id`) REFERENCES `service_types` (`service_type_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
ALTER TABLE `service_type_charges`
CHANGE `created_at` `created_at` datetime NULL AFTER `rate_per_hour`,
CHANGE `updated_at` `updated_at` datetime NULL AFTER `created_at`;
----------------------------------------------------------------------------------------------------------------------------------------
CREATE TABLE `service_supervisor_charges` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `service_type_id` int(11) NOT NULL,
  `no_of_hours` int(11) NOT NULL,
  `charge` decimal(10,2) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `service_type_id` (`service_type_id`),
  CONSTRAINT `service_supervisor_charges_ibfk_1` FOREIGN KEY (`service_type_id`) REFERENCES `service_types` (`service_type_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;
----------------------------------------------------------------------------------------------------------------------------------------
CREATE TABLE `service_supervisor_maid_counts` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `service_type_id` int(11) NOT NULL,
  `no_of_maids` int(11) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `service_type_id_no_of_maids` (`service_type_id`,`no_of_maids`),
  KEY `no_of_maids` (`no_of_maids`),
  CONSTRAINT `service_supervisor_maid_counts_ibfk_1` FOREIGN KEY (`service_type_id`) REFERENCES `service_types` (`service_type_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;
----------------------------------------------------------------------------------------------------------------------------------------
CREATE TABLE `service_type_detail_questions` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `service_type_id` int(11) NOT NULL,
  `question` varchar(255) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `service_type_id` (`service_type_id`),
  CONSTRAINT `service_type_detail_questions_ibfk_1` FOREIGN KEY (`service_type_id`) REFERENCES `service_types` (`service_type_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;
----------------------------------------------------------------------------------------------------------------------------------------
CREATE TABLE `service_type_detail_answers` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `service_type_detail_question_id` bigint(20) unsigned NOT NULL,
  `value` varchar(255) NOT NULL,
  `label` varchar(255) NOT NULL,
  `creates_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `service_type_detail_question_id` (`service_type_detail_question_id`),
  CONSTRAINT `service_type_detail_answers_ibfk_1` FOREIGN KEY (`service_type_detail_question_id`) REFERENCES `service_type_detail_questions` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;
----------------------------------------------------------------------------------------------------------------------------------------
CREATE TABLE `booking_answers` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `booking_id` bigint(20) unsigned NOT NULL,
  `service_type_detail_answer_id` bigint(20) NOT NULL,
  `question` varchar(255) NOT NULL,
  `answer` varchar(255) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `booking_id_service_type_detail_answer_id` (`booking_id`,`service_type_detail_answer_id`),
  KEY `service_type_detail_answer_id` (`service_type_detail_answer_id`),
  CONSTRAINT `booking_answers_ibfk_1` FOREIGN KEY (`booking_id`) REFERENCES `bookings` (`booking_id`),
  CONSTRAINT `booking_answers_ibfk_2` FOREIGN KEY (`service_type_detail_answer_id`) REFERENCES `service_type_detail_answers` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;
----------------------------------------------------------------------------------------------------------------------------------------
ALTER TABLE `service_types`
ADD `min_no_of_maids` tinyint unsigned NOT NULL DEFAULT '1',
ADD `max_no_of_maids` tinyint unsigned NOT NULL DEFAULT '1' AFTER `min_no_of_maids`;
ALTER TABLE `service_types`
ADD `min_no_of_hours` tinyint(3) unsigned NOT NULL DEFAULT '1',
ADD `max_no_of_hours` tinyint(3) unsigned NOT NULL DEFAULT '1' AFTER `min_no_of_hours`;
ALTER TABLE `service_types`
ADD `default_no_of_hours` tinyint(3) unsigned NOT NULL DEFAULT '1';
----------------------------------------------------------------------------------------------------------------------------------------
ALTER TABLE `service_cleaning_supplies`
ADD `rate` decimal(10,2) NOT NULL DEFAULT '0',
ADD `created_at` datetime NULL AFTER `rate`,
ADD `updated_at` datetime NULL AFTER `created_at`,
ADD `deleted_at` datetime NULL AFTER `updated_at`;
----------------------------------------------------------------------------------------------------------------------------------------
ALTER TABLE `service_cleaning_supplies`
ADD `calc_method` enum('PS','PH','PM','PHM') NOT NULL DEFAULT 'PS' COMMENT '[PS] - Per service [PH] - Per hour [PM] - Per maid [PHM] Per hour and maid' AFTER `rate`;
----------------------------------------------------------------------------------------------------------------------------------------
ALTER TABLE `service_what_to_expect_points`
ADD `created_at` datetime NULL,
ADD `updated_at` datetime NULL AFTER `created_at`,
ADD `deleted_at` datetime NULL AFTER `updated_at`;
UPDATE `service_what_to_expect_points` SET
`created_at` = now(),
`updated_at` = now();
----------------------------------------------------------------------------------------------------------------------------------------
ALTER TABLE `service_type_charges`
ADD `final_amount` decimal(5,2) NOT NULL DEFAULT '0' AFTER `rate_per_hour`;
----------------------------------------------------------------------------------------------------------------------------------------
ALTER TABLE `bookings`
ADD `amount_before_discount` decimal(10,2) NULL;
ALTER TABLE `bookings`
ADD `payment_type_charge` decimal(10,2) NULL;
ALTER TABLE `bookings`
ADD `web_book_show` tinyint(1) NULL;
ALTER TABLE `bookings`
ADD `_service_amount` decimal(10,2) unsigned NULL,
ADD `_cleaning_materials_amount` decimal(10,2) unsigned NULL AFTER `_service_amount`,
ADD `_service_addons_amount` decimal(10,2) unsigned NULL AFTER `_cleaning_materials_amount`,
ADD `_packages_amount` decimal(10,2) unsigned NULL AFTER `_service_addons_amount`,
ADD `_subscription_package_amount` decimal(10,2) unsigned NULL AFTER `_packages_amount`,
ADD `_amount_before_discount` decimal(10,2) unsigned NULL AFTER `_subscription_package_amount`,
ADD `_coupon_discount` decimal(10,2) unsigned NULL AFTER `_amount_before_discount`,
ADD `_discount_total` decimal(10,2) unsigned NULL AFTER `_coupon_discount`,
ADD `_taxable_amount` decimal(10,2) unsigned NULL AFTER `_discount_total`,
ADD `_vat_percentage` decimal(10,2) unsigned NULL AFTER `_taxable_amount`,
ADD `_vat_amount` decimal(10,2) unsigned NULL AFTER `_vat_percentage`,
ADD `_taxed_amount` decimal(10,2) unsigned NULL AFTER `_vat_amount`,
ADD `_payment_type_charge` decimal(10,2) unsigned NULL AFTER `_taxed_amount`,
ADD `_total_payable` decimal(10,2) unsigned NULL AFTER `_payment_type_charge`;
ALTER TABLE `bookings`
CHANGE `booked_by` `booked_by` int(11) NULL DEFAULT '0' AFTER `booking_from`;
----------------------------------------------------------------------------------------------------------------------------------------
CREATE TABLE `booking_addons` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `booking_id` bigint(20) unsigned DEFAULT NULL,
  `service_addons_id` bigint(20) unsigned NOT NULL,
  `service_addon_name` varchar(255) DEFAULT NULL,
  `service_minutes` decimal(4,2) unsigned NOT NULL DEFAULT 0.00,
  `strike_amount` decimal(15,2) NOT NULL,
  `unit_price` decimal(15,2) NOT NULL,
  `quantity` int(10) unsigned NOT NULL,
  `amount` decimal(15,2) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `booking_id_service_addons_id` (`booking_id`,`service_addons_id`),
  KEY `booking_id` (`booking_id`),
  KEY `service_addons_id` (`service_addons_id`),
  CONSTRAINT `booking_addons_ibfk_1` FOREIGN KEY (`booking_id`) REFERENCES `bookings` (`booking_id`),
  CONSTRAINT `booking_addons_ibfk_2` FOREIGN KEY (`service_addons_id`) REFERENCES `service_addons` (`service_addons_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
----------------------------------------------------------------------------------------------------------------------------------------
CREATE TABLE `building_types` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `service_type_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `customer_app_banner` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `customer_app_banner` (`customer_app_banner`),
  UNIQUE KEY `service_type_id_name` (`service_type_id`,`name`),
  CONSTRAINT `building_types_ibfk_1` FOREIGN KEY (`service_type_id`) REFERENCES `service_types` (`service_type_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
----------------------------------------------------------------------------------------------------------------------------------------
CREATE TABLE `building_rooms` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `building_type_id` bigint(20) NOT NULL,
  `name` varchar(255) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `building_type_id` (`building_type_id`),
  CONSTRAINT `building_rooms_ibfk_1` FOREIGN KEY (`building_type_id`) REFERENCES `building_types` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
----------------------------------------------------------------------------------------------------------------------------------------
CREATE TABLE `building_type_room_packages` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `building_room_id` bigint(20) NOT NULL,
  `is_offer` tinyint(4) DEFAULT NULL COMMENT '1 means offer',
  `title` varchar(255) DEFAULT NULL,
  `description` text NOT NULL,
  `info_html` text DEFAULT NULL,
  `service_time` time NOT NULL DEFAULT '01:00:00',
  `no_of_maids` int(11) NOT NULL DEFAULT 1,
  `actual_total_amount` decimal(15,2) NOT NULL,
  `total_amount` decimal(15,2) NOT NULL,
  `cart_limit` int(11) NOT NULL DEFAULT 1 COMMENT 'maximum count allowed to add on cart',
  `customer_app_thumbnail` varchar(255) NOT NULL DEFAULT 'default.png',
  `sort_order` int(11) NOT NULL DEFAULT 1,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `building_room_id` (`building_room_id`),
  CONSTRAINT `building_type_room_packages_ibfk_3` FOREIGN KEY (`building_room_id`) REFERENCES `building_rooms` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
----------------------------------------------------------------------------------------------------------------------------------------
CREATE TABLE `booking_packages` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `booking_id` bigint(20) unsigned NOT NULL,
  `building_type_room_package_id` bigint(20) NOT NULL,
  `name` varchar(255) NOT NULL,
  `building_type` varchar(255) NOT NULL,
  `building_room` varchar(255) NOT NULL,
  `service_time` time NOT NULL,
  `no_of_maids` int(11) unsigned NOT NULL DEFAULT 1,
  `strike_unit_price` decimal(15,2) unsigned NOT NULL,
  `unit_price` decimal(15,2) unsigned NOT NULL,
  `quantity` int(11) unsigned NOT NULL,
  `total` decimal(15,2) unsigned NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `booking_id_package_id` (`booking_id`,`building_type_room_package_id`),
  KEY `building_type_room_package_id` (`building_type_room_package_id`),
  CONSTRAINT `booking_packages_ibfk_1` FOREIGN KEY (`booking_id`) REFERENCES `bookings` (`booking_id`),
  CONSTRAINT `booking_packages_ibfk_2` FOREIGN KEY (`building_type_room_package_id`) REFERENCES `building_type_room_packages` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
----------------------------------------------------------------------------------------------------------------------------------------
ALTER TABLE `bookings`
ADD `_supervisor_amount` decimal(10,2) unsigned NULL AFTER `_subscription_package_amount`;
ALTER TABLE `bookings`
ADD `_tools_amount` decimal(10,2) unsigned NULL AFTER `_supervisor_amount`;
----------------------------------------------------------------------------------------------------------------------------------------
CREATE TABLE `booking_subscription_packages` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `booking_id` bigint(20) unsigned NOT NULL,
  `package_id` bigint(20) unsigned NOT NULL,
  `package_name` varchar(255) NOT NULL,
  `cleaning_material` enum('Y','N') NOT NULL DEFAULT 'N',
  `no_of_bookings` int(11) unsigned NOT NULL,
  `working_hours` double(5,2) unsigned NOT NULL,
  `booking_type` enum('MO','WE','YE') NOT NULL,
  `strikethrough_amount` double(10,2) unsigned NOT NULL,
  `amount` double(10,2) unsigned NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `booking_id` (`booking_id`),
  KEY `package_id` (`package_id`),
  CONSTRAINT `booking_subscription_packages_ibfk_1` FOREIGN KEY (`booking_id`) REFERENCES `bookings` (`booking_id`),
  CONSTRAINT `booking_subscription_packages_ibfk_2` FOREIGN KEY (`package_id`) REFERENCES `package` (`package_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
----------------------------------------------------------------------------------------------------------------------------------------
ALTER TABLE `bookings`
ADD `subscription_package_id` bigint unsigned NULL,
ADD FOREIGN KEY (`subscription_package_id`) REFERENCES `package` (`package_id`);
----------------------------------------------------------------------------------------------------------------------------------------
CREATE TABLE `service_type_frequencies` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `service_type_id` int(11) NOT NULL,
  `frequency_id` int(11) NOT NULL,
  `sort_order` tinyint(4) DEFAULT NULL,
  `coupon_id` bigint(20) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `service_type_id_frequency_id` (`service_type_id`,`frequency_id`),
  KEY `frequency_id` (`frequency_id`),
  KEY `coupon_id` (`coupon_id`),
  CONSTRAINT `service_type_frequencies_ibfk_1` FOREIGN KEY (`service_type_id`) REFERENCES `service_types` (`service_type_id`),
  CONSTRAINT `service_type_frequencies_ibfk_2` FOREIGN KEY (`frequency_id`) REFERENCES `frequencies` (`id`),
  CONSTRAINT `service_type_frequencies_ibfk_3` FOREIGN KEY (`coupon_id`) REFERENCES `coupon_code` (`coupon_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;
----------------------------------------------------------------------------------------------------------------------------------------
CREATE TABLE `service_type_settings` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `service_type_id` int(11) NOT NULL,
  `cleaning_materials_opted` tinyint(1) NOT NULL DEFAULT 0,
  `cleaning_materials_option_show` tinyint(1) NOT NULL DEFAULT 1,
  `supervisor_option_show` tinyint(1) NOT NULL DEFAULT 0,
  `instructions_field_label` varchar(155) NOT NULL,
  `instructions_field_placeholder` varchar(155) NOT NULL,
  `instructions_field_required` tinyint(1) NOT NULL DEFAULT 0,
  `default_material_id` bigint(20) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `service_type_id` (`service_type_id`),
  CONSTRAINT `service_type_settings_ibfk_1` FOREIGN KEY (`service_type_id`) REFERENCES `service_types` (`service_type_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;
----------------------------------------------------------------------------------------------------------------------------------------
ALTER TABLE `building_types` ADD `sort_order` INT(11) NOT NULL DEFAULT '1' AFTER `customer_app_banner`;
----------------------------------------------------------------------------------------------------------------------------------------
ALTER TABLE `bookings`
CHANGE `web_book_show` `web_book_show` tinyint(1) NULL DEFAULT '1' AFTER `payment_type_charge`;
----------------------------------------------------------------------------------------------------------------------------------------
ALTER TABLE `online_payments`
CHANGE `post_data` `post_data` text COLLATE 'latin1_swedish_ci' NULL AFTER `user_agent`;
ALTER TABLE `online_payments`
CHANGE `user_agent` `user_agent` varchar(500) COLLATE 'latin1_swedish_ci' NULL AFTER `ip_address`;
----------------------------------------------------------------------------------------------------------------------------------------
CREATE TABLE `recurring_periods` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `service_type_id` int(11) NOT NULL,
  `no_of_months` int(11) unsigned NOT NULL,
  `no_of_weeks` int(11) unsigned NOT NULL,
  `no_of_months_label` varchar(100) NOT NULL,
  `no_of_weeks_label` varchar(100) NOT NULL,
  `sort_order` int(11) unsigned NOT NULL,
  `default` tinyint(1) unsigned DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `service_type_id_no_of_months` (`service_type_id`,`no_of_months`),
  UNIQUE KEY `service_type_id_no_of_weeks` (`service_type_id`,`no_of_weeks`),
  UNIQUE KEY `service_type_id_default` (`service_type_id`,`default`),
  CONSTRAINT `recurring_periods_ibfk_1` FOREIGN KEY (`service_type_id`) REFERENCES `service_types` (`service_type_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
----------------------------------------------------------------------------------------------------------------------------------------
CREATE TABLE `booking_weekdays` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `booking_id` bigint(20) unsigned NOT NULL,
  `week_day_id` int(11) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `booking_id_week_day_id` (`booking_id`,`week_day_id`),
  KEY `week_day_id` (`week_day_id`),
  CONSTRAINT `booking_weekdays_ibfk_1` FOREIGN KEY (`booking_id`) REFERENCES `bookings` (`booking_id`),
  CONSTRAINT `booking_weekdays_ibfk_2` FOREIGN KEY (`week_day_id`) REFERENCES `week_days` (`week_day_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;
----------------------------------------------------------------------------------------------------------------------------------------
CREATE TABLE `service_type_time_slots` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `service_type_id` int(11) NOT NULL,
  `time_slot` time NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `service_type_id_time_slot` (`service_type_id`,`time_slot`),
  CONSTRAINT `service_type_time_slots_ibfk_1` FOREIGN KEY (`service_type_id`) REFERENCES `service_types` (`service_type_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
----------------------------------------------------------------------------------------------------------------------------------------
CREATE TABLE `maid_rating` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `maid_id` int(10) unsigned NOT NULL,
  `service_type_id` int(11) DEFAULT NULL,
  `rated_by_customer_id` bigint(20) unsigned DEFAULT NULL,
  `day_service_id` bigint(20) unsigned DEFAULT NULL,
  `rating` enum('1','2','3','4','5') NOT NULL,
  `comment` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `maid_id` (`maid_id`),
  KEY `day_service_id` (`day_service_id`),
  KEY `rated_by_customer_id` (`rated_by_customer_id`),
  KEY `service_type_id` (`service_type_id`),
  CONSTRAINT `maid_rating_ibfk_1` FOREIGN KEY (`maid_id`) REFERENCES `maids` (`maid_id`),
  CONSTRAINT `maid_rating_ibfk_2` FOREIGN KEY (`day_service_id`) REFERENCES `day_services` (`day_service_id`),
  CONSTRAINT `maid_rating_ibfk_3` FOREIGN KEY (`rated_by_customer_id`) REFERENCES `customers` (`customer_id`),
  CONSTRAINT `maid_rating_ibfk_4` FOREIGN KEY (`service_type_id`) REFERENCES `service_types` (`service_type_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
----------------------------------------------------------------------------------------------------------------------------------------
ALTER TABLE `customers` CHANGE `push_token` `push_token` VARCHAR(255) CHARACTER SET utf8mb3 COLLATE utf8mb3_general_ci NULL DEFAULT NULL;
----------------------------------------------------------------------------------------------------------------------------------------
CREATE TABLE `debug_mobiles` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `mobile_number` bigint(20) unsigned NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;
----------------------------------------------------------------------------------------------------------------------------------------
ALTER TABLE `offers_sb`
CHANGE `offer_image` `offer_image` varchar(250) COLLATE 'utf8mb3_general_ci' NULL AFTER `offer_content`,
CHANGE `offer_timestamp` `offer_timestamp` timestamp NOT NULL DEFAULT current_timestamp() ON UPDATE CURRENT_TIMESTAMP AFTER `offer_status`;
----------------------------------------------------------------------------------------------------------------------------------------
ALTER TABLE `service_what_to_expect_points`
CHANGE `service_type_id` `service_type_id` int(11) NULL AFTER `id`;
----------------------------------------------------------------------------------------------------------------------------------------
CREATE TABLE `customer_faq_help` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `order_priority` int(11) NOT NULL,
  `question` varchar(500) NOT NULL,
  `answer` text NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
----------------------------------------------------------------------------------------------------------------------------------------
CREATE TABLE `customer_faq` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `order_priority` int(11) NOT NULL,
  `question` text NOT NULL,
  `answer` text NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
----------------------------------------------------------------------------------------------------------------------------------------
ALTER TABLE `customer_addresses`
CHANGE `latitude` `latitude` varchar(100) COLLATE 'latin1_swedish_ci' NULL AFTER `customer_address`,
CHANGE `longitude` `longitude` varchar(100) COLLATE 'latin1_swedish_ci' NULL AFTER `latitude`;
----------------------------------------------------------------------------------------------------------------------------------------
ALTER TABLE `tablets` CHANGE `google_reg_id` `google_reg_id` VARCHAR(255) CHARACTER SET latin1 COLLATE latin1_swedish_ci NULL;
----------------------------------------------------------------------------------------------------------------------------------------
ALTER TABLE `service_type_category_and_service_type_mapping`
ADD `sort_order` int(5) unsigned NULL AFTER `service_type_id`;
----------------------------------------------------------------------------------------------------------------------------------------
ALTER TABLE `bookings`
ADD `_service_amount_before_discount` decimal(10,2) unsigned NULL DEFAULT '0.00' AFTER `web_book_show`,
ADD `_service_amount_discount` decimal(10,2) unsigned NULL DEFAULT '0.00' AFTER `_service_amount_before_discount`;
----------------------------------------------------------------------------------------------------------------------------------------
ALTER TABLE `customers`
ADD `added_from_ip_address` varchar(55) NULL AFTER `customer_added_datetime`;
----------------------------------------------------------------------------------------------------------------------------------------
ALTER TABLE `package`
CHANGE `show_in_banner` `show_in_top_banner` tinyint(1) NULL DEFAULT '1' COMMENT '1 - Show' AFTER `status`;
----------------------------------------------------------------------------------------------------------------------------------------
ALTER TABLE `package`
ADD `show_in_bottom_banner` tinyint(1) NULL DEFAULT '1' COMMENT '1 - Show';
----------------------------------------------------------------------------------------------------------------------------------------
ALTER TABLE `package`
CHANGE `vat_charge_percentage` `vat_charge_percentage` double(10,2) NOT NULL DEFAULT '0' AFTER `amount`;
----------------------------------------------------------------------------------------------------------------------------------------
ALTER TABLE `customer_notifications`
ADD `online_payment_id` bigint(20) NULL AFTER `service_date`,
ADD FOREIGN KEY (`online_payment_id`) REFERENCES `online_payments` (`payment_id`);
----------------------------------------------------------------------------------------------------------------------------------------
ALTER TABLE `customer_notifications`
ADD `online_paid_amount` float(10,2) NULL COMMENT 'total paid on gateway' AFTER `online_payment_id`;