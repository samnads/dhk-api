<html>

<head>
	<title>Non-Seamless-kit</title>
	<link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0/dist/css/bootstrap.min.css" rel="stylesheet"
		integrity="sha384-9ndCyUaIbzAi2FUVXJi0CjmCapSmO7SnpJef0486qhLnuZ2cdeRhO02iuK6FUUVM" crossorigin="anonymous">
	<script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0/dist/js/bootstrap.bundle.min.js"
		integrity="sha384-geWF76RCwLtnZ8qwWowPQNguL3RmwHVBC9FhGdlKrxdiJJigb/j/68SIy3Te4Bkz" crossorigin="anonymous">
		</script>
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.4.0/css/all.min.css"
		integrity="sha512-iecdLmaskl7CVkqkXNQ/ZH/XLlvWZOJyj7Yy7tcenmpD1ypASozpmT/E0iPtmFIB46ZmdtAc9eNBvH0H/ZpiBw=="
		crossorigin="anonymous" referrerpolicy="no-referrer" />
	<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.7.0/jquery.min.js"
		integrity="sha512-3gJwYpMe3QewGELv8k/BX9vcqhryRdzRMxVfq6ngyWXwo03GFEzjsUm8Q7RZcHPHksttq7/GFoxjCVUjkjvPdw=="
		crossorigin="anonymous" referrerpolicy="no-referrer"></script>
</head>

<body class="d-flex align-items-center">
	<div class="container text-center">
		<div class="row">
			<div class="col">
				<div id="loading" class="mb-5">
					<div class="spinner-border text-info" role="status">
						<span class="visually-hidden">Loading...</span>
					</div>
				</div>
				<p class="display-6 mb-5">CCAvenue Secure <i class="fa fa-lock text-success" aria-hidden="true"></i></p>
			</div>
		</div>
	</div>
	<center>
		<?php
		error_reporting(0);
		include ('ccavEncrypt.php');
		$merchant_data = '';
		// DEMO
		$working_key = '27174836706C476BCC2F4E92F798B7FC';//Shared by CCAVENUES
		$access_code = 'AVHF04KH72BL63FHLB';//Shared by CCAVENUES
		foreach ($_POST as $key => $value) {
			$merchant_data .= $key . '=' . $value . '&';
		}
		$encrypted_data = ccavEncrypt($merchant_data, $working_key); // Method for encrypting the data.
		?>
		<form method="post" name="redirect"
			action="https://secure.ccavenue.ae/transaction/transaction.do?command=initiateTransaction">
			<?php
			echo "<input type=hidden name=encRequest value=$encrypted_data>";
			echo "<input type=hidden name=access_code value=$access_code>";
			?>
		</form>
	</center>
	<script language='javascript'>document.redirect.submit();</script>
</body>

</html>