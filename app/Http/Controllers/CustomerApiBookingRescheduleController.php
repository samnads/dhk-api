<?php

namespace App\Http\Controllers;

use App\Models\Booking;
use App\Models\Customer;
use Carbon\Carbon;
use Config;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Response;
use stdClass;

class CustomerApiBookingRescheduleController extends Controller
{
    public function reschedule(Request $request)
    {
        try {
            DB::beginTransaction();
            $debug = toggleDebug(); // pass boolean to overide default
            /************************************************************* */
            if (!$debug) {
                // live input
                $data = json_decode($request->getContent(), true);
            } else {
                // test input
                $data['params']['id'] = Config::get('values.debug_customer_id');
                $data['params']['booking_id'] = 176769;
                $data['params']['date'] = "21/11/2023";
                $data['params']['time_from'] = "14:00";
                $professional_prefer = array(
                    // currently allowing same hours for all profs.
                    //array('professional_id' => 316),
                    //array('professional_id' => 3),
                );
                $data['params']['professional_prefer'] = $professional_prefer;
            }
            /************************************************************* */
            // required input check
            $input = @$data['params'];
            $validator = Validator::make(
                (array) $input,
                [
                    'booking_id' => 'required|integer',
                    'date' => 'required|date_format:d/m/Y|after:' . date('d/m/Y', strtotime("-1 day")), // minimum date today
                    'time_from' => 'required|date_format:H:i',
                    'professional_prefer' => 'array',
                ],
                [],
                [
                    'booking_id' => 'Booking ID',
                    'date' => 'Date',
                    'time_from' => 'Start Time',
                    'professional_prefer' => 'Selected Professionals',
                ]
            );
            if ($validator->fails()) {
                return Response::json(array('result' => array('status' => 'failed', 'message' => $validator->errors()->first()), 'debug' => $debug), 200, array(), customerResponseJsonConstants());
            }
            /************************************************************* */
            $input['professional_prefer'] = array_unique(@$input['professional_prefer'] ?: [], SORT_REGULAR); // remove duplicates - rare case or app bug
            /*if (sizeof($input['professional_prefer']) > $input['professionals_count']) {
            return Response::json(array('result' => array('status' => 'failed', 'message' => "Professional count mismatch."), 'debug' => $debug), 200, array(), customerResponseJsonConstants());
            }*/
            /************************************************************* */
            $customer = Customer::where('customer_id', '=', $input['id'])->first();
            $bookings = DB::table('bookings as b')
                ->select(
                    'b.*',
                )
                ->where([['b.customer_id', '=', $input['id']], ['b.booking_status', '!=', 2]])
                ->where(function ($query) use ($input) {
                    $query->where('b.booking_common_id', $input['booking_id']); // multiple maids
                    $query->orWhere('b.booking_id', $input['booking_id']);
                })
                ->get();
            if (sizeof($bookings) == 0) {
                throw new \ErrorException('Booking not found.');
            }
            $booking_ids = array_column($bookings->toArray(), 'booking_id');
            $input['professionals_count'] = sizeof($booking_ids);
            $booking = $bookings[0];
            $booking_common_id = $bookings[0]->booking_common_id ?: $bookings[0]->booking_id;
            /************************************************************* */
            // stop reschedule based on time left
            $booking_time = Carbon::parse($booking->service_start_date.' '. $booking->time_from)->format('Y-m-d H:i:s'); // booked date time
            $booking_time_minus_60_minutes = Carbon::parse($booking_time)->subMinutes(60);
            $current_time = Carbon::now();
            if ($current_time >= $booking_time_minus_60_minutes) {
                throw new \ErrorException('Reschedule is not available at this moment, please contact us.');
            }
            /************************************************************* */
            // format for machine :/
            $input['date'] = Carbon::createFromFormat('d/m/Y', $input['date'])->format('Y-m-d');
            $input['time_from'] = Carbon::createFromFormat('H:i', $input['time_from'])->format('H:i:s');
            $input['hours'] = Carbon::createFromFormat('H:i:s', $booking->time_from)->diffInMinutes(Carbon::createFromFormat('H:i:s', $booking->time_to)) / 60; // based on db
            $input['time_to'] = Carbon::createFromFormat('H:i:s', $input['time_from'])->addMinutes($input['hours'] * 60)->format('H:i:s');
            /************************************************************* */
            /******* check if any day service entry found for any booking */
            $day_service = DB::table('day_services as ds')
                ->select(
                    'ds.day_service_id',
                )
                ->whereIn('ds.booking_id', $booking_ids)->first();
            if ($day_service) {
                throw new \ErrorException('Schedule already started for this booking, reschedule is not possible!');
            }
            /************************************************************* */
            // real time time-slot availability check
            if (($error = is_time_slot_available($input['date'], $input['time_from'], $input['time_to'])) !== true) {
                //throw new \ErrorException($error);
            }
            /************************************************************* */
            /********** find end date based on no. of weeks and service end */
            $service_actual_end_date = $input['date'];
            if ($booking->service_end == "1") {
                if ($booking->booking_type == "WE") {
                    $no_of_service = Carbon::parse($booking->service_actual_end_date)->diffInWeeks($booking->service_start_date) + 1;
                    $service_actual_end_date = Carbon::parse($input['date'])->addWeeks($no_of_service - 1)->format('Y-m-d');
                    //dd($no_of_service);
                } else if ($booking->booking_type == "BW") {
                    $no_of_service = (int) round(round(Carbon::parse($booking->service_actual_end_date)->diffInWeeks($booking->service_start_date) + 1 / 2) / 2);
                    $service_actual_end_date = Carbon::parse($input['date'])->addWeeks($no_of_service * 2 - 2)->format('Y-m-d');
                    //dd($service_actual_end_date);
                }
            }
            /************************************************************* */
            $booking_search = new stdClass();
            $booking_search->service_week_day = Carbon::parse($input['date'])->format('w');
            $booking_search->service_start_date = $input['date'];
            $booking_search->service_actual_end_date = $service_actual_end_date;
            $booking_search->booking_type = $booking->booking_type;
            $booking_search->time_from = $input['time_from'];
            $booking_search->time_to = $input['time_to'];
            $booking_search->service_end = $booking->service_end;
            $busy_bookings = get_busy_bookings($booking_search, $booking_ids)->toArray();
            // trick for BW skipping
            if ($booking->booking_type == "BW") {
                // trick to exclude not overlapped bw bookings
                foreach ($busy_bookings as $key => $busy_booking) {
                    if ($busy_booking->service_start_date_week_difference & 1) { // if odd number
                        // week diff odd number means it will not overlap other bw bookings, so remove it from timed bookings
                        unset($busy_bookings[$key]);
                    }
                }
            }
            $busy_maid_ids = array_unique(array_column($busy_bookings, 'maid_id'));
            $available_maids = DB::table('maids as m')
                ->select(
                    'm.maid_id',
                    'm.maid_name as name',
                )
                ->where(['maid_status' => 1])
                ->whereNotIn('maid_id', $busy_maid_ids)
                ->orderBy('maid_name', 'ASC')
                ->get();
            $available_maid_ids = array_column($available_maids->toArray(), 'maid_id');
            $preferred_professional_ids = array_column($input['professional_prefer'], 'professional_id');
            /************************************************************* */
            // check selected prof. availability
            //dd($available_maid_ids);
            $selected_professionals = [];
            $prof_validated = 0;
            foreach ($preferred_professional_ids as $key => $preferred_professional_id) {
                // manual professional select
                if (in_array($preferred_professional_id, $available_maid_ids)) {
                    // selected prof. available
                    $prof_validated += 1;
                    $selected_professionals[] = $preferred_professional_id;
                    unset($available_maid_ids[array_search($preferred_professional_id, $available_maid_ids)]); // remove from available list
                    $available_maid_ids = array_values($available_maid_ids); // reset array index keys
                } else {
                    // selected prof. not available
                    $maid = DB::table('maids as m')
                        ->select(
                            'm.maid_id',
                            'm.maid_name as name',
                        )
                        ->where(['maid_id' => $preferred_professional_id])
                        ->first();
                    throw new \ErrorException("Slot for " . $maid->name . " is not available.");
                }
            }
            /************************************************************* */
            for ($i = $prof_validated; $i < $input['professionals_count']; $i++) {
                shuffle($available_maid_ids); // just shuffle
                // auto assign professionals
                //die($input['professionals_count']);
                if (isset($available_maid_ids[0])) {
                    $selected_professionals[] = $available_maid_ids[0]; // auto assign first prof. id from list
                    unset($available_maid_ids[0]); // remove from available list
                    $available_maid_ids = array_values($available_maid_ids); // reset array index keys
                } else {
                    if (sizeof($selected_professionals) > 0) {
                        throw new \ErrorException("Currently " . sizeof($selected_professionals) . " professional" . (sizeof($selected_professionals) > 1 ? "s" : "") . " only available.");
                    } else {
                        throw new \ErrorException("Sorry, there're no professionals available.");
                    }
                }
            }
            /************************************************************* */
            // save with new data
            foreach ($bookings as $key => $booking_row) {
                $row = Booking::find($booking_row->booking_id);
                $row->service_start_date = $input['date'];
                $row->service_week_day = Carbon::parse($input['date'])->format('w');
                $row->time_from = $input['time_from'];
                $row->time_to = $input['time_to'];
                $row->service_end_date = $input['date'];
                //$row->maid_id = @$selected_professionals[$key] ?: 0;
                $row->maid_id = 0;
                $row->service_actual_end_date = $service_actual_end_date;
                $row->booking_status = 0;
                $row->save();
            }
            /************************************************************* */
            $response['activelist'] = DB::table('bookings as b')
                ->select(
                    'b.booking_id',
                    DB::raw('IFNULL(st.customer_app_service_type_name,st.service_type_name) as service'),
                    DB::raw('CONCAT((CASE WHEN b.reference_id = "" THEN b.booking_id ELSE b.reference_id END),"","","") as booking_reference'),
                    'f.name as frequency',
                    'ca.customer_address as address',
                    DB::raw('"AED" as currency'),
                    'b.total_amount as total',
                    DB::raw('(CASE WHEN b.payment_type_id IS NULL THEN b.pay_by ELSE pt.name END) as payment_method'),
                    DB::raw('DATE_FORMAT(b.time_from,"%H:%i") as start_time'),
                    DB::raw('DATE_FORMAT(b.time_to,"%H:%i") as end_time'),
                    DB::raw('DATE_FORMAT(SEC_TO_TIME(TIME_TO_SEC(TIMEDIFF(time_to,b.time_from))),"%H:%i") as hours_worked'),
                    DB::raw('"Waiting" as status'),
                    DB::raw('(CASE WHEN b.cleaning_material = "Y" THEN "yes" ELSE "no" END) as cleaning_materials'),
                )
                ->leftJoin('service_types as st', 'b.service_type_id', 'st.service_type_id')
                ->leftJoin('customers as c', 'b.customer_id', 'c.customer_id')
                ->leftJoin('frequencies as f', 'b.booking_type', 'f.code')
                ->leftJoin('payment_types as pt', 'b.payment_type_id', 'pt.id')
                ->leftJoin('customer_addresses as ca', 'b.customer_address_id', 'ca.customer_address_id')
                ->where([['b.booking_id', '=', $booking_common_id]])
                ->first();
            /************************************************************* */
            $response['activelist']->service_details = $response['activelist']->hours_worked . " hours, " . sizeof($selected_professionals) . " Professional " . ($response['activelist']->cleaning_materials == "yes" ? "with " : "without ") . "cleaning material.";
            $response['status'] = 'success';
            $response['message'] = 'Booking with Ref. No. ' . $booking->reference_id . ' is successfully rescheduled.';
            /************************************************************* */
            $notify = new stdClass();
            $notify->customer_id = $input['id'];
            $notify->booking_id = $booking_common_id;
            $notify->service_date = $booking->booking_type == "OD" ? $input['date'] : null;
            $notify->content = "Booking with Ref. Id. {{booking_ref_id}} is rescheduled, we'll contact you soon for confirmation.";
            addCustomerNotification($notify);
            // send push notification
            pushNotification($customer, ['title' => "Rescheduled !", 'body' => "Booking with Ref. Id " . $bookings[0]->reference_id . " is rescheduled."], ['screen' => 'BOOKING_HISTORY']);
            /************************************************************* */
            DB::commit();
            return Response::json(array('result' => $response, 'debug' => $debug), 200, array(), customerResponseJsonConstants());
        } catch (\Exception $e) {
            DB::rollback();
            return Response::json(array('result' => array('status' => 'failed', 'message' => $e->getMessage()), 'debug' => $debug), 200, array(), customerResponseJsonConstants());
        }
    }
}
