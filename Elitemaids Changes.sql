


ALTER TABLE `customers`
CHANGE `push_token` `push_token` varchar(500) COLLATE 'utf8_general_ci' NULL;
----------------------------------------------------------------------------------------------------------------------------------------
CREATE TABLE `service_type_categories` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `service_category_name` varchar(255) NOT NULL,
  `sort_order` int(5) NOT NULL DEFAULT 0,
  `customer_app_icon_file` varchar(255) DEFAULT NULL,
  `created_at` datetime NOT NULL DEFAULT current_timestamp(),
  `updated_at` datetime NOT NULL DEFAULT current_timestamp(),
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
----------------------------------------------------------------------------------------------------------------------------------------
CREATE TABLE `service_type_category_and_service_type_mapping` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `service_type_category_id` bigint(20) unsigned NOT NULL,
  `service_type_id` int(11) NOT NULL,
  `created_at` date NOT NULL,
  `updated_at` date NOT NULL,
  `deleted_at` date DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `service_type_category_id_service_type_id` (`service_type_category_id`,`service_type_id`),
  KEY `service_type_id` (`service_type_id`),
  CONSTRAINT `service_type_category_and_service_type_mapping_ibfk_1` FOREIGN KEY (`service_type_category_id`) REFERENCES `service_type_categories` (`id`),
  CONSTRAINT `service_type_category_and_service_type_mapping_ibfk_2` FOREIGN KEY (`service_type_id`) REFERENCES `service_types` (`service_type_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='one service have multiple category';
----------------------------------------------------------------------------------------------------------------------------------------
CREATE TABLE `service_type_models` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `model` varchar(255) NOT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
INSERT INTO `service_type_models` (`model`)
VALUES ('normal');
----------------------------------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------------------------------
CREATE TABLE `building_types` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `customer_app_banner` varchar(255) NOT NULL,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
----------------------------------------------------------------------------------------------------------------------------------------
CREATE TABLE `building_rooms` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `name` varchar(255) NOT NULL,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
----------------------------------------------------------------------------------------------------------------------------------------
CREATE TABLE `building_type_room` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `building_type_id` bigint(20) NOT NULL,
  `building_room_id` bigint(20) NOT NULL,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `building_type_id_building_room_id` (`building_type_id`,`building_room_id`),
  KEY `building_room_id` (`building_room_id`),
  CONSTRAINT `building_type_room_ibfk_1` FOREIGN KEY (`building_type_id`) REFERENCES `building_types` (`id`),
  CONSTRAINT `building_type_room_ibfk_2` FOREIGN KEY (`building_room_id`) REFERENCES `building_rooms` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
----------------------------------------------------------------------------------------------------------------------------------------
CREATE TABLE `building_type_room_packages` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `service_type_id` int(11) NOT NULL,
  `building_type_room_id` bigint(20) DEFAULT NULL COMMENT 'NULL means Offer',
  `is_offer` tinyint(4) DEFAULT NULL COMMENT '1 means offer',
  `title` varchar(255) DEFAULT NULL,
  `description` text NOT NULL,
  `info_html` text DEFAULT NULL,
  `service_time` time NOT NULL DEFAULT '00:30:00',
  `actual_total_amount` decimal(15,2) NOT NULL,
  `total_amount` decimal(15,2) NOT NULL,
  `customer_app_thumbnail` varchar(255) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `service_type_id` (`service_type_id`),
  KEY `building_type_room_id` (`building_type_room_id`),
  CONSTRAINT `building_type_room_packages_ibfk_1` FOREIGN KEY (`service_type_id`) REFERENCES `service_types` (`service_type_id`),
  CONSTRAINT `building_type_room_packages_ibfk_2` FOREIGN KEY (`building_type_room_id`) REFERENCES `building_type_room` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
ALTER TABLE `building_type_room_packages`
ADD `cart_limit` int(11) NOT NULL DEFAULT '1' COMMENT 'maximum count allowed to add on cart' AFTER `total_amount`;
----------------------------------------------------------------------------------------------------------------------------------------
CREATE TABLE `payment_types` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `name` varchar(100) NOT NULL,
  `code` varchar(30) NOT NULL,
  `description` varchar(255) DEFAULT NULL,
  `charge` decimal(15,2) NOT NULL DEFAULT 0.00,
  `charge_type` enum('F','P') NOT NULL,
  `customer_app_icon` varchar(255) DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `name` (`name`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

INSERT INTO `payment_types` (`id`, `name`, `code`, `description`, `charge`, `charge_type`, `customer_app_icon`, `deleted_at`) VALUES
(1,	'Cash',	'cash',	NULL,	10.00,	'F',	'cash.png',	NULL),
(2,	'Card',	'card',	NULL,	0.00,	'F',	'card.png',	NULL),
(3,	'Apple Pay',	'applepay',	NULL,	0.00,	'F',	'applepay.png',	'2023-06-23 18:41:58');
----------------------------------------------------------------------------------------------------------------------------------------

----------------------------------------------------------------------------------------------------------------------------------------
CREATE TABLE `genders` (
  `gender_id` int unsigned NOT NULL AUTO_INCREMENT,
  `gender` varchar(15) NOT NULL,
  `sort_order_id` int DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`gender_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

INSERT INTO `genders` (`gender_id`, `gender`, `sort_order_id`, `deleted_at`) VALUES
(1,	'Male',	1,	NULL),
(2,	'Female',	2,	NULL),
(3,	'Transgender',	3,	'2023-11-13 05:41:43'),
(4,	'Other',	5,	'2023-11-13 05:41:43'),
(5,	'Not Specify',	4,	NULL);
----------------------------------------------------------------------------------------------------------------------------------------
CREATE TABLE `customer_notifications` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `customer_id` bigint(20) unsigned DEFAULT NULL,
  `booking_id` bigint(20) unsigned DEFAULT NULL,
  `service_date` date DEFAULT NULL,
  `title` text DEFAULT NULL,
  `content` text NOT NULL,
  `read_at` datetime DEFAULT NULL,
  `cleared_at` datetime DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `customer_id` (`customer_id`),
  KEY `booking_id` (`booking_id`),
  CONSTRAINT `customer_notifications_ibfk_1` FOREIGN KEY (`customer_id`) REFERENCES `customers` (`customer_id`),
  CONSTRAINT `customer_notifications_ibfk_2` FOREIGN KEY (`booking_id`) REFERENCES `bookings` (`booking_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
ALTER TABLE `customer_notifications`
ADD `expired_at` datetime NULL AFTER `cleared_at`;
----------------------------------------------------------------------------------------------------------------------------------------
CREATE TABLE `customer_cards` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `customer_id` bigint(20) unsigned NOT NULL,
  `nick_name` varchar(155) NOT NULL,
  `card_number` varchar(16) NOT NULL,
  `expiry_date` date NOT NULL,
  `cvv` varchar(8) DEFAULT NULL,
  `created_at` timestamp NOT NULL DEFAULT current_timestamp(),
  `updated_at` timestamp NULL DEFAULT NULL ON UPDATE current_timestamp(),
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `customer_id` (`customer_id`),
  CONSTRAINT `customer_cards_ibfk_1` FOREIGN KEY (`customer_id`) REFERENCES `customers` (`customer_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
----------------------------------------------------------------------------------------------------------------------------------------
ALTER TABLE `bookings`
ADD `booking_common_id` bigint unsigned NULL AFTER `booking_id`;
ALTER TABLE `bookings`
ADD FOREIGN KEY (`booking_common_id`) REFERENCES `bookings` (`booking_id`);
ALTER TABLE `bookings`
ADD `subscription_package_id` bigint unsigned NULL,
ADD FOREIGN KEY (`subscription_package_id`) REFERENCES `package` (`package_id`);
----------------------------------------------------------------------------------------------------------------------------------------
CREATE TABLE `holidays` (
  `id` int(10) unsigned NOT NULL AUTO_INCREMENT,
  `date` date NOT NULL,
  `holiday_name` varchar(255) NOT NULL,
  `created_by_user` int(11) DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `created_by_user` (`created_by_user`),
  CONSTRAINT `holidays_ibfk_1` FOREIGN KEY (`created_by_user`) REFERENCES `users` (`user_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
----------------------------------------------------------------------------------------------------------------------------------------
CREATE TABLE `rush_slot_charges` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `date_from` date NOT NULL,
  `date_to` date NOT NULL,
  `time` time NOT NULL COMMENT 'time slot',
  `extra_charge` decimal(10,2) NOT NULL DEFAULT 0.00,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
----------------------------------------------------------------------------------------------------------------------------------------
CREATE TABLE `maid_rating` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `maid_id` int(10) unsigned NOT NULL,
  `service_type_id` int(11) DEFAULT NULL,
  `rated_by_customer_id` bigint(20) unsigned DEFAULT NULL,
  `day_service_id` bigint(20) unsigned DEFAULT NULL,
  `rating` enum('1','2','3','4','5') NOT NULL,
  `comment` varchar(255) DEFAULT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `maid_id` (`maid_id`),
  KEY `day_service_id` (`day_service_id`),
  KEY `rated_by_customer_id` (`rated_by_customer_id`),
  KEY `service_type_id` (`service_type_id`),
  CONSTRAINT `maid_rating_ibfk_1` FOREIGN KEY (`maid_id`) REFERENCES `maids` (`maid_id`),
  CONSTRAINT `maid_rating_ibfk_2` FOREIGN KEY (`day_service_id`) REFERENCES `day_services` (`day_service_id`),
  CONSTRAINT `maid_rating_ibfk_3` FOREIGN KEY (`rated_by_customer_id`) REFERENCES `customers` (`customer_id`),
  CONSTRAINT `maid_rating_ibfk_4` FOREIGN KEY (`service_type_id`) REFERENCES `service_types` (`service_type_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
----------------------------------------------------------------------------------------------------------------------------------------
ALTER TABLE `maids`
ADD `maid_name_customer_app` varchar(110) COLLATE 'utf8_general_ci' NULL AFTER `maid_name`;
----------------------------------------------------------------------------------------------------------------------------------------
CREATE TABLE `customer_faq` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `order_priority` int(11) NOT NULL,
  `question` text NOT NULL,
  `answer` text NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

INSERT INTO `customer_faq` (`id`, `order_priority`, `question`, `answer`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1,	2,	'What services do you offer?',	'We offer a range of cleaning services including residential cleaning, commercial cleaning, deep cleaning, move-in/out cleaning, and post-construction cleaning.',	'2023-06-08 10:18:53',	NULL,	NULL),
(2,	1,	'Do you provide cleaning supplies and equipment?',	'Yes, we provide all the necessary cleaning supplies and equipment needed to clean your home or office.',	'2023-06-08 10:19:13',	NULL,	NULL),
(3,	4,	'What areas do you service?',	'We currently provide our cleaning services to [insert area or location]. However, we may be able to service other areas upon request.',	'2023-06-08 10:19:40',	NULL,	NULL),
(4,	5,	'How do you determine the cost of cleaning services?',	'We determine the cost of our cleaning services based on several factors including the size of the space, the type of cleaning required, and the frequency of service. We provide a personalized quote for each customer after a thorough consultation.',	'2023-06-08 10:20:00',	NULL,	NULL),
(5,	3,	'Do you offer eco-friendly cleaning options?',	'Yes, we offer eco-friendly cleaning options upon request. Our eco-friendly cleaning products are safe for use around pets and children and are just as effective as traditional cleaning products.',	'2023-06-08 10:20:16',	NULL,	NULL);
----------------------------------------------------------------------------------------------------------------------------------------
CREATE TABLE `booking_cancel_reasons` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `order_priority` int(11) NOT NULL,
  `reason` text NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

INSERT INTO `booking_cancel_reasons` (`id`, `order_priority`, `reason`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1,	1,	'Personal schedule conflicts',	'2023-06-09 11:31:00',	'2023-06-09 11:31:00',	NULL),
(2,	2,	'Illness or family emergency',	'2023-06-09 11:31:00',	'2023-06-09 11:31:00',	NULL),
(3,	3,	'Dissatisfied with previous cleaning',	'2023-06-09 11:31:00',	'2023-06-09 11:31:00',	NULL),
(4,	4,	'Financial constraints',	'2023-06-09 11:31:00',	'2023-06-09 11:31:00',	NULL),
(5,	5,	'Moving or relocation',	'2023-06-09 11:31:00',	'2023-06-09 11:31:00',	NULL),
(6,	6,	'Change in cleaning needs',	'2023-06-09 11:31:00',	'2023-06-09 11:31:00',	NULL),
(7,	7,	'Unsatisfactory communication with cleaning service',	'2023-06-09 11:31:00',	'2023-06-09 11:31:00',	NULL);
----------------------------------------------------------------------------------------------------------------------------------------
CREATE TABLE `service_addons` (
  `service_addons_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `service_type_id` int(11) NOT NULL,
  `service_addon_name` varchar(255) NOT NULL,
  `service_addon_description` varchar(255) DEFAULT NULL,
  `service_minutes` decimal(4,2) NOT NULL DEFAULT 0.00,
  `strike_amount` decimal(15,2) NOT NULL,
  `amount` decimal(15,2) NOT NULL,
  `cart_limit` int(11) NOT NULL DEFAULT 1 COMMENT 'maximum count allowed to add on cart',
  `customer_app_thumbnail` varchar(255) DEFAULT NULL,
  `populariry` bigint(20) NOT NULL DEFAULT 0 COMMENT 'higher the popularity means top selling addon',
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`service_addons_id`),
  KEY `service_type_id` (`service_type_id`),
  CONSTRAINT `service_addons_ibfk_1` FOREIGN KEY (`service_type_id`) REFERENCES `service_types` (`service_type_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
INSERT INTO `service_addons` (`service_addons_id`, `service_type_id`, `service_addon_name`, `service_addon_description`, `service_minutes`, `strike_amount`, `amount`, `cart_limit`, `customer_app_thumbnail`, `populariry`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1,	1,	'Balcony Cleaning',	NULL,	15.00,	30.00,	15.00,	1,	'balcony-cleaning.png',	10,	'2023-11-13 11:41:59',	'2023-11-13 11:41:59',	NULL),
(2,	1,	'Cupboard Cleaning',	NULL,	15.00,	30.00,	15.00,	1,	'cupboard-cleaning.png',	5,	'2023-11-13 11:41:59',	'2023-11-13 11:41:59',	NULL);
----------------------------------------------------------------------------------------------------------------------------------------
CREATE TABLE `booking_packages` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `booking_id` bigint(20) unsigned NOT NULL,
  `building_type_room_package_id` bigint(20) NOT NULL,
  `unit_price` decimal(15,2) NOT NULL,
  `quantity` int(11) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `booking_id_package_id` (`booking_id`,`building_type_room_package_id`),
  KEY `building_type_room_package_id` (`building_type_room_package_id`),
  CONSTRAINT `booking_packages_ibfk_1` FOREIGN KEY (`booking_id`) REFERENCES `bookings` (`booking_id`),
  CONSTRAINT `booking_packages_ibfk_2` FOREIGN KEY (`building_type_room_package_id`) REFERENCES `building_type_room_packages` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
----------------------------------------------------------------------------------------------------------------------------------------
CREATE TABLE `booking_addons` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `booking_id` bigint(20) unsigned DEFAULT NULL,
  `service_addons_id` bigint(20) unsigned NOT NULL,
  `service_addon_name` varchar(255) DEFAULT NULL,
  `service_minutes` decimal(4,2) unsigned NOT NULL DEFAULT 0.00,
  `strike_amount` decimal(15,2) NOT NULL,
  `unit_price` decimal(15,2) NOT NULL,
  `quantity` int(10) unsigned NOT NULL,
  `amount` decimal(15,2) unsigned NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `booking_id_service_addons_id` (`booking_id`,`service_addons_id`),
  KEY `booking_id` (`booking_id`),
  KEY `service_addons_id` (`service_addons_id`),
  CONSTRAINT `booking_addons_ibfk_1` FOREIGN KEY (`booking_id`) REFERENCES `bookings` (`booking_id`),
  CONSTRAINT `booking_addons_ibfk_2` FOREIGN KEY (`service_addons_id`) REFERENCES `service_addons` (`service_addons_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
----------------------------------------------------------------------------------------------------------------------------------------
CREATE TABLE `service_addons_points` (
  `service_addons_included_id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `service_addons_id` bigint(20) unsigned NOT NULL,
  `point_type` enum('WHAT_INCLUDED','WHAT_EXCLUDED') NOT NULL,
  `sort_order_id` int(10) unsigned DEFAULT NULL,
  `point_html` varchar(255) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`service_addons_included_id`),
  KEY `service_addons_id` (`service_addons_id`),
  CONSTRAINT `service_addons_points_ibfk_1` FOREIGN KEY (`service_addons_id`) REFERENCES `service_addons` (`service_addons_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
----------------------------------------------------------------------------------------------------------------------------------------
ALTER TABLE `settings`
ADD `customer_app_img_version` varchar(10) NULL DEFAULT '1.0' AFTER `quick_refresh_count`;
ALTER TABLE `settings`
ADD `customer_app_google_maps_api_key` varchar(255) COLLATE 'latin1_swedish_ci' NULL AFTER `customer_app_img_version`;
----------------------------------------------------------------------------------------------------------------------------------------
CREATE TABLE `service_type_charges` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `service_type_id` int(11) NOT NULL,
  `no_of_hours_from` decimal(4,2) DEFAULT NULL,
  `no_of_hours_to` decimal(4,2) DEFAULT NULL,
  `rate_per_hour` decimal(5,2) NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `service_type_id` (`service_type_id`),
  CONSTRAINT `service_type_charges_ibfk_1` FOREIGN KEY (`service_type_id`) REFERENCES `service_types` (`service_type_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;
----------------------------------------------------------------------------------------------------------------------------------------
ALTER TABLE `bookings`
ADD `amount_before_discount` decimal(10,2) NULL;
ALTER TABLE `bookings`
ADD `payment_type_charge` decimal(10,2) NULL;

ALTER TABLE `bookings`
ADD `addons_included` tinyint(1) NULL;
ALTER TABLE `bookings`
ADD `addons_amount` decimal(10,2) NULL;
ALTER TABLE `bookings`
ADD `web_book_show` tinyint(1) NULL;
----------------------------------------------------------------------------------------------------------------------------------------
ALTER TABLE `online_payments`
CHANGE `post_data` `post_data` text COLLATE 'latin1_swedish_ci' NULL AFTER `user_agent`;
ALTER TABLE `online_payments`
CHANGE `user_agent` `user_agent` varchar(500) COLLATE 'latin1_swedish_ci' NULL AFTER `ip_address`;
----------------------------------------------------------------------------------------------------------------------------------------
CREATE TABLE `customers_temp` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `customer_id` bigint(20) unsigned NOT NULL,
  `mobile_number_1_country_code` varchar(10) DEFAULT NULL,
  `mobile_number_1` varchar(150) DEFAULT NULL,
  `mobile_number_1_otp` varchar(50) DEFAULT NULL,
  `mobile_number_1_otp_expired_at` datetime DEFAULT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `customer_id` (`customer_id`),
  CONSTRAINT `customers_temp_ibfk_1` FOREIGN KEY (`customer_id`) REFERENCES `customers` (`customer_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COMMENT='save temperory data for verification';
----------------------------------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------------------------------
ALTER TABLE `service_types`
ADD UNIQUE `web_url_slug` (`web_url_slug`);
UPDATE `service_types` SET `web_url_slug` = 'home-cleaning' WHERE `service_type_id` = '1';
----------------------------------------------------------------------------------------------------------------------------------------
CREATE TABLE `booking_subscription_packages` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `booking_id` bigint(20) unsigned NOT NULL,
  `package_id` bigint(20) unsigned NOT NULL,
  `package_name` varchar(255) NOT NULL,
  `cleaning_material` enum('Y','N') NOT NULL DEFAULT 'N',
  `no_of_bookings` int(11) unsigned NOT NULL,
  `working_hours` double(5,2) unsigned NOT NULL,
  `booking_type` enum('MO','WE','YE') NOT NULL,
  `strikethrough_amount` double(10,2) unsigned NOT NULL,
  `amount` double(10,2) unsigned NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `booking_id` (`booking_id`),
  KEY `package_id` (`package_id`),
  CONSTRAINT `booking_subscription_packages_ibfk_1` FOREIGN KEY (`booking_id`) REFERENCES `bookings` (`booking_id`),
  CONSTRAINT `booking_subscription_packages_ibfk_2` FOREIGN KEY (`package_id`) REFERENCES `package` (`package_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
----------------------------------------------------------------------------------------------------------------------------------------
CREATE TABLE `customer_faq_help` (
  `id` int NOT NULL AUTO_INCREMENT,
  `order_priority` int NOT NULL,
  `question` varchar(500) COLLATE utf8mb4_general_ci NOT NULL,
  `answer` text COLLATE utf8mb4_general_ci NOT NULL,
  `created_at` datetime NOT NULL,
  `updated_at` datetime NOT NULL,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;
----------------------------------------------------------------------------------------------------------------------------------------
ALTER TABLE `coupon_code`
CHANGE `type` `type` varchar(50) COLLATE 'latin1_swedish_ci' NOT NULL DEFAULT 'C' COMMENT 'C-cleaning,M-maintenance,F-Frequency' AFTER `percentage`;
ALTER TABLE `coupon_code`
CHANGE `service_id` `service_id` int NULL DEFAULT '0' AFTER `coupon_name`;
----------------------------------------------------------------------------------------------------------------------------------------
ALTER TABLE `bookings`
ADD `_service_amount` decimal(10,2) unsigned NULL,
ADD `_cleaning_materials_amount` decimal(10,2) unsigned NULL AFTER `_service_amount`,
ADD `_service_addons_amount` decimal(10,2) unsigned NULL AFTER `_cleaning_materials_amount`,
ADD `_packages_amount` decimal(10,2) unsigned NULL AFTER `_service_addons_amount`,
ADD `_subscription_package_amount` decimal(10,2) unsigned NULL AFTER `_packages_amount`,
ADD `_amount_before_discount` decimal(10,2) unsigned NULL AFTER `_subscription_package_amount`,
ADD `_coupon_discount` decimal(10,2) unsigned NULL AFTER `_amount_before_discount`,
ADD `_discount_total` decimal(10,2) unsigned NULL AFTER `_coupon_discount`,
ADD `_taxable_amount` decimal(10,2) unsigned NULL AFTER `_discount_total`,
ADD `_vat_percentage` decimal(10,2) unsigned NULL AFTER `_taxable_amount`,
ADD `_vat_amount` decimal(10,2) unsigned NULL AFTER `_vat_percentage`,
ADD `_taxed_amount` decimal(10,2) unsigned NULL AFTER `_vat_amount`,
ADD `_payment_type_charge` decimal(10,2) unsigned NULL AFTER `_taxed_amount`,
ADD `_total_payable` decimal(10,2) unsigned NULL AFTER `_payment_type_charge`;
ALTER TABLE `bookings`
CHANGE `_service_amount` `_service_amount` decimal(10,2) unsigned NULL DEFAULT '0' AFTER `subscription_package_id`,
CHANGE `_cleaning_materials_amount` `_cleaning_materials_amount` decimal(10,2) unsigned NULL DEFAULT '0' AFTER `_service_amount`,
CHANGE `_service_addons_amount` `_service_addons_amount` decimal(10,2) unsigned NULL DEFAULT '0' AFTER `_cleaning_materials_amount`,
CHANGE `_packages_amount` `_packages_amount` decimal(10,2) unsigned NULL DEFAULT '0' AFTER `_service_addons_amount`,
CHANGE `_subscription_package_amount` `_subscription_package_amount` decimal(10,2) unsigned NULL DEFAULT '0' AFTER `_packages_amount`,
CHANGE `_amount_before_discount` `_amount_before_discount` decimal(10,2) unsigned NULL DEFAULT '0' AFTER `_subscription_package_amount`,
CHANGE `_coupon_discount` `_coupon_discount` decimal(10,2) unsigned NULL DEFAULT '0' AFTER `_amount_before_discount`,
CHANGE `_discount_total` `_discount_total` decimal(10,2) unsigned NULL DEFAULT '0' AFTER `_coupon_discount`,
CHANGE `_taxable_amount` `_taxable_amount` decimal(10,2) unsigned NULL DEFAULT '0' AFTER `_discount_total`,
CHANGE `_vat_percentage` `_vat_percentage` decimal(10,2) unsigned NULL DEFAULT '0' AFTER `_taxable_amount`,
CHANGE `_vat_amount` `_vat_amount` decimal(10,2) unsigned NULL DEFAULT '0' AFTER `_vat_percentage`,
CHANGE `_taxed_amount` `_taxed_amount` decimal(10,2) unsigned NULL DEFAULT '0' AFTER `_vat_amount`,
CHANGE `_payment_type_charge` `_payment_type_charge` decimal(10,2) unsigned NULL DEFAULT '0' AFTER `_taxed_amount`,
CHANGE `_total_payable` `_total_payable` decimal(10,2) unsigned NULL DEFAULT '0' AFTER `_payment_type_charge`;
----------------------------------------------------------------------------------------------------------------------------------------
----------------------------------------------------------------------------------------------------------------------------------------
ALTER TABLE `building_type_room_packages`
ADD `sort_order` int(5) NOT NULL DEFAULT '1' AFTER `customer_app_thumbnail`;
----------------------------------------------------------------------------------------------------------------------------------------
ALTER TABLE `bookings`
CHANGE `web_book_show` `web_book_show` tinyint(1) NULL DEFAULT '1' AFTER `addons_amount`;
----------------------------------------------------------------------------------------------------------------------------------------
CREATE TABLE `week_days` (
  `week_day_id` int(11) NOT NULL,
  `weekend` tinyint(1) NOT NULL DEFAULT 0,
  `order_id` int(11) NOT NULL,
  `week_name` varchar(50) NOT NULL,
  PRIMARY KEY (`week_day_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

INSERT INTO `week_days` (`week_day_id`, `weekend`, `order_id`, `week_name`) VALUES
(0,	0,	1,	'Sunday'),
(1,	0,	2,	'Monday'),
(2,	0,	3,	'Tuesday'),
(3,	0,	4,	'Wednesday'),
(4,	0,	5,	'Thursday'),
(5,	0,	6,	'Friday'),
(6,	0,	7,	'Saturday');
----------------------------------------------------------------------------------------------------------------------------------------
ALTER TABLE `package`
ADD `tax_method` enum('I','E') NOT NULL DEFAULT 'I' AFTER `vat_charge_percentage`;
----------------------------------------------------------------------------------------------------------------------------------------
ALTER TABLE `building_type_room_packages`
CHANGE `customer_app_thumbnail` `customer_app_thumbnail` varchar(255) COLLATE 'utf8mb4_general_ci' NULL DEFAULT 'default.png' AFTER `cart_limit`;
UPDATE `building_type_room_packages` SET
`customer_app_thumbnail` = 'default.png';
----------------------------------------------------------------------------------------------------------------------------------------
ALTER TABLE `building_type_room_packages`
CHANGE `customer_app_thumbnail` `customer_app_thumbnail` varchar(255) COLLATE 'utf8mb4_general_ci' NOT NULL DEFAULT 'default.png' AFTER `cart_limit`;
----------------------------------------------------------------------------------------------------------------------------------------
ALTER TABLE `customer_addresses`
CHANGE `latitude` `latitude` varchar(100) COLLATE 'latin1_swedish_ci' NULL AFTER `customer_address`,
CHANGE `longitude` `longitude` varchar(100) COLLATE 'latin1_swedish_ci' NULL AFTER `latitude`;
----------------------------------------------------------------------------------------------------------------------------------------
ALTER TABLE `payment_types`
ADD `default` tinyint NULL AFTER `customer_app_icon`;
ALTER TABLE `payment_types`
ADD `sort_order` int(3) NULL AFTER `customer_app_icon`;
ALTER TABLE `payment_types`
ADD `show_in_web` tinyint(1) NULL AFTER `default`;
ALTER TABLE `payment_types`
ADD `show_in_app` tinyint(1) NULL AFTER `show_in_web`;
ALTER TABLE payment_types CHANGE show_in_web show_in_web TINYINT(1) NULL DEFAULT '1' COMMENT '1-Active,null-inactive';
ALTER TABLE payment_types CHANGE show_in_app show_in_app TINYINT(1) NULL DEFAULT '1' COMMENT '1-Active,null-inactive';
---------------------------------------------------------------------------------------------------------------------------------------- done
----------------------------------------------------------------------------------------------------------------------------------------
CREATE TABLE `building_type_banners` (
  `id` int NOT NULL AUTO_INCREMENT,
  `service_type_id` int NOT NULL,
  `building_type_id` bigint NOT NULL,
  `banner_image` varchar(255) NOT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `service_type_id_building_type_id` (`service_type_id`,`building_type_id`),
  UNIQUE KEY `banner_image` (`banner_image`),
  KEY `building_type_id` (`building_type_id`),
  CONSTRAINT `building_type_banners_ibfk_1` FOREIGN KEY (`service_type_id`) REFERENCES `service_types` (`service_type_id`),
  CONSTRAINT `building_type_banners_ibfk_2` FOREIGN KEY (`building_type_id`) REFERENCES `building_types` (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;

INSERT INTO `building_type_banners` (`id`, `service_type_id`, `building_type_id`, `banner_image`) VALUES
(1,	25,	1,	'25-1.jpg'),
(2,	25,	2,	'25-2.jpg'),
(3,	41,	6,	'41-6.jpg'),
(4,	42,	4,	'42-4.jpg'),
(5,	43,	3,	'43-3.jpg'),
(7,	44,	1,	'44-1.jpg'),
(8,	44,	2,	'44-2.jpg'),
(9,	36,	1,	'36-1.jpg'),
(10,	36,	5,	'36-5.jpg'),
(11,	36,	2,	'36-2.jpg');
----------------------------------------------------------------------------------------------------------------------------------------
ALTER TABLE `building_type_room_packages`
ADD `no_of_maids` int(5) NOT NULL DEFAULT '1' AFTER `service_time`;
----------------------------------------------------------------------------------------------------------------------------------------
ALTER TABLE `bookings`
CHANGE `booked_by` `booked_by` int(11) NULL DEFAULT '0' AFTER `booking_from`;
----------------------------------------------------------------------------------------------------------------------------------------
ALTER TABLE `push_notications`
CHANGE `message` `message` varchar(255) COLLATE 'utf8mb3_general_ci' NOT NULL AFTER `title`;
----------------------------------------------------------------------------------------------------------------------------------------
ALTER TABLE `payment_types`
DROP INDEX `name`;
----------------------------------------------------------------------------------------------------------------------------------------