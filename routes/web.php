<?php

use App\Http\Controllers\PaymentFrameController;
use App\Http\Controllers\ccavenuePaymentController;
use App\Http\Controllers\OutstandingPaymentController;
use App\Http\Controllers\WebviewStaticPageController;
use App\Http\Controllers\ArtisanController;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
 */
Route::group(['middleware' => 'web'], function () {
    Route::any('payment/gateway/checkout/entry/{booking_id}', [PaymentFrameController::class, 'checkout_gateway_entry']); // card entry form
    Route::any('payment/gateway/checkout/go/{booking_id}', [PaymentFrameController::class, 'checkout_gateway_go']); // checkout card form submit
    Route::any('payment/gateway/checkout/process/{booking_id}', [PaymentFrameController::class, 'checkout_payment_process']); // checkout data process
    Route::any('payment/gateway/checkout/success/{booking_id}', [PaymentFrameController::class, 'checkout_success'])->name('checkout-success'); // checkout success
    Route::any('payment/gateway/checkout/failed/{booking_id}', [PaymentFrameController::class, 'checkout_failed'])->name('checkout-failed'); // checkout failed
    // ccavenue pages
    Route::prefix('payment/ccavenue')->group(function () {
        Route::get('entry/{booking_id}', [ccavenuePaymentController::class, 'ccavenue_hidden_form']); // ccavenue hidden ccavenue submit
        Route::get('outstanding-entry', [OutstandingPaymentController::class, 'outstandingPaymentInitiate']); // ccavenue hidden form submit
        Route::any('success/{reference_id}', [ccavenuePaymentController::class, 'loading_webview_success']);
        Route::any('failed/{reference_id}', [ccavenuePaymentController::class, 'loading_webview_failed']);
    });
    // static pages
    Route::get('webview/terms-and-conditions', [WebviewStaticPageController::class, 'terms_and_conditions'])->name('terms-conditions');
    Route::get('webview/privacy-policy', [WebviewStaticPageController::class, 'privacy_policy'])->name('privacy-policy');
    Route::get('webview/about-us', [WebviewStaticPageController::class, 'about_us'])->name('about-us');
});

Route::get('/', function () {
    return '<h1>DHK v1.0</h1><br>Need something more in url ⏩ !';
});
Route::get('/api', function (Request $request) {
    return '<h1>DHK v1.0</h1><br>Which api, add in url 🤐 ?';
});

/*
 |
 | clear data from web
 |
 */
Route::get('php-artisan', [ArtisanController::class, 'php_artisan']);
Route::get('/clear-cache', function () {
    $exitCode = Artisan::call('cache:clear');
    if ($exitCode == 0) {
        return "Cache cleared successfully !";
    }
});
Route::get('/clear-config', function () {
    $exitCode = Artisan::call('config:clear');
    if ($exitCode == 0) {
        return "Config cleared successfully !";
    }
});
Route::get('/cache-config', function () {
    $exitCode = Artisan::call('config:cache');
    if ($exitCode == 0) {
        return "Config cached successfully !";
    }
});
Route::get('/cache-route', function () {
    $exitCode = Artisan::call('route:cache');
    if ($exitCode == 0) {
        return "Routes cached successfully !";
    }
});
Route::get('/clear-route', function () {
    $exitCode = Artisan::call('route:clear');
    if ($exitCode == 0) {
        return "Routes cleared successfully !";
    }
});