{{--<div style="width: 700px; height: auto; border-radius: 5px; overflow: hidden; margin-top: 50px;">
    <a href="javascript:void(0);" style="display: block; cursor: default !important;"><img
            src="{{ Config::get('values.file_server_url').'images/email/email-footer-banner.jpg?v='.@$settings->customer_app_img_version }}" width="100%"
height="" alt="" style="border-radius: 5px; cursor: default !important; overflow: hidden; " />
</a>
</div>--}}

<div style="width: 600px; height:auto; padding: 20px 0px 0px 32px; margin-bottom: 30px; text-align: center;">
    <div style="width: 100%; text-align: center;"><img src="{{asset('images/dhk-logo.png')}}?v={{Config::get('version.mail_img')}}" width="300" height="33" alt="" style="margin:0 auto;" /></div>
    <p style="font-family: Tahoma, Geneva, sans-serif; font-size:14px; color: #555; line-height:20px; padding: 5px 0px 15px 0px; margin:0px; -webkit-text-size-adjust: 100%; -moz-text-size-adjust: 100%; -ms-text-size-adjust: 100%;">
        @include('emails.includes.address_html_footer')
    </p>
</div>