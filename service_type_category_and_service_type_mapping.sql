-- Adminer 4.8.1 MySQL 11.3.2-MariaDB dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

SET NAMES utf8mb4;

DROP TABLE IF EXISTS `service_type_category_and_service_type_mapping`;
CREATE TABLE `service_type_category_and_service_type_mapping` (
  `id` bigint(20) unsigned NOT NULL AUTO_INCREMENT,
  `service_type_category_id` bigint(20) unsigned NOT NULL,
  `service_type_id` int(11) NOT NULL,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `service_type_category_id_service_type_id` (`service_type_category_id`,`service_type_id`),
  KEY `service_type_id` (`service_type_id`),
  CONSTRAINT `service_type_category_and_service_type_mapping_ibfk_1` FOREIGN KEY (`service_type_category_id`) REFERENCES `service_type_categories` (`id`),
  CONSTRAINT `service_type_category_and_service_type_mapping_ibfk_2` FOREIGN KEY (`service_type_id`) REFERENCES `service_types` (`service_type_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci COMMENT='one service have multiple category';

INSERT INTO `service_type_category_and_service_type_mapping` (`id`, `service_type_category_id`, `service_type_id`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1,	1,	1,	'2024-05-26 00:00:00',	'2024-05-26 00:00:00',	NULL),
(2,	1,	4,	'2024-05-30 16:19:47',	'2024-05-30 16:19:47',	NULL),
(3,	2,	5,	'2024-06-11 10:38:32',	'2024-06-11 10:38:32',	NULL),
(4,	2,	52,	'2024-06-11 10:38:32',	'2024-06-11 10:38:32',	NULL),
(5,	2,	47,	'2024-06-11 17:22:34',	'2024-06-11 17:22:34',	NULL);

-- 2024-06-11 11:53:28
