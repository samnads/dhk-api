-- Adminer 4.8.1 MySQL 11.3.2-MariaDB dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

SET NAMES utf8mb4;

DROP TABLE IF EXISTS `building_types`;
CREATE TABLE `building_types` (
  `id` bigint(20) NOT NULL AUTO_INCREMENT,
  `service_type_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `customer_app_banner` varchar(255) DEFAULT NULL,
  `sort_order` int(11) NOT NULL DEFAULT 1,
  `created_at` datetime DEFAULT NULL,
  `updated_at` datetime DEFAULT NULL,
  `deleted_at` datetime DEFAULT NULL,
  PRIMARY KEY (`id`),
  UNIQUE KEY `customer_app_banner` (`customer_app_banner`),
  UNIQUE KEY `service_type_id_name` (`service_type_id`,`name`),
  CONSTRAINT `building_types_ibfk_1` FOREIGN KEY (`service_type_id`) REFERENCES `service_types` (`service_type_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_general_ci;

INSERT INTO `building_types` (`id`, `service_type_id`, `name`, `customer_app_banner`, `sort_order`, `created_at`, `updated_at`, `deleted_at`) VALUES
(1,	53,	'Apartment',	'53-Apartment.jpg',	1,	'2024-06-14 10:58:56',	'2024-06-14 10:58:56',	NULL),
(2,	53,	'Villa',	'53-Villa.jpg',	2,	'2024-06-14 10:58:56',	'2024-06-14 10:58:56',	NULL),
(3,	55,	'Apartment',	'55-Apartment.jpg',	1,	'2024-06-14 10:58:56',	'2024-06-14 10:58:56',	NULL),
(4,	55,	'Villa',	'55-Villa.jpg',	2,	'2024-06-14 10:58:56',	'2024-06-14 10:58:56',	NULL),
(5,	54,	'Apartment',	'54-Apartment.jpg',	1,	'2024-06-14 10:58:56',	'2024-06-14 10:58:56',	NULL),
(6,	54,	'Villa',	'54-Villa.jpg',	2,	'2024-06-14 10:58:56',	'2024-06-14 10:58:56',	NULL),
(7,	56,	'Apartment',	'56-Apartment.jpg',	1,	'2024-06-14 10:58:56',	'2024-06-14 10:58:56',	NULL),
(8,	56,	'Villa',	'56-Villa.jpg',	2,	'2024-06-14 10:58:56',	'2024-06-14 10:58:56',	NULL),
(9,	62,	'Apartment',	'62-Apartment.jpg',	1,	'2024-06-14 10:58:56',	'2024-06-14 10:58:56',	NULL),
(10,	62,	'Villa',	'62-Villa.jpg',	2,	'2024-06-14 10:58:56',	'2024-06-14 10:58:56',	NULL),
(11,	61,	'Apartment',	'61-Apartment.jpg',	1,	'2024-06-14 10:58:56',	'2024-06-14 10:58:56',	NULL),
(12,	61,	'Villa',	'61-Villa.jpg',	2,	'2024-06-14 10:58:56',	'2024-06-14 10:58:56',	NULL),
(13,	60,	'Apartment',	'60-Apartment.jpg',	1,	'2024-06-14 10:58:56',	'2024-06-14 10:58:56',	NULL),
(14,	60,	'Villa',	'60-Villa.jpg',	2,	'2024-06-14 10:58:56',	'2024-06-14 10:58:56',	NULL),
(15,	57,	'Sofa',	'57-Sofa.jpg',	1,	'2024-06-14 10:58:56',	'2024-06-14 10:58:56',	NULL),
(17,	59,	'Carpet',	'59-Carpet.jpg',	1,	'2024-06-14 10:58:56',	'2024-06-14 10:58:56',	NULL),
(18,	58,	'Single',	'58-Single.jpg',	1,	'2024-06-14 10:58:56',	'2024-06-14 10:58:56',	NULL),
(19,	58,	'Queen',	'58-Queen.jpg',	2,	'2024-06-14 10:58:56',	'2024-06-14 10:58:56',	NULL),
(20,	58,	'King',	'58-King.jpg',	3,	'2024-06-14 10:58:56',	'2024-06-14 10:58:56',	NULL);

-- 2024-06-15 06:54:09
