@extends('emails.layouts.main')
@section('content')
    @include('emails.includes.section-message')
    @include('emails.includes.section-booking-address')
    @include('emails.includes.booking-summary-for-customer')
    @include('emails.includes.section-terms-conditions')
    @include('emails.includes.section-footer-address')
@endsection
