<html>

<head>
    <title>Privacy Policy</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-T3c6CoIi6uLrA9TneNEoa7RxnatzjcDSCmG1MXxSR1GAsXEV/Dwwykc2MPK8M2HN" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.2/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-C6RzsynM9kWDrMNeT87bh95OGNyZPhcTNXj1NW7RuBCsyN/o0jlpcV8Qyq46cDfL" crossorigin="anonymous">
    </script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.4.0/css/all.min.css"
        integrity="sha512-iecdLmaskl7CVkqkXNQ/ZH/XLlvWZOJyj7Yy7tcenmpD1ypASozpmT/E0iPtmFIB46ZmdtAc9eNBvH0H/ZpiBw=="
        crossorigin="anonymous" referrerpolicy="no-referrer" />
    <script src="https://code.jquery.com/jquery-3.7.1.min.js"
        integrity="sha256-/JqT3SQfawRcv/BIHPThkBvs0OEvtFFmqPF/lYI/Cxo=" crossorigin="anonymous"></script>
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
</head>

<body>
    <style>
        #button {
            display: inline-block;
            background-color: #1ea242 ;
            width: 50px;
            height: 50px;
            text-align: center;
            border-radius: 4px;
            position: fixed;
            bottom: 30px;
            right: 30px;
            transition: background-color .3s,
                opacity .5s, visibility .5s;
            opacity: 0;
            visibility: hidden;
            z-index: 1000;
        }

        #button::after {
            content: "\f077";
            font-family: FontAwesome;
            font-weight: normal;
            font-style: normal;
            font-size: 2em;
            line-height: 50px;
            color: #fff;
        }

        #button:hover {
            cursor: pointer;
            background-color: #333;
        }

        #button:active {
            background-color: #555;
        }

        #button.show {
            opacity: 1;
            visibility: visible;
        }
    </style>
    <a id="button"></a>
    <div class="container">
        <p>We view protection of your privacy as a very important community principle. We understand clearly that you
            and Your Information is one of our most important assets. We store and process Your Information on our
            servers that are protected by physical as well as technological security devices. If you object to Your
            Information being transferred or used in any way please forward your request to legal@dubaihousekeeping.com
            or deem not to use the website. We do not sell or rent Your Information to any third parties without your
            explicit consent.</p>
        <p>This policy document sets out the basic tenets of our privacy policy applicable to customer data. All
            questions or comments should be addressed to our Management at legal@dubaihousekeeping.com.</p>
        <p>Who are we? In this policy (“Privacy Policy”) the terms (“Our”, “We” and “Us”) refers to Grace Quest Cleaning
            Services, (Dubai Housekeeping™), whose registered office is at 4201B, ASPiN Commercial Tower, Shk Zayed
            Road, Dubai, UAE P.O. Box 263887.</p>
        <p>We are engaged in the business of Residential Property Care Services, Building Cleaning Service, Yacht
            Cleaning Services, and Disinfection & Sterilization Services, in Dubai, UAE. The policy sets forth the terms
            with respect to the way we collect, store, process, share and disclose personal information about you (“you”
            and “your”) which is disclosed to us either directly or through our website at: </p>
        <p><a href="https://www.dubaihousekeeping.com">https://www.dubaihousekeeping.com</a></p>
        <ul>
            <li>
                <h5>Your Account Obligations</h5>
                <p>This Privacy Policy provides the manner your data is collected and used by The Grace Quest Cleaning
                    Services, AKA Dubai Housekeeping. You are advised to read this Privacy Policy carefully. By
                    accessing the services provided by Dubai Housekeeping you agree to the collection and use of your
                    data by Dubai Housekeeping in the manner provided in this Privacy Policy. If you do not agree with
                    this Privacy Policy, please do not use the website – Dubaihousekeeping.com. By accepting the Terms &
                    Conditions during registration, you expressly consent to our use and disclosure of your personal
                    information in accordance with this Privacy Policy. This Privacy Policy is incorporated into and
                    subject to the terms of the User Agreement.</p>
            </li>
            <li>
                <h5>Your Privacy Protection with Dubai Housekeeping</h5>
                <p>Dubai Housekeeping values your trust in us. We want you to feel comfortable using our site and feel
                    secure to share your information with us, thus we are extremely proud of our commitment to protect
                    your privacy. Please continue reading the following policy to understand how your personal
                    information will be treated as you make full use of our Site.</p>
            </li>
            <li>
                <h5>Your Privacy Guarantee with Dubai Housekeeping</h5>
                <p>The Dubai Housekeeping promises not to sell or rent your personal information to third parties
                    (except as provided in this Privacy Policy) without your consent. We and/or certain authorized
                    third-party service providers will use the personal information provided by you to provide you with
                    the services, which we offer.</p>
            </li>
            <li>
                <h5>Third-Party Service Providers</h5>
                <p>We employ other companies and individuals to perform functions on our behalf. Examples include
                    providing services, providing marketing assistance, providing search results, processing credit card
                    payments. They have access to personal information needed to perform their functions, but may not
                    use it for other purposes.</p>
            </li>
            <li>
                <h5>Information May be Collected from You</h5>
                <p>Dubai Housekeeping collects the details provided by you on registration together with information we
                    learn about you from your use of our service and your visits to our Site. We also collect
                    information about the transactions you make including details of payment cards used. We may collect
                    additional information in connection with your participation in any promotions or competitions
                    offered by us and information you provide when giving us feedback or completing profile forms. We
                    also monitor customer traffic patterns and Site use, which enables us to improve the service we
                    provide. We will collect only such information as is necessary and relevant to us to provide you
                    with the services available on the Site.</p>
                <p>We may collect the following personally identifiable information about you like:</p>
                <ul class="list-unstyled">
                    <li><i class="fa fa-check text-success p-2" aria-hidden="true"></i>Name including first and last
                        name</li>
                    <li><i class="fa fa-check text-success p-2" aria-hidden="true"></i>Mobile phone number and contact
                        details</li>
                    <li><i class="fa fa-check text-success p-2" aria-hidden="true"></i>Demographic profile (like
                        address)</li>
                    <li><i class="fa fa-check text-success p-2" aria-hidden="true"></i>Email Address</li>
                </ul>
            </li>
            <li>
                <h5>Your information may remain stored in an archive on our servers even after the deletion or the
                    termination of your account Credit Card Details</h5>
                <p>We do not keep any credit card details in our database or server. It is important for you to protect
                    against unauthorized access to your password and to your computer. Be sure to sign off when finished
                    using a shared computer.</p>
            </li>
            <li>
                <h5>Our Use of Your Information</h5>
                <p>We use your personal & contact information to:</p>
                <ul class="list-unstyled">
                    <li><i class="fa fa-check text-success p-2" aria-hidden="true"></i>Get in touch with you whenever
                        necessary</li>
                    <li><i class="fa fa-check text-success p-2" aria-hidden="true"></i>Supply the orders/ services
                        requested by you</li>
                    <li><i class="fa fa-check text-success p-2" aria-hidden="true"></i>Preserve social history as
                        governed by existing law or policy</li>
                    <li><i class="fa fa-check text-success p-2" aria-hidden="true"></i>Contact you as a survey
                        respondent</li>
                    <li><i class="fa fa-check text-success p-2" aria-hidden="true"></i>Notify you if you win any contest
                    </li>
                    <li><i class="fa fa-check text-success p-2" aria-hidden="true"></i>Send you promotional materials
                        from our contest sponsors or advertisers</li>
                </ul>
            </li>
            <li>
                <h5>Who’s allowed to use Dubai Housekeeping?</h5>
                <p>Dubai Housekeeping does not sell products for purchase by children. If you are under 18, you may
                    browse the Dubai Housekeeping website but you cannot purchase any product without the involvement of
                    a parent or guardian.</p>
            </li>
            <li>
                <h5>Information that You Can Access</h5>
                <p>Dubai Housekeeping gives you access to a range of information about your account and your
                    interactions with Dubai Housekeeping for the purpose of viewing, updating, and editing that
                    information.</p>
            </li>
            <li>
                <h5>Our Disclosure of Your Information</h5>
                <p>We will not use your personal information for any purpose other than to complete a transaction with
                    you. We do not rent, sell or share your personal information and we will not disclose any of your
                    personally identifiable information to third parties unless:</p>
                <ul class="list-unstyled">
                    <li><i class="fa fa-check text-success p-2" aria-hidden="true"></i>We have your permission</li>
                    <li><i class="fa fa-check text-success p-2" aria-hidden="true"></i>To provide products or services
                        you’ve requested</li>
                    <li><i class="fa fa-check text-success p-2" aria-hidden="true"></i>To help investigate, prevent or
                        take action regarding unlawful and illegal activities, suspected fraud, potential threat to the
                        safety or security of any person, violations of Dubai Housekeeping terms of use or to defend
                        against legal claims</li>
                    <li><i class="fa fa-check text-success p-2" aria-hidden="true"></i>Special circumstances such as
                        compliance with subpoenas, court orders, requests/order from legal authorities or law
                        enforcement agencies requiring such disclosure</li>

                </ul>
            </li>
            <li>
                <h5>What Choices are Available to You Regarding Collection, Use, and Distribution of Your Information?
                </h5>
                <p>Supplying personally identifiable information is entirely voluntary. You are not required to register
                    with us in order to browse our Site.</p>
                <p>You may change your interests at any time and may opt-in or opt-out of any marketing / promotional /
                    newsletters mailings by sending an email to:<br>office@dubaihousekeeping.com</p>
                <p>Dubai Housekeeping reserves the right to send you certain service-related communication, considered
                    to be a part of your Dubai Housekeeping account without offering you the facility to opt-out. You
                    may update your information and change your account settings at any time.</p>
                <p>Upon request, we will remove/block your personally identifiable information from our database,
                    thereby canceling your registration. However, your information may remain stored in an archive on
                    our servers even after the deletion or the termination of your account.</p>
                <p>If we plan to use your personally identifiable information for any commercial purposes, we will
                    notify you at the time we collect that information and allow you to opt-out of having your
                    information used for those purposes.</p>
            </li>
            <li>
                <h5>Security</h5>
                <p>To protect against the loss, misuse, and alteration of the information under our control, we have in
                    place appropriate physical, electronic, and managerial procedures. For example, our servers are
                    accessible only to authorized personnel, and that your information is shared with respective
                    personnel on a need to know basis to complete the transaction and to provide the services requested
                    by you.</p>
                <p>Although we will endeavor to safeguard the confidentiality of your personally identifiable
                    information, transmissions made by means of the Internet cannot be made absolutely secure. By using
                    this Site, you agree that we will have no liability for disclosure of your information due to errors
                    in transmission or unauthorized acts of third parties.</p>
            </li>
            <li>
                <h5>Access or Change Your Personally Identifiable Information</h5>
                <p>To protect your privacy and security, we will verify your identity before granting access or making
                    changes to your personally-identifying information. If you have registered your profile on Dubai
                    Housekeeping, your Dubai Housekeeping user name and password are required in order to access your
                    profile information. We reserve the right to change or update our policies at any time. Such changes
                    shall be effective immediately upon posting to this Site.</p>
            </li>

        </ul>
    </div>
    <script>
        var btn = $('#button');
        $(window).scroll(function() {
            if ($(window).scrollTop() > 300) {
                btn.addClass('show');
            } else {
                btn.removeClass('show');
            }
        });

        btn.on('click', function(e) {
            e.preventDefault();
            $('html, body').animate({
                scrollTop: 0
            }, '300');
        });
    </script>
</body>

</html>
