<?php

namespace App\Http\Controllers;

use Config;
use Illuminate\Http\Request;
use App\Models\Customer;
use App\Models\CustomerAddress;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use App\Models\Booking;
use App\Models\OnlinePayment;
use Response;
use App\Models\Area;
use Redirect;
use stdClass;

class CustomerApiInvoiceController extends Controller
{
    public function invoice_payment(Request $request)
    {
        try {
            $debug = toggleDebug(); // pass boolean to overide default
            $data = json_decode($request->getContent(), true);
            $input = @$data['params'];
            /************************************************************* */
            $validator = Validator::make(
                (array) $input,
                [
                    'payment_method' => 'required|integer',
                    'customer_id' => 'required|integer',
                    'order_id' => 'required|string',
                    'amount' => 'required',
                    'platform' => 'required|in:web',
                ],
                [],
                [
                    'payment_method' => 'Payment Mode',
                    'customer_id' => 'Customer ID',
                    'order_id' => 'Order ID',
                    'amount' => 'Amount',
                    'platform' => 'Platform',
                ]
            );
            if ($validator->fails()) {
                return Response::json(array('result' => array('status' => 'failed', 'message' => $validator->errors()->first()), 'debug' => $debug), 200, array(), customerResponseJsonConstants());
            }
            $response['status'] = 'success';
            /************************************************************* */
            if ($input['payment_method'] == 2) {
                /******************************************************************************
                 *
                 * 
                 * Request contains checkout card token data
                 * 
                 * 
                 **************************************************************************** */
                if (@$input['checkout_token_data']) {
                    // have checkout token data in request
                    $response['checkout_token_data'] = $input['checkout_token_data'];
                    $checkout_data = $this->checkoutPaymentForInvoice($input['checkout_token_data'], $input);
                    $response['checkout_data'] = $checkout_data;
                    if (strtolower(@$checkout_data['status']) == "pending") {
                        // have redirect link to enter OTP
                    } else if (@$checkout_data['approved'] == true) {
                        // payment success
                        //afterCheckoutPaymentSuccess($bookings[0]->booking_id, $checkout_data, @$input['platform']);
                    } else {
                        throw new \ErrorException('Card payment not successful.');
                    }
                } else {
                    throw new \ErrorException('No token data.');
                }
                /****************************************************************************** */
            } else if ($input['payment_method'] == 6) {
                $response['telr_data'] = telrCreateOrderForInvoice($input['customer_id'], $input['order_id'], $input['amount'], @$input['platform']);
            } else if ($input['payment_method'] == 7) {
                $response['telr_data'] = telrCreateOrderApplePayForInvoice($input['customer_id'], $input['order_id'], $input['amount'], @$input['platform']);
            }
            $response['input'] = $input;
            return Response::json(array('result' => $response, 'debug' => $debug), 200, array(), JSON_PRETTY_PRINT);
        } catch (\Exception $e) {
            DB::rollback();
            return Response::json(array('result' => array('status' => 'failed', 'message' => $e->getMessage()), 'debug' => $debug), 200, array(), customerResponseJsonConstants());
        }
    }
    public function online_payment(Request $request)
    {
        try {
            $debug = toggleDebug(); // pass boolean to overide default
            $data = json_decode($request->getContent(), true);
            $input = @$data['params'];
            /************************************************************* */
            $validator = Validator::make(
                (array) $input,
                [
                    'payment_method' => 'required|integer',
                    'customer_id' => 'required|integer',
                    'order_id' => 'required|string',
                    'amount' => 'required',
                    'platform' => 'required|in:web',
                ],
                [],
                [
                    'payment_method' => 'Payment Mode',
                    'customer_id' => 'Customer ID',
                    'order_id' => 'Order ID',
                    'amount' => 'Amount',
                    'platform' => 'Platform',
                ]
            );
            if ($validator->fails()) {
                return Response::json(array('result' => array('status' => 'failed', 'message' => $validator->errors()->first()), 'debug' => $debug), 200, array(), customerResponseJsonConstants());
            }
            $response['status'] = 'success';
            /************************************************************* */
            $customer = Customer::where('customer_id', '=', $input['customer_id'])->first();
            $default_address = CustomerAddress::where('customer_id', $input['customer_id'])->where('address_status', 0)->where('default_address', 1)->first(); // get default address
            if ($default_address) {
                $customer_address = $default_address;
            } else {
                $customer_address = CustomerAddress::where(['customer_id' => $input['customer_id']])->where(['address_status' => 0])->first();
            }
            if (!$customer_address) {
                throw new \ErrorException('No customer address found !');
            }
            if ($input['payment_method'] == 2) {
                /**
                 * Billing details
                 */
                $response['billing'] = [
                    'order_id' => $input['order_id'],
                    'amount' => $input['amount'],
                    'name' => ccavenueRemoveSpecialChars($customer->customer_name),
                    'address' => ccavenueRemoveSpecialChars(@$customer_address->customer_address),
                    'city' => 'United Arab Emirates',
                    'state' => null,
                    'zip' => null,
                    'country' => 'United Arab Emirates',
                    'tel' => $customer->mobile_number_1,
                    'email' => strtolower(@$customer->email_address),
                ];
            } else if ($input['payment_method'] == 6) {
                $response['telr_data'] = telrCreateOrderForOnlinePayment($input['customer_id'], $input['order_id'], $input['amount'], @$input['platform']);
            } else if ($input['payment_method'] == 7) {
                $response['telr_data'] = telrCreateOrderApplePayForOnlinePayment($input['customer_id'], $input['order_id'], $input['amount'], @$input['platform']);
            }
            $response['input'] = $input;
            return Response::json(array('result' => $response, 'debug' => $debug), 200, array(), JSON_PRETTY_PRINT);
        } catch (\Exception $e) {
            DB::rollback();
            return Response::json(array('result' => array('status' => 'failed', 'message' => $e->getMessage()), 'debug' => $debug), 200, array(), customerResponseJsonConstants());
        }
    }
    function checkoutPaymentForInvoice($checkout_data, $input)
    {
        $customer = Customer::where('customer_id', $input['customer_id'])->first();
        $success_url = url('api/customer/invoice-payment/checkout/process-and-redirect/' . $input['order_id']); // for 3Ds only
        $failure_url = url('api/customer/invoice-payment/checkout/process-and-redirect/' . $input['order_id']); // for 3Ds only
        try {
            $client = new \GuzzleHttp\Client([
                'verify' => false,
            ]);
            /**************************************************************** */
            $post_header = ["Authorization" => "Bearer " . Config::get('values.checkout_secret_key'), "Content-Type" => "application/json", "Accept" => "application/json"];
            $checkout_data = [
                'source' => ["type" => "token", "token" => $checkout_data['token']],
                'customer' => ["email" => @$customer->email_address, "name" => @$customer->customer_name],
                '3ds' => ["enabled" => true, "attempt_n3d" => true],
                'amount' => ((float) $input['amount']) * 100,
                //'amount' => ((float) 1) * 100, // TEST PURPOSE ONLY
                'currency' => Config::get('values.currency_code'),
                'processing_channel_id' => Config::get('values.checkout_channel_id'),
                "success_url" => $success_url,
                "failure_url" => $failure_url,
                'reference' => $input['order_id'],
                'metadata' => ['coupon_code' => 'test']
            ];
            /**************************************************************** */
            $response_payment_data = $client->request('POST', Config::get('values.checkout_service_url'), [
                'headers' => $post_header,
                'http_errors' => false, // no exceptin fix
                'json' => $checkout_data,
            ]);
            return json_decode((string) $response_payment_data->getBody(), true);
        } catch (\Exception $e) {
            throw new \ErrorException('Processing payment failed.' . $e->getMessage());
        }
    }
    public function checkoutInvoicePaymentProcessAndRedirect(Request $request)
    {
        // this (route) should be called from checkout.com
        $segments = $request->segments();
        $booking_common_id = $segments[5]; //order_id
        $booking = Booking::where('booking_id', $booking_common_id)->first();
        try {
            $bookings = DB::table('bookings as b')
                ->select(
                    'b.*',
                )
                ->where(function ($query) use ($booking_common_id) {
                    $query->where('b.booking_common_id', $booking_common_id);
                    $query->orWhere('b.booking_id', $booking_common_id);
                })
                ->orderBy('b.booking_id', 'ASC')
                ->get();
            $bookings_ids = array_column($bookings->toArray(), 'booking_id');
            $post_header = array("Authorization" => "Bearer " . Config::get('values.checkout_secret_key'), "Content-Type" => "application/json", "Accept" => "application/json");
            $client = new \GuzzleHttp\Client([
                'verify' => false,
            ]);
            $responseFull = $client->request('GET', Config::get('values.checkout_service_url') . $request['cko-session-id'], [
                'headers' => $post_header,
            ]);
            $checkout_data = json_decode((string) $responseFull->getBody(), true);
            //dd($checkout_data);
            if (@$checkout_data['approved'] == true) {
                // payment success
                afterCheckoutPaymentSuccess($booking_common_id, $checkout_data, null);
                return Redirect::to(Config::get('values.web_app_url') . 'booking/success/' . $booking->reference_id);
            } else {
                // payment failed
                return Redirect::to(Config::get('values.web_app_url') . 'booking/failed/' . $booking->reference_id);
            }
        } catch (\Exception $e) {
            // payment failed
            return Redirect::to(Config::get('values.web_app_url') . 'booking/failed/' . $booking->reference_id . '?error=' . $e->getMessage());
        }
    }
    public function saveInvoicePay(Request $request)
    {

        $customer_id = $request['customerId'];
        $description = $request['description'];
        $inv_id = $request['inv_id'];
        $amount = $request['amount'];
        $data = array();
        if ($customer_id != "") {
            $data_post = json_encode($_POST);
            $transaction_charge = 0;
            $onlinePayment = new OnlinePayment();
            $onlinePayment->description = $description;
            $onlinePayment->transaction_charge = $transaction_charge;
            $onlinePayment->amount = $amount;
            $onlinePayment->customer_id = $customer_id;
            $onlinePayment->paid_from = 'O';
            $onlinePayment->payment_datetime = date('Y-m-d H:i:s');
            $onlinePayment->user_agent = $_SERVER['HTTP_USER_AGENT'];
            $onlinePayment->post_data = $data_post;
            $onlinePayment->ip_address = '';
            $onlinePayment->payment_status = 'initiated';
            $onlinePayment->inv_id = $inv_id;
            $onlinePayment->save();
            $customer = Customer::find($customer_id);
            $customer_address_id = $customer->customer_address_id;
            $customerAddress = CustomerAddress::where('customer_id', $customer_id)->where('default_address', 1)->where('address_status', 0)->first();
            if ($customerAddress) {
                $address = $customerAddress->customer_address;
                $area = Area::find($customerAddress->area_id);
                $areaName = $area->area_name;
            } else {
                $address = '';
                $areaName = 'NA';
            }
            $customer_name = $customer->customer_name;
            $customer_mobile = $customer->mobile_number_1;
            $email_id = $customer->email_address;
            $data['customer_name'] = $customer_name;
            $data['customer_email'] = $email_id;
            $data['description'] = $description;
            $data['amount'] = $amount;
            $data['transaction_charge'] = $transaction_charge;
            $data['gross_amount'] = number_format((float) ($amount + $transaction_charge), 2, '.', '');
            $data['phone_number'] = $customer_mobile;
            $data['address'] = $address;
            $data['area'] = $areaName;
            $data['pay_details'] = $onlinePayment;
            $data['reference_id'] =  $onlinePayment->reference_id;
            $data['inv_id'] = $inv_id;
            $data['payment_id'] = $onlinePayment->payment_id;
            return Response::json([
                'status' => 'success',
                'messages' => 'Payment details added.',
                'data' => $data,
            ], 200, []);
        } else {
            return Response::json([
                'status' => 'failed',
                'messages' => 'Customer not found.',
                'data' => null,
            ], 200, []);
        }
    }
    public function saveOnlinePay(Request $request)
    {

        $customer_id = trim($request['customerId']);
        $description = trim($request['description']);
        $inv_id = null;
        $amount = $request['amount'];
        $data = array();
        if ($customer_id != "") {
            $data_post = json_encode($_POST);
            /*********************************** */
            $input['payment_method'] = 2;
            $payment_type = DB::table('payment_types as pt')
                ->select(
                    'pt.*',
                )
                ->where(['pt.id' => $input['payment_method']])
                ->first();
            if ($payment_type->charge_type == "F") {
                // fixed charge
                $data['payment_type_charge'] = $payment_type->charge;
            } else {
                // percentage of total
                $data['payment_type_charge'] = ($payment_type->charge / 100) * $amount;
            }
            /*********************************** */
            $transaction_charge = $data['payment_type_charge'];
            $onlinePayment = new OnlinePayment();
            $onlinePayment->description = $description;
            $onlinePayment->transaction_charge = $transaction_charge;
            $onlinePayment->amount = $amount;
            $onlinePayment->customer_id = $customer_id;
            $onlinePayment->paid_from = 'O';
            $onlinePayment->payment_datetime = date('Y-m-d H:i:s');
            $onlinePayment->user_agent = $_SERVER['HTTP_USER_AGENT'];
            $onlinePayment->post_data = $data_post;
            $onlinePayment->ip_address = '';
            $onlinePayment->payment_status = 'initiated';
            $onlinePayment->inv_id = $inv_id;
            $onlinePayment->save();
            $onlinePayment->reference_id = 'DHK-ON-' . date('Y') . '-' . $onlinePayment->payment_id;
            $onlinePayment->save();
            $customer = Customer::find($customer_id);
            $customer_address_id = $customer->customer_address_id;
            $customerAddress = CustomerAddress::where('customer_id', $customer_id)->where('default_address', 1)->where('address_status', 0)->first();
            if ($customerAddress) {
                $address = $customerAddress->customer_address;
                $area = Area::find($customerAddress->area_id);
                $areaName = $area->area_name;
            } else {
                $address = '';
                $areaName = 'NA';
            }
            $customer_name = $customer->customer_name;
            $customer_mobile = $customer->mobile_number_1;
            $email_id = $customer->email_address;
            $data['customer_name'] = $customer_name;
            $data['customer_email'] = $email_id;
            $data['description'] = $description;
            $data['amount'] = $amount;
            $data['transaction_charge'] = $transaction_charge;
            $data['gross_amount'] = number_format((float) ($amount + $transaction_charge), 2, '.', '');
            $data['phone_number'] = $customer_mobile;
            $data['address'] = $address;
            $data['area'] = $areaName;
            $data['pay_details'] = $onlinePayment;
            $data['reference_id'] = '';
            $data['inv_id'] = $inv_id;
            $data['payment_id'] = $onlinePayment->payment_id;
            return Response::json([
                'status' => 'success',
                'messages' => 'Payment details added.',
                'data' => $data,
            ], 200, []);
        } else {
            return Response::json([
                'status' => 'failed',
                'messages' => 'Customer not found.',
                'data' => null,
            ], 200, []);
        }
    }
    public function online_payment_by_id(Request $request)
    {
        $data = json_decode($request->getContent(), true);
        $input = @$data['params'];
        $response['online_payment'] = OnlinePayment::find($input['payment_id']);
        $response['customer'] = Customer::find($response['online_payment']['customer_id']);
        $response['customer_address'] = CustomerAddress::
            select(
                'customer_addresses.*',
                'a.area_name',
            )
            ->leftJoin('areas as a', 'customer_addresses.area_id', 'a.area_id')
            ->where('customer_id', '=', $response['customer']['customer_id'])
            ->where('default_address', 1)
            ->first();
        $response['status'] = 'success';
        $response['message'] = 'Payment data fetched successfully.';
        return Response::json(array('result' => $response), 200, [], customerResponseJsonConstants());
    }
    public function online_payment_by_ref(Request $request)
    {
        $data = json_decode($request->getContent(), true);
        $input = @$data['params'];
        $response['online_payment'] = OnlinePayment::where('reference_id', $input['reference_id'])->first();
        $response['customer'] = Customer::find($response['online_payment']['customer_id']);
        $response['customer_address'] = CustomerAddress::
            select(
                'customer_addresses.*',
                'a.area_name',
            )
            ->leftJoin('areas as a', 'customer_addresses.area_id', 'a.area_id')
            ->where('customer_id', '=', $response['customer']['customer_id'])
            ->where('default_address', 1)
            ->first();
        $response['status'] = 'success';
        $response['message'] = 'Payment data fetched successfully.';
        return Response::json(array('result' => $response), 200, [], customerResponseJsonConstants());
    }
}
