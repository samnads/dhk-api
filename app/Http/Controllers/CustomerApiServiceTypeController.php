<?php

namespace App\Http\Controllers;

use App\Models\CrewInOption;
use App\Models\ExtraService;
use App\Models\RecurringMonth;
use App\Models\RecurringPeriod;
use App\Models\ServiceCleaningSupply;
use App\Models\ServiceSupervisorCharge;
use App\Models\ServiceSupervisorMaidCount;
use App\Models\ServiceTypeDetailAnswer;
use App\Models\ServiceTypeDetailQuestion;
use App\Models\ServiceTypeFrequency;
use App\Models\ServiceTypeSetting;
use App\Models\ServiceWhatToExpectPoint;
use App\Models\WeekDay;
use Cache;
use Config;
use App\Models\Settings;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Response;

class CustomerApiServiceTypeController extends Controller
{
    public function service_type_data(Request $request)
    {
        $settings = Settings::first();
        /************************************************************* */
        $data = $request->getContent();
        $data = json_decode($data, true);
        $input = @$data['params'];
        /************************************************************* */
        /*$service_type_rating = DB::table('day_services as ds')
        ->select(
        'st.service_type_id',
        DB::raw('COUNT(ds.rating) as rating_count'),
        DB::raw('SUM(ds.rating) as total_rating')
        )
        ->leftJoin('bookings as b', 'ds.booking_id', 'b.booking_id')
        ->leftJoin('service_types as st', 'b.service_type_id', 'st.service_type_id')
        ->where([['b.booking_status', '=', 1], ['ds.service_status', '=', 2], ['ds.rating', '>', 0], ['ds.rating', '<=', 5], ['st.customer_app_status', '=', 1]])
        ->groupBy('st.service_type_id');*/
        /************************************************************* */
        $response['data'] = DB::table('service_types as st')
            ->select(
                'st.service_type_id',
                'st.service_type_model_id',
                'stm.model as service_type_model',
                DB::raw('IFNULL(st.customer_app_service_type_name,st.service_type_name) as service_type_name'),
                'st.info_html',
                'st.min_no_of_maids as min_no_of_professionals',
                'st.max_no_of_maids as max_no_of_professionals',
                'st.min_no_of_hours as min_working_hours',
                'st.max_no_of_hours as max_working_hours',
                'st.default_no_of_hours'
                //DB::raw('round((tr.total_rating/tr.rating_count),1) as rating'),
            )
            /*->leftJoinSub($service_type_rating, 'tr', function ($join) {
            $join->on('st.service_type_id', '=', 'tr.service_type_id');
            })*/
            ->leftJoin('service_type_models as stm', 'st.service_type_model_id', 'stm.id')
            ->where([['service_type_id', '=', $input['service_type_id']]])
            ->where([['customer_app_status', '=', 1]])
            ->orderBy('st.customer_app_order_id', 'ASC')
            ->first();
        /******************************************************** */
        $service_addons = DB::table('service_addons as sao')
            ->select(
                'sao.service_addons_id',
                'sao.service_addon_name',
                'sao.service_addon_description',
                DB::raw('"' . Config::get('values.currency_code') . '" as currency'),
                'sao.service_minutes as additional_minutes',
                'sao.strike_amount',
                'sao.amount',
                'sao.populariry',
                'sao.cart_limit',
                'sao.customer_app_thumbnail as image',
                'sao.service_type_id',
                DB::raw('CONCAT("' . Config::get('values.customer_app_assets_prefix_url') . 'service-addon-images/",IFNULL(sao.customer_app_thumbnail,"service-addons-thumbnail.png"),"?v=' . $settings->customer_app_img_version . '") as image_url'),
            )
            ->where([['sao.deleted_at', "=", null]])
            ->orderBy('sao.populariry', 'DESC')
            ->get()->toArray();
        /******************************************************** */
        $service_addon_points_all = DB::table('service_addons_points as sap')
            ->select(
                'sao.service_type_id',
                'sap.service_addons_id',
                'sap.service_addons_id',
                'sap.point_html',
                'sap.point_type'
            )
            ->leftJoin('service_addons as sao', 'sap.service_addons_id', 'sao.service_addons_id')
            ->where([['sap.deleted_at', "=", null]])
            ->orderBy('sap.sort_order_id', 'ASC')
            ->get()->toArray();
        /******************************************************** */
        // filter addons by service id
        $response['data']->addons = array_filter($service_addons, function ($addon) use ($input) {
            return $addon->service_type_id == $input['service_type_id'];
        });
        $response['data']->addons = array_values($response['data']->addons); // key showing fix
        /******************************************************** */
        foreach ($response['data']->addons as $addon_key => $addon) {
            /******************************************************** */
            // filter points by service id
            $service_addon_points = array_filter($service_addon_points_all, function ($point) use ($input) {
                return $point->service_type_id == $input['service_type_id'];
            });
            /******************************************************** */
            // filter points by type
            $what_included_points = array_filter($service_addon_points, function ($point) {
                return $point->point_type === "WHAT_INCLUDED";
            });
            /******************************************************** */
            // filter points by type
            $what_excluded_points = array_filter($service_addon_points, function ($point) {
                return $point->point_type === "WHAT_EXCLUDED";
            });
            /******************************************************** */
            $response['data']->addons[$addon_key]->what_included = array_column(array_filter($what_included_points, function ($point) use ($addon) {
                // filter points by addon
                return $point->service_addons_id == $addon->service_addons_id;
            }), 'point_html');
            /******************************************************** */
            $response['data']->addons[$addon_key]->what_excluded = array_column(array_filter($what_excluded_points, function ($point) use ($addon) {
                // filter points by addon
                return $point->service_addons_id == $addon->service_addons_id;
            }), 'point_html');
        }
        /**
         * Frequency data (service based)
         */
        $response['data']->frequency_list = ServiceTypeFrequency::
            select(
                'f.id',
                'f.name',
                'f.description',
                'f.code',
                'service_type_frequencies.coupon_id',
                'cc.coupon_name as coupon_code',
                'cc.percentage as percentage',
            )
            ->leftJoin('frequencies as f', 'service_type_frequencies.frequency_id', 'f.id')
            ->leftJoin('coupon_code as cc', 'f.coupon_id', 'cc.coupon_id')
            ->where('service_type_frequencies.service_type_id', $input['service_type_id'])
            ->orderBy('service_type_frequencies.sort_order', 'asc')
            ->get();
        /**
         * Supervisor data
         */
        $service_supervisor_charges = ServiceSupervisorCharge::select('service_supervisor_charges.no_of_hours', 'service_supervisor_charges.charge')->where('service_supervisor_charges.service_type_id', $input['service_type_id'])->get();
        $service_supervisor_maid_counts = ServiceSupervisorMaidCount::select('service_supervisor_maid_counts.no_of_maids', )->where('service_supervisor_maid_counts.service_type_id', $input['service_type_id'])->get();
        $response['data']->supervisor['hours_match'] = array_column($service_supervisor_charges->toArray(), null, 'no_of_hours');
        $response['data']->supervisor['maids_match'] = array_column($service_supervisor_maid_counts->toArray(), null, 'no_of_maids');
        /**
         * Extra services data
         */
        $response['data']->extra_services = ExtraService::select('extra_services.id', 'extra_services.service_type_id', 'extra_services.service', 'extra_services.duration', 'extra_services.cost', DB::raw('CONCAT("' . Config::get('values.customer_app_assets_prefix_url') . 'extra-services/",extra_services.image_url) as image_url'), 'extra_services.show_on_min_hour', 'extra_services.show_on_max_hour')->where('extra_services.service_type_id', $input['service_type_id'])->where('extra_services.status', 0)->get();
        /**
         * Cleaning supplies | YES
         */
        $response['data']->cleaning_materials['yes'] = ServiceCleaningSupply::
            select(
                'service_cleaning_supplies.id',
                'ccs.type',
                'ccs.name',
                'ccs.option_description_html',
                'service_cleaning_supplies.rate',
                'service_cleaning_supplies.calc_method'
            )
            ->leftJoin('config_cleaning_supplies as ccs', 'service_cleaning_supplies.cleaning_supply_id', 'ccs.id')
            ->where('service_cleaning_supplies.service_type_id', $input['service_type_id'])
            ->where('ccs.type', 'PlanBased')
            ->get();
        /**
         * Cleaning supplies | NO
         */
        $response['data']->cleaning_materials['no'] = ServiceCleaningSupply::
            select(
                'service_cleaning_supplies.id',
                'ccs.type',
                'ccs.name',
                'ccs.option_description_html',
                'service_cleaning_supplies.rate',
                'service_cleaning_supplies.calc_method'
            )
            ->leftJoin('config_cleaning_supplies as ccs', 'service_cleaning_supplies.cleaning_supply_id', 'ccs.id')
            ->where('service_cleaning_supplies.service_type_id', $input['service_type_id'])
            ->where('ccs.type', 'Custom')
            ->get();
        /**
         * Questions for feeding additional data
         */
        $response['data']->detail_questions = ServiceTypeDetailQuestion::
            select(
                'id',
                'service_type_id',
                'question',
                'description'
            )
            ->where('service_type_id', $input['service_type_id'])
            ->get();
        $service_type_detail_answers = ServiceTypeDetailAnswer::
            select(
                'service_type_detail_answers.id',
                'service_type_detail_answers.service_type_detail_question_id',
                'stdq.service_type_id',
                'value',
                'label'
            )
            ->leftJoin('service_type_detail_questions as stdq', 'service_type_detail_answers.service_type_detail_question_id', 'stdq.id')
            ->where('stdq.service_type_id', $input['service_type_id'])
            ->where('stdq.deleted_at', null)
            ->get()
            ->toArray();
        foreach ($response['data']->detail_questions as $key => &$question) {
            $question['answers'] = array_values(array_filter($service_type_detail_answers, function ($answer) use ($question) {
                // filter points by addon
                return $question['id'] == $answer['service_type_detail_question_id'];
            }));
        }
        /**
         * What to expect from the service points
         */
        $response['data']->what_to_expect = ServiceWhatToExpectPoint::select('id', 'point_html')
            ->where(function ($query) use ($input) {
                $query->where('service_type_id', $input['service_type_id']);
                $query->orWhereNull('service_type_id');
            })
            ->orderBy('order_id')
            ->get();
        /**
         * Crew in options
         */
        $response['data']->crew_in_options = CrewInOption::
            select(
                'crew_in_options.id',
                'crew_in_options.service_type_id',
                'crew_in_options.text',
                'crew_in_options.sort_order'
            )
            ->where(function ($query) use ($input) {
                $query->where('crew_in_options.service_type_id', null);
                $query->orWhere('crew_in_options.service_type_id', $input['service_type_id']);
            })
            ->orderBy('sort_order')
            ->get();
        /**
         * Settings
         */
        $response['data']->settings = ServiceTypeSetting::
            select(
                'id',
                'service_type_id',
                'cleaning_materials_opted',
                'default_material_id',
                'cleaning_materials_option_show',
                'supervisor_option_show',
                'instructions_field_label',
                'instructions_field_placeholder',
                'instructions_field_required'
            )
            ->where('service_type_id', $input['service_type_id'])
            ->first();
        $response['data']->settings = @$response['data']->settings ? $response['data']->settings->toArray() : [];
        $response['data']->settings['cleaning_materials_opted'] = boolval(@$response['data']->settings['cleaning_materials_opted']);
        $response['data']->settings['cleaning_materials_option_show'] = boolval(@$response['data']->settings['cleaning_materials_option_show']);
        $response['data']->settings['supervisor_option_show'] = boolval(@$response['data']->settings['supervisor_option_show']);
        $response['data']->settings['instructions_field_required'] = boolval(@$response['data']->settings['instructions_field_required']);
        /**
         * Week days
         */
        $week_days = WeekDay::select('week_day_id', 'week_name')->where('weekend', 0)->orderBy('order_id')->get();
        $response['data']->week_days = $week_days->toArray();
        /**
         * Recurring months
         */
        $response['data']->recurring_periods = RecurringPeriod::
            select(
                'recurring_periods.id',
                'recurring_periods.no_of_months',
                'recurring_periods.no_of_months_label',
                'recurring_periods.no_of_weeks',
                'recurring_periods.no_of_weeks_label',
                'recurring_periods.default'
            )
            ->where('recurring_periods.service_type_id', $input['service_type_id'])
            ->orderBy('recurring_periods.sort_order', 'asc')
            ->get();
        /**
         * Service Terms & Conditions
         */
        $response['data']->terms_and_conditions = [
            'In case the scope of the job is found to be more extensive after the inspection upon arrival, we might need extra equipment / time and the pricing might differ.',
            'Client may cancel or adjust without any charges: the time of a cleaning visit/s by giving at least 24 hours advanced notice.',
            'Kindly take note that a cancellation fee amounting to 50% shall be levied should you opt to cancel your booking within 12 hours of the pre-scheduled appointment. In the event of an on the spot cancellation, an entire 100% cancellation fee shall be applied.',
            'You agree to pay the full price of the cleaning visit in the event of an on the spot cancellation, lock-out, no one home to let them in or a problem with your keys.',
            'Proceeding with your booking confirms your acceptance to Dubai Housekeeping terms and conditions.'
        ];
        /******************************************************* */
        $response['status'] = 'success';
        $response['message'] = 'Service type data fetched successfully.';
        return Response::json(array('result' => $response, 'cache' => true), 200, array(), customerResponseJsonConstants());

    }
}
