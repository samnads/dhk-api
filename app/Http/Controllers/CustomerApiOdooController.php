<?php

namespace App\Http\Controllers;

use App\Models\Customer;
use Illuminate\Http\Request;
use Response;

class CustomerApiOdooController extends Controller
{
    public function getCustomerOdooData(Request $request)
    {
        $customer = Customer::where('odoo_package_customer_id', $request->customerId)->where('customer_status', 1)->first();
        if ($customer) {
            return Response::json([
                'status' => 'success',
                'messages' => 'Odoo customer data fetched successfully.',
                'data' => [
                    'name' => $customer->customer_name,
                    'emailId' => $customer->email_address,
                    'phone' => $customer->mobile_number_1,
                    'customerId' => $customer->customer_id
                ]
            ], 200, array(), customerResponseJsonConstants());
        } else {
            return Response::json(['status' => "failed", 'data' => [], 'messages' => 'Customer Id not found'], 200, []);
        }
    }
}
