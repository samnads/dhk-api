<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class BookingCancel extends Model
{
    use HasFactory;
    protected $table = 'booking_cancel';
    public $timestamps = false;
    protected $primaryKey = 'booking_cancel_id';
}
