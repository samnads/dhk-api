<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class CustomerPayments extends Model
{
    use HasFactory;
    protected $table = 'customer_payments';
    public $timestamps = false;
    protected $primaryKey = 'payment_id';
}
