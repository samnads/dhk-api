-- Adminer 4.8.1 MySQL 11.3.2-MariaDB dump

SET NAMES utf8;
SET time_zone = '+00:00';
SET foreign_key_checks = 0;
SET sql_mode = 'NO_AUTO_VALUE_ON_ZERO';

DROP TABLE IF EXISTS `extra_services`;
CREATE TABLE `extra_services` (
  `id` int(11) NOT NULL AUTO_INCREMENT,
  `service_type_id` int(11) DEFAULT NULL,
  `service` varchar(150) DEFAULT NULL,
  `duration` varchar(100) DEFAULT NULL,
  `cost` varchar(100) DEFAULT NULL,
  `image_url` varchar(200) DEFAULT NULL,
  `show_on_min_hour` int(10) unsigned DEFAULT NULL,
  `show_on_max_hour` int(10) unsigned DEFAULT NULL,
  `status` varchar(50) DEFAULT NULL COMMENT '0-On,1-Off',
  `app_image_url` varchar(1000) DEFAULT NULL,
  `imgAltTag` varchar(255) DEFAULT NULL,
  PRIMARY KEY (`id`),
  KEY `status` (`status`),
  KEY `service_type_id` (`service_type_id`),
  CONSTRAINT `extra_services_ibfk_1` FOREIGN KEY (`service_type_id`) REFERENCES `service_types` (`service_type_id`)
) ENGINE=InnoDB DEFAULT CHARSET=latin1 COLLATE=latin1_swedish_ci;

INSERT INTO `extra_services` (`id`, `service_type_id`, `service`, `duration`, `cost`, `image_url`, `show_on_min_hour`, `show_on_max_hour`, `status`, `app_image_url`, `imgAltTag`) VALUES
(1,	NULL,	'Fridge Cleaning',	'30',	'0',	'fridge.png',	NULL,	NULL,	'0',	NULL,	'fridge cleaning'),
(2,	NULL,	'Ironing',	'30',	'0',	'ironing.png',	NULL,	NULL,	'0',	NULL,	'ironing'),
(3,	NULL,	'Oven Cleaning',	'30',	'0',	'oven.png',	NULL,	NULL,	'0',	NULL,	'Oven cleaning'),
(4,	NULL,	'Interior Windows',	'60',	'0',	'interior.png',	NULL,	NULL,	'0',	NULL,	'Interior Windows'),
(5,	1,	'Folding & Tidying Up',	'30',	'0',	'Extra-Tydying-Up1.jpg',	2,	8,	'0',	NULL,	'folding and tidying up'),
(6,	1,	'Ironing',	'30',	'0',	'Extra-Ironing.jpg',	2,	8,	'0',	NULL,	'ironing'),
(7,	1,	'Interior Windows',	'30',	'0',	'Extra-Interior-Windows1.jpg',	3,	8,	'0',	NULL,	'Interior windows'),
(8,	47,	'Folding & Tidying Up\r\n',	'30',	'0',	'Extra-Tydying-Up1.jpg',	2,	8,	'0',	NULL,	'folding and tidying up'),
(9,	47,	'Ironing',	'30',	'0',	'Extra-Ironing.jpg',	2,	8,	'0',	NULL,	'ironing'),
(10,	47,	'Interior Windows',	'30',	'0',	'Extra-Interior-Windows1.jpg\r\n',	3,	8,	'0',	NULL,	'Interior windows');

-- 2024-06-15 06:55:40
