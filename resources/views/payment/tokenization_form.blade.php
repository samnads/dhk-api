<?php
$requestParams = [
    'service_command' => 'TOKENIZATION',
    'access_code' => 'XIj8KVLKSPumGuyfnu6g',
    'merchant_identifier' => 'CKxScKab',
    'merchant_reference' => 'test-' . time(),
    'language' => 'en',
    'expiry_date' => '1223',
    'card_number' => '4005550000000001',
    'card_security_code' => '123',
    'card_holder_name' => 'test',
    'remember_me' => 'NO',
    'return_url' => url('api/customer/payment/payfort/processing'),
];
ksort($requestParams);
$hash_value = 'Test@123SHAIN';
$excludes = ['expiry_date', 'card_number', 'card_security_code', 'card_holder_name'];
foreach ($requestParams as $name => $value) {
    if (!in_array($name, $excludes)) {
        $hash_value .= $name . '=' . $value;
    }
}
$hash_value .= 'Test@123SHAIN';
//dd($hash_value);
$requestParams['signature'] = hash('sha256', $hash_value);
?>
<html>

<head>
    <title>Custom merchant page integration - request</title>
</head>

<body>
    <h1 style=“text-align:center”>Custom merchant page integration - request</h1>
    <form method="post" action="https://sbcheckout.payfort.com/FortAPI/paymentPage" id=form1 name="form1">
        @foreach ($requestParams as $name => $value)
            <b>{{ $name }}</b>
            <input name="{{ $name }}" value="{{ $value }}">
            <br>
        @endforeach
        <br><br>
        <input type="submit" value="Pay Now" id="submit2" name>
    </form>
    <b>Hash Value :</b><br><code>{{ $hash_value }}</code>
</body>

</html>
