<div class="content-section1" style="width: 700px; height: auto; margin: 0px auto;">
    @if($greeting)
    <p
        style="font: normal 16px/20px 'Poppins', sans-serif; color: #555; display: block; margin: 0px; padding: 0px 0px 5px; text-align: justify; -webkit-text-size-adjust: 100%; -moz-text-size-adjust: 100%; -ms-text-size-adjust: 100%;">
        {!!$greeting!!},</p>
    @endif
    <p
        style="font: normal 14px/24px 'Poppins', sans-serif; color: #555; display: block; margin: 0px; padding: 0px 0px 15px ; text-align: justify; -webkit-text-size-adjust: 100%; -moz-text-size-adjust: 100%; -ms-text-size-adjust: 100%;">
        {!!$text_body!!}</p>
</div>