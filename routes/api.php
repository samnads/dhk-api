<?php

use App\Http\Controllers\CalculateController;
use App\Http\Controllers\CustomerApiAddressController;
use App\Http\Controllers\CustomerApiAreaController;
use App\Http\Controllers\CustomerApiBookingCancelController;
use App\Http\Controllers\CustomerApiBookingCancelReasonsController;
use App\Http\Controllers\CustomerApiBookingCreateController;
use App\Http\Controllers\CustomerApiBookingHistoryController;
use App\Http\Controllers\CustomerApiBookingRescheduleController;
use App\Http\Controllers\CustomerApiBookingDetailsController;
use App\Http\Controllers\CustomerApiCardController;
use App\Http\Controllers\CustomerApiController;
use App\Http\Controllers\CustomerApiDataController;
use App\Http\Controllers\CustomerApiDateTimeController;
use App\Http\Controllers\CustomerApiFaqController;
use App\Http\Controllers\CustomerApiServiceTypeController;
use App\Http\Controllers\CustomerApiMaidController;
use App\Http\Controllers\CustomerApiNotificationsController;
use App\Http\Controllers\CustomerApiOtpController;
use App\Http\Controllers\CustomerApiPackagesController;
use App\Http\Controllers\CustomerApiPaymentController;
use App\Http\Controllers\PaymentWebhookController;
use App\Http\Controllers\CustomerApiRetryPaymentController;
use App\Http\Controllers\CustomerApiRatingController;
use App\Http\Controllers\CustomerApiReferralController;
use App\Http\Controllers\CustomerApiServiceAddonsController;
use App\Http\Controllers\CustomerApiSpecialOffersController;
use App\Http\Controllers\CustomerApiWalletController;
use App\Http\Controllers\CustomerApiInvoiceController;
use App\Http\Controllers\ccavenuePaymentController;
use App\Http\Controllers\AdminApiMailController;
use App\Http\Controllers\PaymentFrameController;
use App\Http\Controllers\CustomerApiOdooController;
use App\Http\Controllers\RedirectController;
use App\Http\Controllers\TelrPaymentController;
use App\Http\Controllers\OutstandingPaymentController;
use App\Http\Controllers\RatingController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
 */

Route::prefix('customer')->group(function () {
    // for customer app only
    Route::group(['middleware' => 'validJsonInput'], function () {
        //Route::get('frequencies', [CustomerApiController::class, 'frequencies']); // get | DONE
        Route::get('available_datetime', [CustomerApiDateTimeController::class, 'available_datetime']); // GET | DONE
        Route::get('available_time', [CustomerApiDateTimeController::class, 'available_time']); // GET | DONE
        Route::get('crew_list', [CustomerApiMaidController::class, 'crew_list']); // GET | DONE
        Route::get('faq', [CustomerApiFaqController::class, 'faq']); // GET | DONE
        Route::get('faq_help', [CustomerApiFaqController::class, 'faq_help']); // GET | DONE
        Route::post('customer_login', [CustomerApiOtpController::class, 'customer_login'])->middleware(['throttle:5,1']);
        Route::post('check_otp', [CustomerApiOtpController::class, 'check_otp']);
        Route::any('resend_otp', [CustomerApiOtpController::class, 'resend_otp']);
        Route::get('service_type_data', [CustomerApiServiceTypeController::class, 'service_type_data']); // GET | DONE
        Route::get('cancel_reasons', [CustomerApiBookingCancelReasonsController::class, 'cancel_reasons']); // GET | DONE
        Route::get('special_offers', [CustomerApiSpecialOffersController::class, 'special_offers']); // GET | DONE
        Route::get('packages', [CustomerApiPackagesController::class, 'packages']); // GET | DONE
        Route::get('service_addons', [CustomerApiServiceAddonsController::class, 'service_addons']); // GET | DONE
        Route::get('referral_credits', [CustomerApiReferralController::class, 'referral_credits']);
        Route::any('calculate', [CalculateController::class, 'calculate']);
        Route::get('area_list', [CustomerApiAreaController::class, 'area_list']); // GET | DONE
        Route::get('fcm_test', [CustomerApiNotificationsController::class, 'fcm_test']); // get | DONE
        Route::any('apple-process-pay', [PaymentFrameController::class, 'checkout_apple_pay_process']); // POST | DONE
        Route::any('google-pay-process', [PaymentFrameController::class, 'checkout_google_pay_process']);
    });
    Route::get('data', [CustomerApiDataController::class, 'data']);
    Route::any('payment/iframe', [PaymentFrameController::class, 'iframe']);
    Route::any('payment/payfort/tokenization', [PaymentFrameController::class, 'payfort_tokenization']);
    Route::any('payment/payfort/tokenization_form', [PaymentFrameController::class, 'tokenization_form']);
    Route::any('payment/payfort/processing', [PaymentFrameController::class, 'processing']);
    Route::any('payment/payfort/redirect', [PaymentFrameController::class, 'redirect']);
    Route::any('payment/payfort/verify', [PaymentFrameController::class, 'verify']);
    Route::any('payment/payfort/success', [PaymentFrameController::class, 'success']);
    Route::any('payment/payfort/failed', [PaymentFrameController::class, 'failed']);
    Route::any('payment/payfort/transaction_feedback', [PaymentFrameController::class, 'payfort_transaction_feedback']);
    // payment
    Route::prefix('payment/ccavenue')->group(function () {
        Route::post('response-handler', [ccavenuePaymentController::class, 'response_handler']); // POST
        Route::any('webhook/ccavenue-order-status', [PaymentWebhookController::class, 'ccavenue_order_status']); // POST
    });
    // invoice payment
    Route::post('invoice-payment', [CustomerApiInvoiceController::class, 'invoice_payment']);
    Route::any('invoice-payment/checkout/process-and-redirect/{booking_id}', [CustomerApiInvoiceController::class, 'checkoutInvoicePaymentProcessAndRedirect']);
    // online payment
    Route::post('online-payment', [CustomerApiInvoiceController::class, 'online_payment']);
    //
    Route::any('payment/iframe', [PaymentFrameController::class, 'iframe']);
    Route::group(['middleware' => ['validJsonInput', 'customerTokenCheck']], function () {
        Route::any('validate_token', [CustomerApiController::class, 'validate_token']); // OK
        Route::get('address_list', [CustomerApiAddressController::class, 'address_list']); // OK
        Route::any('set_default_address', [CustomerApiAddressController::class, 'set_default_address']); // OK
        Route::any('name_entry', [CustomerApiController::class, 'name_entry']); // OK
        Route::any('add_address', [CustomerApiAddressController::class, 'add_address']); // OK
        Route::any('edit_address', [CustomerApiAddressController::class, 'edit_address']); // OK
        Route::any('delete_address', [CustomerApiAddressController::class, 'delete_address']); // OK
        Route::any('update_customer_data', [CustomerApiController::class, 'update_customer_data']); // OK
        Route::any('update_avatar', [CustomerApiController::class, 'update_avatar']); // OK
        Route::any('customer_logout', [CustomerApiController::class, 'customer_logout']); //OK
        Route::get('notifications', [CustomerApiNotificationsController::class, 'notifications'])->name('notifications'); // OK
        Route::any('all_notification_read', [CustomerApiNotificationsController::class, 'all_notification_read']); // OK
        Route::any('clear_notiication', [CustomerApiNotificationsController::class, 'clear_notiication']); // OK
        Route::post('delete_account', [CustomerApiController::class, 'deleteAccount']); // OK
        Route::get('customer_data', [CustomerApiController::class, 'customer_data']); // OK
        Route::any('contact_send', [CustomerApiController::class, 'contact_send']);
        // Card
        Route::any('add_card', [CustomerApiCardController::class, 'add_card']);
        Route::any('card_details', [CustomerApiCardController::class, 'card_details']);
        Route::any('link_card', [CustomerApiCardController::class, 'link_card']);
        Route::any('delete_card', [CustomerApiCardController::class, 'delete_card']);
        // Booking
        Route::any('create_booking', [CustomerApiBookingCreateController::class, 'create_booking']); // post
        Route::any('reschedule', [CustomerApiBookingRescheduleController::class, 'reschedule']); // post
        Route::any('booking_history', [CustomerApiBookingHistoryController::class, 'booking_history']); // TEST
        Route::any('booking_cancel', [CustomerApiBookingCancelController::class, 'booking_cancel']); // POST
        Route::any('booking_all_cancel', [CustomerApiBookingCancelController::class, 'booking_all_cancel']); // POST
        Route::any('rate_booking', [CustomerApiRatingController::class, 'rate_booking']); // post
        Route::any('change_payment_mode', [CustomerApiPaymentController::class, 'change_payment_mode']); // POST | DONE
        Route::any('retry_payment', [CustomerApiRetryPaymentController::class, 'retry_payment']); // POST | DONE
        Route::get('customer_soa', [CustomerApiController::class, 'customer_soa']); // GET | DONE
        Route::get('wallet', [CustomerApiWalletController::class, 'wallet']);
        Route::post('booking_details', [CustomerApiBookingDetailsController::class, 'booking_details']);
        Route::post('booking_details_by_ref', [CustomerApiBookingHistoryController::class, 'booking_details_by_ref']); // TEST
        // Outstanding Amount
        Route::get('outstanding-amount', [OutstandingPaymentController::class, 'customerOutstandingAmount'])->name('outstanding-amount');
    });
    Route::post('get-customer-odoo', [CustomerApiOdooController::class, 'getCustomerOdooData']);
    Route::post('save-invoice-pay', [CustomerApiInvoiceController::class, 'saveInvoicePay']);

    Route::post('save-online-pay', [CustomerApiInvoiceController::class, 'saveOnlinePay']);

    Route::get('online-payment-by-id', [CustomerApiInvoiceController::class, 'online_payment_by_id']);
    Route::get('online-payment-by-ref', [CustomerApiInvoiceController::class, 'online_payment_by_ref']);
    Route::get('/', function (Request $request) {
        return '<h1>DHK v1.0</h1><br>You\'re in customer app API base url 😀 !';
    });
});

Route::prefix('admin')->group(function () {
    Route::prefix('mail')->group(function () {
        // mail routes, helpul to resend mails from anywhere outside api
        Route::get('schedule-cancel-to-admin/{booking_id}/{service_date}', [AdminApiMailController::class, 'schedule_cancel_to_admin']);
        Route::get('schedule-cancel-to-customer/{booking_id}/{service_date}', [AdminApiMailController::class, 'schedule_cancel_to_customer']);
        Route::get('registration-success-to-customer/{customer_id}', [AdminApiMailController::class, 'registration_success_to_customer']);
        Route::get('payment-link-to-customer/{customer_id}', [AdminApiMailController::class, 'payment_link_to_customer']);
        Route::get('schedule-confirmation-to-customer/{booking_id}/{service_date}', [AdminApiMailController::class, 'schedule_confirmation_to_customer']);
        Route::get('mail-to-customer', [AdminApiMailController::class, 'mail_to_customer']); // send html text mail to customer
        Route::get('online-payment-confirm-to-customer/{payment_id}', [AdminApiMailController::class, 'online_payment_confirm_to_customer']);
        Route::get('online-payment-confirm-to-admin/{payment_id}', [AdminApiMailController::class, 'online_payment_confirm_to_admin']);
        Route::get('booking-confirmation-to-admin/{booking_id}', [AdminApiMailController::class, 'booking_confirmation_to_admin']);
        Route::get('booking-confirmation-to-customer/{booking_id}', [AdminApiMailController::class, 'booking_confirmation_to_customer']);
        Route::get('login-otp-to-customer/{customer_id}', [AdminApiMailController::class, 'login_otp_to_customer']);
        Route::get('service-rating-feedback-to-admin/{day_service_id}', [AdminApiMailController::class, 'service_rating_feedback_to_admin']);
        Route::get('template', [AdminApiMailController::class, 'template']);
    });
});
Route::post('get-rate-review',[RatingController::class,'getRateReview']);
Route::post('submit-rate',[RatingController::class,'submitRate']);

Route::prefix('telr')->group(function () {
    // for telr gateway
    Route::group(['middleware' => 'validJsonInput'], function () {
        Route::post('process', [TelrPaymentController::class, 'process_order']);
        Route::get('process/{booking_ref}/{booking_id}', [TelrPaymentController::class, 'process_order_url']);
        Route::post('invoice-process', [TelrPaymentController::class, 'invoice_process']);
        Route::post('online-payment-process', [TelrPaymentController::class, 'online_payment_process']);
        Route::any('telr_test', [TelrPaymentController::class, 'telr_test']);
    });
});

Route::any('/redirect/{action}', [RedirectController::class, 'redirect_based_on_action']);

Route::get('email-test', function () {
    $details['email'] = 'samnad.s@azinova.info';
    dispatch(new App\Jobs\SendEmailJob($details));
});

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::fallback(function () {
    return '<h1>DHK v1.0</h1><br>Oops, requested api endpoint not found 🥶 !';
});
