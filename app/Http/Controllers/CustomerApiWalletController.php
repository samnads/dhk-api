<?php

namespace App\Http\Controllers;

use Config;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Response;
use stdClass;

class CustomerApiWalletController extends Controller
{
    public function wallet(Request $request)
    {
        $debug = toggleDebug(); // pass boolean to overide default
        /************************************************************* */
        if (!$debug) {
            // live input
            $data = json_decode($request->getContent(), true);
        } else {
            // test input
            $data['params']['id'] = 1; //Config::get('values.debug_customer_id');
        }
        /************************************************************* */
        $input = @$data['params'];
        /************************************************************* */
        $response['status'] = 'success';
        $wallet_total = DB::table('customer_wallet_credits as cwc')
            ->select(
                DB::raw('SUM(cwc.amount) as balance'),
                DB::raw('MAX(cwc.created_at) as last_topup'),
                DB::raw('MIN(cwc.created_at) as first_topup'),
                DB::raw('COUNT(cwc.id) as total_topups')
            )
            ->where([['cwc.customer_id', '=', $input['id']], ['cwc.deleted_at', '=', null]])
            ->groupBy('cwc.customer_id')
            ->first();
        $trans_sum = DB::table('customer_wallet_transactions as cwt')
            ->select(
                DB::raw('SUM(cwt.amount) as amount'),
            )
            ->where([['cwt.customer_id', '=', $input['id']], ['cwt.deleted_at', '=', null]])
            ->groupBy('cwt.customer_id')
            ->first();
        $response['wallet'] = new stdClass();
        $response['wallet']->balance = (float) (@$wallet_total->balance ?: 0) + (float) (@$trans_sum->amount ?: 0);
        $response['wallet']->last_topup = @$wallet_total->last_topup;
        $response['wallet']->first_topup = @$wallet_total->first_topup;
        $response['wallet']->total_topups = @$wallet_total->total_topups ?: 0;
        $response['wallet']->currency = Config::get('values.currency_code');
        $response['transactions'] = DB::table('customer_wallet_transactions as cwt')
            ->select(
                'cwt.id',
                'cwt.amount_before_transaction as balance_before_transaction',
                DB::raw('(CASE WHEN cwt.amount > 0 THEN "credit" ELSE "debit" END) as type'),
                DB::raw('ABS(cwt.amount) as amount'),
                'cwt.amount_after_transaction as balance_after_transaction',
                'cwt.transaction_description',
                'cwt.refund_for as refund_for_id',
                'cwt.refund_description',
                'cwt.created_at as date_time',
                DB::raw('"' . Config::get('values.currency_code') . '" as currency'),
            )
            ->where([['cwt.customer_id', '=', $input['id']], ['cwt.deleted_at', '=', null]])
            ->orderBy('cwt.id', 'DESC')
            ->get();
        $response['message'] = "Credits fetched successfully.";
        return Response::json(array('result' => $response, 'debug' => $debug), 200, array(), JSON_PRETTY_PRINT);
    }
}
