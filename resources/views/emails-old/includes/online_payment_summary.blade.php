<div class="content-section1" style="width: 700px; height: auto; margin: 30px auto 0;">
    <table width="100%" border="0" cellpadding="0" cellspacing="0">
        <tr>
            <td>&nbsp;</td>
        </tr>
        <tr>
            <td>&nbsp;</td>
            <td>&nbsp;</td>
            <td align="right">
                <p
                    style="font: normal 16px/20px 'Poppins', sans-serif; color: #555; display: block; margin: 0px; padding: 0px 0px 10px; text-align: right; -webkit-text-size-adjust: 100%; -moz-text-size-adjust: 100%; -ms-text-size-adjust: 100%;">
                    Payment Ref.</p>
                <p
                    style="font: normal 20px/25px 'Poppins', sans-serif; color: #f76161; display: block; margin: 0px; padding: 0px 0px 15px; text-align: right; -webkit-text-size-adjust: 100%; -moz-text-size-adjust: 100%; -ms-text-size-adjust: 100%;">
                    {{ @$online_payment->reference_id }}
                </p>
            </td>
        </tr>
        <tr>
            <td colspan="3" align="right">
                <p
                    style="font: normal 16px/20px 'Poppins', sans-serif; color: #555; display: block; margin: 0px; padding: 0px 0px 10px; text-align: right; -webkit-text-size-adjust: 100%; -moz-text-size-adjust: 100%; -ms-text-size-adjust: 100%;">
                    Transaction Ref.</p>
                <p
                    style="font: normal 20px/25px 'Poppins', sans-serif; color: #5180ec; display: block; margin: 0px; padding: 0px 0px 15px; text-align: right; -webkit-text-size-adjust: 100%; -moz-text-size-adjust: 100%; -ms-text-size-adjust: 100%;">
                    {{ @$online_payment->transaction_id }}
                </p>
            </td>
        </tr>
    </table>
</div>
