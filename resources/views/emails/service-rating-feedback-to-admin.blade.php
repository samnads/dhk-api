@extends('emails.layouts.main')
@section('content')
    @include('emails.includes.section-message')
    @include('emails.includes.service-feedaback-summary')
    @include('emails.includes.section-terms-conditions')
    @include('emails.includes.section-footer-address')
@endsection
