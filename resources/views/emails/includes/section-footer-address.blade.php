<div style="width: 600px; height:auto; padding: 20px 0px 0px 0px; margin-bottom: 30px; text-align: center;">
    <div style="width: 100%; text-align: center;"><img
            src="{{ Config::get('values.file_server_url') . 'images/email/footer-logo.png?v=' . $settings->customer_app_img_version }}" width="300" height="33"
            alt="logo" style="margin:0 auto;" /></div>
    <p
        style="font-family: Tahoma, Geneva, sans-serif; font-size:14px; color: #555; line-height:20px; padding: 5px 0px 15px 0px; margin:0px; -webkit-text-size-adjust: 100%; -moz-text-size-adjust: 100%; -ms-text-size-adjust: 100%;">
        Grace Quest Cleaning Services<br />
        Office 4201B, ASPiN Commercial Tower,<br />
        Sheikh Zayed Road, Dubai, U.A.E<br />
        Phone : <a href="tel:97142631976">+971 4 263 1976</a><br />
        Email : <a href="mailto:office@dubaihousekeeping.com">office@dubaihousekeeping.com</a>
    </p>
</div>
