<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class ServiceTypeCharges extends Model
{
    use HasFactory, SoftDeletes;
    protected $table = 'service_type_charges';
    public $timestamps = true;
    protected $primaryKey = 'id';
}
