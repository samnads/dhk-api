<?php
use App\Models\Customer;
use Illuminate\Support\Facades\DB;

function send_sms($mobile_number, $message)
{
    /**
     * global helper to send sms
     */
    /*************** credentials ****************** */
    $user = '';
    $password = '';
    $sid = '';
    /*************** credentials end ************* */
    $num = substr($mobile_number, -9);
    if (in_array($num, debug_mobiles()) || strpos($_SERVER['REQUEST_URI'], '-demo') !== false) {
        // dont send message
        return true;
    } else {
        // send message
        //$sms_url = 'http://sms.azinova.in/WebServiceSMS.aspx?User=' . $user . '&passwd=' . $password . '&mobilenumber=971' . $num . '&message=' . urlencode($message) . '&sid=' . $sid . '&mtype=N';
        $sms_url = 'https://www.azinovatechnologies.com/sms/dhk-sms-api.php?mobile_number=' . $mobile_number . '&message=' . urlencode($message);
        //$sms_url = str_replace(" ", '%20', $sms_url);
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $sms_url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0); // Skip SSL Verification
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 10);
        curl_setopt($ch, CURLOPT_TIMEOUT, 10);
        $res = curl_exec($ch);
        curl_close($ch);
    }
}
function send_customer_login_otp($mobile_number, $otp, $app_signature = null)
{
    /**
     * for customize sms for login OTP 
     */
    // send OTP
    $message = '<#> ' . Config::get('values.company_name') . ' Code: ' . $otp . ' . Please don\'t share this code with anyone. ' . $app_signature;
    send_sms($mobile_number, $message);
}
function send_booking_confirmation_sms_to_customer($booking_common_id)
{
    // get details based on booking id
    $bookings = DB::table('bookings as b')
        ->select(
            'b.*',
        )
        ->where(function ($query) use ($booking_common_id) {
            $query->where('b.booking_common_id', $booking_common_id);
            $query->orWhere('b.booking_id', $booking_common_id);
        })
        ->orderBy('b.booking_id', 'ASC')
        ->get();
    $booking = $bookings[0];
    $customer = Customer::where('customer_id', '=', $booking->customer_id)->first();
    $mobile_number = $customer->mobile_number_1;
    $message = 'Hi ' . $customer->customer_name . ', Your booking received with Ref. Id. ' . $booking->reference_id . '. You will receive a confirmation soon.';
    send_sms($mobile_number, $message);
}
?>