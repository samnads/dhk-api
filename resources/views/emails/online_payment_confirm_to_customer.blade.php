@extends('emails.layouts.main')
@section('content')
    @include('emails.includes.section-message')
    @include('emails.includes.online_payment_summary')
    @include('emails.includes.section-price-online-payment')
    @include('emails.includes.section-terms-conditions')
    @include('emails.includes.section-footer-address')
@endsection
