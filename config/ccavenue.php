<?php
return [
    'merchant_id' => env('CCAVENUE_MERCHANT_ID'),
    'workingkey' => env('CCAVENUE_WORKINGKEY'),
    'req_handler' => env('CCAVENUE_REQ_HANDLER'),
    'resp_handler' => env('CCAVENUE_RESP_HANDLER'),
];