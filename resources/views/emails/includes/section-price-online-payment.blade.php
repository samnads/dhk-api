<div style="width: 560px; height:auto; background: #fafafa; border:1px solid #e5e2e2; padding: 20px 20px;border-top:0px;border-bottom:0px;">

    <div style=""><!--NOTE-->
        <!--Customer not select the Ad-On services, need to CHANGE this div margin-top: 95px; to margin-top: 0px; automatically. -->

        <div class="price-details"
            style="width: 330px; margin: 0px 10px 20px; margin-top: 0px; height: auto; float: right;">
            <p
                style="font: bold 20px/25px 'Poppins', sans-serif; color: #555; display: block; margin: 0px; padding: 20px 0px 15px 10px; text-align: left; -webkit-text-size-adjust: 100%; -moz-text-size-adjust: 100%; -ms-text-size-adjust: 100%;">
                Price Details</p>

            <table width="320px" border="0" cellpadding="0" cellspacing="0" style="width:320px; float: left;">

                <tr>
                    <td>
                        <p
                            style="font: normal 15px/18px 'Poppins', sans-serif; color: #555; display: block; margin: 0px; padding: 0px 0px 15px 10px; text-align: left; -webkit-text-size-adjust: 100%; -moz-text-size-adjust: 100%; -ms-text-size-adjust: 100%;">
                            Payment Status</p>
                    </td>
                    <td>
                        <p
                            style="font: bold 15px/18px 'Poppins', sans-serif; color: #555; display: block; margin: 0px; padding: 0px 0px 15px; text-align: right; -webkit-text-size-adjust: 100%; -moz-text-size-adjust: 100%; -ms-text-size-adjust: 100%;">
                            Paid
                        </p>
                    </td>
                </tr>
                <tr>
                    <td>
                        <p
                            style="font: normal 15px/18px 'Poppins', sans-serif; color: #555; display: block; margin: 0px; padding: 0px 0px 15px 10px; text-align: left; -webkit-text-size-adjust: 100%; -moz-text-size-adjust: 100%; -ms-text-size-adjust: 100%;">
                            Net Amount</p>
                    </td>
                    <td>
                        <p
                            style="font: bold 15px/18px 'Poppins', sans-serif; color: #555; display: block; margin: 0px; padding: 0px 0px 15px; text-align: right; -webkit-text-size-adjust: 100%; -moz-text-size-adjust: 100%; -ms-text-size-adjust: 100%;">
                            <span style="font-size:12px;">AED</span>
                            {{ number_format($online_payment->amount, 2, '.', ',') }}
                        </p>
                    </td>
                </tr>
                <tr>
                    <td>
                        <p
                            style="font: normal 15px/18px 'Poppins', sans-serif; color: #555; display: block; margin: 0px; padding: 0px 0px 15px 10px; text-align: left; -webkit-text-size-adjust: 100%; -moz-text-size-adjust: 100%; -ms-text-size-adjust: 100%;">
                            Transaction Charge</p>
                    </td>
                    <td>
                        <p
                            style="font: bold 15px/18px 'Poppins', sans-serif; color: #555; display: block; margin: 0px; padding: 0px 0px 15px; text-align: right; -webkit-text-size-adjust: 100%; -moz-text-size-adjust: 100%; -ms-text-size-adjust: 100%;">
                            <span style="font-size:12px;">AED</span>
                            {{ number_format($online_payment->transaction_charge, 2, '.', ',') }}
                        </p>
                    </td>
                </tr>
                <tr>
                    <td>
                        <p
                            style="font: normal 20px/20px 'Poppins', sans-serif; color: #355fac; display: block; margin: 0px; padding: 0px 0px 15px 10px; text-align: left; -webkit-text-size-adjust: 100%; -moz-text-size-adjust: 100%; -ms-text-size-adjust: 100%;">
                            Total Payable</p>
                    </td>
                    <td>
                        <p
                            style="font: bold 20px/20px 'Poppins', sans-serif; color: #355fac; display: block; margin: 0px; padding: 0px 0px 15px; text-align: right; -webkit-text-size-adjust: 100%; -moz-text-size-adjust: 100%; -ms-text-size-adjust: 100%;">
                            <span style="font-size:15px;">AED</span>
                            {{ number_format($online_payment->amount + $online_payment->transaction_charge, 2, '.', ',') }}
                        </p>
                    </td>
                </tr>

            </table>

        </div><!--price-details end-->
        <div style="clear:both;"></div>
    </div>
</div>
