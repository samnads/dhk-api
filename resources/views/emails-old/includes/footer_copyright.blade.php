<div style="width: 800px;">
    <p
        style="font: normal 12px/18px 'Poppins', sans-serif; color: #fff; display: block; margin: 30px 0px 0px; padding: 7px 20px; text-align: center; background: #1cac51; -webkit-text-size-adjust: 100%; -moz-text-size-adjust: 100%; -ms-text-size-adjust: 100%;">
        {{date('Y')}} © {{Config::get('values.company_name')}}. All rights reserved.</p>
</div>