<?php

namespace App\Http\Controllers;

use App\Models\Customer;
use App\Models\CustomersTemp;
use App\Models\Settings;
use Carbon\Carbon;
use Config;
use File;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;
use Log;
use Response;

class CustomerApiOtpController extends Controller
{
    public function customer_login(Request $request)
    {
        $data = json_decode($request->getContent(), true);
        // required input check
        $input = @$data['params'];
        /*************************************** */
        if (@$input['type'] == 'mobile') {
            $input['country_code'] = str_replace("+", "", @$input['country_code']); // remove + character
            //$input['mobilenumber'] = substr(@$input['mobilenumber'], -9); // get last 9 digits only
            $input['email'] = null;
            /*********************
             * rules
             *********************/
            $rules = [
                'mobilenumber' => 'required|numeric|digits:9',
                'country_code' => 'nullable|numeric|min:1',
            ];
        } else if (@$input['type'] == 'email') {
            $input['country_code'] = null;
            $input['mobilenumber'] = null;
            /*********************
             * rules
             *********************/
            $rules = [
                'email' => 'required|email:rfc',
            ];
        }
        $rules['type'] = 'required|in:mobile,email';
        $rules['app_signature'] = 'nullable|string';
        //Log::channel('debug')->info(json_encode(@$data ?: []));
        /*************************************** */
        $validator = Validator::make(
            (array) $input,
            $rules,
            [],
            [
                'type' => 'Login Type',
                'mobilenumber' => 'Mobile Number',
                'country_code' => 'Country Code',
                'email' => 'Email',
                'app_signature' => 'App Signature',
            ]
        );
        if ($validator->fails()) {
            return Response::json(array('result' => array('status' => 'failed', 'message' => $validator->errors()->first())), 200, [], customerResponseJsonConstants());
        }
        try {
            DB::beginTransaction();
            if ($input['type'] == 'mobile') {
                /************************************************************* */
                $customer = Customer::where('mobile_number_1', 'like', '%' . $input['country_code'] . $input['mobilenumber'])
                    ->where(function ($query) {
                        $query->where('customers.customer_status', '=', 1)
                            ->orWhere('customers.customer_status', '=', 0);
                    })
                    ->orderBy('customers.customer_status', 'desc') // for picking customer_status = 1 first (if multiple customer with same number exist in db pick the active one)
                    ->first();
                /************************************************************* */
                if ($customer) {
                    // EXISTING CUSTOMER FOUND
                    $otp = generate_mobile_otp($input['mobilenumber']);
                    send_customer_login_otp($input['mobilenumber'], $otp, @$input['app_signature']);
                    $customer->mobile_verification_code = $otp;
                    $customer->login_otp_expired_at = Carbon::now()->addSeconds(Config::get('values.customer_login_otp_expire_seconds'));
                    $customer->save();
                    $response['status'] = 'success';
                    $response['message'] = 'OTP SMS Sent Successfully !';
                    $response['signinup_status'] = $customer->customer_name == null ? "new signup" : "already signup";
                    $response['UserDetails']['country_code'] = $input['country_code'];
                    $response['UserDetails']['mobile'] = $input['mobilenumber'];
                    $response['UserDetails']['id'] = $customer->customer_id;
                } else {
                    // NEW CUSTOMER FOUND
                    $otp = generate_mobile_otp($input['mobilenumber']);
                    send_customer_login_otp($input['mobilenumber'], $otp);
                    $customer = new Customer();
                    $customer->mobile_number_1 = $input['country_code'] . $input['mobilenumber'];
                    $customer->odoo_customer_id = 0;
                    $customer->customer_type = 'HO';
                    $customer->customer_name = null;
                    $customer->customer_nick_name = null;
                    $customer->website_url = null;
                    $customer->payment_type = 'D';
                    $customer->payment_mode = 'Cash';
                    $customer->balance = 0;
                    $customer->signed = null;
                    $customer->mobile_status = 0;
                    $customer->customer_notes = null;
                    $customer->customer_source_id = null;
                    $customer->customer_added_datetime = Carbon::now();
                    $customer->customer_last_modified_datetime = Carbon::now();
                    $customer->mobile_verification_code = $otp;
                    $customer->login_otp_expired_at = Carbon::now()->addSeconds(Config::get('values.customer_login_otp_expire_seconds'));
                    $customer->added_from_ip_address = $request->ip();
                    $customer->save();
                    $response['status'] = 'success';
                    $response['message'] = 'OTP SMS Sent Successfully !';
                    $response['signinup_status'] = 'new signup';
                    $response['UserDetails']['country_code'] = $input['country_code'];
                    $response['UserDetails']['mobile'] = $input['mobilenumber'];
                    $response['UserDetails']['id'] = $customer->customer_id;
                }
                DB::commit();
            } else if ($input['type'] == 'email') {
                /************************************************************* */
                $customer = Customer::where('email_address', '=', $input['email'])
                    ->where(function ($query) {
                        $query->where('customer_status', '=', 1)
                            ->orWhere('customer_status', '=', 0);
                    })
                    ->orderBy('customer_status', 'desc') // for picking customer_status = 1 first (if multiple customer with same number exist in db pick the active one)
                    ->first();
                /************************************************************* */
                if ($customer) {
                    // EXISTING CUSTOMER FOUND
                    $customer->mobile_verification_code = generate_email_otp($input['email']);
                    $customer->login_otp_expired_at = Carbon::now()->addSeconds(Config::get('values.customer_login_otp_expire_seconds'));
                    $customer->save();
                    $response['status'] = 'success';
                    $response['message'] = 'OTP Mail Sent Successfully !';
                    $response['signinup_status'] = $customer->customer_name == null ? "new signup" : "already signup";
                    $response['UserDetails']['country_code'] = '+971';
                    $response['UserDetails']['mobile'] = $input['mobilenumber'];
                    $response['UserDetails']['email'] = $input['email'];
                    $response['UserDetails']['id'] = $customer->customer_id;
                    $mail = new AdminApiMailController;
                    $mail->login_otp_to_customer($customer->customer_id, $customer->mobile_verification_code);
                } else {
                    // NEW CUSTOMER FOUND
                    $customer = new Customer();
                    $customer->mobile_number_1 = null;
                    $customer->email_address = $input['email'];
                    $customer->odoo_customer_id = 0;
                    $customer->customer_type = 'HO';
                    $customer->customer_name = null;
                    $customer->customer_nick_name = null;
                    $customer->website_url = null;
                    $customer->payment_type = 'D';
                    $customer->payment_mode = 'Cash';
                    $customer->balance = 0;
                    $customer->signed = null;
                    $customer->mobile_status = 0;
                    $customer->customer_notes = null;
                    $customer->customer_source_id = null;
                    $customer->customer_added_datetime = Carbon::now();
                    $customer->customer_last_modified_datetime = Carbon::now();
                    $customer->mobile_verification_code = generate_email_otp($input['email']);
                    $customer->login_otp_expired_at = Carbon::now()->addSeconds(Config::get('values.customer_login_otp_expire_seconds'));
                    $customer->added_from_ip_address = $request->ip();
                    $customer->save();
                    $response['status'] = 'success';
                    $response['message'] = 'OTP Mail Sent Successfully !';
                    $response['signinup_status'] = 'new signup';
                    $response['UserDetails']['country_code'] = '+971';
                    $response['UserDetails']['mobile'] = $input['mobilenumber'];
                    $response['UserDetails']['email'] = $input['email'];
                    $response['UserDetails']['id'] = $customer->customer_id;
                    $mail = new AdminApiMailController;
                    $mail->login_otp_to_customer($customer->customer_id, $customer->mobile_verification_code);
                }
                DB::commit();
            }
        } catch (\Exception $e) {
            DB::rollback();
            return Response::json(array('result' => array('status' => 'failed', 'message' => $e->getMessage())), 200, [], customerResponseJsonConstants());
        }
        /************************************************************* */
        return Response::json(array('result' => $response), 200, [], JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES);
    }
    public function check_otp(Request $request)
    {
        $settings = Settings::first();
        /************************************************************* */
        $data = json_decode($request->getContent(), true);
        // required input check
        $input = @$data['params'];
        /*************************************** */
        if (@$input['type'] == 'mobile') {
            $input['mobilenumber'] = @$input['mobilenumber'] ? substr(@$input['mobilenumber'], -9) : null; // get last 9 digits only
            $input['new_mobilenumber'] = @$input['new_mobilenumber'] ? substr(@$input['new_mobilenumber'], -9) : null; // get last 9 digits only
            $input['email'] = null;
            // validations
            if (@$input['new_mobilenumber']) {
                $validation = [
                    'mobilenumber' => 'nullable|numeric|digits:9',
                    'otp' => 'required|string|numeric|digits:4',
                    'fcm' => 'nullable|string',
                    'device_type' => 'nullable|string',
                    'new_mobilenumber' => 'required|numeric|digits:9',
                ];
            } else {
                $validation = [
                    'mobilenumber' => 'required_if:type,==,mobile|nullable|numeric|digits:9',
                    'otp' => 'required|string|numeric|digits:4',
                    'fcm' => 'nullable|string',
                    'device_type' => 'nullable|string',
                    'new_mobilenumber' => 'nullable|numeric|digits:9',
                ];
            }
        } else if (@$input['type'] == 'email') {
            $input['mobilenumber'] = null;
            $input['new_mobilenumber'] = null;
            // validations
            $validation = [
                'otp' => 'required|string|numeric|digits:4',
                'fcm' => 'nullable|string',
                'device_type' => 'nullable|string',
            ];
        }
        $validation['type'] = 'required|in:mobile,email';
        $validation['id'] = 'required|exists:customers,customer_id';
        /*************************************** */
        $validator = Validator::make(
            (array) $input,
            $validation,
            [],
            [
                'type' => 'Login Method',
                'mobilenumber' => 'Mobile Number',
                'otp' => 'OTP',
                'fcm' => 'FCM Token',
                'device_type' => 'Device Type',
                'new_mobilenumber' => 'New Mobile Number',
                'id' => 'Customer ID',
            ]
        );
        if ($validator->fails()) {
            return Response::json(array('result' => array('status' => 'failed', 'message' => $validator->errors()->first())), 200, array(), customerResponseJsonConstants());
        }
        /************************************************************* */
        DB::beginTransaction();
        try {
            if (@$input['type'] == 'mobile') {
                $customer = Customer::select('customers.*', 'ca.customer_address_id')
                    ->leftJoin('customer_addresses as ca', function ($join) {
                        $join->on('customers.customer_id', '=', 'ca.customer_id');
                        $join->on('ca.default_address', '=', DB::raw('1'));
                    })
                    //->where('customers.mobile_number_1', 'like', '%' . $input['mobilenumber'])
                    ->where('customers.customer_id', '=', $input['id'])
                    /*->where(function ($query) {
                        $query->where('customer_status', '=', 1)
                            ->orWhere('customer_status', '=', 0);
                    })*/
                    //->orderBy('customers.customer_status', 'desc') // for picking customer_status = 1 first (if multiple customer with same number exist in db pick the active one)
                    ->first();
                if (@$input['new_mobilenumber']) {
                    // update mobile number with valid otp
                    if (!$customer) {
                        throw new \ErrorException('Customer not found.');
                    }
                    $new_mobile_exist = Customer::where('customer_id', '!=', $input['id'])->where('mobile_number_1', 'like', '%' . $input['new_mobilenumber'])->first();
                    if ($new_mobile_exist) {
                        // duplicate mobile number
                        throw new \ErrorException('Mobile number already exist.');
                    } else {
                        // allow requested new mobile number to db
                        $customer_temp = CustomersTemp::where('customer_id', '=', $input['id'])->where('mobile_number_1', 'like', '%' . $input['new_mobilenumber'])->first();
                        if ($customer_temp) {
                            if ($input['otp'] == $customer_temp->mobile_number_1_otp && $customer_temp->mobile_number_1_otp_expired_at >= date('Y-m-d H:i:s')) {
                                $customer->mobile_verification_code = null;
                                $customer->login_otp_expired_at = null;
                                $customer->mobile_number_1 = $input['new_mobilenumber']; // update with new number
                                $token = Hash::make(Str::random(40));
                                $customer->oauth_token = $token;
                                if (@$input['fcm']) {
                                    $customer->push_token = $input['fcm'];
                                }
                                if (@$input['device_type']) {
                                    $customer->device_type = $input['device_type'];
                                }
                                $customer->mobile_status = 1;
                                $customer->save();
                                $response['status'] = 'success';
                                $response['message'] = 'Mobile number updated successfully.';
                                $response['UserDetails']['id'] = $customer->customer_id;
                                $response['UserDetails']['UserName'] = $customer->customer_name;
                                $response['UserDetails']['email'] = $customer->email_address;
                                $response['UserDetails']['dob'] = $customer->dob;
                                $response['UserDetails']['gender_id'] = $customer->gender_id;
                                $response['UserDetails']['image'] = Config::get('values.customer_avatar_prefix_url') . (($customer->customer_photo_file != '') ? (File::exists(Config::get('values.customer_avatar_storage_path') . $customer->customer_photo_file) ? ($customer->customer_photo_file . '?v=' . $settings->customer_app_img_version) : (Config::get('values.customer_avatar_default_file') . '?v=' . $settings->customer_app_img_version)) : (Config::get('values.customer_avatar_default_file') . '?v=' . $settings->customer_app_img_version));
                                $response['UserDetails']['token'] = $customer->oauth_token;
                                $response['UserDetails']['country_code'] = '+971';
                                $response['UserDetails']['mobile'] = $customer->mobile_number_1;
                                $response['UserDetails']['default_address_id'] = $customer->customer_address_id;
                                DB::commit();
                                return Response::json(array('result' => $response), 200, array(), JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES);
                            } else {
                                throw new \ErrorException('Invalid or Expired OTP !');
                            }
                        } else {
                            throw new \ErrorException('Mobile number change request not found !');
                        }
                    }
                } else {
                    if ($customer) {
                        if ($input['otp'] == $customer->mobile_verification_code && $customer->login_otp_expired_at >= date('Y-m-d H:i:s')) {
                            $customer->mobile_verification_code = null;
                            $customer->login_otp_expired_at = null;
                            $token = Hash::make(Str::random(40));
                            $customer->oauth_token = $token;
                            if (@$input['fcm']) {
                                $customer->push_token = $input['fcm'];
                            }
                            if (@$input['device_type']) {
                                $customer->device_type = $input['device_type'];
                            }
                            //$customer->mobile_status = 1;
                            $customer->save();
                            $response['status'] = 'success';
                            $response['message'] = 'Logged in successfully.';
                            $response['UserDetails']['id'] = $customer->customer_id;
                            $response['UserDetails']['UserName'] = $customer->customer_name;
                            $response['UserDetails']['email'] = $customer->email_address;
                            $response['UserDetails']['dob'] = $customer->dob;
                            $response['UserDetails']['gender_id'] = $customer->gender_id;
                            $response['UserDetails']['image'] = Config::get('values.customer_avatar_prefix_url') . (($customer->customer_photo_file != '') ? (File::exists(Config::get('values.customer_avatar_storage_path') . $customer->customer_photo_file) ? ($customer->customer_photo_file . '?v=' . $settings->customer_app_img_version) : (Config::get('values.customer_avatar_default_file') . '?v=' . $settings->customer_app_img_version)) : (Config::get('values.customer_avatar_default_file') . '?v=' . $settings->customer_app_img_version));
                            $response['UserDetails']['token'] = $token;
                            $response['UserDetails']['country_code'] = '+971';
                            $response['UserDetails']['mobile'] = $input['mobilenumber'];
                            $response['UserDetails']['default_address_id'] = $customer->customer_address_id;
                            DB::commit();
                            return Response::json(array('result' => $response), 200, array(), JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES);
                        } else {
                            throw new \ErrorException('Invalid or Expired OTP !');
                        }
                    } else {
                        throw new \ErrorException('Customer not found.');
                    }
                }
            } else if (@$input['type'] == 'email') {
                $customer = Customer::select('customers.*', 'ca.customer_address_id')
                    ->leftJoin('customer_addresses as ca', function ($join) {
                        $join->on('customers.customer_id', '=', 'ca.customer_id');
                        $join->on('ca.default_address', '=', DB::raw('1'));
                    })
                    ->where('email_address', '=', $input['email'])
                    ->where(function ($query) {
                        $query->where('customer_status', '=', 1)
                            ->orWhere('customer_status', '=', 0);
                    })
                    ->orderBy('customer_status', 'desc') // for picking customer_status = 1 first (if multiple customer with same number exist in db pick the active one)
                    ->first();
                if (@$input['new_mobilenumber']) {
                    // update mobile number with valid otp
                    if (!$customer) {
                        throw new \ErrorException('Customer not found.');
                    }
                    $new_mobile_exist = Customer::where('customer_id', '!=', $customer->customer_id)->where('mobile_number_1', 'like', '%' . $input['new_mobilenumber'])->first();
                    if ($new_mobile_exist) {
                        // duplicate mobile number
                        throw new \ErrorException('Mobile number already exist.');
                    } else {
                        // allow requested new mobile number to db
                        $customer_temp = CustomersTemp::where('customer_id', '=', $customer->customer_id)->where('mobile_number_1', 'like', '%' . $input['new_mobilenumber'])->first();
                        if ($customer_temp) {
                            if ($input['otp'] == $customer_temp->mobile_number_1_otp && $customer_temp->mobile_number_1_otp_expired_at >= date('Y-m-d H:i:s')) {
                                $customer->mobile_verification_code = null;
                                $customer->login_otp_expired_at = null;
                                $customer->mobile_number_1 = $input['new_mobilenumber']; // update with new number
                                $token = Hash::make(Str::random(40));
                                $customer->oauth_token = $token;
                                if (@$input['fcm']) {
                                    $customer->push_token = $input['fcm'];
                                }
                                if (@$input['device_type']) {
                                    $customer->device_type = $input['device_type'];
                                }
                                $customer->mobile_status = 1;
                                $customer->save();
                                $response['status'] = 'success';
                                $response['message'] = 'Mobile number updated successfully.';
                                $response['UserDetails']['id'] = $customer->customer_id;
                                $response['UserDetails']['UserName'] = $customer->customer_name;
                                $response['UserDetails']['email'] = $customer->email_address;
                                $response['UserDetails']['dob'] = $customer->dob;
                                $response['UserDetails']['gender_id'] = $customer->gender_id;
                                $response['UserDetails']['image'] = Config::get('values.customer_avatar_prefix_url') . (($customer->customer_photo_file != '') ? (File::exists(Config::get('values.customer_avatar_storage_path') . $customer->customer_photo_file) ? ($customer->customer_photo_file . '?v=' . $settings->customer_app_img_version) : (Config::get('values.customer_avatar_default_file') . '?v=' . $settings->customer_app_img_version)) : (Config::get('values.customer_avatar_default_file') . '?v=' . $settings->customer_app_img_version));
                                $response['UserDetails']['token'] = $customer->oauth_token;
                                $response['UserDetails']['country_code'] = '+971';
                                $response['UserDetails']['mobile'] = $customer->mobile_number_1;
                                $response['UserDetails']['default_address_id'] = $customer->customer_address_id;
                                DB::commit();
                                return Response::json(array('result' => $response), 200, array(), JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES);
                            } else {
                                throw new \ErrorException('Invalid or Expired OTP !');
                            }
                        } else {
                            throw new \ErrorException('Mobile number change request not found !');
                        }
                    }
                } else {
                    if ($customer) {
                        if ($input['otp'] == $customer->mobile_verification_code && $customer->login_otp_expired_at >= date('Y-m-d H:i:s')) {
                            $customer->mobile_verification_code = null;
                            $customer->login_otp_expired_at = null;
                            $customer->oauth_token = Hash::make(Str::random(40));
                            if (@$input['fcm']) {
                                $customer->push_token = $input['fcm'];
                            }
                            if (@$input['device_type']) {
                                $customer->device_type = $input['device_type'];
                            }
                            $customer->email_verified_at = $customer->email_verified_at ?: Carbon::now();
                            $customer->save();
                            $response['status'] = 'success';
                            $response['message'] = 'Logged in successfully.';
                            $response['UserDetails']['id'] = $customer->customer_id;
                            $response['UserDetails']['UserName'] = $customer->customer_name;
                            $response['UserDetails']['email'] = $customer->email_address;
                            $response['UserDetails']['dob'] = $customer->dob;
                            $response['UserDetails']['gender_id'] = $customer->gender_id;
                            $response['UserDetails']['image'] = Config::get('values.customer_avatar_prefix_url') . (($customer->customer_photo_file != '') ? (File::exists(Config::get('values.customer_avatar_storage_path') . $customer->customer_photo_file) ? ($customer->customer_photo_file . '?v=' . $settings->customer_app_img_version) : (Config::get('values.customer_avatar_default_file') . '?v=' . $settings->customer_app_img_version)) : (Config::get('values.customer_avatar_default_file') . '?v=' . $settings->customer_app_img_version));
                            $response['UserDetails']['token'] = $customer->oauth_token;
                            $response['UserDetails']['country_code'] = '+971';
                            $response['UserDetails']['mobile'] = @$input['mobilenumber'];
                            $response['UserDetails']['default_address_id'] = $customer->customer_address_id;
                            DB::commit();
                            return Response::json(array('result' => $response), 200, array(), JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES);
                        } else {
                            throw new \ErrorException('Invalid or Expired OTP !');
                        }
                    } else {
                        throw new \ErrorException('Customer not found.');
                    }
                }
            }
        } catch (\Exception $e) {
            DB::rollback();
            return Response::json(array('result' => array('status' => 'failed', 'message' => $e->getMessage())), 200, array(), customerResponseJsonConstants());
        }
    }
    public function resend_otp(Request $request)
    {
        $data = json_decode($request->getContent(), true);
        Log::channel('debug')->info(json_encode(@$data ?: []));
        // required input check
        $input = @$data['params'];
        $input['mobilenumber'] = substr(@$input['mobilenumber'], -9); // get last 9 digits only
        $input['new_mobilenumber'] = substr(@$input['new_mobilenumber'], -9); // get last 9 digits only
        $validator = Validator::make(
            (array) $input,
            [
                'id' => 'required|exists:customers,customer_id',
                'type' => 'required|in:mobile,email',
                'mobilenumber' => 'required|numeric|digits:9',
                'new_mobilenumber' => 'nullable|numeric|digits:9',
            ],
            [],
            [
                'id' => 'Customer ID',
                'type' => 'Login Type',
                'mobilenumber' => 'Mobile Number',
                'new_mobilenumber' => 'New Mobile Number',
            ]
        );
        if ($validator->fails()) {
            return Response::json(array('result' => array('status' => 'failed', 'message' => $validator->errors()->first())), 200, [], customerResponseJsonConstants());
        }
        /************************************************************* */
        try {
            DB::beginTransaction();
            if ($input['type'] == 'mobile') {
                $customer = Customer::where('mobile_number_1', 'like', '%' . $input['mobilenumber'])
                    ->where(function ($query) {
                        $query->where('customer_status', '=', 1)
                            ->orWhere('customer_status', '=', 0);
                    })
                    ->orderBy('customer_status', 'desc') // for picking customer_status = 1 first (if multiple customer with same number exist in db pick the active one)
                    ->first();
                if (!$customer) {
                    throw new \ErrorException('Customer not found.');
                }
                $startTime = Carbon::now();
                if (@$input['new_mobilenumber']) {
                    $customer_temp = CustomersTemp::where('customer_id', '=', $customer->customer_id)->where('mobile_number_1', 'like', '%' . $input['new_mobilenumber'])->first();
                    if ($customer_temp) {
                        // check and resend otp
                        $finishTime = Carbon::parse($customer_temp->mobile_number_1_otp_expired_at);
                        $seconds = $finishTime->diffInSeconds($startTime);
                        if ($customer_temp->mobile_number_1_otp_expired_at > Carbon::now() && $seconds >= 1) {
                            throw new \ErrorException('Please wait ' . $seconds . ' second' . ($seconds > 0 ? 's' : '') . ' before resend OTP.');
                        } else {
                            $otp = generate_mobile_otp($input['mobilenumber']);
                            send_customer_login_otp($input['mobilenumber'], $otp);
                            $customer_temp->mobile_number_1_otp = $otp;
                            $customer_temp->mobile_number_1_otp_expired_at = Carbon::now()->addSeconds(Config::get('values.customer_login_otp_expire_seconds'));
                            $customer_temp->save();
                            $response['status'] = 'success';
                            $response['message'] = 'OTP resend successfully!';
                            $response['UserDetails']['country_code'] = '+971';
                            $response['UserDetails']['mobile'] = $input['new_mobilenumber'];
                        }
                    } else {
                        throw new \ErrorException('Mobile number change request not found !');
                    }
                } else {
                    $finishTime = Carbon::parse($customer->login_otp_expired_at);
                    $seconds = $finishTime->diffInSeconds($startTime);
                    if ($customer->login_otp_expired_at > Carbon::now() && $seconds >= 1) {
                        throw new \ErrorException('Please wait ' . $seconds . ' second' . ($seconds > 0 ? 's' : '') . ' before resend OTP.');
                    }
                    $otp = generate_mobile_otp($input['mobilenumber']);
                    send_customer_login_otp($input['mobilenumber'], $otp);
                    $customer->mobile_verification_code = $otp;
                    $customer->login_otp_expired_at = Carbon::now()->addSeconds(Config::get('values.customer_login_otp_expire_seconds'));
                    $customer->save();
                    $response['status'] = 'success';
                    $response['message'] = 'OTP resend successfully!';
                    $response['UserDetails']['country_code'] = '+971';
                    $response['UserDetails']['mobile'] = $input['mobilenumber'];
                }
            } else if ($input['type'] == 'email') {
                /************************************************************* */
                $customer = Customer::where('customer_id', '=', $input['id'])
                    ->where(function ($query) {
                        $query->where('customer_status', '=', 1)
                            ->orWhere('customer_status', '=', 0);
                    })
                    ->orderBy('customer_status', 'desc') // for picking customer_status = 1 first (if multiple customer with same number exist in db pick the active one)
                    ->firstOrFail();
                /************************************************************* */
                // EXISTING CUSTOMER FOUND
                $customer->mobile_verification_code = generate_email_otp($customer->email_address);
                $customer->login_otp_expired_at = Carbon::now()->addSeconds(Config::get('values.customer_login_otp_expire_seconds'));
                $customer->save();
                DB::commit();
                $response['status'] = 'success';
                $response['message'] = 'OTP resend successfully!';
                $response['signinup_status'] = $customer->customer_name == null ? "new signup" : "already signup";
                $response['UserDetails']['country_code'] = null;
                $response['UserDetails']['mobile'] = $customer->mobile_number_1;
                $response['UserDetails']['email'] = $customer->email_address;
                $response['UserDetails']['id'] = $customer->customer_id;
                $mail = new AdminApiMailController;
                $mail->login_otp_to_customer($input['id'], $customer->mobile_verification_code);
            }
            DB::commit();
            return Response::json(array('result' => $response), 200, [], customerResponseJsonConstants());
        } catch (\Exception $e) {
            DB::rollback();
            return Response::json(array('result' => array('status' => 'failed', 'message' => $e->getMessage())), 200, [], customerResponseJsonConstants());
        }
    }
}
