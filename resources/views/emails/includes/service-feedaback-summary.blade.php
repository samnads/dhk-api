<div style="width: 600px; height: auto; margin: 0px 0px 15px 0px;">
    <table width="100%" cellpadding="0" cellspacing="0">
        @if(@$day_service->rating > 0)
        <tr>
            <td style="border: 1px solid #ddd; border-right: 0px">
                <p
                    style="font: normal 14px/18px 'Poppins', sans-serif; color: #555; display: block; margin: 0px; padding: 15px 20px; text-align: left; -webkit-text-size-adjust: 100%; -moz-text-size-adjust: 100%; -ms-text-size-adjust: 100%;">
                    Staff<br />
                    <label
                        style="font-size: 18px; line-height: 25px; display: block; margin: 0px; padding: 2px 0px 0px;"></label>
                </p>
            </td>
            <td style="border: 1px solid #ddd;" colspan="2">
                <p
                    style="font: normal 14px/18px 'Poppins', sans-serif; color: #555; display: block; margin: 0px; padding: 15px 20px; text-align: center; -webkit-text-size-adjust: 100%; -moz-text-size-adjust: 100%; -ms-text-size-adjust: 100%;">
                    {{ @$day_service->maid_name }}
                </p>
            </td>
        </tr>
        <tr>
            <td style="border: 1px solid #ddd; border-right: 0px">
                <p
                    style="font: normal 14px/18px 'Poppins', sans-serif; color: #555; display: block; margin: 0px; padding: 15px 20px; text-align: left; -webkit-text-size-adjust: 100%; -moz-text-size-adjust: 100%; -ms-text-size-adjust: 100%;">
                    Rating<br />
                    <label
                        style="font-size: 18px; line-height: 25px; display: block; margin: 0px; padding: 2px 0px 0px;">{{ @$frequency->name }}</label>
                </p>
            </td>
            <td style="border: 1px solid #ddd;" colspan="2">
                <p
                    style="font: normal 14px/18px 'Poppins', sans-serif; color: #555; display: block; margin: 0px; padding: 15px 20px; text-align: center; -webkit-text-size-adjust: 100%; -moz-text-size-adjust: 100%; -ms-text-size-adjust: 100%;">
                    <img src="{{ Config::get('values.file_server_url').'images/email/service-rating/Star-'.@$day_service->rating.'.png?v='.$settings->customer_app_img_version }}" width="120px" height="" alt="" style="border-radius: 5px; cursor: default !important; overflow: hidden; " />
                </p>
            </td>
        </tr>
        @endif
        @if(@$day_service->comments)
        <tr>
            <td style="border: 1px solid #ddd;" colspan="3">
                <p
                    style="font: normal 14px/18px 'Poppins', sans-serif; color: #555; display: block; margin: 0px; padding: 15px 20px; text-align: left; -webkit-text-size-adjust: 100%; -moz-text-size-adjust: 100%; -ms-text-size-adjust: 100%;">
                    Comments<br />
                    <label
                        style="font-size: 18px; line-height: 25px; display: block; margin: 0px; padding: 2px 0px 0px;color: #c40000;">{{ $day_service->comments }}</label>
                </p>
            </td>
        </tr>
        @endif
    </table>
</div>
