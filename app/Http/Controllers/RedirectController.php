<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use App\Models\Customer;
use App\Models\CustomerAddress;
use Config;
use Session;
use Response;

class RedirectController extends Controller
{
    public function redirect_based_on_action(Request $request, $action)
    {
        if($action == 'telr-gateway-entry-for-booking-payment'){
            $platform = $request['platform'];
            $booking_id = $request['booking_id'];
            $bookings = DB::table('bookings as b')
                ->select(
                    'b.*',
                    'bsp.id as booking_subscription_package_id',
                    'b._total_payable',
                )
                /*************************************************************/
                ->leftJoin('booking_subscription_packages as bsp', function ($join) {
                    $join->on('b.booking_id', '=', 'bsp.booking_id');
                    $join->whereColumn('b.booking_id', 'bsp.booking_id');
                })
                /*************************************************************/
                ->where(function ($query) use ($booking_id) {
                    $query->where('b.booking_common_id', $booking_id); // multiple maids
                    $query->orWhere('b.booking_id', $booking_id);
                })
                ->get();
            $customer = Customer::where('customer_id', '=', $bookings[0]->customer_id)->first();
            $customer_address = CustomerAddress::where(['customer_address_id' => $bookings[0]->customer_address_id])->first();
            if($request['applepay'] == '1'){
                $response['telr_data'] = telrCreateOrderApplePay($bookings, $customer, $customer_address, $bookings[0]->_total_payable, $platform);
            }
            else{
                $response['telr_data'] = telrCreateOrder($bookings, $customer, $customer_address, $bookings[0]->_total_payable, $platform);
            }
            // here using the web app url to store some session data in web for success or failed url purposes
            return redirect($response['telr_data']['order']['url']);
            //return redirect(Config::get('values.web_app_url').'redirect/telr-gateway-entry?url='. $response['telr_data']['order']['url']);
        }
    }
}
