<?php
return [
    'company_name' => env('COMPANY_NAME', "Default Company Name"),
    'customer_avatar_prefix_url' => env('CUSTOMER_AVATAR_PREFIX_URL', "http://example.com/customer_images/"),
    'customer_avatar_default_file' => env('CUSTOMER_AVATAR_DEFAULT_FILE', "default.png"),
    'customer_avatar_storage_path' => env('CUSTOMER_AVATAR_STORAGE_PATH', "undefined"),
    'service_type_img_prefix_url' => env('SERVICE_TYPE_IMG_PREFIX_URL', "http://example.com/service_images/"),
    'maid_avatar_prefix_url' => env('MAID_AVATAR_PREFIX_URL', "http://example.com/maid_images/"),
    'maid_avatar_default_file' => env('MAID_AVATAR_DEFAULT_FILE', "default.png"),
    'special_offer_banner_prefix_url' => env('SPECIAL_OFFER_BANNER_PREFIX_URL', "http://example.com/offer_images/"),
    'package_offer_thumbnail_prefix_url' => env('PACKAGE_OFFER_THUMBNAIL_PREFIX_URL', "http://example.com/uploads/images/package-offers/"),
    'service_addons_img_prefix_url' => env('SERVICE_ADDONS_IMG_PREFIX_URL', 'test'),
    'service_type_category_icon_prefix_url' => env('SERVICE_TYPE_CATEGORY_ICON_PREFIX_URL', "http://example.com/service_type_category_icon_prefix_url/"),
    'service_type_thumbnail_prefix_url' => env('SERVICE_TYPE_THUMBNAIL_PREFIX_URL', "http://example.com/service_type_thumbnail_prefix_url/"),
    'customer_app_assets_prefix_url' => env('CUSTOMER_APP_ASSETS_PREFIX_URL', "customer_app_assets_prefix_url"),
    'currency_code' => env('CURRENCY_CODE', ""),
    'language_code' => env('LANGUAGE_CODE', ""),
    'vat_percentage' => env('VAT_PERCENTAGE', 0),
    //
    'booking_slot_start' => env('BOOKING_SLOT_START'),
    'min_working_hours' => env('MIN_WORKING_HOURS'),
    'booking_slot_end' => env('BOOKING_SLOT_END'),
    'work_end_time' => env('WORK_END_TIME'),
    'customer_login_otp_expire_seconds' => (int) env('CUSTOMER_LOGIN_OTP_EXPIRE_SECONDS', 60),
    // payfort
    'payfort_environment' => env('PAYFORT_ENVIRONMENT'),
    'payfort_api_url' => env('PAYFORT_ENVIRONMENT') == "LIVE" ? env('PAYFORT_API_URL_LIVE') : env('PAYFORT_API_URL_SANDBOX'),
    'payfort_payment_page_url' => env('PAYFORT_ENVIRONMENT') == "LIVE" ? env('PAYFORT_PAYMENT_PAGE_URL_LIVE') : env('PAYFORT_PAYMENT_PAGE_URL_SANDBOX'),
    //
    'payfort_access_code' => env('PAYFORT_ENVIRONMENT') == "LIVE" ? env('PAYFORT_ACCESS_CODE_LIVE') : env('PAYFORT_ACCESS_CODE_SANDBOX'),
    'payfort_merchant_identifier' => env('PAYFORT_ENVIRONMENT') == "LIVE" ? env('PAYFORT_MERCHANT_IDENTIFIER_LIVE') : env('PAYFORT_MERCHANT_IDENTIFIER_SANDBOX'),
    'payfort_sha_request_phrase' => env('PAYFORT_ENVIRONMENT') == "LIVE" ? env('PAYFORT_SHA_REQUEST_PHRASE_LIVE') : env('PAYFORT_SHA_REQUEST_PHRASE_SANDBOX'),
    'payfort_sha_response_phrase' => env('PAYFORT_ENVIRONMENT') == "LIVE" ? env('PAYFORT_SHA_RESPONSE_PHRASE_LIVE') : env('PAYFORT_SHA_RESPONSE_PHRASE_SANDBOX'),
    // debug
    'debug_customer_id' => env('DEBUG_CUSTOMER_ID', 0),
    'debug_customer_address_id' => env('DEBUG_CUSTOMER_ADDRESS_ID', 0),
    //
    'google_fcm_server_key' => env('GOOGLE_FCM_SERVER_KEY', "google_fcm_server_key"),
    'booking_ref_prefix' => env('BOOKING_REF_PREFIX'),
    'subscription_package_booking_ref_prefix' => env('SUBSCRIPTION_PACKAGE_BOOKING_REF_PREFIX'),
    //
    'file_server_url' => env('FILE_SERVER_URL'),
    'web_app_url' => env('WEB_APP_URL'),
    //
    'checkout_primary_key' => env('CHECKOUT_ENVIRONMENT') == "LIVE" ? env('CHECKOUT_PRIMARY_KEY') : env('CHECKOUT_PRIMARY_KEY_SANDBOX'),
    'checkout_secret_key' => env('CHECKOUT_ENVIRONMENT') == "LIVE" ? env('CHECKOUT_SECRET_KEY') : env('CHECKOUT_SECRET_KEY_SANDBOX'),
    'checkout_channel_id' => env('CHECKOUT_ENVIRONMENT') == "LIVE" ? env('CHECKOUT_CHANNEL_ID') : env('CHECKOUT_CHANNEL_ID_SANDBOX'),
    'checkout_service_url' => env('CHECKOUT_ENVIRONMENT') == "LIVE" ? env('CHECKOUT_SERVICE_URL') : env('CHECKOUT_SERVICE_URL_SANDBOX'),
    'checkout_token_url' => env('CHECKOUT_ENVIRONMENT') == "LIVE" ? env('CHECKOUT_TOKEN_URL') : env('CHECKOUT_TOKEN_URL_SANDBOX'),
    //
    'tamara_api_token' => env('TAMARA_ENVIRONMENT') == "LIVE" ? env('TAMARA_API_TOKEN') : env('TAMARA_API_TOKEN_SANDBOX'),
    'tamara_base_url' => env('TAMARA_ENVIRONMENT') == "LIVE" ? env('TAMARA_BASE_URL') : env('TAMARA_BASE_URL_SANDBOX'),
    //
    'telr_auth_key' => env('TELR_AUTH_KEY'),
    'telr_store_id' => env('TELR_STORE_ID'),
    'telr_test_environment' => env('TELR_TEST_ENVIRONMENT'),
    // mobile numbers
    'mobile_number_call' => env('MOBILE_NUMBER_CALL'),
    'mobile_number_whatsapp' => env('MOBILE_NUMBER_WHATSAPP'),
]
?>
