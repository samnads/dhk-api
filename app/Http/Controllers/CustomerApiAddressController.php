<?php

namespace App\Http\Controllers;

use App\Models\CustomerAddress;
use Config;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;
use Response;
use stdClass;

class CustomerApiAddressController extends Controller
{
    public function add_address(Request $request)
    {
        $debug = toggleDebug(); // pass boolean to overide default
        /************************************************************* */
        if (!$debug) {
            // live input
            $data = json_decode($request->getContent(), true);
        } else {
            // test input
            $data['params']['id'] = Config::get('values.debug_customer_id');
            $data['params']['street'] = "Str......";
            $data['params']['building'] = "Olx Buildings";
            $data['params']['flat_no'] = "F1";
            $data['params']['latitude'] = 25.09129863;
            $data['params']['longitude'] = 75.14367269;
            $data['params']['area_id'] = 1;
            $data['params']['address_type'] = "OF";
        }
        /************************************************************* */
        // required input check
        $input = @$data['params'];
        $validator = Validator::make(
            (array) $input,
            [
                'street' => 'required|string',
                'building' => 'required|string',
                'flat_no' => 'required|string',
                'latitude' => 'nullable|numeric',
                'longitude' => 'nullable|numeric',
                'area_id' => 'required|numeric',
                'address_type' => 'nullable|in:HO,OF,OT',
            ],
            [],
            [
                'street' => 'Address',
                'building' => 'Building',
                'flat_no' => 'Unit No.',
                'latitude' => 'Latitude',
                'longitude' => 'Longitude',
                'area_id' => 'Area ID',
                'address_type' => 'Address Type',
            ]
        );
        if ($validator->fails()) {
            return Response::json(array('result' => array('status' => 'failed', 'message' => $validator->errors()->first()), 'debug' => $debug), 200, array(), customerResponseJsonConstants());
        }
        /************************************************************* */
        $default_address = CustomerAddress::where('customer_id', $input['id'])->where('address_status', 0)->where('default_address', 1)->first(); // check already have default address
        if (!$default_address) {
            CustomerAddress::where('customer_id', $input['id'])->update(['default_address' => 0]);
        }
        /************************************************************* */
        DB::beginTransaction();
        try {
            $response['status'] = 'success';
            $row = new stdClass();
            $row->customer_id = $input['id'];
            $row->area_id = $input['area_id'];
            $row->other_area = null;
            $row->customer_address = $input['flat_no'] . ', ' . $input['building'] . ', ' . $input['street'];
            $row->latitude = @$input['latitude'];
            $row->longitude = @$input['longitude'];
            $row->building = $input['building'];
            $row->unit_no = $input['flat_no'];
            $row->street = $input['street'];
            $row->address_category = $input['address_type'] ?: 'OT';
            if (!$default_address) {
                // currently no default address, so make this default
                $row->default_address = 1;
            } else {
                $row->default_address = 0;
            }
            $id = CustomerAddress::insertGetId((array) $row);
            DB::commit();
            $response['message'] = 'Address saved successfully.';
            return Response::json(array('result' => $response, 'debug' => $debug), 200, array(), customerResponseJsonConstants());
        } catch (\Exception $e) {
            DB::rollback();
            return Response::json(array('result' => array('status' => 'failed', 'message' => $e->getMessage()), 'debug' => $debug), 200, array(), customerResponseJsonConstants());
        }
    }
    public function edit_address(Request $request)
    {
        $debug = toggleDebug(); // pass boolean to overide default
        /************************************************************* */
        if (!$debug) {
            // live input
            $data = json_decode($request->getContent(), true);
        } else {
            // test input
            $data['params']['id'] = Config::get('values.debug_customer_id');
            $data['params']['address_id'] = Config::get('values.debug_customer_address_id');
            $data['params']['street'] = "Str......";
            $data['params']['building'] = "Olx Buildingsfdffdf";
            $data['params']['flat_no'] = "F1";
            $data['params']['latitude'] = 25.09129863;
            $data['params']['longitude'] = 75.14367269;
            $data['params']['area_id'] = 12;
            $data['params']['address_type'] = "OF";
        }
        /************************************************************* */
        // required input check
        $input = @$data['params'];
        $validator = Validator::make(
            (array) $input,
            [
                'address_id' => 'required|integer',
                'street' => 'required|string',
                'building' => 'required|string',
                'flat_no' => 'required|string',
                'latitude' => 'nullable|numeric',
                'longitude' => 'nullable|numeric',
                'area_id' => 'required|integer',
                'address_type' => 'required|in:HO,OF,OT',
            ],
            [],
            [
                'address_id' => 'Address ID',
                'street' => 'Address',
                'building' => 'Building',
                'flat_no' => 'Flat No.',
                'latitude' => 'Latitude',
                'longitude' => 'Longitude',
                'area_id' => 'Area ID',
                'address_type' => 'Address Type',
            ]
        );
        if ($validator->fails()) {
            return Response::json(array('result' => array('status' => 'failed', 'message' => $validator->errors()->first()), 'debug' => $debug), 200, array(), customerResponseJsonConstants());
        }
        /************************************************************* */
        $customer_address = CustomerAddress::where(['customer_address_id' => $input['address_id'], 'customer_id' => $input['id'], 'address_status' => 0])->first();
        if (!$customer_address) {
            // validate address
            return Response::json(array('result' => array('status' => 'failed', 'message' => 'Invalid address !'), 'debug' => $debug), 200, array(), customerResponseJsonConstants());
        }
        /************************************************************* */
        $default_address = CustomerAddress::where('customer_id', $input['id'])->where('address_status', 0)->where('default_address', 1)->first(); // check already have default address
        if (!$default_address) {
            $customer_address->default_address = 1;
        }
        /************************************************************* */
        DB::beginTransaction();
        try {
            $response['status'] = 'success';
            $customer_address->area_id = $input['area_id'];
            $customer_address->other_area = null;
            $customer_address->customer_address = $input['flat_no'] . ', ' . $input['building'] . ', ' . $input['street'];
            $customer_address->latitude = @$input['latitude'];
            $customer_address->longitude = @$input['longitude'];
            $customer_address->building = $input['building'];
            $customer_address->unit_no = $input['flat_no'];
            $customer_address->street = $input['street'];
            $customer_address->address_category = $input['address_type'];
            $customer_address->save();
            DB::commit();
            $response['message'] = 'Address updated successfully.';
            return Response::json(array('result' => $response, 'debug' => $debug), 200, array(), customerResponseJsonConstants());
        } catch (\Exception $e) {
            DB::rollback();
            return Response::json(array('result' => array('status' => 'failed', 'message' => $e->getMessage()), 'debug' => $debug), 200, array(), customerResponseJsonConstants());
        }
    }
    public function delete_address(Request $request)
    {
        $debug = toggleDebug(); // pass boolean to overide default
        /************************************************************* */
        if (!$debug) {
            // live input
            $data = json_decode($request->getContent(), true);
        } else {
            // test input
            $data['params']['id'] = Config::get('values.debug_customer_id');
            $data['params']['address_id'] = 4059;
        }
        /************************************************************* */
        // required input check
        $input = @$data['params'];
        $validator = Validator::make(
            (array) $input,
            [
                'address_id' => 'required|integer',
            ],
            [],
            [
                'address_id' => 'Address ID',
            ]
        );
        if ($validator->fails()) {
            return Response::json(array('result' => array('status' => 'failed', 'message' => $validator->errors()->first()), 'debug' => $debug), 200, array(), customerResponseJsonConstants());
        }
        /************************************************************* */
        $customer_address = CustomerAddress::where(['customer_address_id' => $input['address_id'], 'customer_id' => $input['id'], 'address_status' => 0])->first();
        if (!$customer_address) {
            // validate address
            return Response::json(array('result' => array('status' => 'failed', 'message' => 'Invalid address !'), 'debug' => $debug), 200, array(), customerResponseJsonConstants());
        }
        /************************************************************* */
        DB::beginTransaction();
        try {
            $response['status'] = 'success';
            //$customer_address->updated_at = Carbon::now();
            //$customer_address->deleted_at = Carbon::now();
            $customer_address->address_status = 1;
            $customer_address->save();
            DB::commit();
            $response['message'] = 'Address deleted successfully.';
            return Response::json(array('result' => $response, 'debug' => $debug), 200, array(), customerResponseJsonConstants());
        } catch (\Exception $e) {
            DB::rollback();
            return Response::json(array('result' => array('status' => 'failed', 'message' => $e->getMessage()), 'debug' => $debug), 200, array(), customerResponseJsonConstants());
        }
    }
    public function set_default_address(Request $request)
    {
        $debug = toggleDebug(); // pass boolean to overide default
        /************************************************************* */
        if (!$debug) {
            // live input
            $data = json_decode($request->getContent(), true);
        } else {
            // test input
            $data['params']['id'] = Config::get('values.debug_customer_id');
            $data['params']['address_id'] = Config::get('values.debug_customer_address_id');
        }
        /************************************************************* */
        // required input check
        $input = @$data['params'];
        $validator = Validator::make(
            (array) $input,
            [
                'address_id' => 'required|integer',
            ],
            [],
            [
                'address_id' => 'Address ID',
            ]
        );
        if ($validator->fails()) {
            return Response::json(array('result' => array('status' => 'failed', 'message' => $validator->errors()->first()), 'debug' => $debug), 200, array(), customerResponseJsonConstants());
        }
        /************************************************************* */
        try {
            DB::beginTransaction();
            $default_address = CustomerAddress::where([['customer_id', "=", $input['id']], ['customer_address_id', "=", $input['address_id']]])->first();
            if (!$default_address) {
                throw new \ErrorException('Address not exist !');
            }
            $default_address->default_address = 1;
            $default_address->save();
            CustomerAddress::where([['customer_id', "=", $input['id']], ['customer_address_id', "!=", $input['address_id']]])->update(array('default_address' => 0)); // remove old default
            $address_list = DB::table('customer_addresses as ca')
                ->select(
                    'ca.customer_address_id as address_id',
                    'ca.street as street',
                    'ca.building as building',
                    DB::raw('ca.unit_no as flat_no'),
                    'ca.latitude as lat',
                    'ca.longitude as long',
                    'ca.default_address',
                    'z.zone_id'
                )
                ->leftJoin('areas as a', 'ca.area_id', 'a.area_id')
                ->leftJoin('zones as z', 'a.zone_id', 'z.zone_id')
                ->where([['ca.customer_id', '=', $input['id']], ['ca.address_status', '=', 0]])
                ->orderBy('ca.customer_address_id', 'DESC')
                ->get();
            $response['address_list'] = $address_list;
            DB::commit();
            $response['status'] = 'success';
            $response['message'] = 'Default address has been changed.';
            return Response::json(array('result' => $response, 'debug' => $debug), 200, array(), customerResponseJsonConstants());
        } catch (\Exception $e) {
            DB::rollback();
            return Response::json(array('result' => array('status' => 'failed', 'message' => $e->getMessage()), 'debug' => $debug), 200, array(), customerResponseJsonConstants());
        }
    }
    public function address_list(Request $request)
    {
        $debug = toggleDebug(); // pass boolean to overide default
        /************************************************************* */
        if (!$debug) {
            // live input
            $data = json_decode($request->getContent(), true);
        } else {
            // test input
            $data['params']['id'] = Config::get('values.debug_customer_id');
        }
        /************************************************************* */
        $input = @$data['params'];
        /************************************************************* */
        $default_address = CustomerAddress::where('customer_id', $input['id'])->where('address_status', 0)->where('default_address', 1)->first(); // check already have default address
        $response['status'] = 'success';
        $address_list = DB::table('customer_addresses as ca')
            ->select(
                'ca.customer_address_id as address_id',
                'ca.street as street',
                'ca.building as building',
                DB::raw('ca.unit_no as flat_no'),
                'ca.latitude as lat',
                'ca.longitude as long',
                'ca.default_address',
                'z.zone_id',
                'a.area_id',
                'z.zone_name',
                'a.area_name',
                DB::raw('IFNULL(ca.address_category,"OT") as address_type'),
            )
            ->leftJoin('areas as a', 'ca.area_id', 'a.area_id')
            ->leftJoin('zones as z', 'a.zone_id', 'z.zone_id')
            ->where([['ca.customer_id', '=', $input['id']], ['ca.address_status', '=', 0]])
            ->orderBy('ca.customer_address_id', 'DESC')
            ->get();
        if(sizeof($address_list) && !$default_address){
            // if no default address make one
            $address = CustomerAddress::find($address_list[0]->address_id);
            $address->default_address = 1;
            $address->save();
            $address_list[0]->default_address = 1;
        }
        $address_type_names = ['HO' => 'Home', 'OF' => 'Office', 'OT' => 'Other'];
        foreach ($address_list as $key => $address) {
            $address_list[$key]->address_type_name = $address_type_names[$address->address_type];
        }
        $response['address_list'] = $address_list;
        $response['message'] = sizeof($response['address_list']) ? 'Address list fetched successfully.' : "No address found.";
        return Response::json(array('result' => $response, 'debug' => $debug), 200, array(), customerResponseJsonConstants());
    }
}
