<?php

namespace App\Http\Middleware;

use Closure;
use Illuminate\Http\Request;
use Response;

class ValidateJsonInput
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse|\Illuminate\Http\JsonResponse
     */
    public function handle(Request $request, Closure $next)
    {
        $data = $request->getContent();
        if ($data == null) {
            return $next($request);
        }
        json_decode($data);
        if (json_last_error() === JSON_ERROR_NONE) {
            return $next($request);
        } else {
            return Response::json(array('result' => array('status' => 'failed', 'message' => 'Invalid request data !')), 200, array(), customerResponseJsonConstants());
        }
    }
}
