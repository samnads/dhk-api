<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Response;
use Config;

class TelrPaymentController extends Controller
{
    public function process_order(Request $request)
    {
        $data = json_decode($request->getContent(), true);
        DB::beginTransaction();
        try {
            $input = $data['params'];
            /*************************************************** */
            // process order
            $response['telr_data'] = telrCheckOrder($request);
            if (@$response['telr_data']['order']['status']['code'] == 3) {
                /**
                 * transaction status success
                 */
                afterTelrtPaymentSuccess($input['booking_id'], $response['telr_data'], $input['platform']);
            } else {
                throw new \ErrorException('Transaction failed');
            }
            /*************************************************** */
            $response['status'] = 'success';
            $response['input'] = $data['params'];
            $response['message'] = 'Payment completed successfully.';
            DB::commit();
            return Response::json(array('result' => $response), 200, array(), JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES);
        } catch (\Exception $e) {
            DB::rollback();
            return Response::json(array('result' => array('status' => 'failed', 'message' => $e->getMessage())), 200, array(), JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES);
        }
    }
    public function process_order_url(Request $request, $booking_ref, $booking_id)
    {
        DB::beginTransaction();
        try {
            /*************************************************** */
            // process order
            $response['telr_data'] = telrCheckOrderUrl($request);
            if (@$response['telr_data']['order']['status']['code'] == 3) {
                /**
                 * transaction status success
                 */
                afterTelrtPaymentSuccess($booking_id, $response['telr_data'], $request->platform);
            } else {
                throw new \ErrorException('Transaction failed');
            }
            /*************************************************** */
            $response['status'] = 'success';
            $response['message'] = 'Payment completed successfully.';
            DB::commit();
            return redirect(Config::get('values.web_app_url').'booking/success/' . $booking_ref . '?reference_id=' . $booking_ref . '&payment_method=Card&status=Confirmed');
        } catch (\Exception $e) {
            DB::rollback();
            return redirect(Config::get('values.web_app_url').'booking/failed/' . $booking_ref . '?reference_id=' . $booking_ref . '&payment_method=Card&status=Declined');
        }
    }
    public function invoice_process(Request $request)
    {
        $data = json_decode($request->getContent(), true);
        DB::beginTransaction();
        try {
            $input = $data['params'];
            /*************************************************** */
            // process order
            $response['telr_data'] = telrCheckOrder($request);
            if (@$response['telr_data']['order']['status']['code'] == 3) {
                /**
                 * transaction status success
                 */
                afterTelrtInvoicePaymentSuccess($input['payment_id'], $response['telr_data'], $input['platform']);
            } else {
                throw new \ErrorException('Transaction failed');
            }
            /*************************************************** */
            $response['status'] = 'success';
            $response['input'] = $data['params'];
            $response['message'] = 'Invoice Payment completed successfully.';
            DB::commit();
            return Response::json(array('result' => $response), 200, array(), JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES);
        } catch (\Exception $e) {
            DB::rollback();
            return Response::json(array('result' => array('status' => 'failed', 'message' => $e->getMessage())), 200, array(), JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES);
        }
    }
    public function online_payment_process(Request $request)
    {
        $data = json_decode($request->getContent(), true);
        DB::beginTransaction();
        try {
            $input = $data['params'];
            /*************************************************** */
            // process order
            $response['telr_data'] = telrCheckOrder($request);
            if (@$response['telr_data']['order']['status']['code'] == 3) {
                /**
                 * transaction status success
                 */
                afterTelrtOnlinePaymentSuccess($input['payment_id'], $response['telr_data'], $input['platform']);
            } else {
                throw new \ErrorException('Transaction failed');
            }
            /*************************************************** */
            $response['status'] = 'success';
            $response['input'] = $data['params'];
            $response['message'] = 'Invoice Payment completed successfully.';
            DB::commit();
            return Response::json(array('result' => $response), 200, array(), JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES);
        } catch (\Exception $e) {
            DB::rollback();
            return Response::json(array('result' => array('status' => 'failed', 'message' => $e->getMessage())), 200, array(), JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES);
        }
    }
    public function telr_test(Request $request)
    {
        try {
            $apple_json = '{
                "shippingContact": {
                    "addressLines": [
                    "UAE",
                    "12 A"
                    ],
                    "administrativeArea": "",
                    "country": "United Arab Emirates",
                    "countryCode": "AE",
                    "familyName": "N",
                    "givenName": "nayana",
                    "locality": "Uae",
                    "phoneticFamilyName": "",
                    "phoneticGivenName": "",
                    "postalCode": "",
                    "subAdministrativeArea": "",
                    "subLocality": "Uae"
                },
                "token": {
                    "paymentData": {
                    "data": "VxEk0NBo/S+XDpd3G9IMfCB/UHxB+aS/DQH0JQmc5WjD+DqmIHOAiBY1GWvNpGzspzDuXFwvnjLJqwEPbHI06rSYXjiC71pNRskb6aknHgNcXimG1x+GbI2TQUQnFqsNPHzKdf8ZpS3297EcbmTDP54pxP3WNUHWHHN1/RwY5ChgxHpX7sKY/CUXq9A+vRUovh+U8/buz0ciMhvMtMlL5o6Kax/dF4VYwAsZmZm357BIxTPA05rgvcepJlomBOnosy2RzYJ61KOav2TKYjwnGEQ16toubRupeOKYFLfYtOPvOLmZahFdOnA3GlkwV/tIE8zcRhCVYIFQwg8daxhewHfhuz3/xaCd0wKurWbDKHGFimd5fWyh+5/K0yGJL7Yj0/95pl0l7aifqXjebCUuwVm4oc0LDt5NOb/5AYhypkw=",
                    "signature": "MIAGCSqGSIb3DQEHAqCAMIACAQExDTALBglghkgBZQMEAgEwgAYJKoZIhvcNAQcBAACggDCCA+MwggOIoAMCAQICCEwwQUlRnVQ2MAoGCCqGSM49BAMCMHoxLjAsBgNVBAMMJUFwcGxlIEFwcGxpY2F0aW9uIEludGVncmF0aW9uIENBIC0gRzMxJjAkBgNVBAsMHUFwcGxlIENlcnRpZmljYXRpb24gQXV0aG9yaXR5MRMwEQYDVQQKDApBcHBsZSBJbmMuMQswCQYDVQQGEwJVUzAeFw0xOTA1MTgwMTMyNTdaFw0yNDA1MTYwMTMyNTdaMF8xJTAjBgNVBAMMHGVjYy1zbXAtYnJva2VyLXNpZ25fVUM0LVBST0QxFDASBgNVBAsMC2lPUyBTeXN0ZW1zMRMwEQYDVQQKDApBcHBsZSBJbmMuMQswCQYDVQQGEwJVUzBZMBMGByqGSM49AgEGCCqGSM49AwEHA0IABMIVd+3r1seyIY9o3XCQoSGNx7C9bywoPYRgldlK9KVBG4NCDtgR80B+gzMfHFTD9+syINa61dTv9JKJiT58DxOjggIRMIICDTAMBgNVHRMBAf8EAjAAMB8GA1UdIwQYMBaAFCPyScRPk+TvJ+bE9ihsP6K7/S5LMEUGCCsGAQUFBwEBBDkwNzA1BggrBgEFBQcwAYYpaHR0cDovL29jc3AuYXBwbGUuY29tL29jc3AwNC1hcHBsZWFpY2EzMDIwggEdBgNVHSAEggEUMIIBEDCCAQwGCSqGSIb3Y2QFATCB/jCBwwYIKwYBBQUHAgIwgbYMgbNSZWxpYW5jZSBvbiB0aGlzIGNlcnRpZmljYXRlIGJ5IGFueSBwYXJ0eSBhc3N1bWVzIGFjY2VwdGFuY2Ugb2YgdGhlIHRoZW4gYXBwbGljYWJsZSBzdGFuZGFyZCB0ZXJtcyBhbmQgY29uZGl0aW9ucyBvZiB1c2UsIGNlcnRpZmljYXRlIHBvbGljeSBhbmQgY2VydGlmaWNhdGlvbiBwcmFjdGljZSBzdGF0ZW1lbnRzLjA2BggrBgEFBQcCARYqaHR0cDovL3d3dy5hcHBsZS5jb20vY2VydGlmaWNhdGVhdXRob3JpdHkvMDQGA1UdHwQtMCswKaAnoCWGI2h0dHA6Ly9jcmwuYXBwbGUuY29tL2FwcGxlYWljYTMuY3JsMB0GA1UdDgQWBBSUV9tv1XSBhomJdi9+V4UH55tYJDAOBgNVHQ8BAf8EBAMCB4AwDwYJKoZIhvdjZAYdBAIFADAKBggqhkjOPQQDAgNJADBGAiEAvglXH+ceHnNbVeWvrLTHL+tEXzAYUiLHJRACth69b1UCIQDRizUKXdbdbrF0YDWxHrLOh8+j5q9svYOAiQ3ILN2qYzCCAu4wggJ1oAMCAQICCEltL786mNqXMAoGCCqGSM49BAMCMGcxGzAZBgNVBAMMEkFwcGxlIFJvb3QgQ0EgLSBHMzEmMCQGA1UECwwdQXBwbGUgQ2VydGlmaWNhdGlvbiBBdXRob3JpdHkxEzARBgNVBAoMCkFwcGxlIEluYy4xCzAJBgNVBAYTAlVTMB4XDTE0MDUwNjIzNDYzMFoXDTI5MDUwNjIzNDYzMFowejEuMCwGA1UEAwwlQXBwbGUgQXBwbGljYXRpb24gSW50ZWdyYXRpb24gQ0EgLSBHMzEmMCQGA1UECwwdQXBwbGUgQ2VydGlmaWNhdGlvbiBBdXRob3JpdHkxEzARBgNVBAoMCkFwcGxlIEluYy4xCzAJBgNVBAYTAlVTMFkwEwYHKoZIzj0CAQYIKoZIzj0DAQcDQgAE8BcRhBnXZIXVGl4lgQd26ICi7957rk3gjfxLk+EzVtVmWzWuItCXdg0iTnu6CP12F86Iy3a7ZnC+yOgphP9URaOB9zCB9DBGBggrBgEFBQcBAQQ6MDgwNgYIKwYBBQUHMAGGKmh0dHA6Ly9vY3NwLmFwcGxlLmNvbS9vY3NwMDQtYXBwbGVyb290Y2FnMzAdBgNVHQ4EFgQUI/JJxE+T5O8n5sT2KGw/orv9LkswDwYDVR0TAQH/BAUwAwEB/zAfBgNVHSMEGDAWgBS7sN6hWDOImqSKmd6+veuv2sskqzA3BgNVHR8EMDAuMCygKqAohiZodHRwOi8vY3JsLmFwcGxlLmNvbS9hcHBsZXJvb3RjYWczLmNybDAOBgNVHQ8BAf8EBAMCAQYwEAYKKoZIhvdjZAYCDgQCBQAwCgYIKoZIzj0EAwIDZwAwZAIwOs9yg1EWmbGG+zXDVspiv/QX7dkPdU2ijr7xnIFeQreJ+Jj3m1mfmNVBDY+d6cL+AjAyLdVEIbCjBXdsXfM4O5Bn/Rd8LCFtlk/GcmmCEm9U+Hp9G5nLmwmJIWEGmQ8Jkh0AADGCAYkwggGFAgEBMIGGMHoxLjAsBgNVBAMMJUFwcGxlIEFwcGxpY2F0aW9uIEludGVncmF0aW9uIENBIC0gRzMxJjAkBgNVBAsMHUFwcGxlIENlcnRpZmljYXRpb24gQXV0aG9yaXR5MRMwEQYDVQQKDApBcHBsZSBJbmMuMQswCQYDVQQGEwJVUwIITDBBSVGdVDYwCwYJYIZIAWUDBAIBoIGTMBgGCSqGSIb3DQEJAzELBgkqhkiG9w0BBwEwHAYJKoZIhvcNAQkFMQ8XDTI0MDIwNzE0MDY0MFowKAYJKoZIhvcNAQk0MRswGTALBglghkgBZQMEAgGhCgYIKoZIzj0EAwIwLwYJKoZIhvcNAQkEMSIEIG1pffJQADFKhNAg/SYYPFuojxvt2RD25/byP0IttRXaMAoGCCqGSM49BAMCBEgwRgIhAM0n2Sb7HT6VMXxOPWc67P3wX2lbrtZjgBQl37Y2r5mvAiEA9w+xs/D1Q7pbH9XKEI3P/eCyK6H6Iff3xy7bTBahXykAAAAAAAA=",
                    "header": {
                        "publicKeyHash": "7MRhcfQGNrfO4Ac0SmitWIhZZ2YL/ZHp7nMkyVhDmQw=",
                        "ephemeralPublicKey": "MFkwEwYHKoZIzj0CAQYIKoZIzj0DAQcDQgAEVONMJhH9UESMVdRooHmcrKJhywJaQAixEx++UbdH/8pENFQeDzQFmNP5EgjU65kH6f39GUdiVP+Az17tBMHfCw==",
                        "transactionId": "b8f33f0f0d815cb689e95fad5f6fbcdf5d13dcb0135ac03f3ed0ddc862666deb"
                    },
                    "version": "EC_v1"
                    },
                    "paymentMethod": {
                    "displayName": "Visa 0326",
                    "network": "Visa",
                    "type": "debit"
                    },
                    "transactionIdentifier": "B8F33F0F0D815CB689E95FAD5F6FBCDF5D13DCB0135AC03F3ED0DDC862666DEB"
                }
                }';
            $apple_data = json_decode($apple_json, true)['token'];
            $params = [
                'ivp_method' => 'applepay',
                'ivp_store' => Config::get('values.telr_store_id'),
                'ivp_authkey' => Config::get('values.telr_auth_key'),
                'ivp_amount' => '1',
                'ivp_framed' => '1',
                "ivp_currency" => 'AED',
                "ivp_test" => Config::get('values.telr_test_environment'),
                "ivp_desc" => "description",
                "return_auth" => "https://example.com/",
                "return_decl" => "https://example.com/",
                "return_can" => "https://example.com/",
                "bill_fname" => "bill_fname",
                "bill_sname" => "bill_sname",
                "bill_addr1" => "bill_addr1",
                "bill_city" => "bill_city",
                "bill_region" => "bill_region",
                "bill_country" => "United Arab Emirates",
                "bill_zip" => "123456",
                "bill_email" => "test@example.com",
                "ivp_lang" => "en",
                "ivp_cart" => "APPLE-" . time(),
                "ivp_trantype" => "Purchase",
                "ivp_tranclass" => "ecom",
                "delv_addr1" => "delv_addr1",
                "delv_addr2" => "delv_addr2",
                "delv_addr3" => "delv_addr3",
                "delv_city" => "United Arab Emirates",
                "delv_region" => "United Arab Emirates",
                "delv_country" => "United Arab Emirates",
                "applepay_enc_version" => $apple_data['paymentData']['version'],
                "applepay_enc_paydata" => urlencode($apple_data['paymentData']['data']),
                "applepay_enc_paysig" => urlencode($apple_data['paymentData']['signature']),
                "applepay_enc_pubkey" => urlencode($apple_data['paymentData']['header']['ephemeralPublicKey']),
                "applepay_enc_keyhash" => $apple_data['paymentData']['header']['publicKeyHash'],
                "applepay_tran_id" => $apple_data['paymentData']['header']['transactionId'],
                "applepay_card_desc" => $apple_data['paymentMethod']['type'],
                "applepay_card_scheme" => $apple_data['paymentMethod']['displayName'],
                "applepay_card_type" => $apple_data['paymentMethod']['network'],
                "applepay_tran_id2" => $apple_data['transactionIdentifier'],
            ];
            //return Response::json($params, 200, array(), JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES);
            $client = new \GuzzleHttp\Client(['verify' => false]);
            $response_telr = $client->request('POST', 'https://secure.telr.com/gateway/remote.json', [
                'http_errors' => false, // no exceptin fix
                'headers' => [
                    'Content-Type' => 'application/x-www-form-urlencoded'
                ],
                'json' => $params,
            ]);
            $telr_data = json_decode((string) $response_telr->getBody(), true);
            return Response::json($telr_data, 200, array(), JSON_PRETTY_PRINT | JSON_UNESCAPED_SLASHES);
        } catch (\Exception $e) {
            throw new \ErrorException('Processing payment failed.' . $e->getMessage());
        }
    }
}
