<html>

<head>
    <title>CCAvenue</title>
    <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0/dist/css/bootstrap.min.css" rel="stylesheet"
        integrity="sha384-9ndCyUaIbzAi2FUVXJi0CjmCapSmO7SnpJef0486qhLnuZ2cdeRhO02iuK6FUUVM" crossorigin="anonymous">
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@5.3.0/dist/js/bootstrap.bundle.min.js"
        integrity="sha384-geWF76RCwLtnZ8qwWowPQNguL3RmwHVBC9FhGdlKrxdiJJigb/j/68SIy3Te4Bkz" crossorigin="anonymous">
    </script>
    <link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/6.4.0/css/all.min.css"
        integrity="sha512-iecdLmaskl7CVkqkXNQ/ZH/XLlvWZOJyj7Yy7tcenmpD1ypASozpmT/E0iPtmFIB46ZmdtAc9eNBvH0H/ZpiBw=="
        crossorigin="anonymous" referrerpolicy="no-referrer" />
    <script src="https://code.jquery.com/jquery-3.7.1.min.js"
        integrity="sha256-/JqT3SQfawRcv/BIHPThkBvs0OEvtFFmqPF/lYI/Cxo=" crossorigin="anonymous"></script>
    <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no">
</head>

<body class="d-flex align-items-center">
    <style>
        body {
            background-color: #ffffff;
        }

        .modal-backdrop {
            background-color: #ffffff;
        }

        #pay-button {
            border-color: #999;
            color: #FFF;
            background-color: #1572c6;
            box-shadow: 0 1px 3px 0 rgba(58, 90, 122, 0.4);
            border-radius: 7px;
            width: 100%;
        }

        #pay-button:disabled {
            border-color: #999999a4;
            background-color: #DDD;
            color: #999;
            box-shadow: none;
        }

        #amount {
            border-color: #e6e6e6;
            color: #303030;
        }
    </style>
    <div class="container text-center">
        <div class="row">
            <div class="col">
                <div id="loading" class="mb-5">
                    <div class="spinner-border text-info" role="status">
                        <span class="visually-hidden">Loading...</span>
                    </div>
                </div>
                <p class="display-6 mb-5">Redirecting...</p>
            </div>
        </div>
        <div class="col-12">
            <!-- Modal -->
            <div class="modal fade" id="checkoutModal" data-bs-backdrop="static" data-bs-keyboard="false" tabindex="-1"
                aria-hidden="true">
                <div class="modal-dialog">
                    <div class="modal-content">
                        <div class="modal-header">
                            <h1 class="modal-title fs-5">Make Payment</h1>
                        </div>
                        <div class="modal-body mt-4 mb-3 pb-0">
                            <form method="post" id="ccavenue-form" action="{{ config('ccavenue.req_handler') }}"
                                autocomplete="off" class="d-non" style="display: non;">
                                {{ csrf_field() }}
                                <table width="40%" height="100" border='1' align="center"
                                    style="display: none;">
                                    <tr>
                                        <td>Parameter Name:</td>
                                        <td>Parameter Value:</td>
                                    </tr>
                                    <tr>
                                        <td colspan="2"> Compulsory information</td>
                                    </tr>
                                    <tr>
                                        <td>Merchant Id :</td>
                                        <td><input type="text" name="merchant_id"
                                                value="{{ config('ccavenue.merchant_id') }}" readonly /></td>
                                    </tr>
                                    <tr>
                                        <td>Customer Id :</td>
                                        <td><input type="text" name="customer_id" value="{{ $customer_id }}"
                                                readonly /></td>
                                    </tr>
                                    <tr>
                                        <td>Order Id :</td>
                                        <td><input type="text" name="order_id" value="{{ $reference_id }}"
                                                readonly /></td>
                                    </tr>
                                    <tr>
                                        <td>Transaction Charge :</td>
                                        <td><input type="text" name="transaction_charge" value="{{ $transaction_charge }}"
                                                readonly /></td>
                                    </tr>
                                    <tr>
                                        <td>Amount :</td>
                                        <td><input type="text" name="amount" value="{{ $gross_amount }}" readonly />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Currency :</td>
                                        <td><input type="text" name="currency"
                                                value="{{ config('values.currency_code') }}" readonly /></td>
                                    </tr>
                                    <tr>
                                        <td>Redirect URL :</td>
                                        <td><input type="text" name="redirect_url"
                                                value="{{ config('ccavenue.resp_handler') }}" readonly /></td>
                                    </tr>
                                    <tr>
                                        <td>Cancel URL :</td>
                                        <td><input type="text" name="cancel_url"
                                                value="{{ url('payment/ccavenue/failed/'.$reference_id) . '?status=Cancelled' }}"
                                                readonly /></td>
                                    </tr>
                                    <tr>
                                        <td>Language :</td>
                                        <td><input type="text" name="language" value="EN" readonly /></td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">Merchant Params:</td>
                                    </tr>
                                    <tr>
                                        <td>merchant_param1 :</td>
                                        <td><input type="text" name="merchant_param1" value="outstanding-payment" />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td colspan="2">Billing information(optional):</td>
                                    </tr>
                                    <tr>
                                        <td>Billing Name :</td>
                                        <td><input type="text" name="billing_name"
                                                value="{{ ccavenueRemoveSpecialChars($customer->customer_name) }}"
                                                readonly />
                                        </td>
                                    </tr>
                                    <tr>
                                        <td>Billing Address :</td>
                                        <td><input type="text" name="billing_address"
                                                value="{{ $customer_address->customer_address }}" readonly /></td>
                                    </tr>
                                    <tr>
                                        <td>Billing City :</td>
                                        <td><input type="text" name="billing_city"
                                                value="{{ $customer_address->street }}" readonly /></td>
                                    </tr>
                                    <tr>
                                        <td>Billing State :</td>
                                        <td><input type="text" name="billing_state" readonly /></td>
                                    </tr>
                                    <tr>
                                        <td>Billing Zip :</td>
                                        <td><input type="text" name="billing_zip" readonly /></td>
                                    </tr>
                                    <tr>
                                        <td>Billing Country :</td>
                                        <td><input type="text" name="billing_country" value="United Arab Emirates"
                                                readonly /></td>
                                    </tr>
                                    <tr>
                                        <td>Billing Tel :</td>
                                        <td><input type="text" name="billing_tel"
                                                value="{{ $customer->mobile_number_1 }}" readonly /></td>
                                    </tr>
                                    <tr>
                                        <td>Billing Email :</td>
                                        <td><input type="text" name="billing_email"
                                                value="{{ $customer->email_address }}" readonly /></td>
                                    </tr>
                                    <tr>
                                        <td></td>
                                        <td><input type="submit" value="CheckOut"></td>
                                    </tr>
                                </table>
                                <div class="row">

                                    <div class="col-auto me-auto">
                                        <p>Amount</p>
                                        <input id="amount" name="amount" class="text-field" type="number"
                                            step="any" placeholder="Enter amount in AED"
                                            value="{{ @$gross_amount }}" required>
                                    </div>
                                    <div class="col-sm-12 text-field-main">
                                        <p>Description</p>
                                        <textarea name="description" cols="" rows="" class="text-field-big"
                                            placeholder="Enter description..." required>{{ @$description }}</textarea>
                                    </div>
                                    <div class="col-auto">
                                        <button class="btn btn-info pl-4 pr-4" id="pay-button" type="submit">PAY
                                            NOW</button>
                                    </div>
                                </div>
                            </form>

                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <script>
        $(function() {
            var myModal = new bootstrap.Modal(document.getElementById('checkoutModal'));
            //myModal.show()
            var ccavenue_form = document.getElementById("ccavenue-form");
            ccavenue_form.submit();
        });
    </script>
</body>

</html>
