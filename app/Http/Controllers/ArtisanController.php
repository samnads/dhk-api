<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class ArtisanController extends Controller
{
    public function php_artisan(Request $request)
    {
        return view('php-artisan', []);
    }
}
