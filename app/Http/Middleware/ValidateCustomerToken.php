<?php

namespace App\Http\Middleware;

use App\Models\Customer;
use Closure;
use Config;
use Illuminate\Http\Request;
use Illuminate\Routing\Route;
use Illuminate\Support\Facades\Validator;
use Response;

class ValidateCustomerToken
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure(\Illuminate\Http\Request): (\Illuminate\Http\Response|\Illuminate\Http\RedirectResponse)  $next
     * @return \Illuminate\Http\Response|\Illuminate\Http\RedirectResponse|\Illuminate\Http\JsonResponse
     */
    public function handle(Request $request, Closure $next)
    {
        $data = json_decode($request->getContent(), true);
        $input = @$data['params'];
        /************************************************************* */
        $except_route_names = ['notifications']; // bypass token validation for these route names
        if (isset($input['id']) == null && in_array($request->route()->getName(), $except_route_names)) {
            return $next($request);
        }
        /************************************************************* */
        // required input check
        $validator = Validator::make(
            (array) $input,
            [
                'id' => 'required|exists:customers,customer_id',
                'token' => 'required|string',
            ],
            [],
            [
                'id' => 'Customer ID',
                'token' => 'Token',
            ]
        );
        if ($validator->fails()) {
            return Response::json(array('result' => array('status' => 'failed', 'message' => $validator->errors()->first())), 200, array(), customerResponseJsonConstants());
        }
        /************************************************************* */
        $customer = Customer::where('customer_id', $input['id'])->where('oauth_token', $input['token'])->first();
        if (!$customer) {
            return Response::json(array('result' => array('status' => 'failed', 'message' => 'Invalid or Expired Token !', 'status_code' => 401)), 200, array(), customerResponseJsonConstants());
        }
        /************************************************************* */
        return $next($request);
    }
}
