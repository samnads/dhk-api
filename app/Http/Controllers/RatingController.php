<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Booking;
use App\Models\DayServices;
use Illuminate\Support\Facades\Log;

class RatingController extends Controller
{
    public function submitRate(Request $request)
    {
        $date = $request['date'];
        $day_service_id = $request['day_service_id'];
        $booking_id = $request['booking_id'];
        $ratingValue = $request['ratingValue'] ?: '0';
        $review = $request['review'];
        $day_service = DayServices::where('day_service_id', $day_service_id)->where('booking_id', $booking_id)->where('service_date', $date)->first();
        if ($day_service) {
            $day_service->rating = $ratingValue;
            $day_service->comments = $review;
            $day_service->rate_added_date = date("Y-m-d");
            $day_service->save();
            $bookings = Booking::select('time_from', 'time_to')->find($booking_id);
            $customerName = $day_service->customer_name;
            /**
             * Send rating feedback mail to admin
             */
            $mail = new AdminApiMailController;
            $mail->service_rating_feedback_to_admin($request['day_service_id']);
            return response()->json(
                [
                    'status' => 'success',
                    'messages' => 'Rate and review submitted successfully.',
                    'data' => ['bookings' => $bookings, 'customerName' => $customerName],
                ],
                200
            );
        } else {
            return response()->json(
                [
                    'status' => 'failed',
                    'messages' => 'Rate and review submission failed.',
                    'data' => $day_service,
                ],
                200
            );
        }
    }
    public function getRateReview(Request $request)
    {
        $day_service = DayServices::where('day_service_id', $request['day_service_id'])->where('booking_id', $request['booking_id'])->where('service_date', $request['date'])->first();
        if ($day_service && $day_service->rating <= 0) {
            $rateAndReview = ['day_service' => $day_service, 'rating' => $day_service->rating, 'comments' => $day_service->comments];
            return response()->json(
                [
                    'status' => 'success',
                    'messages' => 'Rate and review fetched.',
                    'data' => $rateAndReview
                ],
                200
            );
        } else {
            return response()->json(
                [
                    'status' => 'failed',
                    'messages' => 'Rate and review not got.',
                    'data' => ['day_service' => $day_service],
                ],
                200
            );
        }
    }
}
